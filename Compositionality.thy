(*
Title: Data-dependent security on weak memory models
Authors: Graeme Smith, Nicholas Coughlin, Toby Murray
Based on Covern, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
In turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewenv, Heiko Mantel, Daniel Schoepe
*)
section \<open> Compositionality Proof for Dependent SIFUM-Security Property \<close>

theory Compositionality
imports Security
begin

context sifum_security_init
begin

(* Set of variables that differs between two memory states: *)
definition 
  differing_vars :: "('Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> 'Var set"
where
  "differing_vars mem\<^sub>1 mem\<^sub>2 \<equiv> {x. mem\<^sub>1 x \<noteq> mem\<^sub>2 x}"

definition 
  differing_vars_lists :: "('Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> 
                           ((_, _) Mem \<times> (_, _) Mem) list \<Rightarrow> nat \<Rightarrow> 'Var set"
where
  "differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<equiv>
     (differing_vars mem\<^sub>1 (fst (mems ! i)) \<union> differing_vars mem\<^sub>2 (snd (mems ! i)))"

lemma differing_finite: "finite (differing_vars mem\<^sub>1 mem\<^sub>2)"
  by (metis UNIV_def Un_UNIV_left finite_Un finite_memory)

lemma differing_lists_finite: "finite (differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i)"
  by (simp add: differing_finite differing_vars_lists_def)


fun makes_compatible ::
  "('Com, 'Var, 'Val) GlobalConf \<Rightarrow>
   ('Com, 'Var, 'Val) GlobalConf \<Rightarrow>
   ((_, _) Mem \<times> (_, _) Mem) list \<Rightarrow>
  bool"
where
  "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems =
  (length cms\<^sub>1 = length cms\<^sub>2 \<and> length cms\<^sub>1 = length mems \<and>
   (\<forall> i. i < length cms\<^sub>1 \<longrightarrow>
       (\<forall> \<sigma>. dom \<sigma> = differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<longrightarrow>
         (cms\<^sub>1 ! i, (fst (mems ! i)) [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2 ! i, (snd (mems ! i)) [\<mapsto> \<sigma>])) \<and>
       (\<forall> x. (mem\<^sub>1 x = mem\<^sub>2 x \<or> dma mem\<^sub>1 x = High \<or> x \<in> \<C>) \<longrightarrow>
             x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i)) \<and>
    ((length cms\<^sub>1 = 0 \<and> mem\<^sub>1 =\<^sup>l mem\<^sub>2) \<or> (\<forall> x. \<exists> i. i < length cms\<^sub>1 \<and>
                                          x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i)))"

(* This just restates the previous definition using meta-quantification. This allows
  more readable proof blocks that prove each part separately. *)
lemma makes_compatible_intro [intro]:
  "\<lbrakk> length cms\<^sub>1 = length cms\<^sub>2 \<and> length cms\<^sub>1 = length mems;
     (\<And> i \<sigma>. \<lbrakk> i < length cms\<^sub>1; dom \<sigma> = differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<rbrakk> \<Longrightarrow>
          (cms\<^sub>1 ! i, (fst (mems ! i)) [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2 ! i, (snd (mems ! i)) [\<mapsto> \<sigma>]));
     (\<And> i x. \<lbrakk> i < length cms\<^sub>1; mem\<^sub>1 x = mem\<^sub>2 x \<or> dma mem\<^sub>1 x = High \<or> x \<in> \<C> \<rbrakk> \<Longrightarrow> 
          x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i);
     (length cms\<^sub>1 = 0 \<and> mem\<^sub>1 =\<^sup>l mem\<^sub>2) \<or>
     (\<forall> x. \<exists> i. i < length cms\<^sub>1 \<and> x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i) \<rbrakk> \<Longrightarrow>
  makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  by auto

(* First, some auxiliary lemmas about makes_compatible: *)
lemma compat_low:
  "\<lbrakk> makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems;
     i < length cms\<^sub>1;
     x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<rbrakk> \<Longrightarrow> dma mem\<^sub>1 x = Low"
proof -
  assume "i < length cms\<^sub>1" and *: "x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i" and
    "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  then have
    "(mem\<^sub>1 x = mem\<^sub>2 x \<or> dma mem\<^sub>1 x = High \<or> x \<in> \<C>) \<longrightarrow> x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
    by (simp add: Let_def, blast)
  with * show "dma mem\<^sub>1 x = Low"
    by (cases "dma mem\<^sub>1 x", blast)
qed

lemma compat_different:
  "\<lbrakk> makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems;
     i < length cms\<^sub>1;
     x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<rbrakk> \<Longrightarrow> mem\<^sub>1 x \<noteq> mem\<^sub>2 x \<and> dma mem\<^sub>1 x = Low \<and> x \<notin> \<C>"
  by (cases "dma mem\<^sub>1 x", auto)

lemma doesnt_read_or_modify_vars_subset:
  assumes a: "doesnt_read_or_modify_vars cm mem X"
  assumes b: "Y \<subseteq> X"
  shows "doesnt_read_or_modify_vars cm mem Y"
unfolding doesnt_read_or_modify_vars_def
proof(intro allI impI)
  fix env c' env' mem' 
  fix \<sigma>\<^sub>1 \<sigma>\<^sub>2::"'Var \<Rightarrow> 'Val option"
  assume doms: "dom \<sigma>\<^sub>1 = Y \<and> dom \<sigma>\<^sub>2 = Y" and
         step: "\<langle>cm, env, subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c', env', mem'\<rangle>"
  define \<sigma>\<^sub>1\<^sub>X where "\<sigma>\<^sub>1\<^sub>X \<equiv> \<lambda>v. (if v\<in>X then (if v\<in>Y then \<sigma>\<^sub>1 v else Some (mem v)) else None)"
  define \<sigma>\<^sub>2\<^sub>X where "\<sigma>\<^sub>2\<^sub>X \<equiv> \<lambda>v. (if v\<in>X then (if v\<in>Y then \<sigma>\<^sub>2 v else Some (mem v)) else None)"
  from doms have doms\<^sub>X: "dom \<sigma>\<^sub>1\<^sub>X = X \<and> dom \<sigma>\<^sub>2\<^sub>X = X"
     by(fastforce simp: \<sigma>\<^sub>1\<^sub>X_def \<sigma>\<^sub>2\<^sub>X_def split: if_splits)
  from doms b have "subst \<sigma>\<^sub>1\<^sub>X mem = subst \<sigma>\<^sub>1 mem"
    by(fastforce simp: subst_def \<sigma>\<^sub>1\<^sub>X_def split: if_splits option.splits)
  with step have step\<^sub>X: "\<langle>cm, env, subst \<sigma>\<^sub>1\<^sub>X mem\<rangle> \<leadsto> \<langle>c', env', mem'\<rangle>" by metis
  with a doms\<^sub>X have step\<^sub>X': "\<langle>cm, env, subst \<sigma>\<^sub>1\<^sub>X mem\<rangle> \<leadsto> \<langle>c', env', subst \<sigma>\<^sub>1\<^sub>X mem'\<rangle>"
    unfolding doesnt_read_or_modify_vars_def by blast
  with step\<^sub>X deterministic have subst_eq: "subst \<sigma>\<^sub>1\<^sub>X mem' = mem'" by blast
  have mem_eq: "\<And>v. v \<in> X \<and> v \<notin> Y \<longrightarrow> mem' v = mem v"
  proof -
    fix v
    from doms\<^sub>X subst_eq show "v \<in> X \<and> v \<notin> Y \<longrightarrow> mem' v = mem v"
      by(auto simp: \<sigma>\<^sub>1\<^sub>X_def subst_def split: if_splits option.splits dest!: fun_cong)
  qed

  from step\<^sub>X a doms\<^sub>X have "\<langle>cm, env, subst \<sigma>\<^sub>2\<^sub>X mem\<rangle> \<leadsto> \<langle>c', env', subst \<sigma>\<^sub>2\<^sub>X mem'\<rangle>"
    unfolding doesnt_read_or_modify_vars_def by blast
  moreover from doms doms\<^sub>X b have "subst \<sigma>\<^sub>2\<^sub>X mem = subst \<sigma>\<^sub>2 mem"
    apply(clarsimp simp: subst_def \<sigma>\<^sub>2\<^sub>X_def split: if_splits option.splits intro!: ext)
    by blast
  moreover from doms doms\<^sub>X b have "subst \<sigma>\<^sub>2\<^sub>X mem' = subst \<sigma>\<^sub>2 mem'"
    using mem_eq
    apply(clarsimp simp: subst_def \<sigma>\<^sub>2\<^sub>X_def split: if_splits option.splits intro!: ext)
    by blast
  ultimately show "\<langle>cm, env, subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', env', subst \<sigma>\<^sub>2 mem'\<rangle>"
    by metis
qed

lemma doesnt_read_or_modify_subset:
  "doesnt_read_or_modify cm mem X \<Longrightarrow> Y \<subseteq> X \<Longrightarrow>
   doesnt_read_or_modify cm mem Y"
  using doesnt_read_or_modify_def vars_and_\<C>_mono
        doesnt_read_or_modify_vars_subset
  by auto

lemma sound_envs_no_read :
  "\<lbrakk> sound_env_use (cms, mem); X = (map (GuarNoRW \<circ> snd) cms ! i); i < length cms \<rbrakk> \<Longrightarrow>
  doesnt_read_or_modify (fst (cms ! i)) mem X"
proof -
  fix cms mem X i
  assume sound_envs: "sound_env_use (cms, mem)" and "i < length cms"
  hence "locally_sound_env_use (cms ! i, mem)"
    by (auto simp: sound_env_use_def list_all_length)
  moreover
  assume "X = (map (GuarNoRW \<circ> snd) cms ! i)"
  ultimately show "doesnt_read_or_modify (fst (cms !i)) mem X"
    using \<open>i < length cms\<close>
    unfolding locally_sound_env_use_def 
    using loc_reach_refl 
    by (metis (no_types, lifting) comp_apply nth_map surjective_pairing)
qed

lemma differing_vars_neg: "x \<notin> differing_vars_lists mem1 mem2 mems i \<Longrightarrow>
  (fst (mems ! i) x = mem1 x \<and> snd (mems ! i) x = mem2 x)"
  by (simp add: differing_vars_lists_def differing_vars_def)

lemma differing_vars_neg_intro:
  "\<lbrakk> mem\<^sub>1 x = fst (mems ! i) x;
  mem\<^sub>2 x = snd (mems ! i) x \<rbrakk> \<Longrightarrow> x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  by (auto simp: differing_vars_lists_def differing_vars_def)

lemma differing_vars_elim [elim]:
  "x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<Longrightarrow>
  (fst (mems ! i) x \<noteq> mem\<^sub>1 x) \<or> (snd (mems ! i) x \<noteq> mem\<^sub>2 x)"
  by (auto simp: differing_vars_lists_def differing_vars_def)

lemma makes_compatible_dma_eq:
  assumes compat: "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  assumes ile: "i < length cms\<^sub>1"
  assumes dom\<sigma>: "dom \<sigma> = differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  shows "dma ((fst (mems ! i)) [\<mapsto> \<sigma>]) = dma mem\<^sub>1"
proof(rule dma_\<C>, clarify)
  fix x
  assume "x \<in> \<C>"
  with compat ile have notin_diff: "x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
    by simp
  hence "x \<notin> dom \<sigma>"
    by(metis dom\<sigma>)
  hence "(fst (mems ! i) [\<mapsto> \<sigma>]) x = (fst (mems ! i)) x"
    by(metis subst_not_in_dom)
  moreover have "(fst (mems ! i)) x = mem\<^sub>1 x"
    using notin_diff differing_vars_neg by metis
  ultimately show "(fst (mems ! i) [\<mapsto> \<sigma>]) x = mem\<^sub>1 x" by simp
qed

lemma compat_different_vars:
  "\<lbrakk> fst (mems ! i) x = snd (mems ! i) x;
     x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<rbrakk> \<Longrightarrow>
    mem\<^sub>1 x = mem\<^sub>2 x"
proof -
  assume "x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  hence "fst (mems ! i) x = mem\<^sub>1 x \<and> snd (mems ! i) x = mem\<^sub>2 x"
    by (simp add: differing_vars_lists_def differing_vars_def)
  moreover assume "fst (mems ! i) x = snd (mems ! i) x"
  ultimately show "mem\<^sub>1 x = mem\<^sub>2 x" by auto
qed

lemma differing_vars_subst [rule_format]:
  assumes dom\<sigma>: "dom \<sigma> \<supseteq> differing_vars mem\<^sub>1 mem\<^sub>2"
  shows "mem\<^sub>1 [\<mapsto> \<sigma>] = mem\<^sub>2 [\<mapsto> \<sigma>]"
proof (rule ext)
  fix x
  from dom\<sigma> show "mem\<^sub>1 [\<mapsto> \<sigma>] x = mem\<^sub>2 [\<mapsto> \<sigma>] x"
    unfolding subst_def differing_vars_def
    by (cases "\<sigma> x", auto)
qed

lemma mm_equiv_low_eq:
  "\<lbrakk> \<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle> \<approx> \<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle> \<rbrakk> \<Longrightarrow> mem\<^sub>1 =\<^bsub>env\<^esub>\<^sup>l mem\<^sub>2"
  unfolding mm_equiv.simps strong_low_bisim_mm_def
  by fast

lemma globally_sound_envs_compatible:
  "\<lbrakk> globally_sound_env_use (cms, mem) \<rbrakk> \<Longrightarrow> compatible_envs (map snd cms)"
  apply (simp add: globally_sound_env_use_def reachable_envs_def)
  using meval_sched.simps(1) by blast

lemma compatible_different_asm_no_read :
  assumes compat: "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  assumes envs_eq: "map snd cms\<^sub>1 = map snd cms\<^sub>2"
  assumes ile: "i < length cms\<^sub>1"
  assumes xin: "x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  shows "\<exists>j. j < length cms\<^sub>1 \<and> j \<noteq> i \<and>  x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems j \<and> x \<in> (map (AsmNoRW \<circ> snd) cms\<^sub>1 ! j)"
proof -
  from compat have len: "length cms\<^sub>1 = length cms\<^sub>2"
    by simp

  let ?X\<^sub>i = "differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"

  from compat ile xin have a: "dma mem\<^sub>1 x = Low"
    by (metis compat_low)

  from compat ile xin have b: "mem\<^sub>1 x \<noteq> mem\<^sub>2 x"
    by (metis compat_different)

  from compat ile xin have not_in_\<C>: "x \<notin> \<C>"
    by (metis compat_different)

  hence "dma mem\<^sub>1 x = Low"
    using a by blast

  with xin a and compat ile  obtain j where
    jprop: "j < length cms\<^sub>1 \<and> x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems j"
    by fastforce

  let ?X\<^sub>j = "differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems j"
  obtain \<sigma> :: "'Var \<rightharpoonup> 'Val" where dom\<sigma>: "dom \<sigma> = ?X\<^sub>j"
  proof
    let ?\<sigma> = "\<lambda> x. if (x \<in> ?X\<^sub>j) then Some some_val else None"
    show "dom ?\<sigma> = ?X\<^sub>j" unfolding dom_def by auto
  qed
  let ?mdss = "map snd cms\<^sub>1" and
      ?mems\<^sub>1j = "fst (mems ! j)" and
      ?mems\<^sub>2j = "snd (mems ! j)"

  from jprop dom\<sigma> have subst_eq:
  "?mems\<^sub>1j [\<mapsto> \<sigma>] x = ?mems\<^sub>1j x \<and> ?mems\<^sub>2j [\<mapsto> \<sigma>] x = ?mems\<^sub>2j x"
  by (metis subst_not_in_dom)

  from compat jprop dom\<sigma>
  have "(cms\<^sub>1 ! j, ?mems\<^sub>1j [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2 ! j, ?mems\<^sub>2j [\<mapsto> \<sigma>])"
    by (auto simp: Let_def)

  hence low_eq: "?mems\<^sub>1j [\<mapsto> \<sigma>] =\<^bsub>?mdss ! j\<^esub>\<^sup>l ?mems\<^sub>2j [\<mapsto> \<sigma>]" using envs_eq
  proof -
    have "(snd (cms\<^sub>1 ! j)) = map snd cms\<^sub>1 ! j"
      by (simp add: jprop)
    then show ?thesis
      by (metis (no_types) \<open>(cms\<^sub>1 ! j, subst \<sigma> (fst (mems ! j))) \<approx> (cms\<^sub>2 ! j, subst \<sigma> (snd (mems ! j)))\<close> assms(2) jprop len mm_equiv_low_eq nth_map prod.collapse)
  qed

  with jprop and b have x_nrw: "x \<in> AsmNoRW (?mdss ! j)"
  proof -
    { assume "x \<notin> AsmNoRW (?mdss ! j)"
      then have mems_eq: "?mems\<^sub>1j x = ?mems\<^sub>2j x"
        using `dma mem\<^sub>1 x = Low` low_eq subst_eq
      makes_compatible_dma_eq[OF compat jprop[THEN conjunct1] dom\<sigma>]
      low_mds_eq_def
      by auto

      hence "mem\<^sub>1 x = mem\<^sub>2 x"
        by (metis compat_different_vars jprop)

      hence False by (metis b)
    }
    thus ?thesis by metis
  qed
  hence "x \<in> map (AsmNoRW \<circ> snd) cms\<^sub>1 ! j" using jprop nth_map by auto
  moreover from jprop xin have "j \<noteq> i" by auto
  ultimately show ?thesis using jprop by blast
qed

(* map snd cms1 = map snd cms2 states that both global configurations use the same envs *)
lemma compatible_different_guar_no_read :
  assumes sound_envs: "sound_env_use (cms\<^sub>1, mem\<^sub>1)"
                       "sound_env_use (cms\<^sub>2, mem\<^sub>2)"
  assumes compat: "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  assumes envs_eq: "map snd cms\<^sub>1 = map snd cms\<^sub>2"
  assumes ile: "i < length cms\<^sub>1"
  assumes xin: "x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  shows "x \<in> (map (GuarNoRW \<circ> snd) cms\<^sub>1 ! i)"
proof -
  from compatible_different_asm_no_read obtain j where jprops: "j < length cms\<^sub>1 \<and> j \<noteq> i \<and>
      x \<in> (map (AsmNoRW \<circ> snd) cms\<^sub>1 ! j)"
    using compat envs_eq ile xin by blast
    
  from sound_envs have genv: "globally_sound_env_use (cms\<^sub>1,mem\<^sub>1)"
    unfolding sound_env_use_def by simp
  hence cenvs: "compatible_envs (map snd cms\<^sub>1)"
    using globally_sound_envs_compatible by blast

  from jprops cenvs have "x \<in> (map (GuarNoRW \<circ> snd) cms\<^sub>1 ! i)"
  using List.map.compositionality compatible_envs_def compatible_modes_def ile length_map by auto
  thus ?thesis .
qed

lemma compatible_different_no_read :
  assumes sound_envs: "sound_env_use (cms\<^sub>1, mem\<^sub>1)"
                       "sound_env_use (cms\<^sub>2, mem\<^sub>2)"
  assumes compat: "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  assumes envs_eq: "map snd cms\<^sub>1 = map snd cms\<^sub>2"
  assumes ile: "i < length cms\<^sub>1"
  assumes X: "X = differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  shows "doesnt_read_or_modify (fst (cms\<^sub>1 ! i)) mem\<^sub>1 X \<and> doesnt_read_or_modify (fst (cms\<^sub>2 ! i)) mem\<^sub>2 X"
proof -
  from compatible_different_guar_no_read 
  have "\<And>x. x \<in> X \<Longrightarrow> x \<in> (map (GuarNoRW \<circ> snd) cms\<^sub>1 ! i)"
    using assms by metis

  thus "doesnt_read_or_modify (fst (cms\<^sub>1 ! i)) mem\<^sub>1 X \<and> doesnt_read_or_modify (fst (cms\<^sub>2 ! i)) mem\<^sub>2 X" 
    using sound_envs ile sound_envs_no_read envs_eq 
          doesnt_read_or_modify_subset
    by (metis (no_types, hide_lams) compat makes_compatible.simps map_map subsetI)
qed



(* Note: most of the complexity drops out as doesnt_read_or_modify now implies doesnt_modify *)
fun change_respecting ::
  "('Com, 'Var, 'Val) LocalConf \<Rightarrow>
   ('Com, 'Var, 'Val) LocalConf \<Rightarrow>
   'Var set \<Rightarrow> bool"
  where "change_respecting (cms, mem) (cms', mem') X =
      ((cms, mem) \<leadsto> (cms', mem') \<and>
       (\<forall> \<sigma>. dom \<sigma> = vars_and_\<C> X \<longrightarrow> (cms, mem [\<mapsto> \<sigma>]) \<leadsto> (cms', mem' [\<mapsto> \<sigma>])))"

lemma change_respecting_subst_pres:
  "change_respecting (cms,mem) (cms',mem') X \<Longrightarrow> dom \<sigma> = vars_and_\<C> X \<Longrightarrow>
   change_respecting (cms,subst \<sigma> mem) (cms',subst \<sigma> mem') X"
  by(auto simp: subst_twice)
  

lemma subst_overrides: "dom \<sigma> = dom \<tau> \<Longrightarrow> mem [\<mapsto> \<tau>] [\<mapsto> \<sigma>] = mem [\<mapsto> \<sigma>]"
  unfolding subst_def
  by (metis domIff option.exhaust option.simps(4) option.simps(5))

definition to_partial :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a \<rightharpoonup> 'b)"
  where "to_partial f = (\<lambda> x. Some (f x))"

lemma dom_restrict_total: "dom (to_partial f |` X) = X"
  unfolding to_partial_def
  by (metis Int_UNIV_left dom_const dom_restrict)

lemma change_respecting_doesnt_modify':
  assumes eval: "(cms, mem) \<leadsto> (cms', mem')"
  assumes cr: "\<forall> f. dom f = Y \<longrightarrow> (cms, mem [\<mapsto> f]) \<leadsto> (cms', mem' [\<mapsto> f])"
  assumes x_in_dom: "x \<in> Y"
  shows "mem x = mem' x"
proof -
  let ?f' = "to_partial mem |` Y"
  have domf': "dom ?f' = Y"
    by (metis dom_restrict_total)

  from this cr have eval': "(cms, mem [\<mapsto> ?f']) \<leadsto> (cms', mem' [\<mapsto> ?f'])"
    by (metis)

  have mem_eq: "mem [\<mapsto> ?f'] = mem"
  proof
    fix x
    show "mem [\<mapsto> ?f'] x = mem x"
      unfolding subst_def
      apply (cases "x \<in> Y")
       apply (metis option.simps(5) restrict_in to_partial_def)
      by (metis domf' subst_def subst_not_in_dom)
  qed

  then have mem'_eq: "mem' [\<mapsto> ?f'] = mem'"
    using eval eval' deterministic
    by (metis Pair_inject)

  moreover
  have x_in_dom': "x \<in> dom ?f'"
    by (metis x_in_dom dom_restrict_total)
  hence "?f' x = Some (mem x)"
    by (metis restrict_in to_partial_def x_in_dom)
  hence "mem' [\<mapsto> ?f'] x = mem x"
    using subst_def x_in_dom'
    by (metis option.simps(5))
  thus "mem x = mem' x"
    by (metis mem'_eq)
qed

lemma change_respecting_subset':
  assumes step: "(cms, mem) \<leadsto> (cms', mem')"
  assumes noread: "(\<forall> \<sigma>. dom \<sigma> = X \<longrightarrow> (cms, mem [\<mapsto> \<sigma>]) \<leadsto> (cms', mem' [\<mapsto> \<sigma>]))"
  assumes dom_subset: "dom \<sigma> \<subseteq> X"
  shows "(cms, mem [\<mapsto> \<sigma>]) \<leadsto> (cms', mem' [\<mapsto> \<sigma>])"
proof -
  define \<sigma>\<^sub>X where "\<sigma>\<^sub>X \<equiv> \<lambda>x. if x \<in> X then if x \<in> dom \<sigma> then \<sigma> x else Some (mem x) else None"
  have "dom \<sigma>\<^sub>X = X" using dom_subset by(auto simp: \<sigma>\<^sub>X_def)

  have "mem [\<mapsto> \<sigma>] = mem [\<mapsto> \<sigma>\<^sub>X]"
    apply(rule ext)
    using dom_subset apply(auto simp: subst_def \<sigma>\<^sub>X_def split: option.splits)
    done

  moreover have "mem' [\<mapsto> \<sigma>] = mem' [\<mapsto> \<sigma>\<^sub>X]"
    apply(rule ext)
    using dom_subset apply(auto simp: subst_def \<sigma>\<^sub>X_def split: option.splits simp: change_respecting_doesnt_modify'[OF step noread])
    done

  moreover from noread `dom \<sigma>\<^sub>X = X` have "(cms, mem [\<mapsto> \<sigma>\<^sub>X]) \<leadsto> (cms', mem' [\<mapsto> \<sigma>\<^sub>X])" by metis
  ultimately show ?thesis by simp
qed
 
lemma change_respecting_subst:
  "change_respecting (cms, mem) (cms', mem') X \<Longrightarrow>
       (\<forall> \<sigma>. dom \<sigma> = X \<longrightarrow> (cms, mem [\<mapsto> \<sigma>]) \<leadsto> (cms', mem' [\<mapsto> \<sigma>]))"
  unfolding change_respecting.simps vars_and_\<C>_def
  using change_respecting_subset' by blast
 
lemma change_respecting_intro [iff]:
  "\<lbrakk> \<langle> c, mds, mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle>;
     \<And> f. dom f = vars_and_\<C> X \<Longrightarrow>
           (\<langle> c, mds, mem [\<mapsto> f] \<rangle> \<leadsto> \<langle> c', mds', mem' [\<mapsto> f] \<rangle>) \<rbrakk>
  \<Longrightarrow> change_respecting \<langle>c, mds, mem\<rangle> \<langle>c', mds', mem'\<rangle> X"
  unfolding change_respecting.simps
  by blast


lemma \<C>_vars_finite[simp]:
  "finite (\<C>_vars x)"
  apply(rule finite_subset[OF _ finite_memory])
  by blast

lemma finite_dom:
  "finite (dom (\<sigma>::'Var \<Rightarrow> 'Val option))"
  by(blast intro: finite_subset[OF _ finite_memory])

lemma doesnt_read_or_modify_subst: 
  assumes noread: "doesnt_read_or_modify c mem Y"
  assumes step: "\<langle>c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle>"
  assumes subset: "X \<subseteq> vars_and_\<C> Y"
  shows "\<And> \<sigma>. dom \<sigma> = X \<Longrightarrow> \<langle>c, mds, mem[\<mapsto> \<sigma>]\<rangle> \<leadsto> \<langle>c', mds', mem'[\<mapsto> \<sigma>]\<rangle>"  
  using assms doesnt_read_or_modify_def doesnt_read_or_modify_vars_subset doesnt_read_or_modify_vars_legacy_imp
  by (metis (no_types, lifting))

lemma subst_restrict_twice:
  "dom \<sigma> = A \<union> B \<Longrightarrow>
   mem [\<mapsto> (\<sigma> |` A)] [\<mapsto> (\<sigma> |` B)] = mem [\<mapsto> \<sigma>]"
  by (fastforce simp: subst_def split: option.splits intro!: ext simp: restrict_map_def)

(* Note: this proof is now trivial since change_respecting and doesnt_read_or_modify
         now coincide, because doesnt_read_or_modify is weakened to allow the statement
         to depend on the current memory *)
lemma noread_exists_change_respecting:
  assumes eval: "\<langle> c, mds, mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle>"
  assumes noread: "doesnt_read_or_modify c mem X"
  shows "change_respecting \<langle>c, mds, mem\<rangle> \<langle>c', mds', mem'\<rangle> X"
proof -
  let ?lc = "\<langle>c, mds, mem\<rangle>" and ?lc' = "\<langle>c', mds', mem'\<rangle>"
  from eval noread show "change_respecting \<langle>c, mds, mem\<rangle> \<langle>c', mds', mem'\<rangle> X"
  unfolding change_respecting.simps doesnt_read_or_modify_def 
  using doesnt_read_or_modify_vars_legacy_imp
  by blast
qed


lemma update_nth_eq:
  "\<lbrakk> xs = ys; n < length xs \<rbrakk> \<Longrightarrow> xs = ys [n := xs ! n]"
  by (metis list_update_id)

text \<open> This property is obvious,
  so an unreadable apply-style proof is acceptable here: \<close>
lemma mm_equiv_step:
  assumes bisim: "(cms\<^sub>1, mem\<^sub>1) \<approx> (cms\<^sub>2, mem\<^sub>2)"
  assumes modes_eq: "snd cms\<^sub>1 = snd cms\<^sub>2"
  assumes step: "(cms\<^sub>1, mem\<^sub>1) \<leadsto> (cms\<^sub>1', mem\<^sub>1')"
  shows "\<exists> c\<^sub>2' mem\<^sub>2'. (cms\<^sub>2, mem\<^sub>2) \<leadsto> \<langle> c\<^sub>2', snd cms\<^sub>1', mem\<^sub>2' \<rangle> \<and>
  (cms\<^sub>1', mem\<^sub>1') \<approx> \<langle> c\<^sub>2', snd cms\<^sub>1', mem\<^sub>2' \<rangle>"
  using assms mm_equiv_strong_low_bisim
  unfolding strong_low_bisim_mm_def
  apply clarsimp
  by (metis (no_types, lifting) prod.collapse)

lemma change_respecting_doesnt_modify:
  assumes cr: "change_respecting (cms, mem) (cms', mem') X"
  assumes eval: "(cms, mem) \<leadsto> (cms', mem')"
  assumes x_in_dom: "x \<in> X \<union> vars_\<C> X"
  shows "mem x = mem' x"
  using change_respecting_doesnt_modify'[where Y="X \<union> vars_\<C> X", OF eval] cr     
        change_respecting.simps vars_and_\<C>_def x_in_dom 
  by metis

lemma change_respecting_doesnt_modify_dma:
  assumes cr: "change_respecting (cms, mem) (cms', mem') X"
  assumes eval: "(cms, mem) \<leadsto> (cms', mem')"
  assumes x_in_dom: "x \<in> X"
  shows "dma mem x = dma mem' x"
proof - 
  have "\<And>y. y \<in> \<C>_vars x \<Longrightarrow> mem y = mem' y"
    proof -
    fix y
    assume "y \<in> \<C>_vars x"
    hence "y \<in> vars_\<C> X"
      using x_in_dom by(auto simp: vars_\<C>_def)
    thus "mem y = mem' y"
      using cr eval change_respecting_doesnt_modify by blast
    qed
  thus ?thesis by(metis dma_\<C>_vars)
qed

definition restrict_total :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a set \<Rightarrow> 'a \<rightharpoonup> 'b"
  where "restrict_total f A = to_partial f |` A"

lemma differing_empty_eq:
  "\<lbrakk> differing_vars mem mem' = {} \<rbrakk> \<Longrightarrow> mem = mem'"
  unfolding differing_vars_def
  by auto

lemma adaptation_finite:
  "finite (dom (A::('Var,'Val) adaptation))"
  apply(rule finite_subset[OF _ finite_memory])
  by blast

definition 
  globally_consistent  :: "('Var, 'Val) adaptation \<Rightarrow> ('Var,'Val) Env \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> bool"
where "globally_consistent A env mem\<^sub>1 mem\<^sub>2 \<equiv> 
  (\<forall>x.  case A x of Some (v,v') \<Rightarrow> 
        (mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v') \<longrightarrow> \<not> var_asm_not_written env x | _ \<Rightarrow> True) \<and>
  (\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written env x) \<and>
  (\<forall>R \<in> Relies env. (mem\<^sub>1, (mem\<^sub>1 [\<parallel>\<^sub>1 A])) \<in> R  \<and> (mem\<^sub>2, (mem\<^sub>2 [\<parallel>\<^sub>2 A])) \<in> R ) \<and>
  (\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = Low \<and> (x \<notin> AsmNoRW env \<or> x \<in> \<C>) \<longrightarrow> (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = (mem\<^sub>2 [\<parallel>\<^sub>2 A]) x)"

lemma globally_consistent_adapt_bisim:
  assumes bisim: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<approx> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  assumes globally_consistent: "globally_consistent A mds mem\<^sub>1 mem\<^sub>2"
  shows "\<langle>c\<^sub>1, mds, mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<approx> \<langle>c\<^sub>2, mds, mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
  apply(rule mm_equiv_glob_consistent[simplified closed_glob_consistent_def, rule_format])
   apply(rule bisim)
  apply(fold globally_consistent_def)
  by(rule globally_consistent)

lemma mm_equiv_\<C>_eq: 
  "(a,b) \<approx> (a',b') \<Longrightarrow> snd a = snd a' \<Longrightarrow>
    \<forall>x\<in>\<C>. b x = b' x"
  apply(case_tac a, case_tac a')
  using mm_equiv_strong_low_bisim[simplified strong_low_bisim_mm_def, rule_format]
  by(auto simp: low_mds_eq_def \<C>_Low)

lemma apply_adaptation_not_in_dom: 
  "x \<notin> dom A \<Longrightarrow> apply_adaptation b blah A x = blah x"
  apply(simp add: apply_adaptation_def domIff split: option.splits)
  done

lemma meval_sched_one:
  "(cms, mem) \<leadsto>\<^bsub>k\<^esub> (cms', mem') \<Longrightarrow>
        (cms, mem) \<rightarrow>\<^bsub>[k]\<^esub> (cms', mem')"
  apply(simp)
  done

lemma meval_sched_ConsI: 
  "(cms, mem) \<leadsto>\<^bsub>k\<^esub> (cms', mem') \<Longrightarrow>
   (cms', mem') \<rightarrow>\<^bsub>sched\<^esub> (cms'', mem'') \<Longrightarrow>
   (cms, mem) \<rightarrow>\<^bsub>(k#sched)\<^esub> (cms'', mem'')"
  by fastforce

lemma reachable_envs_subset:
  assumes eval: "(cms, mem) \<leadsto>\<^bsub>k\<^esub> (cms', mem')"
  shows "reachable_envs (cms', mem') \<subseteq> reachable_envs (cms, mem)"
proof
  fix envs
  assume "envs \<in> reachable_envs (cms', mem')"
  thus "envs \<in> reachable_envs (cms, mem)"
    using assms
    unfolding reachable_envs_def 
    by (blast intro: meval_sched_ConsI)
qed


lemma meval_sched_app_iff:
  "(cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>ns@ms\<^esub> (cms\<^sub>1', mem\<^sub>1') =
   (\<exists>cms\<^sub>1'' mem\<^sub>1''. (cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>ns\<^esub> (cms\<^sub>1'', mem\<^sub>1'') \<and> (cms\<^sub>1'', mem\<^sub>1'') \<rightarrow>\<^bsub>ms\<^esub> (cms\<^sub>1', mem\<^sub>1'))"
  apply(induct ns arbitrary: ms cms\<^sub>1 mem\<^sub>1)
   apply simp
  apply force
  done

lemmas meval_sched_appD = meval_sched_app_iff[THEN iffD1]
lemmas meval_sched_appI = meval_sched_app_iff[THEN iffD2, OF exI, OF exI]

lemma meval_sched_snocD:
  "(cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>ns@[n]\<^esub> (cms\<^sub>1', mem\<^sub>1') \<Longrightarrow>
   \<exists>cms\<^sub>1'' mem\<^sub>1''. (cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>ns\<^esub> (cms\<^sub>1'', mem\<^sub>1'') \<and> (cms\<^sub>1'', mem\<^sub>1'') \<leadsto>\<^bsub>n\<^esub> (cms\<^sub>1', mem\<^sub>1')"
  apply(fastforce dest: meval_sched_appD)
  done

lemma meval_sched_snocI:
  "(cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>ns\<^esub> (cms\<^sub>1'', mem\<^sub>1'') \<and> (cms\<^sub>1'', mem\<^sub>1'') \<leadsto>\<^bsub>n\<^esub> (cms\<^sub>1', mem\<^sub>1') \<Longrightarrow>
  (cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>ns@[n]\<^esub> (cms\<^sub>1', mem\<^sub>1')"
  apply(fastforce intro: meval_sched_appI)
  done

lemma side_condition_preservation:
  assumes sc: "side_condition (cms, mem)"
  assumes eval: "(cms, mem) \<leadsto>\<^bsub>k\<^esub> (cms', mem')"
  shows "side_condition (cms',mem')"
  apply(clarsimp simp: side_condition_def)
  using meval_sched_snocI sc eval unfolding side_condition_def
  by (meson meval_sched_ConsI sifum_security_init.meval.meval_intro sifum_security_init_axioms)

lemma globally_sound_envs_invariant:
  assumes globally_sound: "globally_sound_env_use (cms, mem)"
  assumes eval: "(cms, mem) \<leadsto>\<^bsub>k\<^esub> (cms', mem')"
  shows "globally_sound_env_use (cms', mem')"
  using assms reachable_envs_subset
  unfolding globally_sound_env_use_def
  by (metis (no_types) subsetD side_condition_preservation)
  
lemma loc_reach_subset:
  assumes eval: "\<langle>c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle>"
  shows "loc_reach \<langle>c', mds', mem'\<rangle> \<subseteq> loc_reach \<langle>c, mds, mem\<rangle>"
proof (clarify)
  fix c'' mds'' mem''
  from eval have "\<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c, mds, mem\<rangle>"
    by (metis loc_reach.refl loc_reach.step surjective_pairing)
  assume "\<langle>c'', mds'', mem''\<rangle> \<in> loc_reach \<langle>c', mds', mem'\<rangle>"
  thus "\<langle>c'', mds'', mem''\<rangle> \<in> loc_reach \<langle>c, mds, mem\<rangle>"
    apply induct
      apply (metis `\<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c, mds, mem\<rangle>` surjective_pairing)
     apply (metis loc_reach.step)
    by (metis loc_reach.mem_diff)
qed


lemma locally_sound_modes_invariant:
  assumes sound_modes: "locally_sound_env_use \<langle>c, mds, mem\<rangle>"
  assumes eval: "\<langle>c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle>"
  shows "locally_sound_env_use \<langle>c', mds', mem'\<rangle>"
proof -
  from eval have "\<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c, mds, mem\<rangle>"
    by (metis fst_conv loc_reach.refl loc_reach.step snd_conv)
  thus ?thesis
    using sound_modes
    unfolding locally_sound_env_use_def
    by (metis (no_types) Collect_empty_eq eval loc_reach_subset subsetD)
qed



lemma loc_reach_mem_diff_subset:
  assumes mem_diff: "\<forall> x. var_asm_not_written env x \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x \<and> dma mem\<^sub>1 x = dma mem\<^sub>2 x"
  assumes agrel: "\<forall>R\<in> Relies env. (mem\<^sub>2,mem\<^sub>1) \<in> R"
  shows "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem\<^sub>1\<rangle> \<Longrightarrow> \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem\<^sub>2\<rangle>"
proof -
  let ?lc = "\<langle>c', env', mem'\<rangle>"
  assume "?lc \<in> loc_reach \<langle>c, env, mem\<^sub>1\<rangle>"
  thus ?thesis
  proof (induct)
    case refl
    thus ?case
      by (metis fst_conv loc_reach.mem_diff loc_reach.refl mem_diff snd_conv agrel)
  next
    case step
    thus ?case
      by (metis loc_reach.step)
  next
    case mem_diff
    thus ?case
      by (metis loc_reach.mem_diff)
  qed
qed

lemma doesnt_modify_subset:
  "doesnt_modify c mem X \<Longrightarrow> Y \<subseteq> X \<Longrightarrow> doesnt_modify c mem Y"
  apply(auto simp: doesnt_modify_def)
  done

lemma nth_map_o:
  "n < length xs \<Longrightarrow> f1 (map f2 xs ! n) = map (f1 o f2) xs ! n"
  apply (induct xs arbitrary: n, simp)
  apply (case_tac n, auto)
  done
  
lemma sound_modes_invariant:
  assumes sound_envs: "sound_env_use (cms, mem)"
  assumes eval: "(cms, mem) \<leadsto>\<^bsub>k\<^esub> (cms', mem')"
  shows "sound_env_use (cms', mem')"
proof -
  from sound_envs and eval have "globally_sound_env_use (cms', mem')"
    by (metis globally_sound_envs_invariant sound_env_use.simps)
  moreover
  from sound_envs have loc_sound: "\<forall> i < length cms. locally_sound_env_use (cms ! i, mem)"
    unfolding sound_env_use_def
    by simp (metis list_all_length)
  from eval obtain k cms\<^sub>k' where
    ev: "(cms ! k, mem) \<leadsto> (cms\<^sub>k', mem') \<and> k < length cms \<and> cms' = cms [k := cms\<^sub>k']"
    by (metis meval_elim)
  hence "length cms = length cms'"
    by auto
  have "\<And> i. i < length cms' \<Longrightarrow> locally_sound_env_use (cms' ! i, mem')"
  proof -
    fix i
    assume i_le: "i < length cms'"
    thus "?thesis i"
    proof (cases "i = k")
      assume "i = k"
      thus ?thesis
        using i_le ev loc_sound
        by (metis (hide_lams, no_types) locally_sound_modes_invariant nth_list_update surj_pair)
    next
      assume "i \<noteq> k"
      hence "cms' ! i = cms ! i"
        by (metis ev nth_list_update_neq)
      from sound_envs have compat: "compatible_envs (map snd cms)"
        unfolding sound_env_use.simps
        by (metis globally_sound_envs_compatible)

      have rw: "\<And> x. x \<in> map (AsmNoRW o snd) cms ! i \<Longrightarrow> x \<in> map (GuarNoRW o snd) cms ! k"
        using compat
        unfolding compatible_modes_def compatible_envs_def var_asm_not_written_def
        by (metis (no_types) `i \<noteq> k` `length cms = length cms'` ev i_le length_map nth_map_o)

      have w: "\<And> x. x \<in> map (AsmNoW o snd) cms ! i \<Longrightarrow> x \<in> map (GuarNoW o snd) cms ! k"
        using compat
        unfolding compatible_modes_def compatible_envs_def var_asm_not_written_def
        by (metis (no_types) `i \<noteq> k` `length cms = length cms'` ev i_le length_map nth_map_o)

      have i_le': "i < length cms" using `length cms = length cms'` i_le by auto
      have k_le:  "k < length cms" using ev by auto

      have "\<And> x. var_asm_not_written ((snd (cms ! i))) x \<Longrightarrow> x \<in> GuarNoW (snd (cms ! k)) \<or> x \<in> GuarNoRW (snd (cms ! k))"
        using rw w i_le' k_le
        unfolding var_asm_not_written_def
        by auto

      hence "\<And> x. var_asm_not_written ((snd (cms ! i))) x \<longrightarrow> doesnt_modify (fst (cms ! k)) mem {x}"
        using ev loc_sound
        unfolding locally_sound_env_use_def 
        using loc_reach.refl doesnt_read_or_modify_doesnt_modify doesnt_modify_subset
        by (metis (no_types, lifting) empty_iff fst_conv insert_iff snd_conv subsetI)
      with ev have mem_diff: "\<forall> x. var_asm_not_written ((snd (cms ! i))) x \<longrightarrow> mem' x = mem x \<and> dma mem' x = dma mem x"
        unfolding doesnt_modify_def 
        by (metis prod.collapse singletonI)
      have "(mem,mem') \<in> \<Inter> (Guars (snd (cms ! k)))"
      using ev loc_sound
      unfolding locally_sound_env_use_def
      using respects_guarantee_rel_Inter
      proof -
        assume a1: "\<forall>i<length cms. \<forall>c' env' mem'. \<langle>c', env', mem'\<rangle> \<in> loc_reach (cms ! i, mem) \<longrightarrow> doesnt_read_or_modify c' mem' (GuarNoRW env') \<and> doesnt_modify c' mem' (GuarNoW env') \<and> Ball (Guars env') (respects_guarantee_rel c' env' mem')"
        have "\<langle>fst (cms ! k), snd (cms ! k), mem\<rangle> \<in> loc_reach (cms ! k, mem)"
          using loc_reach.refl by auto
        then have "respects_guarantee_rel (fst (cms ! k)) (snd (cms ! k)) mem (\<Inter> (Guars (snd (cms ! k))))"
          using a1 by (meson ev respects_guarantee_rel_Inter)
        then show ?thesis
          by (metis (no_types) ev prod.collapse respects_guarantee_rel_def)
      qed
      hence agrels: "\<forall>R\<in>(Relies (snd (cms ! i))). (mem,mem') \<in> R"
      using `compatible_envs (map snd cms)` `i \<noteq> k` ev i_le
      unfolding compatible_envs_def compatible_agrels_def
      proof -
        assume a1: "compatible_modes ( (map snd cms)) \<and> (\<forall>i<length ( (map snd cms)). \<forall>R\<in>(Relies ((map snd cms) ! i)). \<forall>j<length ( (map snd cms)). j \<noteq> i \<longrightarrow> \<Inter>(Guars ((map snd cms) ! j)) \<subseteq> R)"
        have f2: "k < length ( (map snd cms))"
          by (simp add: ev)
        have f3: "i < length ( (map snd cms))"
          using \<open>length cms = length cms'\<close> i_le by fastforce
        have f4: "i < length (map snd cms)"
          using \<open>length cms = length cms'\<close> i_le by auto
        have f5: "k < length (map snd cms)"
          by (simp add: ev)
        obtain rr :: "(('Var \<Rightarrow> 'Val) \<times> ('Var \<Rightarrow> 'Val)) set" where
          "(\<exists>v0. v0 \<in> Relies (snd (cms ! i)) \<and> (mem, mem') \<notin> v0) = (rr \<in> Relies (snd (cms ! i)) \<and> (mem, mem') \<notin> rr)"
          by moura
        moreover
        { assume "(mem, mem') \<notin> rr"
          { assume "\<Inter> (Guars (snd (cms ! k))) \<subset> rr"
            then have "rr \<notin> Relies (snd (cms ! i))  \<or> (mem, mem') \<in> rr"
              using \<open>(mem, mem') \<in> \<Inter> (Guars (snd (cms ! k)))\<close> by blast }
          then have "rr \<notin> Relies (snd (cms ! i)) \<or> (mem, mem') \<in> rr"
            using f5 f4 f3 f2 a1 by (metis (no_types) \<open>(mem, mem') \<in> \<Inter> (Guars (snd (cms ! k)))\<close> \<open>i \<noteq> k\<close> \<open>length cms = length cms'\<close> ev i_le nth_map psubsetI) }
        ultimately show ?thesis
          by blast
      qed
      from loc_reach_mem_diff_subset[OF mem_diff agrels] have "loc_reach (cms ! i, mem') \<subseteq> loc_reach (cms ! i, mem)"
         apply -
         apply(rule subsetI)
         by (metis prod.collapse)
      thus ?thesis
        using loc_sound i_le `length cms = length cms'`
        unfolding locally_sound_env_use_def
        using \<open>cms' ! i = cms ! i\<close> subsetCE by auto
    qed
  qed
  ultimately show ?thesis
    unfolding sound_env_use.simps
    by (metis (no_types) list_all_length)
qed
  
(* This is the central lemma. Unfortunately, I didn't find
   a nice partitioning into several easier lemmas: *)
lemma makes_compatible_invariant:
  assumes sound_envs: "sound_env_use (cms\<^sub>1, mem\<^sub>1)"
                      "sound_env_use (cms\<^sub>2, mem\<^sub>2)"
  assumes compat: "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  assumes envs_eq: "map snd cms\<^sub>1 = map snd cms\<^sub>2"
  assumes eval: "(cms\<^sub>1, mem\<^sub>1) \<leadsto>\<^bsub>k\<^esub> (cms\<^sub>1', mem\<^sub>1')"
  obtains cms\<^sub>2' mem\<^sub>2' mems' where
      "map snd cms\<^sub>1' = map snd cms\<^sub>2' \<and>
       (cms\<^sub>2, mem\<^sub>2) \<leadsto>\<^bsub>k\<^esub> (cms\<^sub>2', mem\<^sub>2') \<and>
       makes_compatible (cms\<^sub>1', mem\<^sub>1') (cms\<^sub>2', mem\<^sub>2') mems'"
proof -
  let ?X = "\<lambda> i. differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  from sound_envs compat envs_eq have
    a: "\<forall> i < length cms\<^sub>1.  doesnt_read_or_modify (fst (cms\<^sub>1 ! i)) mem\<^sub>1 (?X i) \<and>
                                          doesnt_read_or_modify (fst (cms\<^sub>2 ! i)) mem\<^sub>2 (?X i)"
    by (metis compatible_different_no_read)
  from eval have
    b: "k < length cms\<^sub>1 \<and> (cms\<^sub>1 ! k, mem\<^sub>1) \<leadsto> (cms\<^sub>1' ! k, mem\<^sub>1') \<and>
        cms\<^sub>1' = cms\<^sub>1 [k := cms\<^sub>1' ! k]"
    by (metis meval_elim nth_list_update_eq)

  from envs_eq have equal_size: "length cms\<^sub>1 = length cms\<^sub>2"
    by (metis length_map)

  let ?env\<^sub>k = "snd (cms\<^sub>1 ! k)" and
      ?env\<^sub>k' = "snd (cms\<^sub>1' ! k)" and
      ?mems\<^sub>1k = "fst (mems ! k)" and
      ?mems\<^sub>2k = "snd (mems ! k)" and
      ?n = "length cms\<^sub>1"

  have "finite (?X k)"
    by (metis differing_lists_finite)

  (* Obtaining cms' and mem\<^sub>2' is not in a proof block, since we
     need some of the following statements later: *)
  then have
    c: "change_respecting (cms\<^sub>1 ! k, mem\<^sub>1) (cms\<^sub>1' ! k, mem\<^sub>1') (?X k)"
    using noread_exists_change_respecting b a
    by (metis surjective_pairing)

  from compat have "\<And> \<sigma>. dom \<sigma> = ?X k \<Longrightarrow> ?mems\<^sub>1k [\<mapsto> \<sigma>] = mem\<^sub>1 [\<mapsto> \<sigma>]"
    using differing_vars_subst differing_vars_lists_def
    by (metis Un_upper1)

  hence
    eval\<^sub>\<sigma>: "\<And> \<sigma>. dom \<sigma> = ?X k \<Longrightarrow> (cms\<^sub>1 ! k, ?mems\<^sub>1k [\<mapsto> \<sigma>]) \<leadsto> (cms\<^sub>1' ! k, mem\<^sub>1' [\<mapsto> \<sigma>])"
      by(metis change_respecting_subst[rule_format, where X="?X k"] c)

  moreover
  with b and compat have
    bisim\<^sub>\<sigma>: "\<And> \<sigma>. dom \<sigma> = ?X k \<Longrightarrow> (cms\<^sub>1 ! k, ?mems\<^sub>1k [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> \<sigma>])"
    by auto

  moreover have "snd (cms\<^sub>1 ! k) = snd (cms\<^sub>2 ! k)"
    by (metis b equal_size envs_eq nth_map)
    
  ultimately have d: "\<And> \<sigma>. dom \<sigma> = ?X k \<Longrightarrow> \<exists> c\<^sub>f' mem\<^sub>f'.
    (cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> \<sigma>]) \<leadsto> \<langle> c\<^sub>f', ?env\<^sub>k', mem\<^sub>f' \<rangle> \<and>
    (cms\<^sub>1' ! k, mem\<^sub>1' [\<mapsto> \<sigma>]) \<approx> \<langle> c\<^sub>f', ?env\<^sub>k', mem\<^sub>f' \<rangle>"
    by (metis mm_equiv_step)

  obtain h :: "'Var \<rightharpoonup> 'Val" where domh: "dom h = ?X k"
    by (metis dom_restrict_total)

  then obtain c\<^sub>h mem\<^sub>h where h_prop:
    "(cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> h]) \<leadsto> \<langle> c\<^sub>h, ?env\<^sub>k', mem\<^sub>h \<rangle> \<and>
    (cms\<^sub>1' ! k, mem\<^sub>1' [\<mapsto> h]) \<approx> \<langle> c\<^sub>h, ?env\<^sub>k', mem\<^sub>h \<rangle>"
    using d
    by metis

  then have
    "change_respecting (cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> h]) \<langle> c\<^sub>h, ?env\<^sub>k', mem\<^sub>h \<rangle> (?X k)"
    using a b doesnt_read_or_modify_subst_pres noread_exists_change_respecting change_respecting_subst_pres domh
    by (metis (mono_tags, hide_lams) Un_iff differing_vars_subst prod.collapse sifum_security_init.differing_vars_lists_def sifum_security_init_axioms subsetI)

  \<comment> \<open>The following statements are universally quantified
      since they are reused later:\<close>
  with h_prop have
    "\<forall> \<sigma>. dom \<sigma> = ?X k \<longrightarrow>
      (cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> h] [\<mapsto> \<sigma>]) \<leadsto> \<langle> c\<^sub>h, ?env\<^sub>k', mem\<^sub>h [\<mapsto> \<sigma>] \<rangle>"
    by (metis change_respecting_subst)

  with domh have f:
    "\<forall> \<sigma>. dom \<sigma> = ?X k \<longrightarrow>
      (cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> \<sigma>]) \<leadsto> \<langle> c\<^sub>h, ?env\<^sub>k', mem\<^sub>h [\<mapsto> \<sigma>] \<rangle>"
    by (auto simp: subst_overrides)

  from d and f have g: "\<And> \<sigma>. dom \<sigma> = ?X k \<Longrightarrow>
    (cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> \<sigma>]) \<leadsto> \<langle> c\<^sub>h, ?env\<^sub>k', mem\<^sub>h [\<mapsto> \<sigma>] \<rangle> \<and>
    (cms\<^sub>1' ! k, mem\<^sub>1' [\<mapsto> \<sigma>]) \<approx> \<langle> c\<^sub>h, ?env\<^sub>k', mem\<^sub>h [\<mapsto> \<sigma>] \<rangle>"
    using h_prop
    by (metis deterministic)
  let ?\<sigma>_mem\<^sub>2 = "to_partial mem\<^sub>2 |` ?X k"
  define mem\<^sub>2' where "mem\<^sub>2' \<equiv> mem\<^sub>h [\<mapsto> ?\<sigma>_mem\<^sub>2]"
  define c\<^sub>2' where "c\<^sub>2' \<equiv> c\<^sub>h"

  have dom\<sigma>_mem\<^sub>2: "dom ?\<sigma>_mem\<^sub>2 = ?X k"
    by (metis dom_restrict_total)

  have "mem\<^sub>2 = ?mems\<^sub>2k [\<mapsto> ?\<sigma>_mem\<^sub>2]"
  proof (rule ext)
    fix x
    show "mem\<^sub>2 x = ?mems\<^sub>2k [\<mapsto> ?\<sigma>_mem\<^sub>2] x"
      using dom\<sigma>_mem\<^sub>2
      unfolding to_partial_def subst_def
      apply (cases "x \<in> ?X k")
      apply auto
      by (metis differing_vars_neg)
  qed

  with f dom\<sigma>_mem\<^sub>2 have i: "(cms\<^sub>2 ! k, mem\<^sub>2) \<leadsto> \<langle> c\<^sub>2', ?env\<^sub>k', mem\<^sub>2' \<rangle>"
    unfolding mem\<^sub>2'_def c\<^sub>2'_def
    by metis

  define cms\<^sub>2' where "cms\<^sub>2' \<equiv> cms\<^sub>2 [k := (c\<^sub>2', ?env\<^sub>k')]"

  with i b equal_size have "(cms\<^sub>2, mem\<^sub>2) \<leadsto>\<^bsub>k\<^esub> (cms\<^sub>2', mem\<^sub>2')"
    by (metis meval_intro)

  moreover
  from equal_size have new_length: "length cms\<^sub>1' = length cms\<^sub>2'"
    unfolding cms\<^sub>2'_def
    by (metis eval length_list_update meval_elim)

  with envs_eq have "map snd cms\<^sub>1' = map snd cms\<^sub>2'"
    unfolding cms\<^sub>2'_def
    by (metis b map_update snd_conv)

  moreover

  \<comment> \<open>This is the complicated part of the proof.\<close>
  obtain mems' where "makes_compatible (cms\<^sub>1', mem\<^sub>1') (cms\<^sub>2', mem\<^sub>2') mems'"
  proof
    \<comment> \<open>This is used in two of the following cases, so we prove it beforehand:\<close>
    have x_unchanged: "\<And> x. \<lbrakk> x \<in> ?X k \<rbrakk> \<Longrightarrow>
      mem\<^sub>1 x = mem\<^sub>1' x \<and> mem\<^sub>2 x = mem\<^sub>2' x \<and> dma mem\<^sub>1 x = dma mem\<^sub>1' x"
    proof(intro conjI)
      fix x
      assume "x \<in> ?X k"
      thus "mem\<^sub>1 x = mem\<^sub>1' x"
        using a b c change_respecting_doesnt_modify domh 
        by (metis (erased, hide_lams) Un_upper1 contra_subsetD)
    next
      fix x
      assume "x \<in> ?X k"

      hence eq_mem\<^sub>2: "?\<sigma>_mem\<^sub>2 x = Some (mem\<^sub>2 x)"
        by (metis restrict_in to_partial_def)
      hence "?mems\<^sub>2k [\<mapsto> h] [\<mapsto> ?\<sigma>_mem\<^sub>2] x = mem\<^sub>2 x"
        by (auto simp: subst_def)

      moreover have "mem\<^sub>h [\<mapsto> ?\<sigma>_mem\<^sub>2] x = mem\<^sub>2 x"
        by (auto simp: subst_def `x \<in> ?X k` eq_mem\<^sub>2)

      ultimately have "?mems\<^sub>2k [\<mapsto> h] [\<mapsto> ?\<sigma>_mem\<^sub>2] x = mem\<^sub>h [\<mapsto> ?\<sigma>_mem\<^sub>2] x"
        by auto
      thus "mem\<^sub>2 x = mem\<^sub>2' x"
        by (metis `mem\<^sub>2 = ?mems\<^sub>2k [\<mapsto> ?\<sigma>_mem\<^sub>2]` dom\<sigma>_mem\<^sub>2 domh mem\<^sub>2'_def subst_overrides)
    next
      fix x
      assume "x \<in> ?X k"
      thus "dma mem\<^sub>1 x = dma mem\<^sub>1' x"
        using a b c change_respecting_doesnt_modify_dma domh 
        by (metis (erased, hide_lams))      
    qed

    define mems'_k where "mems'_k \<equiv> \<lambda> x.
      if x \<notin> ?X k
      then (mem\<^sub>1' x, mem\<^sub>2' x)
      else (?mems\<^sub>1k x, ?mems\<^sub>2k x)"

    (* FIXME: see if we can reduce the number of cases *)
    define mems'_i where "mems'_i \<equiv> \<lambda> i x.
      if ((mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x) \<and>
          (mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High))
         then (mem\<^sub>1' x, mem\<^sub>2' x)
         else if ((mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x) \<and>
                  (mem\<^sub>1' x \<noteq> mem\<^sub>2' x \<and> dma mem\<^sub>1' x = Low))
              then (some_val, some_val)
              else if dma mem\<^sub>1 x = High \<and> dma mem\<^sub>1' x = Low then (mem\<^sub>1 x, mem\<^sub>1 x)
              else if dma mem\<^sub>1' x = dma mem\<^sub>1 x then (fst (mems ! i) x, snd (mems ! i) x)
              else (mem\<^sub>1' x, mem\<^sub>2' x)"

    define mems' where "mems' \<equiv>
      map (\<lambda> i.
            if i = k
            then (fst \<circ> mems'_k, snd \<circ> mems'_k)
            else (fst \<circ> mems'_i i, snd \<circ> mems'_i i))
      [0..< length cms\<^sub>1]"
    from b have mems'_k_simp: "mems' ! k = (fst \<circ> mems'_k, snd \<circ> mems'_k)"
      unfolding mems'_def
      by auto

    have mems'_simp2: "\<And>i. \<lbrakk> i \<noteq> k; i < length cms\<^sub>1 \<rbrakk> \<Longrightarrow>
      mems' ! i = (fst \<circ> mems'_i i, snd \<circ> mems'_i i)"
      unfolding mems'_def
      by auto
    (* Some auxiliary statements to allow reasoning about these definitions as if they were given
       by cases instead of nested if clauses. *)
    have mems'_k_1 [simp]: "\<And> x. \<lbrakk> x \<notin> ?X k \<rbrakk> \<Longrightarrow>
      fst (mems' ! k) x = mem\<^sub>1' x \<and> snd (mems' ! k) x = mem\<^sub>2' x"
      unfolding mems'_k_simp mems'_k_def
      by auto
    have mems'_k_2 [simp]: "\<And> x. \<lbrakk> x \<in> ?X k \<rbrakk> \<Longrightarrow>
      fst (mems' ! k) x = fst (mems ! k) x \<and> snd (mems' ! k) x = snd (mems ! k) x"
      unfolding mems'_k_simp mems'_k_def
      by auto

    
    have mems'_k_cases:
      "\<And> P x.
        \<lbrakk>
         \<lbrakk> x \<notin> ?X k;
           fst (mems' ! k) x = mem\<^sub>1' x;
           snd (mems' ! k) x = mem\<^sub>2' x \<rbrakk> \<Longrightarrow> P x;
         \<lbrakk> x \<in> ?X k; 
           fst (mems' ! k) x = fst (mems ! k) x;
           snd (mems' ! k) x = snd (mems ! k) x \<rbrakk> \<Longrightarrow> P x \<rbrakk> \<Longrightarrow> P x"
      apply(case_tac "x \<notin> ?X k")
       apply simp
      apply simp
      done

    have mems'_i_simp:
      "\<And> i. \<lbrakk> i < length cms\<^sub>1; i \<noteq> k \<rbrakk> \<Longrightarrow> mems' ! i = (fst \<circ> mems'_i i, snd \<circ> mems'_i i)"
      unfolding mems'_def
      by auto

    have mems'_i_1 [simp]:
      "\<And> i x. \<lbrakk> i \<noteq> k; i < length cms\<^sub>1;
                 mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x;
                 mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High \<rbrakk> \<Longrightarrow>
               fst (mems' ! i) x = mem\<^sub>1' x \<and> snd (mems' ! i) x = mem\<^sub>2' x"
      unfolding mems'_i_def mems'_i_simp
      by auto

    have mems'_i_2 [simp]:
      "\<And> i x. \<lbrakk> i \<noteq> k; i < length cms\<^sub>1;
                 mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x;
                 mem\<^sub>1' x \<noteq> mem\<^sub>2' x; dma mem\<^sub>1' x = Low \<rbrakk> \<Longrightarrow>
              fst (mems' ! i) x = some_val \<and> snd (mems' ! i) x = some_val"
      unfolding mems'_i_def mems'_i_simp
      by auto

    have mems'_i_3 [simp]:
      "\<And> i x. \<lbrakk> i \<noteq> k; i < length cms\<^sub>1;
                 mem\<^sub>1 x = mem\<^sub>1' x; mem\<^sub>2 x = mem\<^sub>2' x;
                 dma mem\<^sub>1 x = High \<and> dma mem\<^sub>1' x = Low \<rbrakk> \<Longrightarrow>
              fst (mems' ! i) x = mem\<^sub>1 x \<and> snd (mems' ! i) x = mem\<^sub>1 x"
      unfolding mems'_i_def mems'_i_simp
      by auto

    have mems'_i_4 [simp]:
      "\<And> i x. \<lbrakk> i \<noteq> k; i < length cms\<^sub>1;
                 mem\<^sub>1 x = mem\<^sub>1' x; mem\<^sub>2 x = mem\<^sub>2' x;
                 dma mem\<^sub>1' x = dma mem\<^sub>1 x \<rbrakk> \<Longrightarrow>
              fst (mems' ! i) x = fst (mems ! i) x \<and> snd (mems' ! i) x = snd (mems ! i) x"
      unfolding mems'_i_def mems'_i_simp
      by auto

    have mems'_i_5 [simp]:
      "\<And> i x. \<lbrakk> i \<noteq> k; i < length cms\<^sub>1;
                 mem\<^sub>1 x = mem\<^sub>1' x; mem\<^sub>2 x = mem\<^sub>2' x;
                 dma mem\<^sub>1 x = Low \<and> dma mem\<^sub>1' x = High \<rbrakk> \<Longrightarrow>
              fst (mems' ! i) x = mem\<^sub>1' x \<and> snd (mems' ! i) x = mem\<^sub>2' x"
      unfolding mems'_i_def mems'_i_simp
      by auto

    (* This may look complicated, but is actually a rather
       mechanical definition, as it merely spells out the cases
       of the definition: *)
    have mems'_i_cases:
      "\<And> P i x.
         \<lbrakk> i \<noteq> k; i < length cms\<^sub>1;
           \<lbrakk> mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x;
             mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High;
             fst (mems' ! i) x = mem\<^sub>1' x; snd (mems' ! i) x = mem\<^sub>2' x \<rbrakk> \<Longrightarrow> P x;
      \<lbrakk> mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x;
        mem\<^sub>1' x \<noteq>  mem\<^sub>2' x; dma mem\<^sub>1' x = Low;
        fst (mems' ! i) x = some_val; snd (mems' ! i) x = some_val \<rbrakk> \<Longrightarrow> P x;
      \<lbrakk> mem\<^sub>1 x = mem\<^sub>1' x; mem\<^sub>2 x = mem\<^sub>2' x; dma mem\<^sub>1 x = High; 
        dma mem\<^sub>1' x = Low;
        fst (mems' ! i) x = mem\<^sub>1 x; snd (mems' ! i) x = mem\<^sub>1 x \<rbrakk> \<Longrightarrow> P x;
      \<lbrakk> mem\<^sub>1 x = mem\<^sub>1' x; mem\<^sub>2 x = mem\<^sub>2' x; dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High;
        dma mem\<^sub>1' x = dma mem\<^sub>1 x;
        fst (mems' ! i) x = fst (mems ! i) x; snd (mems' ! i) x = snd (mems ! i) x \<rbrakk> \<Longrightarrow> P x;
      \<lbrakk> mem\<^sub>1 x = mem\<^sub>1' x; mem\<^sub>2 x = mem\<^sub>2' x; dma mem\<^sub>1 x = Low; dma mem\<^sub>1' x = High;
        fst (mems' ! i) x = mem\<^sub>1' x; snd (mems' ! i) x = mem\<^sub>2' x \<rbrakk> \<Longrightarrow> P x       \<rbrakk>
      \<Longrightarrow> P x"
      using mems'_i_1 mems'_i_2 mems'_i_3 mems'_i_4 mems'_i_5
      by (metis (full_types) Sec.exhaust)

    let ?X' = "\<lambda> i. differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i"

    have len_unchanged: "length cms\<^sub>1' = length cms\<^sub>1"
      by (metis cms\<^sub>2'_def equal_size length_list_update new_length)

    have mm_equiv': "(cms\<^sub>1' ! k, subst (?\<sigma>_mem\<^sub>2) mem\<^sub>1') \<approx> \<langle>c\<^sub>h, snd (cms\<^sub>1' ! k), mem\<^sub>2'\<rangle>"
      apply(simp add: mem\<^sub>2'_def)
      apply(rule g[THEN conjunct2])
      apply(rule dom_restrict_total)
      done

   hence \<C>_subst_eq: "\<forall>x\<in>\<C>. (subst (?\<sigma>_mem\<^sub>2) mem\<^sub>1') x = mem\<^sub>2' x"
     apply(rule mm_equiv_\<C>_eq)
     by simp

   have low_mds_eq': "(subst (?\<sigma>_mem\<^sub>2) mem\<^sub>1') =\<^bsub>((snd (cms\<^sub>1' ! k)))\<^esub>\<^sup>l mem\<^sub>2'"
     apply(rule mm_equiv_low_eq[where c\<^sub>1="fst (cms\<^sub>1' ! k)"])
     apply(force intro: mm_equiv')
     done

   have \<C>_subst_eq_idemp: "\<And>x. x \<in> \<C> \<Longrightarrow> (subst (?\<sigma>_mem\<^sub>2) mem\<^sub>1') x = mem\<^sub>1' x"
     apply(rule subst_not_in_dom)
     apply(rule notI)
     apply(simp add: dom_restrict_total)
     using compat b by force

    from \<C>_subst_eq \<C>_subst_eq_idemp
    have \<C>_eq: "\<And>x. x \<in> \<C> \<Longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x"
     by simp

    have not_control: "\<And>x i. i < length cms\<^sub>1' \<Longrightarrow> x \<in> ?X' i \<Longrightarrow> x \<notin> \<C>"
    proof(rule ccontr, clarsimp)
      fix x i
      let ?mems\<^sub>1i = "fst (mems ! i)"
      let ?mems\<^sub>2i = "snd (mems ! i)"
      let ?mems\<^sub>1'i = "fst (mems' ! i)"
      let ?mems\<^sub>2'i = "snd (mems' ! i)"
      assume "i < length cms\<^sub>1'"
      have "i < length cms\<^sub>1" by (metis len_unchanged `i < length cms\<^sub>1'`)
      assume "x \<in> ?X' i"
      assume "x \<in> \<C>"
      have "x \<notin> ?X i"
        using compat `i < length cms\<^sub>1'` len_unchanged new_length
        by (metis `x \<in> \<C>` compat_different)
      from `x \<in> \<C>` have  "mem\<^sub>1' x = mem\<^sub>2' x" by(rule \<C>_eq)
      from `x \<in> \<C>` have "dma mem\<^sub>1' x = Low" by(simp add: \<C>_Low)
      show "False"
      proof(cases "i = k")
        assume eq[simp]: "i = k"
        show ?thesis
        using `x \<notin> ?X i` `x \<in> ?X' i`
        by(force simp: differing_vars_lists_def differing_vars_def)
      next
        assume neq: "i \<noteq> k"
        thus ?thesis
        using `x \<in> ?X' i` `x \<notin> ?X i` `x \<in> \<C>` \<C>_Low `mem\<^sub>1' x = mem\<^sub>2' x`
        by(force elim: mems'_i_cases[of "i" "x" "\<lambda>x. False", OF _ `i < length cms\<^sub>1`]
                 simp: differing_vars_lists_def differing_vars_def)
      qed
    qed

    have not_diff': "\<And>i x. i < length cms\<^sub>1' \<Longrightarrow>
           mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High \<or> x \<in> \<C> \<Longrightarrow>
           x \<notin> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i"
    proof -
      fix i x

      let ?mems\<^sub>1'i = "fst (mems' ! i)"
      let ?mems\<^sub>2'i = "snd (mems' ! i)"
      assume i_le: "i < length cms\<^sub>1'"
      assume mem_eq': "mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High \<or> x \<in> \<C>"
      show "x \<notin> ?X' i"
      proof (cases "x \<in> \<C>")
        assume "x \<in> \<C>"
        thus ?thesis by(metis not_control i_le)
      next
        assume "x \<notin> \<C>"
        hence mem_eq: "mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High" by(metis mem_eq')
        thus ?thesis
        proof (cases "i = k")
          assume "i = k"
          thus "x \<notin> ?X' i"
            apply (cases "x \<notin> ?X k")
             apply (metis differing_vars_neg_intro mems'_k_1)
            by(metis compat_different[OF compat] b mem_eq Sec.distinct(1) x_unchanged)
        next
          assume "i \<noteq> k"
          thus "x \<notin> ?X' i"
          proof (rule mems'_i_cases)
            from b i_le show "i < length cms\<^sub>1"
              by (metis length_list_update)
          next
            assume "fst (mems' ! i) x = mem\<^sub>1' x"
              "snd (mems' ! i) x = mem\<^sub>2' x"
            thus "x \<notin> ?X' i"
              by (metis differing_vars_neg_intro)
          next
            assume "mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x"
              "mem\<^sub>1' x \<noteq> mem\<^sub>2' x" and "dma mem\<^sub>1' x = Low"
            \<comment> \<open>In this case, for example, the values of (mems' ! i) are not needed.\<close>
            thus "x \<notin> ?X' i"
              by (metis Sec.simps(2) mem_eq)
          next
            (* FIXME: clean this up -- there is surely a more direct route.
                      Same must be true of mems'_i definition and o, o_downgrade,
                      p etc. lemmas above that are proved with surely lots of
                      overlapping cases *)
            assume case3: "mem\<^sub>1 x = mem\<^sub>1' x" "mem\<^sub>2 x = mem\<^sub>2' x" 
              "dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High"
              "fst (mems' ! i) x = fst (mems ! i) x"
              "snd (mems' ! i) x = snd (mems ! i) x"
            have "x \<in> ?X' i \<Longrightarrow> mem\<^sub>1' x \<noteq> mem\<^sub>2' x \<and> dma mem\<^sub>1' x = Low"
            proof -
              assume "x \<in> ?X' i"
              from case3 and `x \<in> ?X' i` have "x \<in> ?X i"
                by (metis differing_vars_neg differing_vars_elim)
              with case3 have "mem\<^sub>1' x \<noteq> mem\<^sub>2' x \<and> dma mem\<^sub>1 x = Low"
                by (metis b compat compat_different i_le length_list_update)
              with mem_eq have clases: "dma mem\<^sub>1 x = Low \<and> dma mem\<^sub>1' x = High" by auto
              have "fst (mems' ! i) x = mem\<^sub>1' x \<and> snd (mems' ! i) x = mem\<^sub>2' x"
                apply(rule mems'_i_5)
                     apply(rule `i \<noteq> k`)
                    using i_le len_unchanged apply(simp)
                   apply(simp add: case3)+
                 apply(simp add: clases)+
                done
              hence "x \<notin> ?X' i" by (metis differing_vars_neg_intro)
              with `x \<in> ?X' i` show ?thesis by blast
            qed
            with `mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High` show "x \<notin> ?X' i"
              by auto
          next
            assume case4: "mem\<^sub>1 x = mem\<^sub>1' x" "mem\<^sub>2 x = mem\<^sub>2' x"
                   "dma mem\<^sub>1 x = High" "dma mem\<^sub>1' x = Low"
                   "fst (mems' ! i) x = mem\<^sub>1 x" "snd (mems' ! i) x = mem\<^sub>1 x"
            with mem_eq have "mem\<^sub>1' x = mem\<^sub>2' x" by auto
            with case4 show ?thesis by(auto simp: differing_vars_def differing_vars_lists_def)
          next
            assume "fst (mems' ! i) x = mem\<^sub>1' x"
                   "snd (mems' ! i) x = mem\<^sub>2' x" thus ?thesis by(metis differing_vars_neg_intro)
          qed
        qed
      qed
    qed

    have ex_not_diff':
      "length cms\<^sub>1' = 0 \<and> mem\<^sub>1' =\<^sup>l mem\<^sub>2' \<or>
      (\<forall>x. \<exists>i<length cms\<^sub>1'. x \<notin> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i)"
    proof -
      { fix x
        have "\<exists> i < length cms\<^sub>1. x \<notin> ?X' i"
        proof (cases "mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x \<or> dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x")
          assume var_changed: "mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x \<or> dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x"
          have "x \<notin> ?X' k"
            apply (rule mems'_k_cases)
             apply (metis differing_vars_neg_intro)
            by (metis var_changed x_unchanged)
          thus ?thesis by (metis b)
        next
          assume "\<not> (mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x \<or> dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x)"
          hence assms: "mem\<^sub>1 x = mem\<^sub>1' x" "mem\<^sub>2 x = mem\<^sub>2' x" "dma mem\<^sub>1' x = dma mem\<^sub>1 x" by auto

          have "length cms\<^sub>1 \<noteq> 0"
            using b
            by (metis less_zeroE)
          then obtain i where i_prop: "i < length cms\<^sub>1 \<and> x \<notin> ?X i"
            using compat
            by (auto, blast)
          show ?thesis
          proof (cases "i = k")
            assume "i = k"
            have "x \<notin> ?X' k"
              apply (rule mems'_k_cases)
               apply (metis differing_vars_neg_intro)
              by (metis i_prop `i = k`)
            thus ?thesis
              by (metis b)
          next
            from i_prop have "x \<notin> ?X i" by simp
            assume "i \<noteq> k"
            hence "x \<notin> ?X' i"
              (* FIXME: clean up *)
              apply -
              apply(rule mems'_i_cases)
                    apply assumption
                   apply(simp add: i_prop)
                  apply(simp add: assms)+
               using `x \<notin> ?X i` differing_vars_neg
               using assms differing_vars_elim apply auto[1]
              by(metis differing_vars_neg_intro)
            with i_prop show ?thesis
              by auto
          qed
        qed
      }
      thus "(length cms\<^sub>1' = 0 \<and> mem\<^sub>1' =\<^sup>l mem\<^sub>2') \<or> (\<forall> x. \<exists> i < length cms\<^sub>1'. x \<notin> ?X' i)"
        by (metis cms\<^sub>2'_def equal_size length_list_update new_length)
    qed

    have mm_equiv\<^sub>k': "\<And>i \<sigma>. dom \<sigma> = ?X' i \<Longrightarrow> i = k \<Longrightarrow> i < length cms\<^sub>1' \<Longrightarrow> (cms\<^sub>1' ! i, (fst (mems' ! i)) [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2' ! i, (snd (mems' ! i)) [\<mapsto> \<sigma>])"
    proof -
      fix i 
      let ?mems\<^sub>1'i = "fst (mems' ! i)"
      let ?mems\<^sub>2'i = "snd (mems' ! i)"
      fix \<sigma> :: "'Var \<Rightarrow> 'Val option"
      assume [simp]: "i = k"
      assume i_le: "i < length cms\<^sub>1'"
      assume dom\<sigma>: "dom \<sigma> = ?X' i"
      show "(cms\<^sub>1' ! i, (fst (mems' ! i)) [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2' ! i, (snd (mems' ! i)) [\<mapsto> \<sigma>])"
      proof -
        \<comment> \<open>We define another  function from this and reuse the universally quantified statements
          from the first part of the proof.\<close>
        define \<sigma>' where "\<sigma>' \<equiv>
          \<lambda> x. if x \<in> ?X k
                then if x \<in> ?X' k
                     then \<sigma> x
                     else Some (mem\<^sub>1' x)
                else None"
        then have dom\<sigma>': "dom \<sigma>' = ?X k"
          apply (clarsimp, safe)
          by( metis domI domIff, metis `i = k` domD dom\<sigma> )
        have diff_vars_impl [simp]: "\<And>x. x \<in> ?X' k \<Longrightarrow> x \<in> ?X k"
        proof (rule ccontr)
          fix x
          assume "x \<notin> ?X k"
          hence "mem\<^sub>1 x = ?mems\<^sub>1k x \<and> mem\<^sub>2 x = ?mems\<^sub>2k x"
            by (metis differing_vars_neg)
          from `x \<notin> ?X k` have "?mems\<^sub>1'i x = mem\<^sub>1' x \<and> ?mems\<^sub>2'i x = mem\<^sub>2' x"
            by auto
          moreover
          assume "x \<in> ?X' k"
          hence "mem\<^sub>1' x \<noteq> ?mems\<^sub>1'i x \<or> mem\<^sub>2' x \<noteq> ?mems\<^sub>2'i x"
            by (metis `i = k` differing_vars_elim)
          ultimately show False
            by auto
        qed

        (* We now show that we can reuse the earlier statements
           by showing the following equality: *)
        have "?mems\<^sub>1'i [\<mapsto> \<sigma>] = mem\<^sub>1' [\<mapsto> \<sigma>']"
        proof (rule ext)
          fix x

          show "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = mem\<^sub>1' [\<mapsto> \<sigma>'] x"
          proof (cases "x \<in> ?X' k")
            assume x_in_X'k: "x \<in> ?X' k"

            then obtain v where "\<sigma> x = Some v"
              by (metis dom\<sigma> domD `i = k`)
            hence "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = v"
              using `x \<in> ?X' k` dom\<sigma>
              by (auto simp: subst_def)
            moreover
            from dom\<sigma>' and `x \<in> ?X' k` have "x \<in> dom \<sigma>'" by simp
             
            hence "mem\<^sub>1' [\<mapsto> \<sigma>'] x = v"
              using dom\<sigma>' 
              unfolding subst_def
              by (metis \<sigma>'_def `\<sigma> x = Some v` diff_vars_impl option.simps(5) x_in_X'k)

            ultimately show "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = mem\<^sub>1' [\<mapsto> \<sigma>'] x" ..
          next
            assume "x \<notin> ?X' k"

            hence "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = ?mems\<^sub>1'i x"
              using dom\<sigma>
              by (metis `i = k` subst_not_in_dom)
            show ?thesis
            proof(case_tac "x \<in> ?X k")
              assume "x \<in> ?X k"
              from `x \<notin> ?X' k` have "mem\<^sub>1' x = ?mems\<^sub>1'i x"
                by(metis differing_vars_neg `i = k`)
              then have "\<sigma>' x = Some (?mems\<^sub>1'i x)"
                unfolding \<sigma>'_def
                using dom\<sigma>' domh
                by(simp add: `x \<in> ?X k` `x \<notin> ?X' k`)
              hence "mem\<^sub>1' [\<mapsto> \<sigma>'] x = ?mems\<^sub>1'i x"
                unfolding subst_def
                by (metis option.simps(5))
              thus ?thesis
                by (metis `?mems\<^sub>1'i [\<mapsto>\<sigma>] x = ?mems\<^sub>1'i x`)
            next
              assume "x \<notin> ?X k"
              then have "mem\<^sub>1' [\<mapsto> \<sigma>'] x = mem\<^sub>1' x"
                by (metis dom\<sigma>' subst_not_in_dom)
              moreover
              have "?mems\<^sub>1'i x = mem\<^sub>1' x"
                by (metis `i = k` `x \<notin> ?X' k` differing_vars_neg)
              ultimately show ?thesis
                by (metis `?mems\<^sub>1'i [\<mapsto>\<sigma>] x = ?mems\<^sub>1'i x`)
            qed
          qed
        qed
        (* And the same for the second memories: *)
        moreover have "?mems\<^sub>2'i [\<mapsto> \<sigma>] = mem\<^sub>h [\<mapsto> \<sigma>']"
        proof (rule ext)
          fix x

          show "?mems\<^sub>2'i [\<mapsto> \<sigma>] x = mem\<^sub>h [\<mapsto> \<sigma>'] x"
          proof (cases "x \<in> ?X' k")
            assume "x \<in> ?X' k"

            then obtain v where "\<sigma> x = Some v"
              using dom\<sigma>
              by (metis domD `i = k`)
            hence "?mems\<^sub>2'i [\<mapsto> \<sigma>] x = v"
              using `x \<in> ?X' k` dom\<sigma>
              unfolding subst_def
              by (metis option.simps(5))
            moreover
            from `x \<in> ?X' k` have "x \<in> ?X k"
              by auto
            hence "x \<in> dom (\<sigma>')"
              by (metis dom\<sigma>'  `x \<in> ?X' k`)
            hence "mem\<^sub>2' [\<mapsto> \<sigma>'] x = v"
              using dom\<sigma>' c 
              unfolding subst_def
              by (metis \<sigma>'_def `\<sigma> x = Some v` option.simps(5) `x \<in> ?X' k`)

            ultimately show ?thesis
              by (metis dom\<sigma>' dom_restrict_total mem\<^sub>2'_def subst_overrides)
          next
            assume "x \<notin> ?X' k"

            hence "?mems\<^sub>2'i [\<mapsto> \<sigma>] x = ?mems\<^sub>2'i x"
              using dom\<sigma>
              by (metis `i = k` subst_not_in_dom) 
            show ?thesis
            
            proof(case_tac "x \<in> ?X k")
              assume "x \<in> ?X k"  
              (* This case can't happen so derive a contradiction *)
              hence "mem\<^sub>1 x = mem\<^sub>1' x \<and> mem\<^sub>2 x = mem\<^sub>2' x" by (metis x_unchanged)

              moreover from `x \<notin> ?X' k` `i = k` have "?mems\<^sub>1'i x = mem\<^sub>1' x \<and> ?mems\<^sub>2'i x = mem\<^sub>2' x"
                by(metis differing_vars_neg)
               
              moreover from `x \<in> ?X k` have "fst (mems ! i) x \<noteq> mem\<^sub>1 x \<or> snd (mems ! i) x \<noteq> mem\<^sub>2 x"
                by(metis differing_vars_elim `i = k`)

              moreover from `x \<in> ?X k` have "fst (mems' ! i) x = fst (mems ! i) x \<and>
                                             snd (mems' ! i) x = snd (mems ! i) x"
                by(metis mems'_k_2 `i = k`)
                
              ultimately have "False" by auto

              thus ?thesis by blast
            next
              assume "x \<notin> ?X k"
              hence "x \<notin> dom \<sigma>'" by (simp add: dom\<sigma>')
              then have "mem\<^sub>h [\<mapsto> \<sigma>'] x = mem\<^sub>h x"
                by (metis subst_not_in_dom)
              moreover
              have "?mems\<^sub>2'i x = mem\<^sub>2' x"
                by (metis `i = k`  mems'_k_1 `x \<notin> ?X k`)

              hence "?mems\<^sub>2'i x = mem\<^sub>h x"
                unfolding mem\<^sub>2'_def
                by (metis dom\<sigma>_mem\<^sub>2 subst_not_in_dom `x \<notin> ?X k`)
              ultimately show ?thesis
                by (metis `?mems\<^sub>2'i [\<mapsto>\<sigma>] x = ?mems\<^sub>2'i x`)
            qed
          qed
        qed

        ultimately show
          "(cms\<^sub>1' ! i, (fst (mems' ! i)) [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2' ! i, (snd (mems' ! i)) [\<mapsto> \<sigma>])"
          using dom\<sigma> dom\<sigma>' g b `i = k`
          by (metis c\<^sub>2'_def cms\<^sub>2'_def equal_size nth_list_update_eq)
      qed
    qed

    hence low_mds_eq\<^sub>k': "\<And>i \<sigma>. dom \<sigma> = ?X' i \<Longrightarrow> i = k \<Longrightarrow> i < length cms\<^sub>1' \<Longrightarrow> low_mds_eq ( (snd (cms\<^sub>1' ! i))) ((fst (mems' ! i)) [\<mapsto> \<sigma>]) ((snd (mems' ! i)) [\<mapsto> \<sigma>])"
      using mm_equiv_low_eq 
      by (metis cms\<^sub>2'_def equal_size len_unchanged nth_list_update_eq prod.collapse) 

    (* Note: below we extract a bunch of reasoning that was previously done inside the
       proof that the adaptation is closed_globally_consistent. In particular we extract
       everything proving that the adaptation preserves low_mds_eq. The reason we
       extract this is because we need this to conclude that all variables in the new memories
       that differ from their hypothetical memories for component i are guaranteed not readable
       by i. We extract all of it here and then export a bunch of facts which are needed to 
       prove various parts of adaptation being closed_globally_consistent. *)
    define \<sigma>' where "\<sigma>' \<equiv> \<lambda> \<sigma> i x. if x \<in> ?X i
                   then if x \<in> ?X' i
                        then \<sigma> x
                        else Some (mem\<^sub>1' x)
                   else None"
    let ?\<Delta> = "\<lambda> (\<sigma>::'Var \<Rightarrow> 'Val option) i. differing_vars ((fst (mems ! i)) [\<mapsto> \<sigma>' \<sigma> i]) ((fst (mems' ! i)) [\<mapsto> \<sigma>]) \<union>
              differing_vars ((snd (mems ! i)) [\<mapsto> \<sigma>' \<sigma> i]) ((snd (mems' ! i)) [\<mapsto> \<sigma>])"

    have \<Delta>_finite: "\<And> \<sigma> i. finite (?\<Delta> \<sigma> i)"
      by (metis (no_types) differing_finite finite_UnI)
    \<comment> \<open>We first define the adaptation, then prove that it does the right thing.\<close>
    define A where "A \<equiv> \<lambda> \<sigma> i x. if x \<in> ?\<Delta> \<sigma> i
                  then if dma ((fst (mems' ! i)) [\<mapsto> \<sigma>]) x = High
                       then Some ((fst (mems' ! i)) [\<mapsto> \<sigma>] x, (snd (mems' ! i)) [\<mapsto> \<sigma>] x)
                       else if x \<in> ?X' i
                            then (case \<sigma> x of
                                    Some v \<Rightarrow> Some (v, v)
                                  | None \<Rightarrow> None)
                            else Some (mem\<^sub>1' x, mem\<^sub>1' x)
                  else None"

    {    
      fix i
      fix \<sigma> :: "'Var \<rightharpoonup> 'Val"
      let ?mems\<^sub>1'i = "fst (mems' ! i)"
      let ?mems\<^sub>2'i = "snd (mems' ! i)"
      assume i_le: "i < length cms\<^sub>1'"
      assume dom\<sigma>: "dom \<sigma> = ?X' i"
      assume "i \<noteq> k"

      define \<sigma>'' where "\<sigma>'' \<equiv> \<sigma>' \<sigma> i"
      note \<sigma>''_def2 = \<sigma>''_def[unfolded \<sigma>'_def]

      let ?mems\<^sub>1i = "fst (mems ! i)" and
          ?mems\<^sub>2i = "snd (mems ! i)"
      have "dom \<sigma>'' = ?X i"
        unfolding \<sigma>''_def2
        apply auto
         apply (metis option.simps(2))
        by (metis domD dom\<sigma>)

      have o: "\<And> x.
               ((?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or>
                ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x) \<and>
               (dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High) \<and>
               (dma mem\<^sub>1' x = dma mem\<^sub>1 x))
               \<longrightarrow> (mem\<^sub>1' x \<noteq> mem\<^sub>1 x \<or> mem\<^sub>2' x \<noteq> mem\<^sub>2 x)"
      proof -
        fix x
        {
          assume eq_mem: "mem\<^sub>1' x = mem\<^sub>1 x \<and> mem\<^sub>2' x = mem\<^sub>2 x"
             and clas: "dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High"
             and clas_eq: "dma mem\<^sub>1' x = dma mem\<^sub>1 x"
          hence mems'_simp: "?mems\<^sub>1'i x = ?mems\<^sub>1i x \<and> ?mems\<^sub>2'i x = ?mems\<^sub>2i x"
            using mems'_i_4
            by (metis `i \<noteq> k` b i_le length_list_update)
          have
            "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<and> ?mems\<^sub>2'i [\<mapsto> \<sigma>] x = ?mems\<^sub>2i [\<mapsto> \<sigma>''] x"
          proof (cases "x \<in> ?X' i")
            assume "x \<in> ?X' i"
            hence "?mems\<^sub>1'i x \<noteq> mem\<^sub>1' x \<or> ?mems\<^sub>2'i x \<noteq> mem\<^sub>2' x"
              by (metis differing_vars_neg_intro)
            hence "x \<in> ?X i"
              using eq_mem mems'_simp
              by (metis differing_vars_neg)
            hence "\<sigma>'' x = \<sigma> x"
              by (metis \<sigma>''_def2 `x \<in> ?X' i`)
            thus ?thesis
              by (clarsimp simp: subst_def mems'_simp split: option.splits)
          next
            assume "x \<notin> ?X' i"
            hence "?mems\<^sub>1'i x = mem\<^sub>1' x \<and> ?mems\<^sub>2'i x = mem\<^sub>2' x"
              by (metis differing_vars_neg)
            hence "x \<notin> ?X i"
              using eq_mem mems'_simp
              by (auto simp: differing_vars_neg_intro)
            thus ?thesis
              by (metis `dom \<sigma>'' = ?X i` `x \<notin> ?X' i` dom\<sigma> mems'_simp subst_not_in_dom)
          qed
        }
        thus "?thesis x" by blast
      qed

      (* FIXME: clean this up once we optimise the definition of mems'_i *)
      (* Note: try to establish something similar to o for the downgrading case *)
      have o_downgrade: "\<And>x. x \<notin> ?X' i \<and> (subst \<sigma> (fst (mems' ! i)) x \<noteq> subst \<sigma>'' (fst (mems ! i)) x \<or>
                  subst \<sigma> (snd (mems' ! i)) x \<noteq> subst \<sigma>'' (snd (mems ! i)) x) \<and>
                 (dma mem\<^sub>1 x = High \<and> dma mem\<^sub>1' x = Low) \<longrightarrow>
                  mem\<^sub>1' x \<noteq> mem\<^sub>1 x \<or> mem\<^sub>2' x \<noteq> mem\<^sub>2 x"
        proof -
        fix x {
          assume mem_eq: "mem\<^sub>1' x = mem\<^sub>1 x \<and> mem\<^sub>2' x = mem\<^sub>2 x"
             and clas: "(dma mem\<^sub>1 x = High \<and> dma mem\<^sub>1' x = Low)"
             and notin: "x \<notin> ?X' i"
          hence mems'_simp [simp]: "?mems\<^sub>1'i x = mem\<^sub>1 x \<and> ?mems\<^sub>2'i x = mem\<^sub>1 x"
            using mems'_i_3
            by (metis `i \<noteq> k` b i_le length_list_update)
          have
            "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<and> ?mems\<^sub>2'i [\<mapsto> \<sigma>] x = ?mems\<^sub>2i [\<mapsto> \<sigma>''] x"
          proof (cases "x \<in> ?X' i")
            assume "x \<in> ?X' i"
            thus ?thesis using notin by blast
          next
            assume "x \<notin> ?X' i"
            hence "?mems\<^sub>1'i x = mem\<^sub>1' x \<and> ?mems\<^sub>2'i x = mem\<^sub>2' x"
              by (metis differing_vars_neg)
            moreover have "x \<notin> ?X i"
              using clas compat i_le len_unchanged
              by (force)
            ultimately show ?thesis
              using dom\<sigma> `dom \<sigma>'' = ?X i` `x \<notin> ?X' i` apply(simp add: subst_not_in_dom)
              apply(simp add: mem_eq)
              apply(force simp: differing_vars_def differing_vars_lists_def)
              done
          qed
          
        } thus "?thesis x" by blast
      qed

      have modifies_no_var_asm_not_written:
           "\<And>x. mem\<^sub>1' x \<noteq> mem\<^sub>1 x \<or> mem\<^sub>2' x \<noteq> mem\<^sub>2 x \<or>
                 dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x \<or> dma mem\<^sub>2' x \<noteq> dma mem\<^sub>2 x \<Longrightarrow>
            \<not> var_asm_not_written ( (snd (cms\<^sub>1 ! i))) x"
      proof -
        fix x
        assume "mem\<^sub>1' x \<noteq> mem\<^sub>1 x \<or> mem\<^sub>2' x \<noteq> mem\<^sub>2 x \<or> dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x \<or> dma mem\<^sub>2' x \<noteq> dma mem\<^sub>2 x"
        hence modified: " \<not> (doesnt_modify (fst (cms\<^sub>1 ! k)) mem\<^sub>1 {x}) \<or> \<not> (doesnt_modify (fst (cms\<^sub>2 ! k)) mem\<^sub>2 {x})"
          using b i singletonI unfolding doesnt_modify_def
          by (metis surjective_pairing)

        hence modified_r: " \<not> (doesnt_read_or_modify (fst (cms\<^sub>1 ! k)) mem\<^sub>1 {x}) \<or> \<not> (doesnt_read_or_modify (fst (cms\<^sub>2 ! k)) mem\<^sub>2 {x})" using doesnt_read_or_modify_doesnt_modify by fastforce

        from sound_envs have loc_modes:
          "locally_sound_env_use (cms\<^sub>1 ! k, mem\<^sub>1) \<and>
           locally_sound_env_use (cms\<^sub>2 ! k, mem\<^sub>2)"
          unfolding sound_env_use.simps
          by (metis b equal_size list_all_length)
        moreover
        have "snd (cms\<^sub>1 ! k) = snd (cms\<^sub>2 ! k)"
          by (metis b equal_size envs_eq nth_map)
        have "(cms\<^sub>1 ! k, mem\<^sub>1) \<in> loc_reach (cms\<^sub>1 ! k, mem\<^sub>1) \<and>
              (cms\<^sub>2 ! k, mem\<^sub>2) \<in> loc_reach (cms\<^sub>2 ! k, mem\<^sub>2)"
          using loc_reach.refl by auto
        hence guars:
              "doesnt_modify (fst (cms\<^sub>1 ! k)) mem\<^sub>1 (GuarNoW (snd (cms\<^sub>1 ! k)) ) \<and>
               doesnt_modify (fst (cms\<^sub>2 ! k)) mem\<^sub>2 (GuarNoW (snd (cms\<^sub>2 ! k)) ) \<and>
               doesnt_read_or_modify (fst (cms\<^sub>1 ! k)) mem\<^sub>1 (GuarNoRW (snd (cms\<^sub>1 ! k)) ) \<and>
               doesnt_read_or_modify (fst (cms\<^sub>2 ! k)) mem\<^sub>2 (GuarNoRW (snd (cms\<^sub>2 ! k)) )"
          using loc_modes
          unfolding locally_sound_env_use_def
          by (metis \<open>snd (cms\<^sub>1 ! k) = snd (cms\<^sub>2 ! k)\<close> prod.collapse)

        hence "\<not> {x} \<subseteq> GuarNoW (snd (cms\<^sub>1 ! k)) \<and> \<not> {x} \<subseteq> GuarNoRW (snd (cms\<^sub>1 ! k))"
          using modified modified_r loc_modes locally_sound_env_use_def
                doesnt_modify_subset doesnt_read_or_modify_subset
          by (metis (no_types, lifting) \<open>snd (cms\<^sub>1 ! k) = snd (cms\<^sub>2 ! k)\<close>)
        moreover
        from sound_envs have "compatible_envs (map snd cms\<^sub>1)"
          by (metis globally_sound_envs_compatible sound_env_use.simps)

        ultimately show "(?thesis x)"
          unfolding compatible_modes_def var_asm_not_written_def
          using `i \<noteq> k` i_le
          using b compatible_envs_def compatible_modes_def len_unchanged length_map nth_map singletonD subsetI by auto
      qed
      
      from o o_downgrade have
        p: "\<And> x. \<lbrakk> ?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or>
                    ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x;
                   x \<notin> ?X' i \<or> ((dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High) \<and>
                                 (dma mem\<^sub>1' x = dma mem\<^sub>1 x)) \<rbrakk> \<Longrightarrow>
        \<not> var_asm_not_written ((snd (cms\<^sub>1 ! i))) x"
      proof -
        fix x
        assume mems_neq:
          "?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or> ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x"
           and nin:
          "x \<notin> ?X' i \<or> ((dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High) \<and>
                         (dma mem\<^sub>1' x = dma mem\<^sub>1 x))"
        hence "mem\<^sub>1' x \<noteq> mem\<^sub>1 x \<or> mem\<^sub>2' x \<noteq> mem\<^sub>2 x \<or> dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x"
          (* FIXME: clean this up *)
          apply -
          apply(erule disjE[where P="x \<notin> ?X' i"])
           apply(case_tac "(dma mem\<^sub>1 x = High \<and> dma mem\<^sub>1' x = Low)")
            apply(metis o_downgrade[rule_format])
           apply(case_tac "dma mem\<^sub>1' x = dma mem\<^sub>1 x")
            (* use o *)
            apply(metis (poly_guards_query) o Sec.exhaust)
           (* should follow trivially *)
           apply fastforce
          apply(metis (poly_guards_query) o Sec.exhaust)
          done
        thus "?thesis x"
          by(force simp: modifies_no_var_asm_not_written)
      qed

      have q':
        "\<And> x. \<lbrakk> dma mem\<^sub>1 x = Low; dma mem\<^sub>1' x = Low;
                 ?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or>
                 ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x;
                 x \<notin> ?X' i \<rbrakk> \<Longrightarrow>
               mem\<^sub>1' x = mem\<^sub>2' x"
        by (metis `i \<noteq> k` b compat_different_vars i_le length_list_update mems'_i_2 o)
      have "i < length cms\<^sub>1"
        by (metis cms\<^sub>2'_def equal_size i_le length_list_update new_length)
      with compat and `dom \<sigma>'' = ?X i` have
        bisim: "(cms\<^sub>1 ! i, ?mems\<^sub>1i [\<mapsto> \<sigma>'']) \<approx> (cms\<^sub>2 ! i, ?mems\<^sub>2i [\<mapsto> \<sigma>''])"
        by auto

      define \<sigma>'\<^sub>k where "\<sigma>'\<^sub>k \<equiv> \<lambda>x. if x \<in> ?X k then Some (undefined::'Val) else None"
      have "dom \<sigma>'\<^sub>k = ?X k" unfolding \<sigma>'\<^sub>k_def by (simp add: dom_def)
      with compat and `dom \<sigma>'\<^sub>k = ?X k` and b have
        bisim\<^sub>k: "(cms\<^sub>1 ! k, ?mems\<^sub>1k [\<mapsto> \<sigma>'\<^sub>k]) \<approx> (cms\<^sub>2 ! k, ?mems\<^sub>2k [\<mapsto> \<sigma>'\<^sub>k])"
        by auto

      have q_downgrade:
        "\<And> x. \<lbrakk> dma mem\<^sub>1 x = High; dma mem\<^sub>1' x = Low;
                 ?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or>
                 ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x;
                 x \<notin> ?X' i \<rbrakk> \<Longrightarrow>
               mem\<^sub>1' x = mem\<^sub>2' x"
      by (metis (erased, hide_lams) `i \<noteq> k` compat_different_vars i_le len_unchanged mems'_i_2 o_downgrade)

      have q: "\<And> x. \<lbrakk> dma mem\<^sub>1' x = Low;
                 ?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or>
                 ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x;
                 x \<notin> ?X' i \<rbrakk> \<Longrightarrow>
               mem\<^sub>1' x = mem\<^sub>2' x"
        apply(case_tac "dma mem\<^sub>1 x")
         apply(blast intro: q_downgrade)
        by(blast intro: q')

      let ?\<Delta> = "differing_vars (?mems\<^sub>1i [\<mapsto> \<sigma>'']) (?mems\<^sub>1'i [\<mapsto> \<sigma>]) \<union>
                differing_vars (?mems\<^sub>2i [\<mapsto> \<sigma>'']) (?mems\<^sub>2'i [\<mapsto> \<sigma>])"

      have \<Delta>_finite: "finite ?\<Delta>"
        by (metis (no_types) differing_finite finite_UnI)
      \<comment> \<open>We first define the adaptation, then prove that it does the right thing.\<close>
      define A' where "A' \<equiv> A \<sigma> i"
      note A'_def2 = A'_def[unfolded A_def, folded \<sigma>''_def]
      have domA: "dom A' = ?\<Delta>"
      proof
        show "dom A' \<subseteq> ?\<Delta>"
          using A'_def2
          apply (auto simp: domD)
          by (metis option.simps(2))
      next
        show "?\<Delta> \<subseteq> dom A'"
          unfolding A'_def2
          apply auto
           apply (metis (no_types) domIff dom\<sigma> option.exhaust option.simps(5))
          by (metis (no_types) domIff dom\<sigma> option.exhaust option.simps(5))
      qed

      (* FIXME: clean up *)
      have dma_eq: "dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) = dma mem\<^sub>1'"
        apply(rule dma_\<C>)
        apply(rule ballI)
        apply(case_tac "x \<in> ?X' i")
         apply(drule not_control[rotated])
          apply (metis i_le)
         apply blast
        apply(subst subst_not_in_dom)
         apply(simp add: dom\<sigma>)
        apply(simp add: differing_vars_lists_def differing_vars_def)
        done

      (* FIXME: clean up *)
      have dma_eq'': "dma (?mems\<^sub>1i [\<mapsto> \<sigma>'']) = dma mem\<^sub>1"
        apply(rule dma_\<C>)
        apply(rule ballI)
        apply(case_tac "x \<in> ?X i")
         using compat compat i_le len_unchanged apply fastforce
        apply(subst subst_not_in_dom)
         apply(simp add: `dom \<sigma>'' = ?X i`)
        apply(simp add: differing_vars_lists_def differing_vars_def)
        done

      have dma_eq': "dma (subst ((to_partial mem\<^sub>2 |` differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems k)) mem\<^sub>1') = dma mem\<^sub>1'"
        using compat b
        by(force intro!: dma_\<C> subst_not_in_dom)

      have A_correct:
            "\<And> x.
             ?mems\<^sub>1i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>1 A'] x = ?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<and>
             ?mems\<^sub>2i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>2 A'] x = ?mems\<^sub>2'i [\<mapsto> \<sigma>] x"
      proof -
        fix x
        show "?thesis x"
          (is "?Eq\<^sub>1 \<and> ?Eq\<^sub>2")
        proof (cases "x \<in> ?\<Delta>")
          assume "x \<in> ?\<Delta>"
          hence diff:
            "?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or> ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x"
            by (auto simp: differing_vars_def)
          show ?thesis
          proof (cases "dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) x")
            assume "dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) x = High"
            from `dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) x = High` have A_simp [simp]:
              "A' x = Some (?mems\<^sub>1'i [\<mapsto> \<sigma>] x, ?mems\<^sub>2'i [\<mapsto> \<sigma>] x)"
              unfolding A'_def2
              by (metis `x \<in> ?\<Delta>`)
            from A_simp have ?Eq\<^sub>1 ?Eq\<^sub>2
              unfolding A_def apply_adaptation_def
              by auto
            thus ?thesis
              by auto
          next
            assume "dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) x = Low"
            show ?thesis
            proof (cases "x \<in> ?X' i")
              assume "x \<in> ?X' i"
              then obtain v where "\<sigma> x = Some v"
                by (metis domD dom\<sigma>)
              hence eq: "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = v \<and> ?mems\<^sub>2'i [\<mapsto> \<sigma>] x = v"
                unfolding subst_def
                by auto
              moreover
              from `x \<in> ?X' i` and `dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) x = Low` have A_simp [simp]:
                "A' x = (case \<sigma> x of
                          Some v \<Rightarrow> Some (v, v)
                        | None \<Rightarrow> None)"
                unfolding A'_def2
                using `x \<in> ?\<Delta>` by auto
              ultimately show ?thesis
                using domA `x \<in> ?\<Delta>` `\<sigma> x = Some v`
                by (auto simp: apply_adaptation_def)

            next
              assume "x \<notin> ?X' i"

              hence A_simp [simp]: "A' x = Some (mem\<^sub>1' x, mem\<^sub>1' x)"
                unfolding A'_def2
                using `x \<in> ?\<Delta>` `x \<notin> ?X' i` `dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) x = Low`
                by auto

              from q have "mem\<^sub>1' x = mem\<^sub>2' x"
                by (metis `dma (?mems\<^sub>1'i [\<mapsto> \<sigma>]) x = Low` diff `x \<notin> ?X' i` dma_eq dma_eq'')

              from `x \<notin> ?X' i` have
                "?mems\<^sub>1'i [\<mapsto> \<sigma>] x = ?mems\<^sub>1'i x \<and> ?mems\<^sub>2'i [\<mapsto> \<sigma>] x = ?mems\<^sub>2'i x"
                by (metis dom\<sigma> subst_not_in_dom)
              moreover
              from `x \<notin> ?X' i` have "?mems\<^sub>1'i x = mem\<^sub>1' x \<and> ?mems\<^sub>2'i x = mem\<^sub>2' x"
                by (metis differing_vars_neg)
              ultimately show ?thesis
                using `mem\<^sub>1' x = mem\<^sub>2' x`
                by (auto simp: apply_adaptation_def)
            qed
          qed
        
        next
          assume "x \<notin> ?\<Delta>"
          hence "A' x = None"
            by (metis domA domIff)
          from `A' x = None` have "x \<notin> dom A'"
            by (metis domIff)
          from `x \<notin> ?\<Delta>` have "?mems\<^sub>1i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>1 A'] x = ?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<and>
                               ?mems\<^sub>2i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>2 A'] x = ?mems\<^sub>2'i [\<mapsto> \<sigma>] x"
            using `A' x = None`
            unfolding differing_vars_def apply_adaptation_def
            by auto

          thus ?thesis
            by auto
        qed
      qed

      hence adapt_eq: 
            "?mems\<^sub>1i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>1 A'] = ?mems\<^sub>1'i [\<mapsto> \<sigma>] \<and>
             ?mems\<^sub>2i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>2 A'] = ?mems\<^sub>2'i [\<mapsto> \<sigma>]"
        by auto

      have "cms\<^sub>1' ! i = cms\<^sub>1 ! i"
        by (metis `i \<noteq> k` b nth_list_update_neq)

      have ugly_monster: " \<And>x x2 x1 x2a.
             A' x = Some (x1, x2a) \<Longrightarrow>
             x2 = (x1, x2a) \<Longrightarrow>
             subst \<sigma>'' (fst (mems ! i)) x \<noteq> x1 \<or> subst \<sigma>'' (snd (mems ! i)) x \<noteq> x2a \<Longrightarrow>
             \<not> var_asm_not_written ( (snd (cms\<^sub>1 ! i))) x"
      proof -
        fix x v v'
        assume A_updates_x\<^sub>1: "A' x = Some (v, v')"
           and A_updates_x\<^sub>2:"subst \<sigma>'' (fst (mems ! i)) x \<noteq> v \<or> subst \<sigma>'' (snd (mems ! i)) x \<noteq> v'"
        hence "x \<in> dom A'" by(blast)
        hence diff:
          "?mems\<^sub>1'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>1i [\<mapsto> \<sigma>''] x \<or> ?mems\<^sub>2'i [\<mapsto> \<sigma>] x \<noteq> ?mems\<^sub>2i [\<mapsto> \<sigma>''] x"
           by (auto simp: differing_vars_def domA)
        show "\<not> var_asm_not_written ( (snd (cms\<^sub>1 ! i))) x"
        proof (cases "x \<notin> ?X' i \<or> ((dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High) \<and> dma mem\<^sub>1' x = dma mem\<^sub>1 x)")
          assume "x \<notin> ?X' i \<or> ((dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High) \<and> (dma mem\<^sub>1' x = dma mem\<^sub>1 x))"
          from this p and diff show writable: "\<not> var_asm_not_written ( (snd (cms\<^sub>1 ! i))) x"
            by auto
        next
          assume "\<not> (x \<notin> ?X' i \<or> ((dma mem\<^sub>1 x = Low \<or> dma mem\<^sub>1' x = High) \<and> (dma mem\<^sub>1' x = dma mem\<^sub>1 x)))"
          hence "x \<in> ?X' i" "((dma mem\<^sub>1 x = High \<and> dma mem\<^sub>1' x = Low) \<or> (dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x))"
            by (metis Sec.exhaust)+
             thus ?thesis by(fastforce simp add: modifies_no_var_asm_not_written)
        qed
                    
      qed
          
      have reclas: 
        "(\<forall>x. dma ((subst \<sigma>'' (fst (mems ! i))) [\<parallel>\<^sub>1 A']) x \<noteq> dma (subst \<sigma>'' (fst (mems ! i))) x \<longrightarrow>
              \<not> var_asm_not_written ( (snd (cms\<^sub>1 ! i))) x)"
        apply(simp add: adapt_eq dma_eq dma_eq'')
        apply(simp add: modifies_no_var_asm_not_written)
        done

      have "snd (cms\<^sub>2 ! i) = snd (cms\<^sub>1 ! i)"
        by (metis `i < length cms\<^sub>1` equal_size envs_eq nth_map)

      hence low_mds_eq: "(subst \<sigma>'' (fst (mems ! i))) =\<^bsub> (snd (cms\<^sub>1 ! i))\<^esub>\<^sup>l (subst \<sigma>'' (snd (mems ! i)))"
        apply -
        apply(rule mm_equiv_low_eq[where c\<^sub>1="fst (cms\<^sub>1 ! i)" and c\<^sub>2="fst (cms\<^sub>2 ! i)"])
        using bisim
        by (metis prod.collapse)

      have "snd (cms\<^sub>2 ! k) = snd (cms\<^sub>1 ! k)"
        by (metis b equal_size envs_eq nth_map)

      hence low_mds_eq\<^sub>k: "(subst \<sigma>'\<^sub>k (fst (mems ! k))) =\<^bsub> (snd (cms\<^sub>1 ! k))\<^esub>\<^sup>l (subst \<sigma>'\<^sub>k (snd (mems ! k)))"
        apply -
        apply(rule mm_equiv_low_eq[where c\<^sub>1="fst (cms\<^sub>1 ! k)" and c\<^sub>2="fst (cms\<^sub>2 ! k)"])
        using bisim\<^sub>k
        by (metis prod.collapse)

      have eq: "\<forall>x. dma ((subst \<sigma>'' (fst (mems ! i))) [\<parallel>\<^sub>1 A']) x = Low \<and> 
                    (x \<in> (AsmNoRW (snd (cms\<^sub>1 ! i)))  \<longrightarrow> x \<in> \<C>) \<longrightarrow>
                    (subst \<sigma>'' (fst (mems ! i))) [\<parallel>\<^sub>1 A'] x = (subst \<sigma>'' (snd (mems ! i))) [\<parallel>\<^sub>2 A'] x"
        (* FIXME: clean up *)
        apply(clarsimp simp: adapt_eq dma_eq)
        apply(case_tac "x \<in> dom \<sigma>")
         apply(force simp: subst_def)
        apply(simp add: subst_not_in_dom)
        apply(simp add: dom\<sigma>)
        apply(clarsimp simp: differing_vars_lists_def differing_vars_def)
        apply(case_tac "i = k")
         apply(simp add: `i \<noteq> k`)
        apply(erule mems'_i_cases)
            apply(rule `i < length cms\<^sub>1'`[simplified len_unchanged])
           apply force
          apply fastforce
         apply clarsimp
        apply clarsimp

        apply(case_tac "x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i")
         apply(force simp: differing_vars_lists_def differing_vars_def)

        apply(insert low_mds_eq)[1]
        apply(simp add: low_mds_eq_def)
        apply(drule_tac x=x in spec)

        apply(subst (asm) makes_compatible_dma_eq)
           apply(rule compat)
          apply(rule `i < length cms\<^sub>1`)
         apply(rule `dom \<sigma>'' = differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i`)
        apply simp
        apply(subgoal_tac "x \<notin> dom \<sigma>''")
         apply(simp add: subst_not_in_dom)
         apply force
        apply(simp add: `dom \<sigma>'' = differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i`)+
        done
      
      have low_mds_eq': "low_mds_eq ( (snd (cms\<^sub>1' ! i))) ((fst (mems' ! i)) [\<mapsto> \<sigma>]) ((snd (mems' ! i)) [\<mapsto> \<sigma>])"
        using A_correct eq low_mds_eq_def
        using \<open>cms\<^sub>1' ! i = cms\<^sub>1 ! i\<close> adapt_eq by auto

      note low_mds_eq' eq reclas ugly_monster A_correct bisim adapt_eq
    }
    note i_facts = this

    with low_mds_eq\<^sub>k' have low_mds_eq': "\<And>i \<sigma>. dom \<sigma> = ?X' i \<Longrightarrow> i < length cms\<^sub>1' \<Longrightarrow> low_mds_eq ( (snd (cms\<^sub>1' ! i))) ((fst (mems' ! i)) [\<mapsto> \<sigma>]) ((snd (mems' ! i)) [\<mapsto> \<sigma>])"
      by blast

    show "makes_compatible (cms\<^sub>1', mem\<^sub>1') (cms\<^sub>2', mem\<^sub>2') mems'"
    proof
      have "length cms\<^sub>1' = length cms\<^sub>1"
        by (metis cms\<^sub>2'_def equal_size length_list_update new_length)
      then show "length cms\<^sub>1' = length cms\<^sub>2' \<and> length cms\<^sub>1' = length mems'"
        using compat new_length
        unfolding mems'_def
        by auto
    next
      fix i
      fix \<sigma> :: "'Var \<rightharpoonup> 'Val"
      let ?mems\<^sub>1'i = "fst (mems' ! i)"
      let ?mems\<^sub>2'i = "snd (mems' ! i)"
      assume i_le: "i < length cms\<^sub>1'"
      assume dom\<sigma>: "dom \<sigma> = ?X' i"
      show "(cms\<^sub>1' ! i, (fst (mems' ! i)) [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2' ! i, (snd (mems' ! i)) [\<mapsto> \<sigma>])"
      proof (cases "i = k")
        assume "i = k"
        thus ?thesis
          using mm_equiv\<^sub>k' i_le dom\<sigma> by blast
      next
        assume "i \<noteq> k"
        define \<sigma>'' where "\<sigma>'' \<equiv> \<sigma>' \<sigma> i"
        note \<sigma>''_def2 = \<sigma>''_def[unfolded \<sigma>'_def]
        define A' where "A' \<equiv> A \<sigma> i"
        note A'_def2 = A'_def[unfolded A_def, folded \<sigma>''_def]
        let ?mems\<^sub>1i = "fst (mems ! i)"
        let ?mems\<^sub>2i = "snd (mems ! i)"
        have A_correct': "globally_consistent A' (snd (cms\<^sub>1 ! i)) (?mems\<^sub>1i [\<mapsto> \<sigma>'']) (?mems\<^sub>2i [\<mapsto> \<sigma>''])"
          (* FIXME: clean up *)
          apply(clarsimp simp: globally_consistent_def)
          apply(rule conjI)
           apply(split option.split)
           apply(intro allI conjI)
            apply simp
           apply(intro allI impI)
           apply(split prod.split)
           apply(intro allI impI)
           apply(simp only:)

           (* do some mangling on the second goal, FIXME: remove *) 
           prefer 2
           apply(subst conj_commute)
           apply(subst conj_assoc)
           apply(rule conjI)
            defer
            apply(subst (2) conj_commute)
            prefer 2
            using i_facts(4)
            using A'_def \<open>i \<noteq> k\<close> \<sigma>''_def dom\<sigma> i_le
            using \<sigma>''_def2 A_def apply auto[1]
           apply(rule conjI)
            apply(simp add: \<sigma>''_def A'_def)
            using i_facts 
            using \<open>i \<noteq> k\<close> dom\<sigma> i_le apply blast
           apply(simp add: \<sigma>''_def A'_def)
           using i_facts 
           using \<open>i \<noteq> k\<close> dom\<sigma> i_le apply blast
        proof -
          have "{x. subst \<sigma>'' (fst (mems ! i)) x \<noteq> mem\<^sub>1 x} \<subseteq> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
            by(force simp: subst_def \<sigma>''_def2 split: option.splits if_splits dest: differing_vars_neg simp: differing_vars_neg)

          moreover have "{x. subst \<sigma>'' (snd (mems ! i)) x \<noteq> mem\<^sub>2 x} \<subseteq> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
            by(force simp: subst_def \<sigma>''_def2 split: option.splits if_splits dest: differing_vars_neg simp: differing_vars_neg)

          moreover
          have "\<And>x. x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i \<Longrightarrow> 
                     \<exists>j. j < length cms\<^sub>1' \<and> j \<noteq> i \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>1 ! j))  \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>2 ! j)) "
          using compatible_different_asm_no_read
          proof - 
            fix x :: 'Var
            assume "x \<in> differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
            then obtain j where jprops: "j < length cms\<^sub>1" "j \<noteq> i" "x \<in> (map (AsmNoRW \<circ> snd) cms\<^sub>1 ! j)"
              by (metis compat compatible_different_asm_no_read envs_eq i_le len_unchanged)
            then have "x \<in> AsmNoRW (snd (cms\<^sub>1 ! j)) \<and> x \<in> AsmNoRW (snd (cms\<^sub>2 ! j))"
              by (metis (no_types) b comp_apply envs_eq equal_size i_le length_list_update nth_map)
            thus "\<exists>j<length cms\<^sub>1'.
            j \<noteq> i \<and>
            x \<in> AsmNoRW (snd (cms\<^sub>1 ! j))  \<and>
            x \<in> AsmNoRW (snd (cms\<^sub>2 ! j))  "
              using jprops
              using len_unchanged by auto
          qed


          ultimately 
          have before:
            "\<And>x. subst \<sigma>'' (fst (mems ! i)) x \<noteq> mem\<^sub>1 x \<or> subst \<sigma>'' (snd (mems ! i)) x \<noteq> mem\<^sub>2 x \<Longrightarrow>
                     \<exists>j. j < length cms\<^sub>1' \<and> j \<noteq> i \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>1 ! j))  \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>2 ! j)) "
            by blast

          have "\<And>x. (subst \<sigma> (fst (mems' ! i))) x \<noteq> mem\<^sub>1' x \<Longrightarrow> x \<in> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i"
          proof -
            fix x 
            assume diff: "(subst \<sigma> (fst (mems' ! i))) x \<noteq> mem\<^sub>1' x"
            show "?thesis x"
            proof(cases "x \<in> dom \<sigma>")
              assume "x \<in> dom \<sigma>"
              thus ?thesis using dom\<sigma> by blast
            next
              assume "x \<notin> dom \<sigma>"
              with diff show ?thesis 
                using subst_not_in_dom differing_vars_neg
                by metis
            qed
          qed

          moreover
          have "\<And>x. (subst \<sigma> (snd (mems' ! i))) x \<noteq> mem\<^sub>2' x \<Longrightarrow> x \<in> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i"
          proof -
            fix x 
            assume diff: "(subst \<sigma> (snd (mems' ! i))) x \<noteq> mem\<^sub>2' x"
            show "?thesis x"
            proof(cases "x \<in> dom \<sigma>")
              assume "x \<in> dom \<sigma>"
              thus ?thesis using dom\<sigma> by blast
            next
              assume "x \<notin> dom \<sigma>"
              with diff show ?thesis 
                using subst_not_in_dom differing_vars_neg
                by metis
            qed
          qed

          moreover
          have "\<And>x. x \<in> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i \<Longrightarrow>
                     \<exists>j. j < length cms\<^sub>1' \<and> j \<noteq> i \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>1' ! j))  \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>2' ! j)) "
          proof -
            fix x
            let ?X = "differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i"
            assume xin: "x \<in> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i"
            hence x_prop: "dma mem\<^sub>1' x = Low \<and> x \<notin> \<C> \<and> mem\<^sub>1' x \<noteq> mem\<^sub>2' x"
              using i_le not_diff' 
              using Sec.exhaust by blast
            with xin a and ex_not_diff' i_le obtain j where
              jprop: "j < length cms\<^sub>1' \<and> x \<notin> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' j"
              by fastforce

            let ?X\<^sub>j = "differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' j"
            obtain \<sigma> :: "'Var \<rightharpoonup> 'Val" where dom\<sigma>: "dom \<sigma> = ?X\<^sub>j"
            proof
              let ?\<sigma> = "\<lambda> x. if (x \<in> ?X\<^sub>j) then Some some_val else None"
              show "dom ?\<sigma> = ?X\<^sub>j" unfolding dom_def by auto
            qed
            let ?mdss = "map  snd cms\<^sub>1'" and
            ?mems\<^sub>1j = "fst (mems' ! j)" and
            ?mems\<^sub>2j = "snd (mems' ! j)"

            from jprop dom\<sigma> have subst_eq:
            "?mems\<^sub>1j [\<mapsto> \<sigma>] x = ?mems\<^sub>1j x \<and> ?mems\<^sub>2j [\<mapsto> \<sigma>] x = ?mems\<^sub>2j x"
              by (metis subst_not_in_dom)

            (* FIXME: clean up *)
            have dma_eq: "dma (?mems\<^sub>1j [\<mapsto> \<sigma>]) = dma mem\<^sub>1'"
              apply(rule dma_\<C>)
              apply(rule ballI)
              apply(case_tac "x \<in> ?X' j")
               apply(drule not_control[rotated])
                apply (metis jprop)
               apply blast
              apply(subst subst_not_in_dom)
               apply(simp add: dom\<sigma>)
              apply(simp add: differing_vars_lists_def differing_vars_def)
              done

            have low_eq: "?mems\<^sub>1j [\<mapsto> \<sigma>] =\<^bsub>?mdss ! j\<^esub>\<^sup>l ?mems\<^sub>2j [\<mapsto> \<sigma>]"
              using low_mds_eq' dom\<sigma> i_le 
              using comp_eq_dest_lhs jprop nth_map by auto
            with jprop and b have x_nrw: "x \<in> AsmNoRW (?mdss ! j)"
            proof -
              { assume "x \<notin> AsmNoRW (?mdss ! j)"
              then have mems_eq: "?mems\<^sub>1j x = ?mems\<^sub>2j x"
                using x_prop low_eq subst_eq dma_eq
                using low_mds_eq_def by auto

              hence "mem\<^sub>1' x = mem\<^sub>2' x"
                by (metis compat_different_vars jprop)

              hence False 
                using x_prop by blast
              }
              thus ?thesis by metis
            qed
            from x_prop xin jprop have jnei: "j \<noteq> i" by auto
            thus " \<exists>j<length cms\<^sub>1'.
            j \<noteq> i \<and>
            x \<in> AsmNoRW (snd (cms\<^sub>1' ! j)) \<and>
            x \<in> AsmNoRW (snd (cms\<^sub>2' ! j)) "
              using cms\<^sub>2'_def x_nrw  
              by (metis (no_types, lifting) \<open>map snd cms\<^sub>1' = map snd cms\<^sub>2'\<close> jprop new_length nth_map)
          qed

          moreover have "\<And>j. j \<noteq> k \<Longrightarrow> snd (cms\<^sub>1' ! j) = snd (cms\<^sub>1 ! j) \<and> snd (cms\<^sub>2' ! j) = snd (cms\<^sub>2 ! j)"
            using eval `i \<noteq> k` cms\<^sub>2'_def
            using b nth_list_update_neq by auto

          ultimately
          have after:
            "\<And>x. subst \<sigma> (fst (mems' ! i)) x \<noteq> mem\<^sub>1' x \<or> subst \<sigma> (snd (mems' ! i)) x \<noteq> mem\<^sub>2' x \<Longrightarrow>
                     (\<exists>j. j < length cms\<^sub>1' \<and> j \<noteq> i  \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>1' ! j))  \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>2' ! j)) )"
            by fastforce
          hence after':
            "\<And>x. (subst \<sigma>'' (fst (mems ! i))) [\<parallel>\<^sub>1 A'] x \<noteq> mem\<^sub>1' x \<or> 
                  (subst \<sigma>'' (snd (mems ! i))) [\<parallel>\<^sub>2 A'] x \<noteq> mem\<^sub>2' x \<Longrightarrow>
                     (\<exists>j. j < length cms\<^sub>1' \<and> j \<noteq> i \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>1' ! j))  \<and>
                       x \<in> AsmNoRW (snd (cms\<^sub>2' ! j)) )"
            by (fastforce simp: A'_def \<open>i \<noteq> k\<close> \<sigma>''_def dom\<sigma> i_facts(7) i_le)

          show "\<forall>R\<in>Relies (snd (cms\<^sub>1 ! i)) .
                  (subst \<sigma>'' (fst (mems ! i)), (subst \<sigma>'' (fst (mems ! i))) [\<parallel>\<^sub>1 A']) \<in> R \<and>
                  (subst \<sigma>'' (snd (mems ! i)), (subst \<sigma>'' (snd (mems ! i))) [\<parallel>\<^sub>2 A']) \<in> R"
          proof
            fix R
            assume Rasm: "R \<in> Relies (snd (cms\<^sub>1 ! i)) "
            have inR: "(mem\<^sub>1,mem\<^sub>1') \<in> R \<and> (mem\<^sub>2,mem\<^sub>2') \<in> R" 
            proof -
              from sound_envs
              have "globally_sound_env_use (cms\<^sub>1, mem\<^sub>1) \<and> globally_sound_env_use (cms\<^sub>2, mem\<^sub>2)"
                unfolding sound_env_use_def by blast
              hence "compatible_envs (map snd cms\<^sub>1) \<and> compatible_envs (map snd cms\<^sub>2)"
                using globally_sound_envs_compatible by blast
              with `i \<noteq> k` Rasm have Rguar: "R \<supseteq> \<Inter> (Guars (snd (cms\<^sub>1 ! k)))"
                unfolding compatible_envs_def compatible_agrels_def
                using b i_le len_unchanged length_map map_update nth_list_update_eq update_nth_eq by auto
              from sound_envs
              have l: "locally_sound_env_use ((cms\<^sub>1 ! k),mem\<^sub>1) \<and> locally_sound_env_use ((cms\<^sub>2 ! k),mem\<^sub>2)"
                using b 
                by (simp add: equal_size list_all_length)
              hence "respects_guarantee_rel (fst (cms\<^sub>1 ! k)) (snd (cms\<^sub>1 ! k))  mem\<^sub>1 (\<Inter> (Guars (snd (cms\<^sub>1 ! k))) ) \<and> 
                     respects_guarantee_rel (fst (cms\<^sub>2 ! k)) (snd (cms\<^sub>2 ! k)) mem\<^sub>2 (\<Inter> (Guars (snd (cms\<^sub>1 ! k))) )"
                unfolding locally_sound_env_use_def using loc_reach.refl respects_guarantee_rel_Inter
                by (metis (no_types, lifting) \<open>snd (cms\<^sub>1 ! k) = snd (cms\<^sub>2 ! k)\<close> fst_conv snd_conv)
              with Rguar
              have  "respects_guarantee_rel (fst (cms\<^sub>1 ! k)) (snd (cms\<^sub>1 ! k)) mem\<^sub>1 R \<and> 
                     respects_guarantee_rel (fst (cms\<^sub>2 ! k)) (snd (cms\<^sub>2 ! k)) mem\<^sub>2 R"
                using respects_guarantee_rel_mono by blast
              thus ?thesis
                unfolding respects_guarantee_rel_def
                by (metis b i prod.collapse)
            qed

            moreover 
            have sc: "side_condition (cms\<^sub>1,mem\<^sub>1) \<and> side_condition (cms\<^sub>2,mem\<^sub>2)"
              using sound_envs unfolding sound_env_use_def globally_sound_env_use_def by blast

            hence ign: "rel_ignores_vars  R (not_readable_vars (map  snd cms\<^sub>1)) (not_readable_vars (map ( snd) cms\<^sub>1')) mem\<^sub>1 mem\<^sub>1'"
              unfolding side_condition_def
              by (metis Rasm eval i_le len_unchanged meval_sched.simps(1))

            from sc[THEN conjunct2] 
            have "rel_ignores_vars  R (not_readable_vars (map ( snd) cms\<^sub>2)) (not_readable_vars (map ( snd) cms\<^sub>2')) mem\<^sub>2 mem\<^sub>2'"
              unfolding side_condition_def using `(cms\<^sub>2, mem\<^sub>2) \<leadsto>\<^bsub>k\<^esub> (cms\<^sub>2', mem\<^sub>2')` Rasm
              by (metis (mono_tags) envs_eq equal_size i_le len_unchanged meval_sched.simps(1) nth_map)
            hence ign\<^sub>2: "rel_ignores_vars  R (not_readable_vars (map ( snd) cms\<^sub>1)) (not_readable_vars (map ( snd) cms\<^sub>1')) mem\<^sub>2 mem\<^sub>2'"
              by (metis \<open>map snd cms\<^sub>1' = map snd cms\<^sub>2'\<close> envs_eq)

            show "(subst \<sigma>'' (fst (mems ! i)), (subst \<sigma>'' (fst (mems ! i))) [\<parallel>\<^sub>1 A']) \<in> R \<and>
                  (subst \<sigma>'' (snd (mems ! i)), (subst \<sigma>'' (snd (mems ! i))) [\<parallel>\<^sub>2 A']) \<in> R"
              apply(rule conjI)
               apply(rule ign[unfolded rel_ignores_vars_def, rule_format, OF inR[THEN conjunct1], simplified, rule_format])
               apply(clarsimp simp: not_readable_vars_def)
               using before after' 
               apply (metis len_unchanged nth_mem)
              apply(rule ign\<^sub>2[unfolded rel_ignores_vars_def, rule_format, OF inR[THEN conjunct2], simplified, rule_format])
              apply(clarsimp simp: not_readable_vars_def)
              using before after'
              apply (metis len_unchanged nth_mem)
              done
          qed
        qed

        note gc = this
        have m: "snd (cms\<^sub>1 ! i) = snd (cms\<^sub>2 ! i)"
          by (metis (no_types, lifting) envs_eq equal_size i_le len_unchanged nth_map)

        from gc have "(cms\<^sub>1 ! i, ?mems\<^sub>1i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>1 A']) \<approx> (cms\<^sub>2 ! i, ?mems\<^sub>2i [\<mapsto> \<sigma>''] [\<parallel>\<^sub>2 A'])"
          apply (subst surjective_pairing[of "cms\<^sub>1 ! i"])
          apply (subst surjective_pairing[of "cms\<^sub>2 ! i"])
          apply (simp only: m)
           apply(rule globally_consistent_adapt_bisim)
           using i_facts(6) 
           apply (metis \<open>i \<noteq> k\<close> \<sigma>''_def dom\<sigma> i_le m prod.collapse)       
          by (simp add: m)
        thus ?thesis using i_facts(7) \<sigma>''_def
          by (metis (no_types, lifting) A'_def \<open>i \<noteq> k\<close> b cms\<^sub>2'_def dom\<sigma> i_le nth_list_update_neq)
      qed
    next
      show "\<And>i x. i < length cms\<^sub>1' \<Longrightarrow>
           mem\<^sub>1' x = mem\<^sub>2' x \<or> dma mem\<^sub>1' x = High \<or> x \<in> \<C> \<Longrightarrow>
           x \<notin> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i"
         by(rule not_diff')
    next
      show "length cms\<^sub>1' = 0 \<and> mem\<^sub>1' =\<^sup>l mem\<^sub>2' \<or>
      (\<forall>x. \<exists>i<length cms\<^sub>1'. x \<notin> differing_vars_lists mem\<^sub>1' mem\<^sub>2' mems' i)"
        by(rule ex_not_diff')
    qed
  qed

  ultimately show ?thesis using that by blast
qed

text \<open> The Isar proof language provides a readable
way of specifying assumptions while also giving them names for subsequent
usage. \<close>
lemma compat_low_eq:
  assumes compat: "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  assumes modes_eq: "map snd cms\<^sub>1 = map snd cms\<^sub>2"
  assumes x_low: "dma mem\<^sub>1 x = Low"
  assumes x_readable: "x \<in> \<C> \<or> (\<forall> i < length cms\<^sub>1. x \<notin> AsmNoRW (snd (cms\<^sub>1 ! i)) )"
  shows "mem\<^sub>1 x = mem\<^sub>2 x"
proof -
  let ?X = "\<lambda> i. differing_vars_lists mem\<^sub>1 mem\<^sub>2 mems i"
  from compat have "(length cms\<^sub>1 = 0 \<and> mem\<^sub>1 =\<^sup>l mem\<^sub>2) \<or>
                    (\<forall> x. \<exists> j. j < length cms\<^sub>1 \<and> x \<notin> ?X j)"
    by auto
  thus "mem\<^sub>1 x = mem\<^sub>2 x"
  proof
    assume "length cms\<^sub>1 = 0 \<and> mem\<^sub>1 =\<^sup>l mem\<^sub>2"
    with x_low show ?thesis
      by (simp add: low_eq_def)
  next
    assume "\<forall> x. \<exists> j. j < length cms\<^sub>1 \<and> x \<notin> ?X j"
    then obtain j where j_prop: "j < length cms\<^sub>1 \<and> x \<notin> ?X j"
      by auto
    let ?mems\<^sub>1j = "fst (mems ! j)" and
        ?mems\<^sub>2j = "snd (mems ! j)"

    obtain \<sigma> :: "'Var \<rightharpoonup> 'Val" where dom\<sigma>: "dom \<sigma> = ?X j"
      by (metis dom_restrict_total)

    from compat x_low makes_compatible_dma_eq j_prop `dom \<sigma> = ?X j`
    have x_low: "dma (?mems\<^sub>1j [\<mapsto> \<sigma>]) x = Low"
      by metis

    from dom\<sigma> compat and j_prop have "(cms\<^sub>1 ! j, ?mems\<^sub>1j [\<mapsto> \<sigma>]) \<approx> (cms\<^sub>2 ! j, ?mems\<^sub>2j [\<mapsto> \<sigma>])"
      by auto
    
    moreover
    have "snd (cms\<^sub>1 ! j) = snd (cms\<^sub>2 ! j)"
      using modes_eq j_prop length_map nth_map
      by (metis (no_types, hide_lams))

    ultimately have "?mems\<^sub>1j [\<mapsto> \<sigma>] =\<^bsub> (snd (cms\<^sub>1 ! j))\<^esub>\<^sup>l ?mems\<^sub>2j [\<mapsto> \<sigma>]"
      using j_prop
      by (metis mm_equiv_low_eq prod.collapse)
    hence "?mems\<^sub>1j x = ?mems\<^sub>2j x"
      using x_low x_readable j_prop `dom \<sigma> = ?X j`
      unfolding low_mds_eq_def
      by (metis subst_not_in_dom)

    thus ?thesis
      using j_prop
      by (metis compat_different_vars)
  qed
qed


lemma app_Cons_rewrite:
  "ns @ (a # ms) = ((ns @ [a]) @ ms)"
  apply simp
  done

  
lemma makes_compatible_eval_sched:
  assumes compat: "makes_compatible (cms\<^sub>1, mem\<^sub>1) (cms\<^sub>2, mem\<^sub>2) mems"
  assumes modes_eq: "map snd cms\<^sub>1 = map snd cms\<^sub>2"
  assumes sound_envs: "sound_env_use (cms\<^sub>1, mem\<^sub>1)" "sound_env_use (cms\<^sub>2, mem\<^sub>2)"
  assumes eval: "(cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>1', mem\<^sub>1')"
  shows "\<exists> cms\<^sub>2' mem\<^sub>2' mems'. sound_env_use (cms\<^sub>1', mem\<^sub>1') \<and>
                              sound_env_use (cms\<^sub>2', mem\<^sub>2') \<and>
                              map snd cms\<^sub>1' = map snd cms\<^sub>2' \<and>
                              (cms\<^sub>2, mem\<^sub>2) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>2', mem\<^sub>2') \<and>
                              makes_compatible (cms\<^sub>1', mem\<^sub>1') (cms\<^sub>2', mem\<^sub>2') mems'"
proof -
  (* cms\<^sub>1' and mem\<^sub>1' need to be arbitrary so
     that the induction hypothesis is sufficiently general. *)
  from eval show ?thesis
  proof (induct "sched" arbitrary: cms\<^sub>1' mem\<^sub>1' rule: rev_induct)
    case Nil
    hence "cms\<^sub>1' = cms\<^sub>1 \<and> mem\<^sub>1' = mem\<^sub>1"
      by (simp add: Pair_inject meval_sched.simps(1))
    thus ?case
      by (metis compat meval_sched.simps(1) modes_eq sound_envs)
  next
    case (snoc n ns)
    then obtain cms\<^sub>1'' mem\<^sub>1'' where eval'':
      "(cms\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>ns\<^esub> (cms\<^sub>1'', mem\<^sub>1'') \<and> (cms\<^sub>1'', mem\<^sub>1'') \<leadsto>\<^bsub>n\<^esub> (cms\<^sub>1', mem\<^sub>1')"
      by (metis meval_sched_snocD prod_cases3 snd_conv)
    moreover
    from eval'' obtain cms\<^sub>2'' mem\<^sub>2'' mems'' where IH:
      "sound_env_use (cms\<^sub>1'', mem\<^sub>1'') \<and>
       sound_env_use (cms\<^sub>2'', mem\<^sub>2'') \<and>
       map snd cms\<^sub>1'' = map snd cms\<^sub>2'' \<and>
       (cms\<^sub>2, mem\<^sub>2) \<rightarrow>\<^bsub>ns\<^esub> (cms\<^sub>2'', mem\<^sub>2'') \<and>
       makes_compatible (cms\<^sub>1'', mem\<^sub>1'') (cms\<^sub>2'', mem\<^sub>2'') mems''"
      using snoc
      by metis
    ultimately obtain cms\<^sub>2' mem\<^sub>2' mems' where
      "map snd cms\<^sub>1' = map snd cms\<^sub>2' \<and>
       (cms\<^sub>2'', mem\<^sub>2'') \<leadsto>\<^bsub>n\<^esub> (cms\<^sub>2', mem\<^sub>2') \<and>
       makes_compatible (cms\<^sub>1', mem\<^sub>1') (cms\<^sub>2', mem\<^sub>2') mems'"
      using makes_compatible_invariant
      by blast
    thus ?case
      using IH eval'' meval_sched_snocI sound_modes_invariant
      by metis
  qed
qed

lemma differing_vars_initially_empty:
  "i < n \<Longrightarrow> x \<notin> differing_vars_lists mem\<^sub>1 mem\<^sub>2 (zip (replicate n mem\<^sub>1) (replicate n mem\<^sub>2)) i"
  unfolding differing_vars_lists_def differing_vars_def
  by auto

lemma compatible_refl:
  assumes coms_secure: "list_all com_sifum_secure cmds"
  assumes low_eq: "mem\<^sub>1 =\<^sup>l mem\<^sub>2"
  assumes INIT: "INIT mem\<^sub>1 \<and> INIT mem\<^sub>2"
  shows "makes_compatible (cmds, mem\<^sub>1)
                          (cmds, mem\<^sub>2)
                          (replicate (length cmds) (mem\<^sub>1, mem\<^sub>2))"
proof -
  let ?n = "length cmds"
  let ?mems = "replicate ?n (mem\<^sub>1, mem\<^sub>2)" and
      ?mdss = "map snd cmds"
  let ?X = "differing_vars_lists mem\<^sub>1 mem\<^sub>2 ?mems"
  have diff_empty: "\<forall> i < ?n. ?X i = {}"
    by (metis differing_vars_initially_empty ex_in_conv min.idem zip_replicate)

  show ?thesis
  proof
    show "length cmds = length cmds \<and> length cmds = length ?mems"
      by auto
  next
    fix i and \<sigma> :: "'Var \<Rightarrow> 'Val option"
    let ?mems\<^sub>1i = "fst (?mems ! i)" and ?mems\<^sub>2i = "snd (?mems ! i)"
    let ?mdss\<^sub>i = "?mdss ! i"
    assume i: "i < length cmds"
    assume dom\<sigma>: "dom \<sigma> =
                  differing_vars_lists mem\<^sub>1 mem\<^sub>2
                                      (replicate (length cmds) (mem\<^sub>1, mem\<^sub>2)) i"
    from i have "?mems\<^sub>1i = mem\<^sub>1" "?mems\<^sub>2i = mem\<^sub>2"
      by auto

    with dom\<sigma> have [simp]: "dom \<sigma> = {}" by(auto simp: differing_vars_lists_def differing_vars_def i)

    from i coms_secure have "com_sifum_secure (cmds ! i)"
      using coms_secure
      by (metis list_all_length)
    with i have "\<And> mem\<^sub>1 mem\<^sub>2. mem\<^sub>1 =\<^bsub> ?mdss\<^sub>i\<^esub>\<^sup>l mem\<^sub>2 \<Longrightarrow> INIT mem\<^sub>1 \<Longrightarrow> INIT mem\<^sub>2 \<Longrightarrow>
      (cmds ! i, mem\<^sub>1) \<approx> (cmds ! i, mem\<^sub>2)"
      using com_sifum_secure_def low_indistinguishable_def
      by auto

    moreover
    have "\<And>x. \<sigma> x = None" using `dom \<sigma> = {}`
      by (metis domIff empty_iff)
    hence [simp]: "\<And> mem. mem [\<mapsto> \<sigma>] = mem"
      by(simp add: subst_def)

    from i have "?mems\<^sub>1i = mem\<^sub>1" "?mems\<^sub>2i = mem\<^sub>2"
      by auto
    with low_eq have "?mems\<^sub>1i [\<mapsto> \<sigma>] =\<^bsub> ?mdss\<^sub>i\<^esub>\<^sup>l ?mems\<^sub>2i [\<mapsto> \<sigma>] \<and> INIT ?mems\<^sub>1i \<and> INIT ?mems\<^sub>2i"
      by (auto simp: low_mds_eq_def low_eq_def INIT)

    ultimately show "(cmds ! i, ?mems\<^sub>1i [\<mapsto> \<sigma>]) \<approx> (cmds ! i, ?mems\<^sub>2i [\<mapsto> \<sigma>])"
      by simp
  next
    fix i x
    assume "i < length cmds"
    with diff_empty show "x \<notin> ?X i" by auto
  next
    show "(length cmds = 0 \<and> mem\<^sub>1 =\<^sup>l mem\<^sub>2) \<or> (\<forall> x. \<exists> i < length cmds. x \<notin> ?X i)"
      using diff_empty
      by (metis bot_less bot_nat_def empty_iff low_eq)
  qed
qed

theorem sifum_compositionality_cont:
  assumes com_secure: "list_all com_sifum_secure cmds"
  assumes sound_modes: "\<forall> mem. INIT mem \<longrightarrow> sound_env_use (cmds, mem)"
  shows "prog_sifum_secure_cont cmds"
  unfolding prog_sifum_secure_cont_def
  using assms
proof (clarify)
  fix mem\<^sub>1 mem\<^sub>2 :: "'Var \<Rightarrow> 'Val"
  fix sched cms\<^sub>1' mem\<^sub>1'
  let ?n = "length cmds"
  let ?mems = "zip (replicate ?n mem\<^sub>1) (replicate ?n mem\<^sub>2)"
  assume INIT\<^sub>1: "INIT mem\<^sub>1" and INIT\<^sub>2: "INIT mem\<^sub>2"
  assume low_eq: "mem\<^sub>1 =\<^sup>l mem\<^sub>2"
  with com_secure have compat:
    "makes_compatible (cmds, mem\<^sub>1) (cmds, mem\<^sub>2) ?mems"
    by (metis compatible_refl fst_conv length_replicate map_replicate snd_conv zip_eq_conv INIT\<^sub>1 INIT\<^sub>2)

  also assume eval: "(cmds, mem\<^sub>1) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>1', mem\<^sub>1')"

  ultimately obtain cms\<^sub>2' mem\<^sub>2' mems'
    where p: "map snd cms\<^sub>1' = map snd cms\<^sub>2' \<and>
             (cmds, mem\<^sub>2) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>2', mem\<^sub>2') \<and>
             makes_compatible (cms\<^sub>1', mem\<^sub>1') (cms\<^sub>2', mem\<^sub>2') mems'"
    using sound_modes makes_compatible_eval_sched INIT\<^sub>1 INIT\<^sub>2
    by blast

  thus "\<exists> cms\<^sub>2' mem\<^sub>2'. (cmds, mem\<^sub>2) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>2', mem\<^sub>2') \<and>
                        map snd cms\<^sub>1' = map snd cms\<^sub>2' \<and>
                        length cms\<^sub>2' = length cms\<^sub>1' \<and>
                        (\<forall> x. dma mem\<^sub>1' x = Low \<and> (x \<in> \<C> \<or> (\<forall> i < length cms\<^sub>1'. x \<notin> (AsmNoRW \<circ> snd) (cms\<^sub>1' ! i) ))
          \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x)"
    using p compat_low_eq
    by (metis (mono_tags, lifting) comp_eq_dest_lhs makes_compatible.simps)
qed

end

end
