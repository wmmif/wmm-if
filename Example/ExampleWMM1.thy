theory ExampleWMM1
  imports "../TypeSystem" 
begin

datatype addr = inmode | outmode | xdata | high | low  (* program variable in examples *)

definition 
   Globals :: "addr set" 
   where
    "Globals \<equiv> {xdata,high,low,inmode, outmode}"

definition
   Locals :: "addr set"
   where
    "Locals \<equiv> {}"

type_synonym mem = "(addr, nat) Mem"

(* Instruction set of language *)

datatype aexp = Load "addr" | Const "nat" | PlusOne aexp

fun
  ev\<^sub>A :: "mem \<Rightarrow> aexp \<Rightarrow> nat"
where
  "ev\<^sub>A mem (Load x) = mem x" |
  "ev\<^sub>A mem (Const c) = c" |
  "ev\<^sub>A mem (PlusOne a) = (ev\<^sub>A mem a + 1)"

fun
  aexp_vars :: "aexp \<Rightarrow> addr set"
where
  "aexp_vars (Load x) = {x}" |
  "aexp_vars (PlusOne a) = aexp_vars a" |
  "aexp_vars _ = {}"

lemma ev\<^sub>A_det:
  " \<forall>x\<in>aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>A mem\<^sub>1 e = ev\<^sub>A mem\<^sub>2 e"
  by (induct e ; auto)

primrec
  aexp_to_lexp :: "aexp \<Rightarrow> (addr,nat) lexp"
where
  "aexp_to_lexp (Load x) = (Var x)" |
  "aexp_to_lexp (Const x) = (lexp.Const x)" |
  "aexp_to_lexp (PlusOne a) = (lexp.LBinOp (+) (lexp.Const 1) (aexp_to_lexp a))"
  
fun
  aexp_replace :: "aexp \<Rightarrow> addr \<Rightarrow> aexp \<Rightarrow> aexp"
  where
  "aexp_replace (Load y) x c = (if (x = y) then c else (Load y))" |
  "aexp_replace (Const v) a c = Const v" |
  "aexp_replace (PlusOne v) a c = PlusOne (aexp_replace v a c)"

lemma aexp_to_lexp_vars':
  "lexp_vars (aexp_to_lexp e) = aexp_vars e"
  by (induct e ; auto)

lemma aexp_to_lexp_correct':
  "leval mem (aexp_to_lexp e) = ev\<^sub>A mem e"
  by (induct e ; auto)

lemma aexp_replace_correct':
  "aexp_to_lexp (aexp_replace e x f) = lexp_subst (aexp_to_lexp e) (aexp_to_lexp f) x"
  by (induct e; simp)

lemma aexp_replace_nop':
  "x \<notin> aexp_vars e \<Longrightarrow> aexp_replace e x f = e"
  by (induct e; simp)

(* Boolean expressions *)

datatype bexp = Eq "aexp" "aexp" |
                Neq "aexp" "aexp" |
                Or bexp bexp |
                And bexp bexp |
                TT | FF

fun
  bexp_replace :: "bexp \<Rightarrow> addr \<Rightarrow> aexp \<Rightarrow> bexp"
  where
  "bexp_replace (Eq a\<^sub>1 a\<^sub>2) z c = Eq (aexp_replace a\<^sub>1 z c) (aexp_replace a\<^sub>2 z c)" |
  "bexp_replace (Neq a\<^sub>1 a\<^sub>2) z c = Neq (aexp_replace a\<^sub>1 z c) (aexp_replace a\<^sub>2 z c)" |
  "bexp_replace (Or b\<^sub>1 b\<^sub>2) z c = Or (bexp_replace b\<^sub>1 z c) (bexp_replace b\<^sub>2 z c)" |
  "bexp_replace (And b\<^sub>1 b\<^sub>2) z c = And (bexp_replace b\<^sub>1 z c) (bexp_replace b\<^sub>2 z c)" |
  "bexp_replace i z c = i"

primrec
  bexp_to_lpred :: "bexp \<Rightarrow> (addr,nat) lpred"
  where
  "bexp_to_lpred (Eq a\<^sub>1 a\<^sub>2) = PCmp (=) (aexp_to_lexp a\<^sub>1) (aexp_to_lexp a\<^sub>2)" |
  "bexp_to_lpred (Neq a\<^sub>1 a\<^sub>2) = PNeg (PCmp (=) (aexp_to_lexp a\<^sub>1) (aexp_to_lexp a\<^sub>2))" |
  "bexp_to_lpred (Or a b) = (PDisj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred (And a b) = (PConj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred TT = PTrue" |
  "bexp_to_lpred FF = PFalse"

(* instantiates the eval_B function in sifum-lang-no-dma with own definition ev_B *)

fun
  ev\<^sub>B :: "mem \<Rightarrow> bexp \<Rightarrow> bool"
  where
  "ev\<^sub>B mem (Eq a\<^sub>1 a\<^sub>2) = (ev\<^sub>A mem a\<^sub>1 = ev\<^sub>A mem a\<^sub>2)" |
  "ev\<^sub>B mem (Neq a\<^sub>1 a\<^sub>2) = (ev\<^sub>A mem a\<^sub>1 \<noteq> ev\<^sub>A mem a\<^sub>2)" |
  "ev\<^sub>B mem (Or x y) = (ev\<^sub>B mem x \<or> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem (And x y) = (ev\<^sub>B mem x \<and> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem TT = True" |
  "ev\<^sub>B mem FF = False"

fun
  bexp_vars :: "bexp \<Rightarrow> addr set"
where
  "bexp_vars (Neq x c) = aexp_vars x \<union> aexp_vars c" |
  "bexp_vars (Eq x c) = aexp_vars x \<union> aexp_vars c"  |
  "bexp_vars (Or x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars (And x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars _ = {}"

lemma ev\<^sub>B_det:
  "\<forall> x\<in> bexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>B mem\<^sub>1 e = ev\<^sub>B mem\<^sub>2 e"
  by (induct e; simp; metis ev\<^sub>A_det in_mono sup.cobounded1 sup.cobounded2)

lemma bexp_to_lpred_vars':
  "lpred_vars (bexp_to_lpred e) = bexp_vars e"
  by (induct e; simp add: aexp_to_lexp_vars')

lemma bexp_to_lpred_correct':
  "lpred_eval mem (bexp_to_lpred e) = ev\<^sub>B mem e"
  by(induct e; simp add: aexp_to_lexp_correct')

lemma bexp_replace_correct':
  "bexp_to_lpred (bexp_replace b x f) = lpred_subst (bexp_to_lpred b) (aexp_to_lexp f) x"
  by (induct b; simp add: aexp_replace_correct')

lemma bexp_replace_nop':
  "x \<notin> bexp_vars b \<longrightarrow> bexp_replace b x f = b"
  by (induct b; simp add: aexp_replace_nop')

fun
  bexp_neg :: "bexp \<Rightarrow> bexp"
where
  "bexp_neg (Neq x y) = (Eq x y)" |
  "bexp_neg (Eq x y) = (Neq x y)" |
  "bexp_neg (Or x y) = And (bexp_neg x) (bexp_neg y)" |
  "bexp_neg (And x y) = Or (bexp_neg x) (bexp_neg y)" |
  "bexp_neg TT = FF" |
  "bexp_neg FF = TT"

lemma bexp_neg_negates':
  "ev\<^sub>B mem (bexp_neg e) = (\<not> ev\<^sub>B mem e)"
  by (induct e rule:ev\<^sub>B.induct, simp+)

lemma bexp_neg_vars':
  "bexp_vars (bexp_neg e) = bexp_vars e"
  by (induct e, simp+)

(* domain assignments, i.e., L function *)
(* Check: type system has both versions (dma and dma_types) and an axiom that both coincide? *)

definition
  dma :: "mem \<Rightarrow> addr \<Rightarrow> Sec"
where                                                 (* this def is example specific def of L(x) *)
  "dma m x \<equiv> (if x = xdata then (if (m) inmode = 0 then Low else High)
                  else if x = high then High
                  else Low)"

(* Variables and Control variables *)

definition
  \<C>_vars :: "addr \<Rightarrow> addr set"
where
  "\<C>_vars x \<equiv>  if x = xdata then {inmode}                   
                   else {}"

abbreviation
  \<C> :: "addr set"
where
  "\<C> \<equiv> \<Union>x. \<C>_vars x"

lemma UNIV[simp]:                  (* all variables *)
  "UNIV = {inmode, outmode, xdata, high, low}"
  apply(clarsimp | safe)+
   apply (metis addr.exhaust)
  by auto

instance addr :: finite
  by standard simp

(* linear order of addresses *)

definition 
  addr_ORDERING :: "addr list"
where
  "addr_ORDERING \<equiv> [ xdata, inmode, outmode, high, low ]"

primrec 
  index_of :: "'a \<Rightarrow> 'a list \<Rightarrow> nat option"
where
  "index_of x [] = None" |
  "index_of x (y#ys) = (if x = y then Some 0 else map_option Suc (index_of x ys))"

lemma addr_ORDERING_complete:
  "\<exists>i. index_of a addr_ORDERING = Some i"
  by (case_tac a, (auto simp: addr_ORDERING_def))

lemma index_of_inject:
  "index_of a xs = Some i \<Longrightarrow> index_of b xs = Some i \<Longrightarrow> a = b"
  by (induct xs arbitrary: i, auto split: if_splits)

instantiation addr :: linorder
begin

definition
  less_eq_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_eq_addr a b \<equiv> a = b \<or> the (index_of a addr_ORDERING) < the (index_of b addr_ORDERING)"

definition
  less_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_addr a b \<equiv> the (index_of a addr_ORDERING) < the (index_of b addr_ORDERING)"

instance 
  apply(intro_classes)
      apply(clarsimp simp: less_addr_def less_eq_addr_def | safe)+
  using addr_ORDERING_complete index_of_inject
  by (metis option.sel)

fun                                            (* L(x) = low iff bexp ; example specific def *)
  dma_type :: "addr \<Rightarrow> bexp"
where
  "dma_type xdata = (Eq (Load inmode) (Const 0))" |
  "dma_type high = FF" |
  "dma_type _ = TT"

definition myINIT :: "_ \<Rightarrow> bool"
  where
  "myINIT mem \<equiv> (mem inmode = mem outmode \<and> mem outmode = 0)"  

definition P\<^sub>0 :: "(addr,nat) lpred"
  where
   "P\<^sub>0 \<equiv> PTrue"

definition \<Gamma>\<^sub>0 :: "addr \<rightharpoonup> Sec"
  where
   "\<Gamma>\<^sub>0 \<equiv> \<lambda>x. if x = outmode then Some Low else None" 

definition mds\<^sub>0
  where
  "mds\<^sub>0 \<equiv> \<lambda>m. case m of GuarNoWrite \<Rightarrow> {}
                      | GuarNoReadOrWrite \<Rightarrow> {}
                      | AsmNoReadOrWrite \<Rightarrow> {}
                      | AsmNoWrite \<Rightarrow> {inmode, outmode}"

lemma \<Gamma>\<^sub>0_dom:
  "dom \<Gamma>\<^sub>0 = { outmode }"
  unfolding \<Gamma>\<^sub>0_def by (auto split: if_splits)

lemma valid_\<Gamma>\<^sub>0_dom': 
  "dom \<Gamma>\<^sub>0 = (mds\<^sub>0 AsmNoReadOrWrite \<union> mds\<^sub>0 AsmNoWrite) - \<C>"
  unfolding mds\<^sub>0_def \<C>_vars_def by (auto simp: \<Gamma>\<^sub>0_dom)

lemma valid_\<Gamma>\<^sub>0': 
  "\<forall>x \<in> dom \<Gamma>\<^sub>0. 
    (the (\<Gamma>\<^sub>0 x) = Low \<longrightarrow> (myINIT mem\<^sub>1 \<and> myINIT mem\<^sub>2 \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x)) \<and>
    (the (\<Gamma>\<^sub>0 x) > dma mem\<^sub>1 x \<longrightarrow> x \<in> mds\<^sub>0 AsmNoReadOrWrite)"
  unfolding myINIT_def \<Gamma>\<^sub>0_dom mds\<^sub>0_def \<Gamma>\<^sub>0_def
  using domIff less_Sec_def by auto

end


lemma t2:
  "dma mem x = (if ev\<^sub>B mem (dma_type x) then Low else High)"
  by (cases x; simp add: dma_def)

lemma t3:
  "\<C>_vars x = bexp_vars (dma_type x)"
  by (cases x; simp add: \<C>_vars_def)

(* interpretation of a sifum_type with the example-specific instances for the parameters *)

interpretation sifum_types Globals Locals undefined myINIT ev\<^sub>A ev\<^sub>B aexp_vars bexp_vars 
                            aexp_replace bexp_replace aexp_to_lexp bexp_to_lpred bexp_neg dma \<C>_vars \<C> dma_type P\<^sub>0 \<Gamma>\<^sub>0 mds\<^sub>0
   apply(unfold_locales)
                      apply (auto simp add: Globals_def Locals_def)[1] 
                      apply (simp add: ev\<^sub>A_det)
                      apply (simp add: ev\<^sub>B_det)
                      apply (simp add: aexp_to_lexp_correct')
                      apply (simp add: bexp_to_lpred_correct')
                     apply (simp add: aexp_to_lexp_vars')
                    apply (simp add: bexp_to_lpred_vars')
                   apply (simp add: bexp_neg_negates')
                  apply (simp add: bexp_neg_vars')
                 apply (simp add: aexp_replace_correct')
                apply (simp add: bexp_replace_correct')
               apply (simp add: aexp_replace_nop')
              apply (simp add: bexp_replace_nop')
             apply (simp add: \<C>_vars_def)+
           apply (simp add: dma_def)
          apply (simp add: \<C>_vars_def dma_def)
         apply (simp add: t2)
        apply (simp add: t3)
       apply (simp add: P\<^sub>0_def)
      apply (simp add: P\<^sub>0_def mds\<^sub>0_def)
     apply (simp add: P\<^sub>0_def mds\<^sub>0_def Locals_def )
    apply (simp add: valid_\<Gamma>\<^sub>0_dom')
   apply (simp add: valid_\<Gamma>\<^sub>0')
  by (auto simp: P\<^sub>0_def mds\<^sub>0_def Locals_def)

(* simplifications to simp rule *)

declare Collect_conj_eq [simp]
declare Collect_disj_eq [simp]
declare subtype_def [simp]
declare pred_entailment_def [simp]
declare pred_def [simp]
declare restrict_map_def [simp]
declare context_equiv_def [simp]
declare tyenv_wellformed_def [simp]
declare to_total_def [simp]
declare \<C>_vars_def [simp]
declare \<S>\<^sub>0_def [simp]
declare \<Gamma>\<^sub>0_def [simp]

lemma if_fun_disj[simp]:
  "(\<lambda>  x. if x = a | P x then (f x) else g x)
    = ((\<lambda> x. if P x then (f x) else g x)(a := f a))"
  "(\<lambda> x. if x = a then (f x) else g x)
    = (g(a := f a))"
  by auto

lemma if_fun_conj[simp]:
  "(\<lambda>x. if x = a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a then (g x) else f x))"
  by auto


text \<open>
       Example program: Loading a shared variable to low or high 
         if inmode=0
            then  low:=xdata
            else  high:=xdata \<close>

definition
  simple_load :: "(addr, aexp, bexp) Stmt"
where
  "simple_load \<equiv>
     If (Eq (Load inmode) (Const 0))
        ((Assign low (Load xdata)) ;;; Stop)
        ((Assign high (Load xdata)) ;;; Stop)"

(* helper lemmas ----------*)

lemma [simp]: "known\<^sub>w (update D\<^sub>0 ([Eq (Load inmode) (Const 0)?])) (low \<leftarrow> (Load xdata)) = UNIV"
  by (simp add: known_def update_def D_kill_def D_gen_def D\<^sub>0_def fromReads_def fromWrites_def) 

lemma [simp]: "lpred_vars P\<^sub>0 = {}"
  by (simp add: P\<^sub>0_def)

lemma [simp]: "stable (Stable (mds\<^sub>0 AsmNoWrite) (mds\<^sub>0 AsmNoReadOrWrite)) = {inmode, outmode}"
  by (simp add: stable_def mds\<^sub>0_def)

lemma [simp]: "pred (sec Low) mem" 
  by (simp add: sec_def)

lemma simple_load_typed:
  "\<exists>\<Gamma>' \<S>' P' D'. has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 D\<^sub>0 simple_load \<Gamma>' \<S>' P' D'"
  unfolding simple_load_def If_def \<S>\<^sub>0_def (* Expand program *)
  apply(intro if_type_weak exI act_type)  (* Break into Actions and Stop *)
       apply (rule guard_type; simp)          (* Positive Guard  *)
      apply (rule action_type_unstable; simp) (* Positive Assign *)
     apply (rule stop_type; simp)             (* Positive Stop   *)
    apply (rule guard_type; simp)             (* Negative Guard  *)
   apply (rule action_type_unstable; simp)    (* Negative Assign *)
  by (rule stop_type; simp)                   (* Negative Stop   *)

(* Compositionality proof *)

theorem ExampleWMM1_sifum_secure_cont:
  "prog_sifum_secure_cont [((simple_load, []) , env\<^sub>0)]" (is "prog_sifum_secure_cont ?cs")
proof (intro type_soundness_global type_global.intros)
  show "\<forall>((c, d), env)\<in>set ?cs. \<exists>\<Gamma>' \<S>' P' D'. has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 D\<^sub>0 c \<Gamma>' \<S>' P' D'"
    using simple_load_typed by auto
next
  show "\<forall>((c, d), env)\<in>set ?cs. env = env\<^sub>0" by auto
next
  show "\<forall>mem. myINIT mem \<longrightarrow> global_invariant mem"
    unfolding global_invariant_def by auto
next
  show "\<forall>mem. myINIT mem \<longrightarrow> sound_env_use (?cs, mem)"
    sorry (* Beyond the scope of this work *)
qed

end
