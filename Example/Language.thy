theory Language
  imports "../TypeSystem" 
begin

(* Arithmetic expressions *)
datatype ('Addr) aexp = 
                Load "'Addr" | 
                Const "nat" | 
                Add "('Addr) aexp" "('Addr) aexp" | 
                Mult "('Addr) aexp" "('Addr) aexp"

(* Boolean expressions *)
datatype ('Addr) bexp = 
                Eq "('Addr) aexp" "('Addr) aexp" |
                Neg "('Addr) bexp" |
                Or "('Addr) bexp" "('Addr) bexp" |
                And "('Addr) bexp" "('Addr) bexp" |
                Ex "nat \<Rightarrow> ('Addr) bexp" |
                TT | FF

locale addresses =
  fixes addrs :: "'Addr list"
  assumes finite_addr: "finite (set addrs)"
  assumes complete: "set addrs = UNIV"
(*  assumes distinct_addr: "distinct addrs"  *)
begin

lemma addr_not_empty: 
  "set addrs \<noteq> {}" 
  by (simp add: complete)

primrec 
  index_of :: "'Addr \<Rightarrow> 'Addr list \<Rightarrow> nat option"
where
  "index_of x [] = None" |
  "index_of x (y#ys) = (if x = y then Some 0 else map_option Suc (index_of x ys))"


lemma index_of_inject:
  "index_of a xs = Some i \<Longrightarrow> index_of b xs = Some i \<Longrightarrow> a = b" 
  apply(induct xs arbitrary: i, auto split: if_splits)
  done


(* Instruction set of language *)

fun
  ev\<^sub>A :: "('Addr \<Rightarrow> nat) \<Rightarrow> 'Addr aexp \<Rightarrow> nat"
where
  "ev\<^sub>A mem (Load v) = mem v" |
  "ev\<^sub>A mem (Const c) = c" |
  "ev\<^sub>A mem (Add a b) = (ev\<^sub>A mem a + ev\<^sub>A mem b)" |
  "ev\<^sub>A mem (Mult a b) = (ev\<^sub>A mem a * ev\<^sub>A mem b)"

fun
  aexp_vars :: "'Addr aexp \<Rightarrow> 'Addr set"
where
  "aexp_vars (Load v) = {v}" |
  "aexp_vars (Add a b) = aexp_vars a \<union> aexp_vars b" |
  "aexp_vars (Mult a b) = aexp_vars a \<union> aexp_vars b" |
  "aexp_vars _ = {}"

lemma ev\<^sub>A_det:
  "\<forall>x\<in>aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>A mem\<^sub>1 e = ev\<^sub>A mem\<^sub>2 e"
  by (induct e ; auto)

primrec 
  aexp_to_lexp :: "'Addr aexp \<Rightarrow> ('Addr,nat) lexp"
where
  "aexp_to_lexp (Load v) = (Var v)" |
  "aexp_to_lexp (Const c) = (lexp.Const c)" |
  "aexp_to_lexp (Add a b) = (lexp.LBinOp (+) (aexp_to_lexp a) (aexp_to_lexp b))" |
  "aexp_to_lexp (Mult a b) = (lexp.LBinOp (*) (aexp_to_lexp a) (aexp_to_lexp b))"

fun
  aexp_replace :: "('Addr) aexp \<Rightarrow> 'Addr \<Rightarrow> ('Addr) aexp \<Rightarrow> ('Addr) aexp"
  where
  "aexp_replace (Load y) x c = (if (x = y) then c else (Load y))" |
  "aexp_replace (Const v) a c = Const v" |
  "aexp_replace (Add v v') a c = Add (aexp_replace v a c) (aexp_replace v' a c)" |
  "aexp_replace (Mult v v') a c = Mult (aexp_replace v a c) (aexp_replace v' a c)"

lemma aexp_to_lexp_vars':
  "lexp_vars (aexp_to_lexp e) = aexp_vars e" 
  by (induct e, auto)  

lemma aexp_to_lexp_correct':
  "leval mem (aexp_to_lexp e) = ev\<^sub>A mem e"  
  by (induct e, auto)  

lemma aexp_replace_correct':
  "aexp_to_lexp (aexp_replace e x f) = lexp_subst (aexp_to_lexp e) (aexp_to_lexp f) x"
  by (induct e; simp)

lemma aexp_replace_nop':
  "x \<notin> aexp_vars e \<Longrightarrow> aexp_replace e x f = e"
  by (induct e; simp)


primrec
  bexp_replace :: "('Addr) bexp \<Rightarrow> 'Addr \<Rightarrow> ('Addr) aexp \<Rightarrow> ('Addr) bexp"
  where
  "bexp_replace (Eq a\<^sub>1 a\<^sub>2) z c = Eq (aexp_replace a\<^sub>1 z c) (aexp_replace a\<^sub>2 z c)" |
  "bexp_replace (Neg a\<^sub>1) z c = Neg (bexp_replace a\<^sub>1 z c)" |
  "bexp_replace (Or b\<^sub>1 b\<^sub>2) z c = Or (bexp_replace b\<^sub>1 z c) (bexp_replace b\<^sub>2 z c)" |
  "bexp_replace (And b\<^sub>1 b\<^sub>2) z c = And (bexp_replace b\<^sub>1 z c) (bexp_replace b\<^sub>2 z c)" |
  "bexp_replace (bexp.Ex v) z c = bexp.Ex (\<lambda>x. (bexp_replace (v x) z c))" |
  "bexp_replace TT b c = TT" |
  "bexp_replace FF b c = FF"

primrec
  bexp_to_lpred :: "('Addr) bexp \<Rightarrow> ('Addr,nat) lpred"
  where
  "bexp_to_lpred (Eq a\<^sub>1 a\<^sub>2) = PCmp (=) (aexp_to_lexp a\<^sub>1) (aexp_to_lexp a\<^sub>2)" |
  "bexp_to_lpred (Neg a\<^sub>1) = PNeg (bexp_to_lpred a\<^sub>1)" |
  "bexp_to_lpred (Or a b) = (PDisj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred (And a b) = (PConj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred (bexp.Ex v) = PEx (\<lambda>x. (bexp_to_lpred (v x)))" |
  "bexp_to_lpred TT = PTrue" |
  "bexp_to_lpred FF = PFalse"

primrec
  ev\<^sub>B :: "('Addr \<Rightarrow> nat) \<Rightarrow> ('Addr) bexp \<Rightarrow> bool"
  where
  "ev\<^sub>B mem (Eq a\<^sub>1 a\<^sub>2) = (ev\<^sub>A mem a\<^sub>1 = ev\<^sub>A mem a\<^sub>2)" |
  "ev\<^sub>B mem (Neg a\<^sub>1) = (\<not> (ev\<^sub>B mem a\<^sub>1))" |
  "ev\<^sub>B mem (Or x y) = (ev\<^sub>B mem x \<or> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem (And x y) = (ev\<^sub>B mem x \<and> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem (Ex fe) = (\<exists>v. ev\<^sub>B mem (fe v))" | 
  "ev\<^sub>B mem TT = True" |
  "ev\<^sub>B mem FF = False"

primrec
  bexp_vars :: "('Addr) bexp \<Rightarrow> 'Addr set"
where
  "bexp_vars (Neg x) = bexp_vars x" |
  "bexp_vars (Eq x c) = aexp_vars x \<union> aexp_vars c"  |
  "bexp_vars (Or x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars (And x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars (Ex fe) = (\<Union>v. bexp_vars (fe v))" |
  "bexp_vars TT = {}" |
  "bexp_vars FF = {}"

lemma ev\<^sub>B_det:
  "\<forall> x\<in> bexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>B mem\<^sub>1 e = ev\<^sub>B mem\<^sub>2 e"
  by (induct e; simp; metis ev\<^sub>A_det in_mono sup.cobounded1 sup.cobounded2)

lemma bexp_to_lpred_vars':
  "lpred_vars (bexp_to_lpred e) = bexp_vars e"
  by (induct e; simp add: aexp_to_lexp_vars')

lemma bexp_to_lpred_correct':
  "lpred_eval mem (bexp_to_lpred e) = ev\<^sub>B mem e"
  by(induct e; simp add: aexp_to_lexp_correct')

lemma bexp_replace_correct':
  "bexp_to_lpred (bexp_replace b x f) = lpred_subst (bexp_to_lpred b) (aexp_to_lexp f) x"
  by (induct b; simp add: aexp_replace_correct')

lemma bexp_replace_nop':
  "x \<notin> bexp_vars b \<longrightarrow> bexp_replace b x f = b"
  by (induct b; simp add: aexp_replace_nop')

fun bexp_neg :: "('Addr) bexp \<Rightarrow> ('Addr) bexp"
  where "bexp_neg b = Neg b"

lemma bexp_neg_negates':
  "ev\<^sub>B mem (bexp_neg e) = (\<not> ev\<^sub>B mem e)"
  by auto

lemma bexp_neg_vars':
  "bexp_vars (bexp_neg e) = bexp_vars e"
  by auto

end

locale nat_lang = 
   addresses addr + 
   sifum_types globals locals some_val INIT ev\<^sub>A ev\<^sub>B 
               aexp_vars bexp_vars aexp_replace bexp_replace aexp_to_lexp bexp_to_lpred bexp_neg dma \<C>_vars \<C> dma_type P\<^sub>0 \<Gamma>\<^sub>0 mds\<^sub>0
    for addr :: "('Addr ::{linorder, finite}) list"
    and globals :: "'Addr set"
    and locals :: "'Addr set"
    and INIT :: "('Addr \<Rightarrow> nat) \<Rightarrow> bool"
    and some_val :: "nat"
    and dma :: "('Addr \<Rightarrow> nat) \<Rightarrow> 'Addr \<Rightarrow> Sec"
    and \<C>_vars :: "'Addr \<Rightarrow> 'Addr set"
    and \<C> :: "'Addr set"
    and dma_type :: "'Addr \<Rightarrow> 'Addr bexp" 
    and P\<^sub>0 :: "('Addr, nat) lpred"
    and \<Gamma>\<^sub>0 :: "'Addr \<Rightarrow> Sec option"
    and mds\<^sub>0 :: "(Mode \<Rightarrow> ('Addr) set)"

begin

declare Collect_conj_eq [simp]
declare Collect_disj_eq [simp]
declare subtype_def [simp]
declare pred_entailment_def [simp]
declare pred_def [simp]
declare restrict_map_def [simp]

declare context_equiv_def [simp]
declare tyenv_wellformed_def [simp]
declare stable_def [simp]
declare to_total_def [simp]

end (* end of locale nat_lang *)

end