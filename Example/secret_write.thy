theory secret_write
  imports "Language" 
begin

datatype addr = x | z | secret
type_synonym mem = "(addr, nat) Mem"

definition Addrs :: "addr list"
  where "Addrs \<equiv> [secret, x, z]"

lemma UNIV:
  "set Addrs = UNIV"
  apply(clarsimp | safe)+ 
  by (metis (full_types) Addrs_def addr.exhaust list.set_intros(1) list.set_intros(2)) 

lemma finite_Vars:
  "finite {x::addr. True}"
  by (metis List.finite_set UNIV UNIV_def)

interpretation addresses Addrs
  apply (unfold_locales) 
   apply simp 
  using UNIV 
  by (simp )

lemma addr_complete:
  "\<exists>i. index_of a Addrs = Some i" 
  apply(case_tac Addrs,  simp add: addr_not_empty) 
  using complete addr_not_empty Addrs_def apply blast 
proof -
  fix aa :: addr and list :: "addr list"
  assume "Addrs = aa # list"
  have "a = x \<or> a = z \<or> a = secret"
    by (meson addr.exhaust)
  then show ?thesis
    by (metis Addrs_def index_of.simps(2) option.simps(9))
qed

instance addr :: finite
  apply standard 
  using UNIV finite_addr by auto

(*-------------------------------------*)
instantiation addr :: linorder
begin

definition
  less_eq_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_eq_addr a b \<equiv> a = b \<or> the (index_of a Addrs) < the (index_of b Addrs)"

definition
  less_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_addr a b \<equiv> the (index_of a Addrs) < the (index_of b Addrs)"

instance 
  apply(intro_classes)
      apply(clarsimp simp: less_addr_def less_eq_addr_def | safe)+
  using addr_complete index_of_inject
  by (metis option.sel)
(*-------------------------------------*)

definition
   Globals :: "addr set" 
   where
    "Globals \<equiv> {secret, x, z}"

definition
   Locals :: "addr set"
   where
    "Locals \<equiv> {}"

fun
  dma_type :: "addr \<Rightarrow> addr bexp"
where
  "dma_type x = Ex (\<lambda>n. Eq (Load z) (Const (2 * n)))" |
  "dma_type secret = FF" |
  "dma_type _ = TT"

definition INIT :: "_ \<Rightarrow> bool"
  where
  "INIT mem \<equiv> (mem z = 0 \<and> mem x = 0)"  

definition P\<^sub>0 :: "(addr,nat) lpred"
  where
   "P\<^sub>0 \<equiv> PCmp (=) (Var z) (lexp.Const 0)"

definition \<Gamma>\<^sub>0 :: "addr \<rightharpoonup> Sec"
  where
   "\<Gamma>\<^sub>0 \<equiv> \<lambda>y. if y = x then Some Low else None" 

definition mds\<^sub>0
  where
  "mds\<^sub>0 \<equiv> \<lambda>m. case m of GuarNoWrite \<Rightarrow> {}
                      | GuarNoReadOrWrite \<Rightarrow> {}
                      | AsmNoReadOrWrite \<Rightarrow> {}
                      | AsmNoWrite \<Rightarrow> {z,x}"


definition
  dma :: "mem \<Rightarrow> addr \<Rightarrow> Sec"
where
  "dma m v \<equiv> if ev\<^sub>B m (dma_type v) then Low else High"

definition
  \<C>_vars :: "addr \<Rightarrow> addr set"
where
  "\<C>_vars v \<equiv> bexp_vars (dma_type v)"

abbreviation
  \<C> :: "addr set"
where
  "\<C> \<equiv> \<Union>x. \<C>_vars x"

lemma \<C>_def:
  shows "\<C> = {z}"
proof (auto)
  fix y e assume a: "y \<in> \<C>_vars e"
  thus "y = z" by (cases e; auto simp add: dma_def \<C>_vars_def)
next
  have "z \<in> \<C>_vars x" by (auto simp add: dma_def \<C>_vars_def)
  thus "\<exists>x. z \<in> \<C>_vars x" by auto
qed

lemma valid_\<Gamma>\<^sub>0_dom': 
  "dom \<Gamma>\<^sub>0 = (mds\<^sub>0 AsmNoReadOrWrite \<union> mds\<^sub>0 AsmNoWrite) - \<C>"
  unfolding mds\<^sub>0_def \<C>_def  
  by (auto simp: \<Gamma>\<^sub>0_def split: if_splits)

lemma valid_\<Gamma>\<^sub>0': 
  "\<forall>x\<in>dom \<Gamma>\<^sub>0.
          (the (\<Gamma>\<^sub>0 x) = Low \<longrightarrow> INIT mem\<^sub>1 \<and> INIT mem\<^sub>2 \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x) \<and>
          (dma mem\<^sub>1 x < the (\<Gamma>\<^sub>0 x) \<longrightarrow> x \<in> mds\<^sub>0 AsmNoReadOrWrite)"
  unfolding INIT_def mds\<^sub>0_def \<Gamma>\<^sub>0_def
  using domIff less_Sec_def by auto

lemma t0:
  "y \<in> \<C> \<longrightarrow> \<C>_vars y = {}"
  using  \<C>_def
  by (cases y; auto simp add: dma_def \<C>_vars_def)

lemma t1:
  "\<forall>x\<in>\<C>_vars y. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> dma mem\<^sub>1 y = dma mem\<^sub>2 y"
  by (cases y; simp add: dma_def \<C>_vars_def)

lemma t2:
  "dma mem y = (if ev\<^sub>B mem (dma_type y) then Low else High)"
  by (cases y; simp add: dma_def)

lemma t3:
  "\<C>_vars y = bexp_vars (dma_type y)"
  by (cases y; simp add: \<C>_vars_def)

lemma t4:
  "\<forall>y\<in>\<C>. dma mem y = Low"
  unfolding \<C>_def
  by (auto simp add: dma_def \<C>_vars_def)

(* interpretation of a sifum_type with the example-specific instances for the parameters *)

interpretation nat_lang Addrs Globals Locals INIT undefined dma \<C>_vars \<C> dma_type P\<^sub>0 \<Gamma>\<^sub>0 mds\<^sub>0
   apply(unfold_locales)
                      apply (metis Addrs_def Globals_def Locals_def UNIV inf_bot_right list.set(1) list.set(2) sup_inf_absorb)
                      apply (simp add: ev\<^sub>A_det)
                      apply (simp add: ev\<^sub>B_det)
                      apply (simp add: aexp_to_lexp_correct')
                      apply (simp add: bexp_to_lpred_correct')
                     apply (simp add: aexp_to_lexp_vars')
                    apply (simp add: bexp_to_lpred_vars')
                   apply (simp add: bexp_neg_negates')
                  apply (simp add: bexp_neg_vars')
                 apply (simp add: aexp_replace_correct')
                apply (simp add: bexp_replace_correct')
               apply (simp add: aexp_replace_nop')
              apply (simp add: bexp_replace_nop')
             apply (simp add: finite_Vars)
            apply (metis t0)
           apply (simp add: t1)
          apply (metis t4)
         apply (simp add: t2)
        apply (simp add: t3)
       apply (simp add: INIT_def P\<^sub>0_def)
      apply (simp add: P\<^sub>0_def mds\<^sub>0_def)
     apply (simp add: Locals_def P\<^sub>0_def mds\<^sub>0_def)
    apply (simp add: valid_\<Gamma>\<^sub>0_dom')
   apply (simp add: valid_\<Gamma>\<^sub>0')
  by (auto)

text \<open> Example program: secret_write from FM_2019 paper \<close>

definition
  secret_write :: "(addr, addr aexp, 'bexp) Stmt"
where
  "secret_write \<equiv>
    z \<leftarrow> (Add (Load z) (Const 1)) ;;;   
    Fence ;;;
    x \<leftarrow> (Load secret) ;;;
    x \<leftarrow> (Const 0) ;;;
    Fence ;;;
    z \<leftarrow> (Add (Load z) (Const 1)) ;;;   
    Stop"

(* helper lemmas ----------*)

lemma no_vars_inP[simp]: 
  "lpred_vars P\<^sub>0 = {z}"
  by (simp add: P\<^sub>0_def)

lemma [simp]:
  "\<C> = {z}"
proof -
  have "z \<in> bexp_vars (dma_type x)" by auto
  thus ?thesis
  unfolding \<C>_vars_def 
  apply auto
   apply (case_tac xa; auto)  
  using \<open>z \<in> bexp_vars (dma_type x)\<close> by blast
qed

lemma [simp]:
  "\<Gamma>\<^sub>0 z = None"
  by (simp add: \<Gamma>\<^sub>0_def)

lemma [simp]:
  "dom \<Gamma>\<^sub>0 = {x}"   
  by (simp add: mds\<^sub>0_def valid_\<Gamma>\<^sub>0_dom')

lemma [simp]:
  "(NoW \<S>\<^sub>0 \<union>  NoRW \<S>\<^sub>0) = {z, x}"
  by (metis Mode.simps(13) Mode.simps(14) Stable.case Un_empty_right \<S>\<^sub>0_def mds\<^sub>0_def)

lemma [simp]:
  "falling \<S>\<^sub>0 P\<^sub>0 UNIV (z \<leftarrow> (Add (Load z) (aexp.Const (Suc 0)))) = {}"
  unfolding falling_def
  by (auto; case_tac x; auto simp: P\<^sub>0_def)

lemma [simp]:
  "\<forall>a. known D\<^sub>0 a = UNIV"
  unfolding D\<^sub>0_def known_def by auto

lemma [simp]:
  "\<forall>D a. writes a \<union> reads a \<noteq> {} \<longrightarrow> known\<^sub>w (update_abv D Fence) a = UNIV"
  unfolding update_def known_def fromWrites_def fromReads_def D_gen_def
  by auto

lemma [simp]:
  "\<forall>D a. writes a \<union> reads a \<noteq> {} \<longrightarrow> known\<^sub>r (update_abv D Fence) a = UNIV"
  unfolding update_def known_def fromWrites_def fromReads_def D_gen_def
  by auto

lemma control_writes:
  "{y. writes (z \<leftarrow> e) \<inter> \<C>_vars y \<noteq> {}} = {x}"
  unfolding \<C>_vars_def by (auto; case_tac xa; auto)
  
lemma falling_max[simp]:
  "\<forall>S P D e. falling S P D (z \<leftarrow> e) \<subseteq> {x}"
  unfolding falling_def using control_writes
  by auto

lemma rising_max[simp]:
  "\<forall>S P D e. rising S P D (z \<leftarrow> e) \<subseteq> {x}"
  unfolding rising_def using control_writes
  by auto

lemma [simp]:
  "\<forall>P D e. falling \<S>\<^sub>0 P D (z \<leftarrow> e) \<subseteq> (insert x (NoRW \<S>\<^sub>0))"
  using falling_max by blast

lemma secret_write_typed:
  "\<exists>\<Gamma>' \<S>' P' D'. has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 D\<^sub>0 secret_write \<Gamma>' \<S>' P' D'"
  unfolding secret_write_def (* Expand program *)
  apply(intro exI act_type)  (* Break into Actions and Stop *)
        apply (rule action_type_control; simp add: secure_update_def sec_def)
       apply (rule fence_type)
      apply (rule action_type_stable_high; auto simp: P\<^sub>0_def reassign_var_def)
     apply (rule action_type_stable_low; simp)
    apply (rule fence_type)
   apply (rule action_type_control; simp add: secure_update_def sec_def)
  by (rule stop_type)

(* Compositionality proof *)

theorem secure_write_sifum_secure_cont:
  "prog_sifum_secure_cont [((secret_write, []) , env\<^sub>0)]" (is "prog_sifum_secure_cont ?cs")
proof (intro type_soundness_global type_global.intros)
  show "\<forall>((c, d), env)\<in>set ?cs. \<exists>\<Gamma>' \<S>' P' D'. has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 D\<^sub>0 c \<Gamma>' \<S>' P' D'"
    using secret_write_typed by auto
next
  show "\<forall>((c, d), env)\<in>set ?cs. env = env\<^sub>0" by auto
next
  show "\<forall>mem. secret_write.INIT mem \<longrightarrow> global_invariant mem"
    unfolding global_invariant_def by auto
next
  show "\<forall>mem. secret_write.INIT mem \<longrightarrow> sound_env_use (?cs, mem)"
    unfolding sound_env_use_def sorry (* Beyond the scope of this work *)
qed

end
