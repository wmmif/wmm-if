(*
Title: Data-dependent security on weak memory models
Authors: Graeme Smith, Nicholas Coughlin, Toby Murray
Based on Covern, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
In turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewenv, Heiko Mantel, Daniel Schoepe
*)
section \<open> Language for Logic Predicates \<close>

theory PredicateLanguage
  imports Main Preliminaries
begin

text \<open>
  We represent types and logic predicates using the following language.
  The language is capable of basic entailment and equivalence.
  Additionally, it allows for weakening and reassignment of variables.
\<close>

subsection \<open> Syntax \<close>

datatype ('var,'val) lexp =
  Var "'var"
  | Const "'val"
  | LBinOp "('val \<Rightarrow> 'val \<Rightarrow> 'val)" "('var,'val) lexp" "('var,'val) lexp"

datatype ('var,'val) lpred = 
  PTrue 
  | PFalse 
  | PDisj "('var,'val) lpred" "('var,'val) lpred"
  | PConj "('var,'val) lpred" "('var,'val) lpred"
  | PNeg "('var,'val) lpred"
  | PImp "('var,'val) lpred" "('var,'val) lpred"
  | PCmp "('val \<Rightarrow> 'val \<Rightarrow> bool)" "('var,'val) lexp" "('var,'val) lexp"
  | PEx "('val \<Rightarrow> ('var,'val) lpred)"
  | PAll "('val \<Rightarrow> ('var,'val) lpred)"

subsection \<open> Evaluation Semantics \<close>

primrec
  leval :: "('var,'val) Mem \<Rightarrow> ('var,'val) lexp \<Rightarrow> 'val"
where
  "leval mem (Var v) = mem v" |
  "leval mem (Const c) = c" |
  "leval mem (LBinOp bop e f) = (bop (leval mem e) (leval mem f))"

primrec 
  lpred_eval :: "('var,'val) Mem \<Rightarrow> ('var,'val) lpred  \<Rightarrow> bool"
where
  "lpred_eval mem PTrue = True" |
  "lpred_eval mem PFalse = False" |
  "lpred_eval mem (PDisj p q) = ((lpred_eval mem p) \<or> (lpred_eval mem q))" |
  "lpred_eval mem (PConj p q) = ((lpred_eval mem p) \<and> (lpred_eval mem q))" |
  "lpred_eval mem (PNeg p) = (\<not> (lpred_eval mem p))" |
  "lpred_eval mem (PImp p q) = ((lpred_eval mem p) \<longrightarrow> (lpred_eval mem q))" |
  "lpred_eval mem (PCmp cmp e f) = (cmp (leval mem e) (leval mem f))" |
  "lpred_eval mem (PEx fe) = (\<exists>v. lpred_eval mem (fe v))" |
  "lpred_eval mem (PAll fe) = (\<forall>v. lpred_eval  mem (fe v))"

primrec
  lexp_vars :: "('var,'val) lexp \<Rightarrow> 'var set"
where
  "lexp_vars (Var v) = { v }" |
  "lexp_vars (Const c) = {}" |
  "lexp_vars (LBinOp bop e f) = (lexp_vars e \<union> lexp_vars f)"

primrec
  lpred_vars :: "('var,'val) lpred \<Rightarrow> 'var set"
where
  "lpred_vars PTrue = {}" |
  "lpred_vars PFalse = {}"  |
  "lpred_vars (PNeg p) = lpred_vars p" |
  "lpred_vars (PImp p q) = (lpred_vars p \<union> lpred_vars q)" |
  "lpred_vars (PConj p q) = (lpred_vars p \<union> lpred_vars q)" |
  "lpred_vars (PDisj p q) = (lpred_vars p \<union> lpred_vars q)" |
  "lpred_vars (PCmp cmp e f) = (lexp_vars e \<union> lexp_vars f)" |
  "lpred_vars (PEx fe) = (\<Union>v. lpred_vars (fe v))" |
  "lpred_vars (PAll fe) = (\<Union>v. lpred_vars (fe v))"

primrec lexp_subst :: "('Var,'Val) lexp \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) lexp"
  where
    "lexp_subst (Var y) e x = (if x = y then e else Var y)" |
    "lexp_subst (Const c) e x = Const c" |
    "lexp_subst (LBinOp opf f f') e x = (LBinOp opf (lexp_subst f e x) (lexp_subst f' e x))"

primrec lpred_subst :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) lpred"
  where
    "lpred_subst PTrue e x = PTrue" |
    "lpred_subst PFalse e x = PFalse" |
    "lpred_subst (PConj P Q) e x = (PConj (lpred_subst P e x) (lpred_subst Q e x))" |
    "lpred_subst (PDisj P Q) e x = (PDisj (lpred_subst P e x) (lpred_subst Q e x))" |
    "lpred_subst (PImp P Q) e x = (PImp (lpred_subst P e x) (lpred_subst Q e x))" |
    "lpred_subst (PNeg P) e x = (PNeg (lpred_subst P e x))" |
    "lpred_subst (PCmp cmp f f') e x = (PCmp cmp (lexp_subst f e x) (lexp_subst f' e x))" |
    "lpred_subst (PAll fP) e x = (PAll (\<lambda>x'. lpred_subst (fP x') e x))" |
    "lpred_subst (PEx fP) e x = (PEx (\<lambda>x'. lpred_subst (fP x') e x))"

subsection \<open> Locale \<close>

locale predicate_lang =
  fixes local_type_constraint:: "'Var::{linorder, finite} \<times> 'Val::type" 

context predicate_lang
begin

lemma leval_vars_det:
  "\<forall>v \<in> lexp_vars e. mem v = mem' v \<Longrightarrow> leval mem e = leval mem' e"
  by (induct e, auto)

lemma lpred_eval_vars_det:
  "\<forall>v \<in> lpred_vars P. mem v = mem' v \<Longrightarrow> lpred_eval mem P = lpred_eval mem' P"
  by (induct P, (auto | metis leval_vars_det  UnCI)+)

subsubsection \<open> Entailment Definitions \<close>

definition pred_entailment :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool"
  (infix "\<turnstile>" 50)
  where "pred_entailment P P' \<equiv> \<forall>mem. lpred_eval mem P \<longrightarrow> lpred_eval mem P'"

abbreviation pred_entailment_inv :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool"
  (infix "!\<turnstile>" 50)
  where "P !\<turnstile> t \<equiv> P \<turnstile> PNeg t"

definition pred_equiv :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool"
  (infix "=\<^sub>P" 50)
  where "pred_equiv P P' \<equiv> \<forall>mem. lpred_eval mem P = lpred_eval mem P'"

subsubsection \<open> Entailment Properties \<close>

lemma pred_entailment_refl [simp]:
  "P \<turnstile> P"
  unfolding pred_entailment_def by auto

lemma pred_entailment_trans [trans]:
  "P \<turnstile> P' \<Longrightarrow> P' \<turnstile> P'' \<Longrightarrow> P \<turnstile> P''"
  unfolding pred_entailment_def by auto

lemma pred_entailment_PConj:
  "P \<turnstile> P' \<Longrightarrow> P \<turnstile> P'' \<Longrightarrow> P \<turnstile> (PConj P' P'')"
  unfolding pred_entailment_def by auto

lemma pred_entailment_mono_PConj:
  "P \<turnstile> P' \<Longrightarrow> (PConj P P'') \<turnstile> (PConj P' P'')"
  unfolding pred_entailment_def by auto

lemma PConj_entailment_mono:
  "(PConj P P') \<turnstile> P'' \<Longrightarrow> P''' \<turnstile> P \<Longrightarrow> (PConj P''' P') \<turnstile> P''"
  unfolding pred_entailment_def by force

subsubsection \<open> Equiv Properties \<close>

lemma pred_equiv_refl [simp]:
  shows "P =\<^sub>P P"
  unfolding pred_equiv_def by auto

lemma pred_equiv_trans [trans]:
  shows "P1 =\<^sub>P P2 \<Longrightarrow> P2 =\<^sub>P P3 \<Longrightarrow> P1 =\<^sub>P P3"
  unfolding pred_entailment_def pred_equiv_def by metis

lemma pred_equiv_entail:
  shows "P =\<^sub>P P' = (P \<turnstile> P' \<and> P' \<turnstile> P)"
  unfolding pred_entailment_def pred_equiv_def by auto

lemma pred_equiv_sym:
  shows "P1 =\<^sub>P P2 \<Longrightarrow> P2 =\<^sub>P P1"
  unfolding pred_entailment_def pred_equiv_def by metis

lemma pred_equiv_common:
  shows "P =\<^sub>P P' \<Longrightarrow> P \<turnstile> t = (P' \<turnstile> t)"
  unfolding pred_equiv_entail by (meson pred_entailment_trans)

lemma pred_equiv_common_inv:
  shows "P =\<^sub>P P' \<Longrightarrow> P !\<turnstile> t = (P' !\<turnstile> t)"
  unfolding pred_equiv_entail by (meson pred_entailment_trans)

lemma PEx_nop [simp]:
  shows "PEx (\<lambda>x'. P) =\<^sub>P P"
  unfolding pred_entailment_def pred_equiv_def
  by auto

lemma PEx_swap [simp]:
  "PEx (\<lambda>x1. PEx (\<lambda>x2. F x1 x2)) =\<^sub>P PEx (\<lambda>x2. PEx (\<lambda>x1. F x1 x2))"
  unfolding pred_equiv_def pred_entailment_def
  by auto

lemma ex_equiv:
  shows "(\<forall>x. P x =\<^sub>P P' x) \<Longrightarrow> PEx P =\<^sub>P PEx P'"
  unfolding pred_equiv_def by auto

lemma conj_equiv:
  assumes "P =\<^sub>P P'"
  shows "PConj P B =\<^sub>P PConj P' B"
  using assms unfolding pred_equiv_def pred_entailment_def by auto

lemma ex_entail:
  assumes "\<forall>x. P x \<turnstile> P' x"
  shows "PEx (\<lambda>x. P x) \<turnstile> PEx (\<lambda>x. P' x)"
  using assms unfolding pred_entailment_def by auto

lemma pred_fold_PConj:
  "lpred_eval mem (fold PConj Ps Q) \<Longrightarrow> (\<forall>P \<in> set Ps. lpred_eval mem P) \<and> lpred_eval mem Q"
  by (induct Ps arbitrary: Q , clarsimp ; metis fold_simps(2) lpred_eval.simps(4) set_ConsD)

lemma pred_fold_PConj':
  "(\<forall>P \<in> set Ps. lpred_eval mem P) \<and> lpred_eval mem Q \<Longrightarrow> lpred_eval mem (fold PConj Ps Q)"
  by (induct Ps arbitrary: Q, auto)

lemma pred_fold_PConj_eq:
  "((\<forall>P \<in> set Ps. lpred_eval mem P) \<and> lpred_eval mem Q) = lpred_eval mem (fold PConj Ps Q)"
  using pred_fold_PConj pred_fold_PConj' by blast
  
subsubsection \<open> lexp Substitution \<close>

lemma lexp_subst_mem_upd:
  assumes "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y)"
  shows "leval mem' (lexp_subst f (Const (mem x)) x) = leval mem f"
  using assms by (induct f, auto)

lemma lexp_subst_nop [simp]:
  assumes "x \<notin> lexp_vars f"
  shows "lexp_subst f v x = f"
  using assms by (induct f, auto)

lemma lexp_subst_vars [simp]:
  shows "lexp_vars (lexp_subst f v x) = lexp_vars f - {x} \<union> (if x \<in> lexp_vars f then lexp_vars v else {})"
  by (induct f, auto)

lemma lexp_subst_comm_diff:
  assumes "x \<noteq> y"
  assumes "x \<notin> lexp_vars v1"
  assumes "y \<notin> lexp_vars v2"
  shows "(lexp_subst (lexp_subst f v1 y) v2 x) = (lexp_subst (lexp_subst f v2 x) v1 y)"
  using assms by (induct f, auto)

lemma lexp_subst_comm_same [simp]:
  shows "lexp_subst (lexp_subst f v1 x) v2 x = lexp_subst f (lexp_subst v1 v2 x) x"
  by (induct f, auto)

subsubsection \<open> lpred Substitution \<close>

lemma lpred_subst_mem_upd:
  assumes "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y)"
  shows "lpred_eval mem' (lpred_subst P (Const (mem x)) x) = lpred_eval mem P"
  using assms by (induct P, auto simp: lexp_subst_mem_upd)

lemma lpred_subst_nop [simp]:
  shows "x \<notin> lpred_vars P \<Longrightarrow> lpred_subst P v x = P"
  by (induct P, auto)

lemma lpred_subst_vars [simp]:
  shows "lpred_vars (lpred_subst P v x) = lpred_vars P - {x} \<union> (if x \<in> lpred_vars P then lexp_vars v else {})"
  by (induct P, auto split: if_splits)

lemma lpred_subst_comm_diff:
  assumes "x \<noteq> y"
  assumes "x \<notin> lexp_vars v1"
  assumes "y \<notin> lexp_vars v2"
  shows "(lpred_subst (lpred_subst P v1 y) v2 x) = (lpred_subst (lpred_subst P v2 x) v1 y)"
  using assms by (induct P, auto simp: lexp_subst_comm_diff)

lemma lpred_subst_fold [simp]:
  shows "lpred_subst (lpred_subst P (Const v1) x) (Const v2) x = lpred_subst P (Const v1) x"
  by (induct P, auto)

lemma lpred_vars_empty:
  "lpred_vars P = {} \<Longrightarrow> P =\<^sub>P PTrue \<or> P =\<^sub>P PFalse"
  by (metis equals0D lpred_eval.simps(1,2) lpred_eval_vars_det pred_equiv_def)

text \<open> Entailment is preserved across substitution \<close>
lemma lpred_subst_entailment:
  shows "P \<turnstile> P' \<Longrightarrow> lpred_subst P (Const v) x \<turnstile> lpred_subst P' (Const v) x"
  unfolding pred_entailment_def
proof clarify
  fix mem
  assume "\<forall>mem. lpred_eval mem P \<longrightarrow> lpred_eval mem P'"
  moreover assume "lpred_eval mem (lpred_subst P (Const v) x)"
  ultimately show "lpred_eval mem (lpred_subst P' (Const v) x)"
    using lpred_subst_mem_upd [of x mem "mem (x := v)"]
    by simp
qed

subsubsection \<open> Weaken Variable \<close>

text \<open> Weaken variable knowledge by reassigning to some constant \<close>
definition weaken_var :: "'Var \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred"
  where
  "weaken_var x P = (if x \<in> lpred_vars P then PEx (\<lambda>x'. lpred_subst P (Const x') x) else P)"

lemma weaken_var_weakens:
  shows "P \<turnstile> weaken_var x P"
  using lpred_subst_mem_upd by (auto simp: pred_entailment_def weaken_var_def, metis)

lemma weaken_var_vars [simp]:
  shows "lpred_vars (weaken_var x P) = lpred_vars P - {x}"
  unfolding weaken_var_def lpred_vars.simps by (simp add: SUP_eq_const)

lemma weaken_var_nop [simp]:
  shows "x \<notin> lpred_vars P \<Longrightarrow> weaken_var x P = P"
  using lpred_subst_nop [of x P] by (auto simp: weaken_var_def)

lemma weaken_var_fold [simp]:
  shows "weaken_var x (weaken_var x P) =\<^sub>P weaken_var x P"
  by auto 

lemma weaken_var_comm:
  shows "weaken_var y (weaken_var x P) =\<^sub>P weaken_var x (weaken_var y P)"
  unfolding weaken_var_def
  by (cases "x = y", auto simp: lpred_subst_comm_diff [of x])

lemma weaken_var_entailment [intro]:
  assumes "P \<turnstile> P'"
  shows "weaken_var x P \<turnstile> weaken_var x P'"
  using lpred_subst_entailment [OF assms] 
  unfolding weaken_var_def pred_entailment_def
  by (cases "x \<in> lpred_vars P" ; cases "x \<in> lpred_vars P'" ; auto ; metis lpred_subst_nop)

lemma weaken_var_equiv:
  shows "P =\<^sub>P C \<Longrightarrow> weaken_var x P =\<^sub>P weaken_var x C"           
  by (metis weaken_var_entailment pred_equiv_entail)

lemma weaken_var_conj_l:
  shows "x \<notin> lpred_vars P \<Longrightarrow> weaken_var x (PConj P P') =\<^sub>P PConj P (weaken_var x P')" 
  by (simp add: weaken_var_def pred_equiv_def pred_entailment_def)

lemma weaken_var_conj_r:
  shows "x \<notin> lpred_vars P' \<Longrightarrow> weaken_var x (PConj P P') =\<^sub>P PConj (weaken_var x P) P'" 
  by (simp add: weaken_var_def pred_equiv_def pred_entailment_def)

subsubsection \<open> Weaken List of Variables \<close>

lemma weaken_var_fold_entailment [intro]:
  shows "P \<turnstile> P' \<Longrightarrow> fold weaken_var V P \<turnstile> fold weaken_var V P'"
  by (induct V arbitrary: P P', auto)

lemma weaken_var_fold_equiv:
  shows "P =\<^sub>P P' \<Longrightarrow> fold weaken_var V P =\<^sub>P fold weaken_var V P'"
  by (metis weaken_var_fold_entailment pred_equiv_entail)

lemma weaken_var_fold_weakens [intro]:
  shows "P \<turnstile> fold weaken_var V P"
  by (induct V arbitrary: P ; auto ; metis fold.simps(2) weaken_var_weakens pred_entailment_trans comp_def)

lemma weaken_var_fold_remove:
  shows "x \<in> set V \<Longrightarrow> fold weaken_var V P =\<^sub>P fold weaken_var (remove1 x V) (weaken_var x P)"
proof (induct V arbitrary: P)
  case Nil
  then show ?case by auto
next
  case (Cons a V)
  let ?P = "weaken_var a P"
  show ?case
  proof (cases "x = a")
    case True
    then show ?thesis by auto
  next
    case False
    hence "fold weaken_var V ?P =\<^sub>P fold weaken_var (remove1 x V) (weaken_var x ?P)"
      using Cons by auto
    also have "... =\<^sub>P fold weaken_var (remove1 x V) (weaken_var a (weaken_var x P))"
      using weaken_var_comm weaken_var_fold_equiv by auto
    finally show ?thesis by auto      
  qed
qed

lemma waken_var_fold_unique [simp]:
  assumes "x \<in> set V"
  shows "fold weaken_var (x # V) P =\<^sub>P fold weaken_var V P" (is "?a =\<^sub>P ?b")
proof -
  let ?cmn = "fold weaken_var (remove1 x V) (weaken_var x P)"
  have rhs: "?b =\<^sub>P ?cmn" using assms weaken_var_fold_remove by auto
  have lhs: "?a =\<^sub>P ?cmn"
    using assms weaken_var_fold_remove [of x V "weaken_var x P"] weaken_var_fold_equiv by auto
  show ?thesis using rhs lhs pred_equiv_sym pred_equiv_trans by metis
qed

lemma weaken_var_fold_set_subset:
  assumes "P \<turnstile> P'"
  assumes "set V \<subseteq> set V'"
  shows "fold weaken_var V P \<turnstile> fold weaken_var V' P'"
  using assms
proof (induct V arbitrary: V' P P')
  case Nil
  then show ?case using weaken_var_fold_weakens pred_entailment_trans by auto
next
  case (Cons a V1)
  hence "weaken_var a P \<turnstile> weaken_var a P'"
    using weaken_var_entailment by auto
  thus ?case
    using Cons(3) Cons(1) [of "weaken_var a P" "weaken_var a P'" V']
    using waken_var_fold_unique weaken_var_entailment pred_entailment_trans
    unfolding pred_equiv_entail by auto
qed

lemma weaken_var_rearrange:
  shows "weaken_var x (fold weaken_var V P) =\<^sub>P fold weaken_var (x#V) P"
proof (induct V arbitrary: P)
  case Nil
  then show ?case by auto
next
  case (Cons a V)
  let ?P = "weaken_var a P"
  have "weaken_var x (fold weaken_var V ?P) =\<^sub>P fold weaken_var (x # V) ?P"
    using Cons by auto
  also have "... =\<^sub>P fold weaken_var V (weaken_var a (weaken_var x P))"
    using weaken_var_fold_equiv weaken_var_comm  by auto
  finally show ?case by auto
qed

lemma weaken_var_fold_vars [simp]:
  shows "lpred_vars (fold weaken_var V P) = lpred_vars P - set V"
  by (induct V arbitrary: P, auto)

lemma weaken_var_fold_nop:
  assumes "\<forall>x \<in> set V. x \<notin> lpred_vars P"
  shows "fold weaken_var V P = P"
  using assms
proof (induct V arbitrary: P)
  case Nil
  then show ?case by auto
next
  case (Cons a V)
  show ?case 
    using Cons(1) [of "weaken_var a P"] Cons(2) weaken_var_nop
    by (auto)
qed

lemma weaken_var_fold_conj:
  assumes "lpred_vars P \<inter> (set V) = {}"
  shows "fold weaken_var V (PConj P P') =\<^sub>P PConj P (fold weaken_var V P')"
  using assms
proof (induct V arbitrary: P P')
  case Nil
  then show ?case by auto
next
  case (Cons a V)
  let ?P = "P"
  have a: "fold weaken_var (a # V) (PConj ?P P') = fold weaken_var V (weaken_var a (PConj ?P P'))" by auto
  have "a \<notin> lpred_vars P" using Cons by auto
  hence "weaken_var a (PConj ?P P') =\<^sub>P PConj ?P (weaken_var a P')"
    by (simp add: weaken_var_conj_l)
  hence "fold weaken_var (a # V) (PConj ?P P') =\<^sub>P 
          fold weaken_var V (PConj ?P (weaken_var a P'))" using a
    by (simp add: weaken_var_fold_equiv)
  thus ?case
    using Cons(1) [of P "weaken_var a P'"] Cons(2)
    by (metis (mono_tags, hide_lams) disjoint_insert(1) fold_simps(2) list.simps(15) pred_equiv_trans)
qed

subsubsection \<open> Restrict to Set of Variables \<close>

definition lpred_restrict :: "('Var,'Val) lpred \<Rightarrow> ('Var) set \<Rightarrow> ('Var,'Val) lpred"
  ("_ \<restriction> _" [110, 120] 100)
where
  "lpred_restrict P V = fold weaken_var ((sorted_list_of_set (UNIV - V))) P"

lemma sorted_list_equiv [simp]:
  "set (sorted_list_of_set V) = (V :: ('Var) set)"
  using finite_subset subset_UNIV set_sorted_list_of_set
  by simp

lemma lpred_restrict_entailment [intro]:
  shows "P \<turnstile> C \<Longrightarrow> P \<restriction> V \<turnstile> C \<restriction> V"
  using weaken_var_fold_set_subset
  by (auto simp: lpred_restrict_def)

lemma lpred_restrict_equiv [intro]:
  assumes "P =\<^sub>P C"
  shows "P \<restriction> V =\<^sub>P C \<restriction> V"
  using assms lpred_restrict_entailment
  unfolding pred_equiv_entail by auto

lemma lpred_restrict_nop [simp]:
  assumes "lpred_vars P \<subseteq> V"
  shows "P \<restriction> V = P"
proof -
  let ?l = "sorted_list_of_set (UNIV - V)"
  have notin: "\<forall>x \<in> set ?l. x \<notin> lpred_vars P" using assms by auto
  show ?thesis using weaken_var_fold_nop [OF notin] by (auto simp: lpred_restrict_def)
qed

lemma lpred_restrict_vars [simp]:
  shows "lpred_vars (P \<restriction> V) = lpred_vars P \<inter> V"
  by (auto simp: lpred_restrict_def)

lemma lpred_restrict_weakens [intro]:
  shows "P \<turnstile> P \<restriction> V"
  by (auto simp: lpred_restrict_def)

lemma lpred_restrict_inter [simp]:
  shows "(P \<restriction> V1) \<restriction> V2 =\<^sub>P P \<restriction> (V1 \<inter> V2)"
proof -
  let ?l1 = "sorted_list_of_set (UNIV - V1)"
  let ?l2 = "sorted_list_of_set (UNIV - V2)"
  let ?l3 = "sorted_list_of_set (UNIV - V1 \<inter> V2)"

  have "set (?l1@?l2) = set ?l3" by auto
  moreover have "P \<turnstile> P" by auto
  moreover have "fold weaken_var ?l2 (fold weaken_var ?l1 P) = fold weaken_var (?l1@?l2) P" by auto
  ultimately show  ?thesis
    by (metis lpred_restrict_def pred_equiv_entail subset_refl weaken_var_fold_set_subset)
qed

lemma lpred_restrict_comm:
  shows "(P \<restriction> V1) \<restriction> V2 =\<^sub>P (P \<restriction> V2) \<restriction> V1"
  using lpred_restrict_inter
  by (metis Int_commute pred_equiv_sym pred_equiv_trans)

lemma lpred_restrict_entailment_subset [intro]:
  assumes e: "P \<turnstile> C"
  assumes s: "V \<supseteq> V'"
  shows "P \<restriction> V \<turnstile> C \<restriction> V'"
proof -
  have "P \<restriction> V \<turnstile> C \<restriction> V" using e by auto
  also have "... \<turnstile> (C \<restriction> V) \<restriction> V'" by auto
  also have "... \<turnstile> C \<restriction> (V \<inter> V')" using lpred_restrict_inter by (auto simp: pred_equiv_entail)
  also have "... \<turnstile> C \<restriction> V'" using s by (simp add: inf.absorb2)
  finally show ?thesis by simp
qed

lemma lpred_restrict_conj_l:
  shows "PConj P P' \<restriction> V \<turnstile> P \<restriction> V"
proof -
  have "PConj P P' \<turnstile> P" by (auto simp: pred_entailment_def)
  thus ?thesis by (metis lpred_restrict_entailment)
qed

lemma lpred_restrict_conj_r:
  shows "PConj P P' \<restriction> V \<turnstile> P' \<restriction> V"
proof -
  have "PConj P P' \<turnstile> P'" by (auto simp: pred_entailment_def)
  thus ?thesis by (metis lpred_restrict_entailment)
qed

lemma lpred_restrict_conj_eq_r:
  assumes "lpred_vars P \<subseteq> V"
  shows "PConj P P' \<restriction> V =\<^sub>P PConj P (P' \<restriction> V)"
proof -
  let ?V = "sorted_list_of_set (UNIV - V)"
  have "lpred_vars P \<inter> set ?V = {}" using assms by auto
  hence "fold weaken_var ?V (PConj P P') =\<^sub>P PConj P (fold weaken_var ?V P')"
    by (metis weaken_var_fold_conj)
  thus ?thesis unfolding lpred_restrict_def by auto
qed

lemma lpred_restrict_conj_eq_l:
  assumes "lpred_vars P' \<subseteq> V"
  shows "PConj P P' \<restriction> V =\<^sub>P PConj (P \<restriction> V) P'"
proof -
  have "PConj P P'  =\<^sub>P PConj P' P" by (auto simp: pred_equiv_def)
  hence "PConj P P' \<restriction> V  =\<^sub>P PConj P' P \<restriction> V" by auto
  also have "... =\<^sub>P PConj P' (P \<restriction> V)" using lpred_restrict_conj_eq_r [OF assms] by auto
  also have "... =\<^sub>P PConj (P \<restriction> V) P'" by (auto simp: pred_equiv_def)
  finally show ?thesis by assumption
qed

subsubsection \<open> Reassign variables \<close>

text \<open> Reassign a variable, by weakening existing and adding new expression \<close>
definition reassign_var ::
  "'Var \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred"
  where
    "reassign_var x e P =
      PEx (\<lambda>v. (PConj (lpred_subst P (Const v) x) (PCmp (=) (Var x) (lexp_subst e (Const v) x))))"

definition spec_var ::
  "'Var \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred"
  where
    "spec_var x e P =
      PEx (\<lambda>v. PConj (lpred_subst P (Const v) x) (PCmp (=) (Const v) e))"

lemma reassign_var_vars [simp]:
  shows "lpred_vars (reassign_var x e P) = lpred_vars P \<union> { x} \<union> lexp_vars e"
  by (auto simp: reassign_var_def split: if_splits)

lemma reassign_always_valid:
  shows "\<exists>v. v = leval mem (lexp_subst (lexp_subst e (Const va) x) (Const v) x)"
  by (metis lexp_subst_comm_same lexp_subst.simps(2))

lemma reassign_var_entailment [intro]:
  assumes "P \<turnstile> P'"
  shows "reassign_var x e P \<turnstile> reassign_var x e P'"
  using lpred_subst_entailment [OF assms] 
  unfolding reassign_var_def weaken_var_def pred_entailment_def
  by (cases "x \<in> lpred_vars P" ; cases "x \<in> lpred_vars P'" ; auto ; metis lpred_subst_nop)

lemma reassign_var_equiv [intro]:
  assumes "P =\<^sub>P P'"
  shows "reassign_var x e P =\<^sub>P reassign_var x e P'"
  using assms unfolding pred_equiv_entail
  by (meson reassign_var_entailment)

lemma reassign_var_eval:
  assumes "lpred_eval mem P"
  shows "lpred_eval (mem (x := leval mem e)) (reassign_var x e P)" (is "lpred_eval ?mem ?P")
proof -
  have r: "\<forall>y. y \<noteq> x \<longrightarrow> ?mem y = mem y" by auto
  hence p: "lpred_eval ?mem (lpred_subst P (Const (mem x)) x)"
    using assms lpred_subst_mem_upd by metis
  moreover have "leval mem e = leval ?mem (lexp_subst e (Const (mem x)) x)"
    using assms lexp_subst_mem_upd r by metis
  ultimately show ?thesis by (auto simp: reassign_var_def)
qed

lemma reassign_subst_comm':
  assumes "x \<noteq> y"
  assumes "y \<notin> lexp_vars f"
  shows "lpred_subst (reassign_var y e P) f x = reassign_var y (lexp_subst e f x) (lpred_subst P f x)"
proof -
  let ?p = "\<lambda>v. PCmp (=) (Var y) (lexp_subst e (Const v) y)"
  let ?P = "\<lambda>v. lpred_subst P (Const v) y"

  have "lpred_subst (reassign_var y e P) f x = lpred_subst (PEx (\<lambda>v. PConj (?P v) (?p v))) f x"
    by (auto simp: reassign_var_def)
  also have "... = PEx (\<lambda>y. lpred_subst (PConj (?P y) (?p y)) f x)" by auto
  also have "... = PEx (\<lambda>y. PConj (lpred_subst (?P y) f x) (lpred_subst (?p y) f x))" by auto
  also have "... = PEx (\<lambda>z. PConj (lpred_subst (?P z) f x) (PCmp (=) (Var y) (lexp_subst (lexp_subst e (Const z) y) f x)))"
    using assms by auto
  also have "... = PEx (\<lambda>z. PConj (lpred_subst (?P z) f x) (PCmp (=) (Var y) (lexp_subst (lexp_subst e f x) (Const z) y)))"
    by (simp add: assms lexp_subst_comm_diff)
  also have "... = reassign_var y (lexp_subst e f x) (lpred_subst P f x)"
    unfolding reassign_var_def by (simp add: assms lpred_subst_comm_diff)
  finally show ?thesis by assumption
qed

lemma reassign_subst_comm:
  assumes "x \<noteq> y"
  assumes "x \<notin> lexp_vars e"
  shows "lpred_subst (reassign_var y e P) (Const v) x = reassign_var y e (lpred_subst P (Const v) x)"
proof -
  let ?p = "\<lambda>v. PCmp (=) (Var y) (lexp_subst e (Const v) y)"
  let ?P = "\<lambda>v. lpred_subst P (Const v) y"
  have notin: "\<forall>v. x \<notin> lpred_vars (?p v)" using assms by auto

  have "lpred_subst (reassign_var y e P) (Const v) x = lpred_subst (PEx (\<lambda>v. PConj (?P v) (?p v))) (Const v) x"
    by (auto simp: reassign_var_def)
  also have "... = PEx (\<lambda>y. lpred_subst (PConj (?P y) (?p y)) (Const v) x)" by auto
  also have "... = PEx (\<lambda>y. PConj (lpred_subst (?P y) (Const v) x) (?p y))" using notin by auto
  also have "... = reassign_var y e (lpred_subst P (Const v) x)"
    unfolding reassign_var_def by (simp add: lpred_subst_comm_diff [OF assms(1)])
  finally show ?thesis by assumption
qed

lemma reassign_pex_comm:
  shows "PEx (\<lambda>v. (reassign_var y e (P v))) =\<^sub>P reassign_var y e (PEx P)"
  unfolding reassign_var_def pred_equiv_def by auto

lemma reassign_weaken_comm:
  assumes "x \<noteq> y"
  assumes "x \<notin> lexp_vars e"
  shows "weaken_var x (reassign_var y e P) =\<^sub>P reassign_var y e (weaken_var x P)"
  using assms by (auto simp: weaken_var_def reassign_pex_comm reassign_subst_comm) 

lemma reassign_weaken_fold_comm:
  assumes "y \<notin> set V"
  assumes "lexp_vars e \<inter> set V = {}"
  shows "fold weaken_var V (reassign_var y e P) =\<^sub>P reassign_var y e (fold weaken_var V P)"
  using assms
proof (induct V arbitrary: P)
  case Nil
  then show ?case by auto
next
  case (Cons a V)
  let ?P = "weaken_var a P"
  have "weaken_var a (reassign_var y e P) =\<^sub>P reassign_var y e ?P" 
    using reassign_weaken_comm Cons(2,3) by auto
  hence "fold weaken_var V (weaken_var a (reassign_var y e P)) =\<^sub>P fold weaken_var V (reassign_var y e ?P)" 
    using weaken_var_fold_equiv by metis
  also have "... =\<^sub>P reassign_var y e (fold weaken_var V ?P)" using Cons by auto
  finally show ?case by auto 
qed

lemma reassign_restrict_comm:
  assumes "y \<in> V"
  assumes "lexp_vars e \<subseteq> V"
  shows "reassign_var y e P \<restriction> V =\<^sub>P reassign_var y e (P \<restriction> V)"
  using assms reassign_weaken_fold_comm [of y "sorted_list_of_set (UNIV - V)" e] sorted_list_equiv
  unfolding lpred_restrict_def
  by (metis (no_types, hide_lams) Diff_disjoint Diff_iff inf.orderE inf_assoc order_refl)

lemma reassign_weaken_comm_same:
  shows "weaken_var x (reassign_var x e P) =\<^sub>P weaken_var x P"
  by (auto simp: weaken_var_def reassign_var_def pred_equiv_def reassign_always_valid)

lemma reassign_weaken_fold_comm_same:
  assumes "x \<in> set V"
  shows "fold weaken_var V (reassign_var x e P) =\<^sub>P fold weaken_var V P"
  using assms
proof (induct V arbitrary: P)
  case Nil
  then show ?case by auto
next
  case (Cons a V)
  then show ?case
  proof (cases "x = a")
    case True
    then show ?thesis using reassign_weaken_comm_same weaken_var_fold_equiv by auto
  next
    case False
    hence "fold weaken_var V (reassign_var x e P) =\<^sub>P fold weaken_var V P" (is "?l =\<^sub>P ?r")
      using Cons by auto
    hence "weaken_var a ?l =\<^sub>P weaken_var a ?r"
      using weaken_var_equiv by auto
    then show ?thesis 
      using weaken_var_rearrange pred_entailment_trans pred_equiv_def by meson
  qed
qed

lemma reassign_restrict_comm_same:
  assumes "x \<notin> V"
  shows "reassign_var x e P \<restriction> V =\<^sub>P P \<restriction> V"
  using assms reassign_weaken_fold_comm_same
  unfolding lpred_restrict_def
  by auto 

lemma reassign_var_conj_l:
  assumes "x \<notin> lpred_vars P"
  shows "reassign_var x e (PConj P P') =\<^sub>P PConj P (reassign_var x e P')"
  using assms by (auto simp: reassign_var_def pred_equiv_def pred_entailment_def)

lemma reassign_var_conj_r:
  assumes "x \<notin> lpred_vars P'"
  shows "reassign_var x e (PConj P P') =\<^sub>P PConj (reassign_var x e P) P'"
  using assms by (auto simp: reassign_var_def pred_equiv_def pred_entailment_def)

lemma lpred_subst_weaken:
  assumes "x \<noteq> y"
  shows "weaken_var x (lpred_subst P (Const v) y) = (lpred_subst (weaken_var x P) (Const v) y)"
  unfolding weaken_var_def using assms by (auto split: if_splits simp: lpred_subst_comm_diff)

lemma lpred_subst_weaken_fold:
  assumes "y \<notin> set V"
  shows "fold weaken_var V (lpred_subst P (Const v) y) = (lpred_subst (fold weaken_var V P) (Const v) y)"
  using assms
proof (induct V arbitrary: P)
  case Nil
  then show ?case by auto
next
  case (Cons a V)
  let ?P = "weaken_var a P"
  have a: "fold weaken_var V (lpred_subst ?P (Const v) y) = lpred_subst (fold weaken_var V ?P) (Const v) y"
    using Cons by auto
  have "y \<noteq> a" using Cons by auto
  hence "weaken_var a (lpred_subst P (Const v) y) = lpred_subst ?P (Const v) y" (is "?l = ?r")
    using lpred_subst_weaken by metis
  hence "fold weaken_var V ?l = fold weaken_var V ?r" by (simp add: weaken_var_fold_equiv)
  then show ?case unfolding fold_simps(2) using a by metis
qed

lemma subst_in:
  assumes "x \<in> S"
  shows "(lpred_subst P (Const v) x) \<restriction> S = lpred_subst (P \<restriction> S) (Const v) x"
  using assms
  by (simp add: lpred_restrict_def lpred_subst_weaken_fold)

lemma weaken_var_PEx:
  shows "weaken_var x (PEx P) =\<^sub>P PEx (\<lambda>v. weaken_var x (P v))"
  unfolding pred_equiv_def weaken_var_def
  by (auto ; metis lpred_subst_nop)

lemma ex_fold_weaken_var:
  "fold weaken_var V (PEx (\<lambda>v. P v)) =\<^sub>P PEx (\<lambda>v. fold weaken_var V (P v))"
proof (induct V arbitrary: P)
  case Nil                               
  then show ?case by auto
next
  case (Cons a V)
  let ?P = "\<lambda>v. weaken_var a (P v)"
  have a: "fold weaken_var V (PEx ?P) =\<^sub>P PEx (\<lambda>v. fold weaken_var V (?P v))"
    using Cons by auto
  have "weaken_var a (PEx P) =\<^sub>P PEx ?P" using weaken_var_PEx by auto
  hence "fold weaken_var (a # V) (PEx P) =\<^sub>P fold weaken_var V (PEx (\<lambda>v. weaken_var a (P v)))"
    by (simp add: weaken_var_fold_equiv)
  then show ?case using a pred_equiv_trans unfolding fold_simps by blast 
qed

lemma ex_lpred_restrict:
  "PEx (\<lambda>v. P v) \<restriction> S =\<^sub>P PEx (\<lambda>v. P v \<restriction> S)"
  using ex_fold_weaken_var
  unfolding lpred_restrict_def
  by auto 

text \<open> Half of the comm proof, moves both sides into a common form \<close>
lemma reassign_comm_half:
  assumes "x \<noteq> y"
  assumes "y \<notin> lexp_vars e"
  assumes "x \<notin> lexp_vars f"
  assumes sub: "lpred_vars P \<subseteq> S"
  assumes "x \<in> S"
  shows "reassign_var x e (reassign_var y f P \<restriction> S) \<restriction> S =\<^sub>P 
         PEx (\<lambda>v1. PEx (\<lambda>v2. PConj (PConj 
             (lpred_subst (lpred_subst P (Const v1) x) (Const v2) y) 
             (PCmp (=) (Var y) (lexp_subst f (Const v2) y) \<restriction> S)) 
             (PCmp (=) (Var x) (lexp_subst e (Const v1) x) \<restriction> S)))"
    (is "?l =\<^sub>P ?r")
proof -
  let ?P1 = "\<lambda>v. lpred_subst P (Const v) x"
  let ?P2 = "\<lambda>v. lpred_subst P (Const v) y"
  let ?P = "\<lambda>v1 v2. lpred_subst (lpred_subst P (Const v1) x) (Const v2) y"
  let ?B1 = "\<lambda>v. PCmp (=) (Var x) (lexp_subst e (Const v) x)"
  let ?B2 = "\<lambda>v. PCmp (=) (Var y) (lexp_subst f (Const v) y)"

  have s: "\<forall>v. lpred_vars (lpred_subst (reassign_var y f P \<restriction> S) (Const v) x) \<subseteq> S" using sub by auto

  have "?l =\<^sub>P PEx (\<lambda>v. PConj (lpred_subst (reassign_var y f P \<restriction> S) (Const v) x) (?B1 v)) \<restriction> S"
    unfolding reassign_var_def by auto
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (lpred_subst (reassign_var y f P \<restriction> S) (Const v) x) (?B1 v \<restriction> S))"
    using s ex_equiv lpred_restrict_conj_eq_r ex_lpred_restrict pred_equiv_trans by meson
  also have "... =\<^sub>P PEx (\<lambda>v. PConj ((lpred_subst (reassign_var y f P) (Const v) x) \<restriction> S) (?B1 v \<restriction> S))"
    by (simp add: assms(5) subst_in)
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (reassign_var y f (lpred_subst P (Const v) x) \<restriction> S) (?B1 v \<restriction> S))"
    by (simp add: assms(1) assms(3) reassign_subst_comm)

  also have "... =\<^sub>P PEx (\<lambda>v1. PConj (PEx (\<lambda>v2. PConj (?P v1 v2) (?B2 v2)) \<restriction> S) (?B1 v1 \<restriction> S))"
    unfolding reassign_var_def by auto
  also have "... =\<^sub>P PEx (\<lambda>v1. PConj (PEx (\<lambda>v2. PConj (?P v1 v2) (?B2 v2 \<restriction> S))) (?B1 v1 \<restriction> S))"
  proof -
    have "\<forall>v1 v2. lpred_vars (?P v1 v2) \<subseteq> S" using sub by auto
    hence "\<forall>v1. PEx (\<lambda>v2. PConj (?P v1 v2) (?B2 v2)) \<restriction> S =\<^sub>P PEx (\<lambda>v2. PConj (?P v1 v2) (?B2 v2 \<restriction> S))"
      using ex_equiv lpred_restrict_conj_eq_r ex_lpred_restrict by (meson pred_equiv_trans)
    thus ?thesis unfolding pred_equiv_def by simp
  qed
  also have "... =\<^sub>P PEx (\<lambda>v1. PEx (\<lambda>v2. PConj (PConj (?P v1 v2) (?B2 v2 \<restriction> S)) (?B1 v1 \<restriction> S)))"
    unfolding pred_equiv_def by auto

  finally show ?thesis by auto
qed

lemma reassign_comm:
  assumes "x \<noteq> y"
  assumes "y \<notin> lexp_vars e"
  assumes "x \<notin> lexp_vars f"
  assumes sub: "lpred_vars P \<subseteq> S"
  shows "reassign_var x e (reassign_var y f P \<restriction> S) \<restriction> S =\<^sub>P reassign_var y f (reassign_var x e P \<restriction> S) \<restriction> S"
    (is "?l =\<^sub>P ?r")
proof -
  let ?P = "\<lambda>v1 v2. lpred_subst (lpred_subst P (Const v1) x) (Const v2) y"
  let ?P' = "\<lambda>v1 v2. lpred_subst (lpred_subst P (Const v1) y) (Const v2) x"
  let ?B1 = "\<lambda>v. PCmp (=) (Var x) (lexp_subst e (Const v) x)"
  let ?B2 = "\<lambda>v. PCmp (=) (Var y) (lexp_subst f (Const v) y)"

  show ?thesis
  proof (cases "x \<in> S \<and> y \<in> S")
    case True

    have "?l =\<^sub>P PEx (\<lambda>v1. PEx (\<lambda>v2. PConj (PConj (?P v1 v2) (?B2 v2 \<restriction> S)) (?B1 v1 \<restriction> S)))"
      (is "?l =\<^sub>P ?l'")
      using True reassign_comm_half assms by blast

    moreover have "?r =\<^sub>P PEx (\<lambda>v1. PEx (\<lambda>v2. PConj (PConj (?P' v1 v2) (?B1 v2 \<restriction> S)) (?B2 v1 \<restriction> S)))"
      (is "?r =\<^sub>P ?r'")
      using True reassign_comm_half [of y x] assms by blast

    moreover have "?l' =\<^sub>P ?r'"
    proof -
      have "\<forall>v1 v2. lpred_subst (lpred_subst P (Const v1) x) (Const v2) y = 
                    lpred_subst (lpred_subst P (Const v2) y) (Const v1) x"
        using assms by (simp add: lpred_subst_comm_diff)
      thus ?thesis unfolding pred_equiv_def by auto
    qed
    ultimately show ?thesis using pred_equiv_trans pred_equiv_sym by meson
  next
    case False
    show ?thesis
    proof (cases "x \<in> S")
      case True
      hence notin: "y \<notin> S" using False by auto
      
      have "?l =\<^sub>P reassign_var x e P \<restriction> S" using notin
        by (metis sub lpred_restrict_equiv lpred_restrict_nop reassign_restrict_comm_same reassign_var_equiv)
      moreover have "?r =\<^sub>P reassign_var x e P \<restriction> S" using notin
        by (metis inf_le2 lpred_restrict_nop lpred_restrict_vars reassign_restrict_comm_same)

      ultimately show ?thesis by (meson pred_equiv_sym pred_equiv_trans)
    next
      case False
      hence notin: "x \<notin> S" by auto

      have "?r =\<^sub>P reassign_var y f P \<restriction> S" using notin
        by (metis sub lpred_restrict_equiv lpred_restrict_nop reassign_restrict_comm_same reassign_var_equiv)
      moreover have "?l =\<^sub>P reassign_var y f P \<restriction> S" using notin
        by (metis inf_le2 lpred_restrict_nop lpred_restrict_vars reassign_restrict_comm_same)

      ultimately show ?thesis by (meson pred_equiv_sym pred_equiv_trans)
    qed
  qed
qed

lemma lexp_subst_eval_const:
  shows "leval mem (lexp_subst P (Const (leval mem e)) x) = leval mem (lexp_subst P e x)"
  by (induct P, auto)

lemma lpred_subst_eval_const:
  shows "lpred_eval mem (lpred_subst P (Const (leval mem e)) x) = lpred_eval mem (lpred_subst P e x)"
  by (induct P, auto simp: lexp_subst_eval_const)

lemma lpred_subst_weaken_var:
  shows "lpred_subst P e x \<turnstile> weaken_var x P"
  unfolding weaken_var_def pred_entailment_def
  using lpred_subst_eval_const [where P=P and e=e]
  unfolding lpred_eval.simps
  by auto

lemma conj_comm:
  assumes sub: "lpred_vars P \<subseteq> S"
  shows "PConj (PConj P B  \<restriction> S) B' \<restriction> S =\<^sub>P PConj (PConj P B' \<restriction> S) B \<restriction> S" (is "?l =\<^sub>P ?r")
proof -
  have vars: "\<forall>V l. lpred_vars (l \<restriction> V) \<subseteq> V" by simp
  have "?l =\<^sub>P PConj (PConj P (B \<restriction> S)) (B' \<restriction> S)"
    by (meson vars sub conj_equiv lpred_restrict_conj_eq_r pred_equiv_trans)
  moreover have "?r =\<^sub>P PConj (PConj P (B' \<restriction> S)) (B \<restriction> S)"
    by (meson vars sub conj_equiv lpred_restrict_conj_eq_r pred_equiv_trans)
  ultimately show ?thesis unfolding pred_equiv_def by auto
qed

lemma assign_conj_comm:
  assumes notin: "x \<notin> lpred_vars B"
  assumes sub: "lpred_vars P \<subseteq> S"
  shows "reassign_var x e (PConj P B \<restriction> S) \<restriction> S =\<^sub>P PConj (reassign_var x e P \<restriction> S) B \<restriction> S" (is "?l =\<^sub>P ?r")
proof -
  have notin: "x \<notin> lpred_vars (B \<restriction> S)" using notin lpred_restrict_vars by auto

  have "?l =\<^sub>P reassign_var x e (PConj P (B \<restriction> S)) \<restriction> S" 
    by (meson reassign_var_equiv lpred_restrict_equiv sub lpred_restrict_conj_eq_r)
  also have "... =\<^sub>P PConj (reassign_var x e P) (B \<restriction> S) \<restriction> S"
    by (meson notin reassign_var_conj_r lpred_restrict_equiv)
  also have "... =\<^sub>P ?r"
    using lpred_restrict_conj_eq_l lpred_restrict_conj_eq_r pred_equiv_def by auto
  finally show ?thesis by simp
qed

lemma conj_assign_comm:
  assumes notin: "x \<notin> lpred_vars B"
  assumes sub: "lpred_vars P \<subseteq> S"
  shows "PConj (reassign_var x e P \<restriction> S) B \<restriction> S =\<^sub>P reassign_var x e (PConj P B \<restriction> S) \<restriction> S"
  using assms by (meson assign_conj_comm pred_equiv_sym)

lemma pred_entailment_list:
  assumes "B\<^sub>2 \<turnstile> B\<^sub>1"
  assumes "\<forall>x \<in> set l. \<Gamma>\<^sub>2 x \<turnstile> \<Gamma>\<^sub>1 x"
  shows "fold PConj (map \<Gamma>\<^sub>2 l) B\<^sub>2 \<turnstile> fold PConj (map \<Gamma>\<^sub>1 l) B\<^sub>1"
  using assms
proof (induct l arbitrary: B\<^sub>1 B\<^sub>2)
  case Nil
  then show ?case by auto
next
  case (Cons a x)
  hence "\<Gamma>\<^sub>2 a \<turnstile> \<Gamma>\<^sub>1 a" by auto
  hence "PConj (\<Gamma>\<^sub>2 a) B\<^sub>2 \<turnstile> PConj (\<Gamma>\<^sub>1 a) B\<^sub>1"
    using Cons(2,3) by (auto simp: pred_entailment_def)
  thus ?case using Cons by auto
qed


lemma spec_var_nop:
  assumes "x \<notin> lpred_vars P"
  shows "spec_var x e P =\<^sub>P P"
proof -
  have "\<forall>v. lpred_subst P (Const v) x = P" using assms lpred_subst_nop by auto
  thus ?thesis
    unfolding spec_var_def pred_equiv_def pred_entailment_def
    by auto
qed

lemma lpred_subst_equiv:
  assumes "P =\<^sub>P C"
  shows "lpred_subst P (Const v) x =\<^sub>P lpred_subst C (Const v) x"
  using assms by (simp add: lpred_subst_entailment pred_equiv_entail)

lemma weaken_var_D:
  shows "weaken_var x P =\<^sub>P PEx(\<lambda>v. lpred_subst P (Const v) x)"
  by (simp add: pred_equiv_def weaken_var_def)

lemma lpred_restrict_weaken:
  assumes "x \<notin> V"
  shows "weaken_var x P \<restriction> V =\<^sub>P P \<restriction> V"
proof -
  have "x \<in> set (sorted_list_of_set (UNIV - V))" using assms by auto
  thus ?thesis using waken_var_fold_unique by (auto simp: lpred_restrict_def)
qed

lemma lpred_restrict_single [simp]:
  shows "P \<restriction> (- {x}) = weaken_var x P"
  unfolding lpred_restrict_def by auto

lemma spec_reassign_equiv:
  assumes "lpred_vars P \<subseteq> V"
  assumes "lpred_vars P' \<subseteq> V"
  shows "weaken_var x (PConj (spec_var x e P' \<restriction> V) P) =\<^sub>P weaken_var x (PConj P' (reassign_var x e P \<restriction> V))"
    (is "?l =\<^sub>P ?r")
proof -
  let ?P = "\<lambda>v. lpred_subst P (Const v) x"
  let ?e = "\<lambda>v. PCmp (=) (Var x) (lexp_subst e (Const v) x)"
  let ?P' = "\<lambda>v. lpred_subst P' (Const v) x"
  let ?e' = "\<lambda>v. PCmp (=) (Const v) e"

  let ?e'' = "\<lambda>v v'. PCmp (=) (Const v) (lexp_subst e (Const v') x)"

  (* Reduce left side to common form *)
  have "?l =\<^sub>P weaken_var x (PConj (PEx (\<lambda>v. PConj (?P' v) (?e' v)) \<restriction> V) P)"
    unfolding spec_var_def by auto

  (* Move V filter inwards, to expression e *)
  also have "... =\<^sub>P weaken_var x (PConj (PEx (\<lambda>v. PConj (?P' v) (?e' v \<restriction> V))) P)"
  proof -
    have "PEx (\<lambda>v. PConj (?P' v) (?e' v)) \<restriction> V =\<^sub>P PEx (\<lambda>v. PConj (?P' v) (?e' v) \<restriction> V)"
      using ex_lpred_restrict by blast
    also have "... =\<^sub>P PEx (\<lambda>v. PConj (?P' v) (?e' v \<restriction> V))"
    proof -
      have "\<forall>v. lpred_vars (?P' v) \<subseteq> V" using assms(2) lpred_subst_vars by auto
      hence "\<forall>v. PConj (?P' v) (?e' v) \<restriction> V =\<^sub>P PConj (?P' v) (?e' v \<restriction> V)"
        by (simp add: lpred_restrict_conj_eq_r)
      thus ?thesis unfolding pred_equiv_def by auto
    qed
    finally show ?thesis using pred_equiv_def weaken_var_equiv by auto
  qed

  (* Rearrange terms to match right hand side *)
  also have "... =\<^sub>P weaken_var x (PEx (\<lambda>v. (PConj (?P' v) (PConj P (?e' v \<restriction> V) ))))"
  proof -
    have "\<forall>P\<^sub>1 P\<^sub>2 P\<^sub>3. PConj (PEx (\<lambda>v. PConj (P\<^sub>1 v) (P\<^sub>2 v))) P\<^sub>3 =\<^sub>P 
                    PEx (\<lambda>v. PConj (P\<^sub>1 v) (PConj P\<^sub>3 (P\<^sub>2 v)))"
      unfolding pred_equiv_def by auto
    thus ?thesis using weaken_var_equiv by meson
  qed

  (* Expand weaken var *)
  also have "... =\<^sub>P PEx (\<lambda>v'. lpred_subst (PEx (\<lambda>v. PConj (?P' v) (PConj P (?e' v \<restriction> V))))  (Const v') x)"
    using weaken_var_D by meson

  (* Expand weaken var *)
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (?P' v) (PEx (\<lambda>v'. (PConj (?P v') (lpred_subst (?e' v \<restriction> V) (Const v') x)))))"
    by (auto simp add: pred_equiv_def)
  finally have l: "?l =\<^sub>P PEx (\<lambda>v. PConj (?P' v) (PEx (\<lambda>v'. (PConj (?P v') (lpred_subst (?e' v \<restriction> V) (Const v') x)))))"
    by simp

  (* Reduce right side to common form *)
  have "?r =\<^sub>P weaken_var x (PConj P' (PEx (\<lambda>v. PConj (?P v) (?e v)) \<restriction> V))"
    unfolding reassign_var_def by auto

  (* Move V filter inwards, to expression e *)
  also have "... =\<^sub>P weaken_var x (PConj P' (PEx (\<lambda>v. PConj (?P v) (?e v \<restriction> V))))"
  proof -
    have "PEx (\<lambda>v. PConj (?P v) (?e v)) \<restriction> V =\<^sub>P PEx (\<lambda>v. PConj (?P v) (?e v) \<restriction> V)"
      using ex_lpred_restrict by blast
    also have "... =\<^sub>P PEx (\<lambda>v. PConj (?P v) (?e v \<restriction> V))"
    proof -
      have "\<forall>v. lpred_vars (?P v) \<subseteq> V" using assms(1) lpred_subst_vars by auto
      hence "\<forall>v. PConj (?P v) (?e v) \<restriction> V =\<^sub>P PConj (?P v) (?e v \<restriction> V)"
        by (simp add: lpred_restrict_conj_eq_r)
      thus ?thesis unfolding pred_equiv_def by auto
    qed
    finally show ?thesis
      using pred_equiv_def weaken_var_equiv by auto
  qed

  (* Expand the weaken var *)
  also have "... =\<^sub>P PEx (\<lambda>v'. (lpred_subst (PConj P' (PEx (\<lambda>v. PConj (?P v) (?e v \<restriction> V)))) (Const v') x))"
    using weaken_var_D by meson

  (* Move the subst inwards, to expression e *)
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (?P' v) (PEx (\<lambda>v'. PConj (?P v') (lpred_subst (?e v' \<restriction> V) (Const v) x))))"
    by auto

  finally have r: "?r =\<^sub>P PEx (\<lambda>v. PConj (?P' v) (PEx (\<lambda>v'. PConj (?P v') (lpred_subst (?e v' \<restriction> V) (Const v) x))))"
    by simp

  hence m: "\<forall>v v'. lpred_subst (?e' v) (Const v') x =\<^sub>P lpred_subst (?e v') (Const v) x"
    by simp

  show ?thesis
  proof (cases "x \<in> V")
    case True
    hence "\<forall>v v'. lpred_subst (?e' v \<restriction> V) (Const v') x =\<^sub>P lpred_subst (?e v' \<restriction> V) (Const v) x "
      using m by (metis lpred_restrict_equiv subst_in)
    then show ?thesis using l r pred_equiv_def by auto
  next
    case False
    hence "x \<notin> lpred_vars P" "x \<notin> lpred_vars P'" 
          "\<forall>v. x \<notin> lpred_vars (?e' v \<restriction> V)" "\<forall>v. x \<notin> lpred_vars (?e v \<restriction> V)"
      using assms by auto
    hence eq: "\<forall>v. ?P v = P" "\<forall>v. ?P' v = P'" 
              "\<forall>v v'. lpred_subst (?e' v \<restriction> V) (Const v') x = ?e' v \<restriction> V"
              "\<forall>v v'. lpred_subst (?e v \<restriction> V) (Const v') x = ?e v \<restriction> V"
      using lpred_subst_nop by auto

    have l: "?l =\<^sub>P PEx (\<lambda>v. PConj P' (PEx (\<lambda>v'. (PConj P (?e' v \<restriction> V)))))"
      using l eq by auto
    have r: "?r =\<^sub>P PEx (\<lambda>v. PConj P' (PEx (\<lambda>v'. PConj P (?e v' \<restriction> V))))"
      using r eq by auto

    have "PEx (\<lambda>v. weaken_var x (?e v)) =\<^sub>P PEx (\<lambda>v. weaken_var x (?e' v))"
      unfolding weaken_var_def pred_equiv_def by auto
    hence "PEx (\<lambda>v. weaken_var x (?e v)) \<restriction> V =\<^sub>P PEx (\<lambda>v. weaken_var x (?e' v)) \<restriction> V"
      by auto
    hence "PEx (\<lambda>v. weaken_var x (?e v) \<restriction> V) =\<^sub>P PEx (\<lambda>v. weaken_var x (?e' v) \<restriction> V)"
      using ex_lpred_restrict pred_equiv_def by auto
    hence m: "PEx(\<lambda>v. ?e v \<restriction> V) =\<^sub>P PEx(\<lambda>v. ?e' v \<restriction> V)"
      using False lpred_restrict_weaken pred_equiv_def by auto 

    show ?thesis using l r m pred_equiv_def by auto
  qed
qed

text \<open> Series of speculation properties to show how it corresponds to evaluation \<close>

lemma lexp_subst_mem_eval:
  shows "leval mem (lexp_subst f e x) = leval (mem (x := leval mem e)) f"
  by (induct f, auto)

lemma lpred_subst_mem_eval:
  shows "lpred_eval mem (lpred_subst P e x) = lpred_eval (mem (x := leval mem e)) P"
  by (induct P; simp add: fun_upd_def lexp_subst_mem_eval)

lemma spec_var_mem_eval:
  shows "lpred_eval mem (spec_var x e P) = lpred_eval (mem (x := leval mem e)) P"
  by (simp add: spec_var_def lpred_subst_mem_eval)

lemma lpred_subst_comm_exp:
  assumes "a \<notin> lpred_vars P"
  shows "lpred_subst (spec_var x e P) (Const v) a = spec_var x (lexp_subst e (Const v) a) P"
  using assms by (auto simp: spec_var_def)

lemma weaken_list_spec_var:
  assumes "set V \<inter> lpred_vars (P :: ('Var, 'Val) lpred) = {}"
  assumes "lpred_eval mem (fold weaken_var V (spec_var x e P))"
  shows "\<exists>m. lpred_eval (m (x := leval m e)) P \<and> (\<forall>v. v \<notin> set V \<longrightarrow> m v = mem v)"
  using assms
proof (induct V arbitrary: e)
  case Nil
  then show ?case by (metis fold_simps(1) spec_var_mem_eval)
next
  case (Cons a V)
  hence notin: "a \<notin> lpred_vars P" "set V \<inter> lpred_vars P = {}" using Cons by auto

  let ?P = "spec_var x e P"

  (* Push the weakening of a into the expression, extracting the constant v *)
  have "weaken_var a ?P \<turnstile> PEx (\<lambda>v. lpred_subst ?P (Const v) a)"
    using pred_equiv_entail weaken_var_D by blast 
  hence "lpred_eval mem (fold weaken_var V (PEx (\<lambda>v. lpred_subst ?P (Const v) a)))"
    using Cons.prems(2) pred_entailment_def weaken_var_fold_entailment by fastforce
  then obtain v where pred: "lpred_eval mem (fold weaken_var V (lpred_subst ?P (Const v) a))"
    using ex_fold_weaken_var pred_equiv_def by auto 
 
  let ?e = "lexp_subst e (Const v) a"

  (* Use the inductive hypothesis and show its maintained across the assignment *)
  have "lpred_eval mem (fold weaken_var V (spec_var x (lexp_subst e (Const v) a) P))"
    using notin pred lpred_subst_comm_exp by metis
  then obtain m where mem: "lpred_eval (m(x := leval m ?e)) P" "\<forall>v. v \<notin> set V \<longrightarrow> m v = mem v" 
    using notin Cons(1) [of ?e] by metis  
  thus ?case using notin spec_var_mem_eval
    by (metis fun_upd_apply list.set_intros(1,2) lpred_subst_comm_exp lpred_subst_mem_eval)
qed

text \<open>Given a speculated assignment, restricted to some set S, its possible to show a memory
      must exist where performing the assignment maintains the original predicate.
      Its not possible to show this memory and the original are equivalent, due to the set S\<close>
lemma restricted_spec_var:
  assumes vars: "lpred_vars P \<subseteq> S"
  assumes pred: "lpred_eval mem (spec_var x e P \<restriction> S)"
  shows "\<exists>m. lpred_eval (m (x := leval m e)) P \<and> (\<forall>v\<in>S. m v = mem v)"
proof -
  let ?V="sorted_list_of_set (UNIV - S)"
  have "lpred_eval mem (fold weaken_var ?V (spec_var x e P))" using pred lpred_restrict_def by auto
  moreover have "set ?V \<inter> lpred_vars P = {}" using vars by auto
  ultimately show ?thesis by (metis weaken_list_spec_var DiffE sorted_list_equiv)
qed

text \<open>Given a predicate P on a memory with an assignment, its possible to reverse the assignment
      and maintain the predicate on the original memory\<close>
lemma restricted_spec_var_rev:
  assumes vars: "lpred_vars P \<subseteq> S"
  assumes pred: "lpred_eval (mem (x := leval mem e)) P" (is "lpred_eval ?M\<^sub>2 P")
  shows "lpred_eval mem (spec_var x e P \<restriction> \<S>)"  (is " lpred_eval ?M\<^sub>1 ?P")
  using assms spec_var_mem_eval [of mem x e P] pred_entailment_def by blast


lemma pred_equiv_fold_split:
  shows "fold PConj l P =\<^sub>P PConj P (fold PConj l PTrue)"
proof (induct l arbitrary: P)
  case Nil
  then show ?case unfolding pred_equiv_def by auto
next
  case (Cons a l)
  let ?P1 = "PConj a P" and ?a = "PConj a PTrue"
  have a: "fold PConj l ?a =\<^sub>P PConj ?a (fold PConj l PTrue)" using Cons by auto
  have "fold PConj l ?P1 =\<^sub>P PConj ?P1 (fold PConj l PTrue)" using Cons by auto
  also have "... =\<^sub>P PConj P (fold PConj l (PConj a PTrue))" using a by (auto simp: pred_equiv_def)
  finally show ?case by auto
qed

end

end