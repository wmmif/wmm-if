# Value-Dependent Information-Flow Security on Weak Memory Models

This project contains the Isabelle/HOL formalisation and soundness proof for [Value-Dependent Information-Flow Security on Weak Memory Models](https://link.springer.com/chapter/10.1007/978-3-030-30942-8_32), along with some basic examples of its application.
This work is based on a formalisation of [Covern](http://covern.org), with weak memory semantics based on a [syntactic instruction reordering](https://arxiv.org/abs/1802.04406).

## Formalisation Details

The formalisation includes an information flow logic, similar to that of **Covern**, extended with a data-flow analysis capable of identifying situations where a variable's value may be ambiguous due to a weak memory model. See the paper for the full details.

This was a first attempt at coupling weak memory and information flow.
Consequently, it has limited support for specifying how variables are shared between threads.
Specifically, variables must be classified as unconstrained, not writable, or both not readable and not writable by each thread.
This classification cannot change throughout the thread.
[Later work]() has improved on this constraint.

## Project Structure  
* Preliminaries.thy: Security classification definitions, inherited from **Covern**
* Compositionality.thy: Compositionality theory for variable annotations, inherited from **Covern**
* Security.thy: Security properties demonstrated by the logic, inherited from **Covern**
* PredicateLanguage.thy: Deeply embedded predicate language used for the logic's assertions
* WeakMemoryFlow.thy: Theory for the data-flow analysis
* WeakMemoryLanguage.thy: Program language and weak memory model semantics
* TypeSystem.thy: Logic rules and soundness proof
