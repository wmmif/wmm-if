(*
Title: Data-dependent security on weak memory models
Authors: Graeme Smith, Nicholas Coughlin, Toby Murray
Based on Covern, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
In turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewenv, Heiko Mantel, Daniel Schoepe
*)

section \<open> Definition of the Dependent SIFUM-Security Property \<close>

theory Security
imports Preliminaries
begin

type_synonym ('var, 'val) adaptation = "'var \<rightharpoonup> ('val \<times> 'val)"

definition apply_adaptation ::
  "bool \<Rightarrow> ('Var, 'Val) Mem \<Rightarrow> ('Var, 'Val) adaptation \<Rightarrow> ('Var, 'Val) Mem"
  where "apply_adaptation first mem A =
         (\<lambda> x. case (A x) of
               Some (v\<^sub>1, v\<^sub>2) \<Rightarrow> if first then v\<^sub>1 else v\<^sub>2
             | None \<Rightarrow> mem x)"

abbreviation apply_adaptation\<^sub>1 ::
  "('Var, 'Val) Mem \<Rightarrow> ('Var, 'Val) adaptation \<Rightarrow> ('Var, 'Val) Mem"
  ("_ [\<parallel>\<^sub>1 _]" [900, 0] 1000)
  where "mem [\<parallel>\<^sub>1 A] \<equiv> apply_adaptation True mem A"

abbreviation apply_adaptation\<^sub>2 ::
  "('Var, 'Val) Mem \<Rightarrow> ('Var, 'Val) adaptation \<Rightarrow> ('Var, 'Val) Mem"
  ("_ [\<parallel>\<^sub>2 _]" [900, 0] 1000)
  where "mem [\<parallel>\<^sub>2 A] \<equiv> apply_adaptation False mem A"

definition
  var_asm_not_written :: "('Var,'Val) Env \<Rightarrow> 'Var \<Rightarrow> bool"
where
  "var_asm_not_written env x \<equiv> x \<in> AsmNoW env \<union> AsmNoRW env"

context sifum_security_init begin

subsection \<open> Evaluation of Concurrent Programs \<close>

abbreviation eval_abv :: "('Com, 'Var, 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>" 70)
where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval"

abbreviation conf_abv :: "'Com \<Rightarrow> ('Var,'Val) Env \<Rightarrow> ('Var, 'Val) Mem \<Rightarrow> (_,_,_) LocalConf"
  ("\<langle>_, _, _\<rangle>" [0, 0, 0] 1000)
where
  "\<langle> c, env, mem \<rangle> \<equiv> ((c, env), mem)"

(* Labelled evaluation global configurations: *)
inductive_set meval :: "((_,_,_) GlobalConf \<times> nat \<times> (_,_,_) GlobalConf) set"
  and meval_abv :: "_ \<Rightarrow> _ \<Rightarrow> _ \<Rightarrow> bool" ("_ \<leadsto>\<^bsub>_\<^esub> _" 70)
where
  "conf \<leadsto>\<^bsub>k\<^esub> conf' \<equiv> (conf, k, conf') \<in> meval" |
  meval_intro [iff]: "\<lbrakk> (cms ! n, mem) \<leadsto> (cm', mem'); n < length cms \<rbrakk> \<Longrightarrow>
  ((cms, mem), n, (cms [n := cm'], mem')) \<in> meval"

inductive_cases meval_elim [elim!]: "((cms, mem), k, (cms', mem')) \<in> meval"

inductive neval :: "('Com, 'Var, 'Val) LocalConf \<Rightarrow> nat \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>\<^bsup>_\<^esup>" 70)
where
  neval_0: "x = y \<Longrightarrow> x \<leadsto>\<^bsup>0\<^esup> y" |
  neval_S_n: "x \<leadsto> y \<Longrightarrow> y \<leadsto>\<^bsup>n\<^esup> z \<Longrightarrow> x \<leadsto>\<^bsup>Suc n\<^esup> z"

inductive_cases neval_ZeroE: "neval x 0 y"
inductive_cases neval_SucE: "neval x (Suc n) y"

lemma neval_det:
  "x \<leadsto>\<^bsup>n\<^esup> y \<Longrightarrow> x \<leadsto>\<^bsup>n\<^esup> y' \<Longrightarrow> y = y'"
  apply(induct arbitrary: y' rule: neval.induct)
   apply(blast elim: neval_ZeroE)
  apply(blast elim: neval_SucE dest: deterministic)
  done

lemma neval_Suc_simp [simp]:
  "neval x (Suc 0) y = x \<leadsto> y"
proof
  assume a: "neval x (Suc 0) y"
  have "\<And>n. neval x n y \<Longrightarrow> n = Suc 0 \<Longrightarrow> x \<leadsto> y"
  proof -
    fix n
    assume "neval x n y"
       and "n = Suc 0"
    thus "x \<leadsto> y"
    by(induct rule: neval.induct, auto elim: neval_ZeroE)
  qed
  with a show "x \<leadsto> y" by simp
  next
  assume "x \<leadsto> y"
  thus "neval x (Suc 0) y"
    by(force intro: neval.intros)
qed

fun 
  lc_set_var :: "(_, _, _) LocalConf \<Rightarrow> 'Var \<Rightarrow> 'Val \<Rightarrow> (_, _, _) LocalConf"
where
  "lc_set_var (c, mem) x v = (c, mem (x := v))"

fun 
  meval_sched :: "nat list \<Rightarrow> ('Com, 'Var, 'Val) GlobalConf \<Rightarrow> (_, _, _) GlobalConf \<Rightarrow> bool"
where
  "meval_sched [] c c' = (c = c')" |
  "meval_sched (n#ns) c c' = (\<exists> c''.  c \<leadsto>\<^bsub>n\<^esub> c'' \<and> meval_sched ns c'' c')"

abbreviation
  meval_sched_abv :: "(_,_,_) GlobalConf \<Rightarrow> nat list \<Rightarrow> (_,_,_) GlobalConf \<Rightarrow> bool" ("_ \<rightarrow>\<^bsub>_\<^esub> _" 70)
where
  "c \<rightarrow>\<^bsub>ns\<^esub> c' \<equiv> meval_sched ns c c'"

lemma meval_sched_det:
  "meval_sched ns c c' \<Longrightarrow> meval_sched ns c c'' \<Longrightarrow> c' = c''"
  apply(induct ns arbitrary: c)
   apply(auto dest: deterministic)
  done

subsection \<open> Low-equivalence and Strong Low Bisimulations \<close>

(* Low-equality between memory states: *)
definition 
  low_eq :: "('Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> bool" (infixl "=\<^sup>l" 80)
where
  "mem\<^sub>1 =\<^sup>l mem\<^sub>2 \<equiv> (\<forall> x. dma mem\<^sub>1 x = Low \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x)"

(* Low-equality modulo a given mode state: *)
definition low_mds_eq :: "('Var, 'Val) Env \<Rightarrow> ('Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> bool"
  ("_ =\<index>\<^sup>l _" [100, 100] 80)
where
  "(mem\<^sub>1 =\<^bsub>env\<^esub>\<^sup>l mem\<^sub>2) \<equiv> (\<forall> x. dma mem\<^sub>1 x = Low \<and> (x \<in> \<C> \<or> x \<notin> AsmNoRW env) \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x)"

(*
lemma low_eq_low_mds_eq:
  "(mem\<^sub>1 =\<^sup>l mem\<^sub>2) = (mem\<^sub>1 =\<^bsub>(\<lambda>m. {})\<^esub>\<^sup>l mem\<^sub>2)"
  by(simp add: low_eq_def low_mds_eq_def) *)

lemma low_mds_eq_dma:
  "(mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2) \<Longrightarrow> dma mem\<^sub>1 = dma mem\<^sub>2"
  apply(rule dma_\<C>)
  apply(simp add: low_mds_eq_def \<C>_Low)
  done

lemma low_mds_eq_sym:
  "(mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2) \<Longrightarrow> (mem\<^sub>2 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>1)"
  apply(frule low_mds_eq_dma)
  apply(simp add: low_mds_eq_def)
  done

(*
lemma low_eq_sym:
  "(mem\<^sub>1 =\<^sup>l mem\<^sub>2) \<Longrightarrow> (mem\<^sub>2 =\<^sup>l mem\<^sub>1)"
  unfolding low_eq_def by 
  apply (frule low_eq_def)
  apply(simp add: low_eq_low_mds_eq low_mds_eq_sym)
  done *)

lemma [simp]: "mem =\<^sup>l mem' \<Longrightarrow> mem =\<^bsub>mds\<^esub>\<^sup>l mem'"
  by (simp add: low_mds_eq_def low_eq_def)

(*
lemma [simp]: "(\<forall> mds. mem =\<^bsub>mds\<^esub>\<^sup>l mem') \<Longrightarrow> mem =\<^sup>l mem'"
  by (auto simp: low_mds_eq_def low_eq_def) *)

lemma High_not_in_\<C> [simp]:
  "dma mem\<^sub>1 x = High \<Longrightarrow> x \<notin> \<C>"
  apply(case_tac "x \<in> \<C>")
  by(simp add: \<C>_Low)

(* Closedness under globally consistent changes: *)
definition 
  closed_glob_consistent :: "(('Com, 'Var, 'Val) LocalConf) rel \<Rightarrow> bool"
where
  "closed_glob_consistent \<R> =
  (\<forall> c\<^sub>1 env mem\<^sub>1 c\<^sub>2 mem\<^sub>2. (\<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle>, \<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle>) \<in> \<R> \<longrightarrow>
   (\<forall> A. ((\<forall>x. case A x of Some (v,v') \<Rightarrow> (mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v') \<longrightarrow> \<not> var_asm_not_written env x | _ \<Rightarrow> True) \<and>
          (\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written env x) \<and>
          (\<forall>R \<in> Relies env. (mem\<^sub>1, (mem\<^sub>1 [\<parallel>\<^sub>1 A])) \<in> R  \<and> (mem\<^sub>2, (mem\<^sub>2 [\<parallel>\<^sub>2 A])) \<in> R ) \<and>
          (\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = Low \<and> (x \<notin> AsmNoRW env \<or> x \<in> \<C>) \<longrightarrow> (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = (mem\<^sub>2 [\<parallel>\<^sub>2 A]) x)) \<longrightarrow>
         (\<langle> c\<^sub>1, env, mem\<^sub>1[\<parallel>\<^sub>1 A] \<rangle>, \<langle> c\<^sub>2, env, mem\<^sub>2[\<parallel>\<^sub>2 A] \<rangle>) \<in> \<R>))"

(* Strong low bisimulations modulo modes: *)
definition 
  strong_low_bisim_mm :: "(('Com, 'Var, 'Val) LocalConf) rel \<Rightarrow> bool"
where
  "strong_low_bisim_mm \<R> \<equiv>
  closed_glob_consistent \<R> \<and>
  (\<forall> c\<^sub>1 env mem\<^sub>1 c\<^sub>2 mem\<^sub>2. (\<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle>, \<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle>) \<in> \<R> \<longrightarrow>
   (mem\<^sub>1 =\<^bsub>env\<^esub>\<^sup>l mem\<^sub>2) \<and>
   (\<forall> c\<^sub>1'  env' mem\<^sub>1'. \<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle> \<leadsto> \<langle> c\<^sub>1', env', mem\<^sub>1' \<rangle> \<longrightarrow>
    (\<exists> c\<^sub>2' mem\<^sub>2'. \<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle> \<leadsto> \<langle> c\<^sub>2', env', mem\<^sub>2' \<rangle> \<and>
                  (\<langle> c\<^sub>1', env', mem\<^sub>1' \<rangle>, \<langle> c\<^sub>2', env', mem\<^sub>2' \<rangle>) \<in> \<R>)))"

inductive_set mm_equiv :: "(('Com, 'Var, 'Val) LocalConf) rel"
  and mm_equiv_abv :: "('Com, 'Var, 'Val) LocalConf \<Rightarrow> 
  ('Com, 'Var, 'Val) LocalConf \<Rightarrow> bool" (infix "\<approx>" 60)
where
  "mm_equiv_abv x y \<equiv> (x, y) \<in> mm_equiv" |
  mm_equiv_intro [iff]: "\<lbrakk> strong_low_bisim_mm \<R> ; (lc\<^sub>1, lc\<^sub>2) \<in> \<R> \<rbrakk> \<Longrightarrow> (lc\<^sub>1, lc\<^sub>2) \<in> mm_equiv"

inductive_cases mm_equiv_elim [elim]: "\<langle> c\<^sub>1, mds, mem\<^sub>1 \<rangle> \<approx> \<langle> c\<^sub>2, mds, mem\<^sub>2 \<rangle>"

definition low_indistinguishable :: "('Var,'Val) Env \<Rightarrow> 'Com \<Rightarrow> 'Com \<Rightarrow> bool"
  ("_ \<sim>\<index> _" [100, 100] 80)
where
  "c\<^sub>1 \<sim>\<^bsub>env\<^esub> c\<^sub>2 = (\<forall> mem\<^sub>1 mem\<^sub>2. INIT mem\<^sub>1 \<and> INIT mem\<^sub>2 \<and> mem\<^sub>1 =\<^bsub>env\<^esub>\<^sup>l mem\<^sub>2 \<longrightarrow> \<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle> \<approx> \<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle>)"

subsection \<open> Dependent SIFUM-Security \<close>

(* Dependent SIFUM-security for commands: *)
definition 
  com_sifum_secure :: "'Com \<times> ('Var,'Val) Env \<Rightarrow> bool"
where 
  "com_sifum_secure cmd \<equiv> case cmd of (c,env\<^sub>s) \<Rightarrow> c \<sim>\<^bsub>env\<^sub>s\<^esub> c"

(* Continuous Dependent SIFUM-security
   (where we don't care about whether the prog has any assumptions at termination *)
definition 
  prog_sifum_secure_cont :: "('Com \<times> ('Var,'Val) Env) list \<Rightarrow> bool"
where "prog_sifum_secure_cont cmds =
   (\<forall> mem\<^sub>1 mem\<^sub>2. INIT mem\<^sub>1 \<and> INIT mem\<^sub>2 \<and> mem\<^sub>1 =\<^sup>l mem\<^sub>2 \<longrightarrow>
    (\<forall> sched cms\<^sub>1' mem\<^sub>1'.
     (cmds, mem\<^sub>1) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>1', mem\<^sub>1') \<longrightarrow>
      (\<exists> cms\<^sub>2' mem\<^sub>2'. (cmds, mem\<^sub>2) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>2', mem\<^sub>2') \<and>
                      map snd cms\<^sub>1' = map snd cms\<^sub>2' \<and>
                      length cms\<^sub>2' = length cms\<^sub>1' \<and>
                      (\<forall> x. dma mem\<^sub>1' x = Low \<and> (x \<in> \<C> \<or> (\<forall> i < length cms\<^sub>1'.
                        x \<notin> (AsmNoRW \<circ> snd) (cms\<^sub>1' ! i))) \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x))))"

(* Note that it is equivalent to the following because the 
   programming language is deterministic *)
lemma prog_sifum_secure_cont_def2:
  "prog_sifum_secure_cont cmds \<equiv>
   (\<forall> mem\<^sub>1 mem\<^sub>2. INIT mem\<^sub>1 \<and> INIT mem\<^sub>2 \<and> mem\<^sub>1 =\<^sup>l mem\<^sub>2 \<longrightarrow>
    (\<forall> sched cms\<^sub>1' mem\<^sub>1'.
     (cmds, mem\<^sub>1) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>1', mem\<^sub>1') \<longrightarrow>
      (\<exists> cms\<^sub>2' mem\<^sub>2'. (cmds, mem\<^sub>2) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>2', mem\<^sub>2')) \<and>
      (\<forall> cms\<^sub>2' mem\<^sub>2'. (cmds, mem\<^sub>2) \<rightarrow>\<^bsub>sched\<^esub> (cms\<^sub>2', mem\<^sub>2') \<longrightarrow>
                      map snd cms\<^sub>1' = map snd cms\<^sub>2' \<and>
                      length cms\<^sub>2' = length cms\<^sub>1' \<and>
                      (\<forall> x. dma mem\<^sub>1' x = Low \<and> (x \<in> \<C> \<or> (\<forall> i < length cms\<^sub>1'.
                        x \<notin> (AsmNoRW \<circ> snd) (cms\<^sub>1' ! i))) \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x))))"
  apply(rule eq_reflection)
  unfolding prog_sifum_secure_cont_def
  apply(rule iffI)
   apply(blast dest: meval_sched_det)
  by fastforce

subsection \<open> Sound Mode Use \<close>


(* Suggestive notation for substitution / function application *)
definition 
  subst :: "('a \<rightharpoonup> 'b) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('a \<Rightarrow> 'b)"
where
  "subst f mem = (\<lambda> x. case f x of
                         None \<Rightarrow> mem x |
                         Some v \<Rightarrow> v)"

abbreviation 
  subst_abv :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a \<rightharpoonup> 'b) \<Rightarrow> ('a \<Rightarrow> 'b)" ("_ [\<mapsto>_]" [900, 0] 1000)
where
  "f [\<mapsto> \<sigma>] \<equiv> subst \<sigma> f"

lemma subst_not_in_dom : "\<lbrakk> x \<notin> dom \<sigma> \<rbrakk> \<Longrightarrow> mem [\<mapsto> \<sigma>] x = mem x"
  by (simp add: domIff subst_def)

(* Note: we restrict not reading to also imply not modifying a variable.
         This is done mostly to simplify part of the reasoning in the
         Compositionality theory, but also because reconciling doesnt_read_or_modify
         forcing also not reading the variable's classification in the case
         in the case where it would allow non-reading modifications proved
         to get too unwieldy *)
(* Note: the following definition also rules out the case in which a command
         appears not to read a variable because the variable's value causes
         the command to be stuck.*)
definition 
  doesnt_read_or_modify_vars :: "'Com \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> 'Var set \<Rightarrow> bool"
where
  "doesnt_read_or_modify_vars c mem X = (\<forall> mds c' mds' mem'.
  (\<forall>\<sigma>\<^sub>1 \<sigma>\<^sub>2. dom \<sigma>\<^sub>1 = X \<and> dom \<sigma>\<^sub>2 = X \<longrightarrow> 
  \<langle> c, mds, subst \<sigma>\<^sub>1 mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle> \<longrightarrow>
  \<langle> c, mds, subst \<sigma>\<^sub>2 mem \<rangle> \<leadsto> \<langle> c', mds', subst \<sigma>\<^sub>2 mem'\<rangle>))"

lemma subst_twice:
  "dom \<sigma> \<supseteq> dom \<sigma>' \<Longrightarrow> subst \<sigma> (subst \<sigma>' mem) = (subst  \<sigma> mem)"
  apply(clarsimp simp: subst_def intro!: ext split: option.splits dest: subsetD domI)
  by blast

text \<open> An essential property for @{term doesnt_read_or_modify_vars} to satisfy. \<close>
lemma doesnt_read_or_modify_vars_subst_pres:
  assumes nrw: "doesnt_read_or_modify_vars c mem X"
  assumes dom\<sigma>: "dom \<sigma> \<subseteq> X"
  shows "doesnt_read_or_modify_vars c (subst \<sigma> mem) X"
  using assms by(auto simp: doesnt_read_or_modify_vars_def subst_twice)

(* The conclusion of this lemma was the previous definition of 
   doesnt_read_or_modify_vars but it turns out that doesn't give us the
   above property. *)
lemma doesnt_read_or_modify_vars_legacy_imp:
  assumes nrw: "doesnt_read_or_modify_vars c mem X"
  shows "(\<forall> mds c' mds' mem'.
  \<langle> c, mds, mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle> \<longrightarrow>
  (\<forall>\<sigma>. dom \<sigma> = X \<longrightarrow> \<langle> c, mds, subst \<sigma> mem \<rangle> \<leadsto> \<langle> c', mds', subst \<sigma> mem'\<rangle>))"
proof(intro allI impI)
  fix env c' env' mem' 
  fix \<sigma>::"'Var \<Rightarrow> 'Val option"
  assume step: "\<langle>c, env, mem\<rangle> \<leadsto> \<langle>c', env', mem'\<rangle>" and dom\<sigma>: "dom \<sigma> = X"
  define \<sigma>\<^sub>1 where "\<sigma>\<^sub>1\<equiv> \<lambda>v. if v \<in> X then Some (mem v) else None"
  have "subst \<sigma>\<^sub>1 mem = mem" and "dom \<sigma>\<^sub>1 = X"
    by(auto simp: subst_def \<sigma>\<^sub>1_def split: if_splits)
  with step dom\<sigma> nrw show "\<langle>c, env, subst \<sigma> mem\<rangle> \<leadsto> \<langle>c', env', subst \<sigma> mem'\<rangle>"
    unfolding doesnt_read_or_modify_vars_def
    by presburger
qed

definition
  vars_\<C> :: "'Var set \<Rightarrow> 'Var set"
where
  "vars_\<C> X \<equiv> \<Union>x\<in>X. \<C>_vars x"

lemma vars_\<C>_subset_\<C>:
  "vars_\<C> X \<subseteq> \<C>"
  by(auto simp: \<C>_def vars_\<C>_def)

definition
  vars_and_\<C> :: "'Var set \<Rightarrow> 'Var set"
where
  "vars_and_\<C> X \<equiv> X \<union> vars_\<C> X"

lemma vars_\<C>_mono:
  "X \<subseteq> Y \<Longrightarrow> vars_\<C> X \<subseteq> vars_\<C> Y"
  by(auto simp: vars_\<C>_def)

lemma vars_\<C>_Un:
  "vars_\<C> (X \<union> Y) = (vars_\<C> X \<union> vars_\<C> Y)"
  by(simp add: vars_\<C>_def)

lemma vars_\<C>_insert:
  "vars_\<C> (insert x Y) = (vars_\<C> {x}) \<union> (vars_\<C> Y)"
  apply(subst insert_is_Un)
  apply(rule vars_\<C>_Un)
  done

lemma vars_\<C>_empty[simp]:
  "vars_\<C> {} = {}"
  by(simp add: vars_\<C>_def)

lemma \<C>_vars_of_\<C>_vars_empty:
  "x \<in> \<C>_vars y \<Longrightarrow> \<C>_vars x = {}"
  apply(drule subsetD[OF \<C>_vars_subset_\<C>])
  apply(erule \<C>_vars_\<C>)
  done

lemma vars_and_\<C>_mono:
  "X \<subseteq> X' \<Longrightarrow> vars_and_\<C> X \<subseteq> vars_and_\<C> X'"
  apply(unfold vars_and_\<C>_def)
  apply(metis Un_mono vars_\<C>_mono)
  done

lemma doesnt_read_or_modify_nowrite:
  assumes d: "doesnt_read_or_modify_vars c mem X"
  assumes step: "\<langle> c, mds, mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle>"
  assumes inX: "v \<in> X"
  shows "mem v = mem' v"
proof -
  define \<sigma>\<^sub>X where "\<sigma>\<^sub>X \<equiv> \<lambda>v. (if v \<in> X then Some (mem v) else None)"
  have "dom \<sigma>\<^sub>X = X"
    unfolding \<sigma>\<^sub>X_def by (auto split: if_splits)
  with d step have step\<^sub>X: "\<langle>c, mds, subst \<sigma>\<^sub>X mem\<rangle> \<leadsto> \<langle>c', mds', subst \<sigma>\<^sub>X mem'\<rangle>"
    using doesnt_read_or_modify_vars_legacy_imp by blast
  have "subst \<sigma>\<^sub>X mem = mem"
    by(auto simp: subst_def \<sigma>\<^sub>X_def) 
  with step\<^sub>X have step\<^sub>X': "\<langle>c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', subst \<sigma>\<^sub>X mem'\<rangle>" by simp
  with step have "subst \<sigma>\<^sub>X mem' = mem'"
    using deterministic
    using old.prod.inject by blast
  with inX show ?thesis
    by(fastforce simp: subst_def \<sigma>\<^sub>X_def dest!: fun_cong split: option.splits if_splits)
qed

lemma doesnt_read_or_modify_vars_legacy_imp_subset:
  "doesnt_read_or_modify_vars c mem X \<Longrightarrow>
  (\<forall> mds c' mds' mem'.
     \<langle> c, mds, mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle> \<longrightarrow>
     (\<forall>\<sigma>. dom \<sigma> \<subseteq> X \<longrightarrow> \<langle> c, mds, mem [\<mapsto> \<sigma>] \<rangle> \<leadsto> \<langle> c', mds', mem' [\<mapsto> \<sigma>] \<rangle>))"
proof(intro impI allI)
  assume d: "doesnt_read_or_modify_vars c mem X"
  fix mds c' mds' mem' 
  fix \<sigma>::"'Var \<Rightarrow> 'Val option"
  assume step: "\<langle>c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle>"
  assume dom\<sigma>: "dom \<sigma> \<subseteq> X"

  from step dom\<sigma> show "\<langle>c, mds, subst \<sigma> mem\<rangle> \<leadsto> \<langle>c', mds', subst \<sigma> mem'\<rangle>"
  proof -
    define \<sigma>\<^sub>X where "\<sigma>\<^sub>X \<equiv> \<lambda>v. (if v \<in> X then 
                     (if v \<in> dom \<sigma> then \<sigma> v else Some (mem v))
                   else None)"
    have "dom \<sigma>\<^sub>X = X"
      unfolding \<sigma>\<^sub>X_def by (auto split: if_splits)
    with d step have step\<^sub>X: "\<langle>c, mds, subst \<sigma>\<^sub>X mem\<rangle> \<leadsto> \<langle>c', mds', subst \<sigma>\<^sub>X mem'\<rangle>"
      using doesnt_read_or_modify_vars_legacy_imp by blast
    from dom\<sigma> have "subst \<sigma>\<^sub>X mem = subst \<sigma> mem"
      by(fastforce simp: subst_def \<sigma>\<^sub>X_def split: option.splits)
    moreover from dom\<sigma> have "subst \<sigma>\<^sub>X mem' = subst \<sigma> mem'"
      using doesnt_read_or_modify_nowrite[OF d step]
      by(fastforce simp: subst_def \<sigma>\<^sub>X_def split: option.splits)
    ultimately show ?thesis using step\<^sub>X by simp
  qed
qed

(* Note: doesnt_read_or_modify now implies that the classification is not read too *)
definition 
  doesnt_read_or_modify :: "'Com \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> 'Var set \<Rightarrow> bool"
where
  "doesnt_read_or_modify c mem X \<equiv> doesnt_read_or_modify_vars c mem (vars_and_\<C> X)"

lemma doesnt_read_or_modify_subst_pres:
  assumes nrw: "doesnt_read_or_modify c mem X"
  assumes dom\<sigma>: "dom \<sigma> \<subseteq> X"
  shows "doesnt_read_or_modify c (subst \<sigma> mem) X"
  using assms doesnt_read_or_modify_vars_subst_pres vars_and_\<C>_def
  unfolding doesnt_read_or_modify_def
  by (metis inf_sup_aci(6) le_iff_sup)

definition 
  doesnt_modify :: "'Com \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> 'Var set \<Rightarrow> bool"
where
  "doesnt_modify c mem X = (\<forall> mds c' mds' mem' x. x \<in> X \<longrightarrow> (\<langle> c, mds, mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle>) \<longrightarrow>
                       mem x = mem' x \<and>  dma mem x = dma mem' x)"

lemma noread_nowrite:
  assumes step: "\<langle>c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle>"
  assumes noread: "(\<And>v. \<langle>c, mds, mem(x := v)\<rangle> \<leadsto> \<langle>c', mds', mem'(x := v)\<rangle>)"
  shows "mem x = mem' x"
proof -
  from noread have "\<langle>c, mds, mem(x := (mem x))\<rangle> \<leadsto> \<langle>c', mds', mem'(x := (mem x))\<rangle>"
    by blast
  hence "\<langle>c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'(x := (mem x))\<rangle>" by simp
  from step this have "mem' = mem'(x := (mem x))" by (blast dest: deterministic)
  hence "mem' x = (mem'(x := (mem x))) x" by(rule arg_cong)
  thus ?thesis by simp
qed

(* Note: not sure whether this implication should just be made explicit in the
         definition of doesnt_read_or_modify or not *)
lemma doesnt_read_or_modify_doesnt_modify:
  "doesnt_read_or_modify c mem X \<Longrightarrow> doesnt_modify c mem X"
  by(fastforce simp: doesnt_modify_def doesnt_read_or_modify_def vars_and_\<C>_def vars_\<C>_def  
               intro: doesnt_read_or_modify_nowrite dma_\<C>_vars)

(* Local reachability of local configurations: *)
inductive_set 
  loc_reach :: "('Com, 'Var, 'Val) LocalConf \<Rightarrow> ('Com, 'Var, 'Val) LocalConf set"
  for lc :: "(_, _, _) LocalConf"
where
  refl : "\<langle>fst (fst lc), snd (fst lc), snd lc\<rangle> \<in> loc_reach lc" |
  step : "\<lbrakk> \<langle>c', env', mem'\<rangle> \<in> loc_reach lc;
            \<langle>c', env', mem'\<rangle> \<leadsto> \<langle>c'', env'', mem''\<rangle> \<rbrakk> \<Longrightarrow>
          \<langle>c'', env'', mem''\<rangle> \<in> loc_reach lc" |
  mem_diff : "\<lbrakk> \<langle> c', env', mem' \<rangle> \<in> loc_reach lc;
                (\<forall> x. var_asm_not_written env' x \<longrightarrow> mem' x = mem'' x \<and> dma mem' x = dma mem'' x);
                (\<forall>R\<in> Relies env'. (mem',mem'') \<in> R) \<rbrakk> \<Longrightarrow>
              \<langle> c', env', mem'' \<rangle> \<in> loc_reach lc"

lemma loc_reach_refl:
  "lc \<in> loc_reach lc"
  using loc_reach.refl by auto

lemma neval_loc_reach:
  "neval lc' n lc'' \<Longrightarrow> lc' \<in> loc_reach lc \<Longrightarrow> lc'' \<in> loc_reach lc"
proof(induct rule: neval.induct)
  case (neval_0 x y)
    thus ?case by simp
next
  case (neval_S_n x y n z)
  from `x \<in> loc_reach lc` and `x \<leadsto> y` have "y \<in> loc_reach lc" 
    (* TODO: can we get rid of this case_tac nonsense? *)
    apply(case_tac x, rename_tac a b, case_tac a, clarsimp)
    apply(case_tac y, rename_tac c d, case_tac c, clarsimp)
    by(blast intro: loc_reach.step)
  thus ?case
    using neval_S_n(3) by blast
qed

definition
  respects_guarantee_rel :: "'Com \<Rightarrow> ('Var,'Val) Env \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> ('Var,'Val) Mem rel \<Rightarrow> bool"
where
  "respects_guarantee_rel c env mem R = (\<forall> c' env' mem'. (\<langle> c, env, mem \<rangle> \<leadsto> \<langle> c', env', mem' \<rangle>) \<longrightarrow>
                        (mem,mem') \<in> R)"

definition
  rel_ignores_vars :: "('Var,'Val) Mem rel \<Rightarrow> 'Var set \<Rightarrow> 'Var set \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> bool"
where
  "rel_ignores_vars R X Y mem\<^sub>1 mem\<^sub>2 \<equiv> (mem\<^sub>1,mem\<^sub>2) \<in> R \<longrightarrow>
      (\<forall>mem\<^sub>1' mem\<^sub>2'. (\<forall>x. x \<notin> X \<longrightarrow> mem\<^sub>1 x = mem\<^sub>1' x) \<and> (\<forall>y. y \<notin> Y \<longrightarrow> mem\<^sub>2 y = mem\<^sub>2' y) \<longrightarrow> (mem\<^sub>1',mem\<^sub>2') \<in> R)"

text \<open>
  We add the novel condition that all assumption relations must ignore all
  variables that the component is guaranteeing not to read. We do this because, otherwise, 
  the assumption relation can have the other components' behaviours depend on the value of the 
  variable the current component is supposedly promising not to read --- a clear contradiction of 
  the spirit of the assumption.
  (Also it's needed for the compositionality proof...)
  \<close>
definition 
  locally_sound_env_use :: "(_, _, _) LocalConf \<Rightarrow> bool"
where
  "locally_sound_env_use lc =
  (\<forall> c' env' mem'. \<langle> c', env', mem' \<rangle> \<in> loc_reach lc \<longrightarrow>
    doesnt_read_or_modify c' mem' (GuarNoRW env') \<and>
    doesnt_modify c' mem' (GuarNoW env') \<and>
    (\<forall>R \<in> Guars env'. respects_guarantee_rel c' env' mem' R))"

lemma respects_guarantee_rel_Inter:
  "(\<forall>R\<in>S. respects_guarantee_rel c env mem R) = respects_guarantee_rel c env mem (\<Inter>S)"
  by(auto simp: respects_guarantee_rel_def)

lemma respects_guarantee_rel_mono:
  "R \<subseteq> R' \<Longrightarrow> respects_guarantee_rel c env mem R \<Longrightarrow> respects_guarantee_rel c env mem R'"
  by(auto simp: respects_guarantee_rel_def)

definition 
  compatible_modes :: "('Var,'Val) Env list \<Rightarrow> bool"
where
  "compatible_modes mdss = (\<forall> (i :: nat) x. i < length mdss \<longrightarrow>
    (x \<in> AsmNoRW (mdss ! i) \<longrightarrow>
     (\<forall> j < length mdss. j \<noteq> i \<longrightarrow> x \<in> GuarNoRW (mdss ! j))) \<and>
    (x \<in> AsmNoW (mdss ! i) \<longrightarrow>
     (\<forall> j < length mdss. j \<noteq> i \<longrightarrow> x \<in> GuarNoW (mdss ! j))))"

definition
  compatible_agrels :: "('Var,'Val) Env list \<Rightarrow> bool"
where
  "compatible_agrels agrels \<equiv> \<forall>i < length agrels. 
      (\<forall>R \<in> Relies (agrels ! i). 
           (\<forall>j < length agrels. j \<noteq> i \<longrightarrow> R \<supseteq> \<Inter> (Guars (agrels ! j))))"

lemma compatible_agrels_def2:
  "compatible_agrels agrels = (\<forall>i < length agrels.
           (\<forall>j < length agrels. j \<noteq> i \<longrightarrow> \<Inter> (Relies (agrels ! i)) \<supseteq> \<Inter> (Guars (agrels ! j))))"
  apply(clarsimp simp: compatible_agrels_def)
  apply auto
  done

(* Note: this is what the definition of compatible_agrels used to look like.
   this lemma is here as a sanity check to make sure that when we generalised it,
   we only made it weaker (i.e. easier to establish) *)
lemma compatible_agrels_legacy_imp:
  "\<forall>i < length agrels. 
      (\<forall>R \<in> Relies (agrels ! i). 
           (\<forall>j < length agrels. j \<noteq> i \<longrightarrow> R \<in> Guars (agrels ! j))) \<Longrightarrow>
  compatible_agrels agrels"
  by(auto simp: compatible_agrels_def)

definition
  compatible_envs :: "('Var,'Val) Env list \<Rightarrow> bool"
where
  "compatible_envs envs \<equiv> 
     compatible_modes envs \<and> compatible_agrels envs"

definition 
  reachable_envs :: "('Com, 'Var, 'Val) GlobalConf \<Rightarrow> ((('Var,'Val) Env) list) set"
where 
  "reachable_envs gc \<equiv>
     {mdss. (\<exists> cms' mem' sched. gc \<rightarrow>\<^bsub>sched\<^esub> (cms', mem') \<and> map snd cms' = mdss)}"

definition
  not_readable_vars :: "('Var,'Val) Env list \<Rightarrow> 'Var set"
where
  "not_readable_vars envs \<equiv> \<Union> (set (map AsmNoRW envs))"

(* FIXME: name this thing *)
definition
  side_condition :: "('Com, 'Var, 'Val) GlobalConf \<Rightarrow> bool"
where
  "side_condition gc \<equiv> 
      \<forall>sched k cms mem cms' mem'. (gc \<rightarrow>\<^bsub>sched\<^esub> (cms,mem)) \<and> ((cms,mem) \<leadsto>\<^bsub>k\<^esub> (cms',mem')) \<longrightarrow>
          (\<forall>i<length cms. (\<forall>R \<in> Relies (snd (cms ! i)). rel_ignores_vars R (not_readable_vars (map snd cms)) (not_readable_vars (map snd cms')) mem mem'))"
 

definition 
  globally_sound_env_use :: "('Com, 'Var, 'Val) GlobalConf \<Rightarrow> bool"
where 
  "globally_sound_env_use gc \<equiv>
     (\<forall> envs. envs \<in> reachable_envs gc \<longrightarrow> compatible_envs envs) \<and>
     side_condition gc"

primrec                         
  sound_env_use :: "(_, _, _) GlobalConf \<Rightarrow> bool"
where
  "sound_env_use (cms, mem) =
     (list_all (\<lambda> cm. locally_sound_env_use (cm, mem)) cms \<and>
      globally_sound_env_use (cms, mem))"

(* We now show that mm_equiv itself forms a strong low bisimulation modulo modes: *)
(*lemma mm_equiv_sym:
  assumes equivalent: "\<langle>c\<^sub>1, mds\<^sub>1, mem\<^sub>1\<rangle> \<approx> \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>"
  shows "\<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle> \<approx> \<langle>c\<^sub>1, mds\<^sub>1, mem\<^sub>1\<rangle>"
proof -
  from equivalent obtain \<R>
    where \<R>_bisim: "strong_low_bisim_mm \<R> \<and> (\<langle>c\<^sub>1, mds\<^sub>1, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>) \<in> \<R>"
    by (metis mm_equiv.simps)
  hence "sym \<R>"
    by (auto simp: strong_low_bisim_mm_def)
  hence "(\<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>, \<langle>c\<^sub>1, mds\<^sub>1, mem\<^sub>1\<rangle>) \<in> \<R>"
    by (metis \<R>_bisim symE)
  thus ?thesis
    by (metis \<R>_bisim mm_equiv.intros)
qed

lemma low_indistinguishable_sym: "lc \<sim>\<^bsub>mds\<^esub> lc' \<Longrightarrow> lc' \<sim>\<^bsub>mds\<^esub> lc"
  apply(clarsimp simp: low_indistinguishable_def)
  apply(rule mm_equiv_sym)
  apply(blast dest: low_mds_eq_sym)
  done*)

lemma mm_equiv_glob_consistent: "closed_glob_consistent mm_equiv"
  unfolding closed_glob_consistent_def
  apply clarify
  apply (erule mm_equiv_elim)
  by (auto simp: strong_low_bisim_mm_def closed_glob_consistent_def)

lemma mm_equiv_strong_low_bisim: "strong_low_bisim_mm mm_equiv"
  unfolding strong_low_bisim_mm_def
proof (clarsimp | safe)+
  show "closed_glob_consistent mm_equiv" by (rule mm_equiv_glob_consistent)
next
  fix c\<^sub>1 env mem\<^sub>1 c\<^sub>2 mem\<^sub>2 x
  assume "\<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle> \<approx> \<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle>"
  then obtain \<R> where
    "strong_low_bisim_mm \<R> \<and> (\<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle>, \<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle>) \<in> \<R>"
    by blast
  thus "mem\<^sub>1 =\<^bsub>env\<^esub>\<^sup>l mem\<^sub>2" by (auto simp: strong_low_bisim_mm_def)
next
  fix c\<^sub>1 :: 'Com
  fix env mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' env' mem\<^sub>1'
  let ?lc\<^sub>1 = "\<langle> c\<^sub>1, env, mem\<^sub>1 \<rangle>" and
      ?lc\<^sub>1' = "\<langle> c\<^sub>1', env', mem\<^sub>1' \<rangle>" and
      ?lc\<^sub>2 = "\<langle> c\<^sub>2, env, mem\<^sub>2 \<rangle>"
  assume "?lc\<^sub>1 \<approx> ?lc\<^sub>2"
  then obtain \<R> where "strong_low_bisim_mm \<R> \<and> (?lc\<^sub>1, ?lc\<^sub>2) \<in> \<R>"
    by (rule mm_equiv_elim, blast)
  moreover assume "?lc\<^sub>1 \<leadsto> ?lc\<^sub>1'"
  ultimately show "\<exists> c\<^sub>2' mem\<^sub>2'. ?lc\<^sub>2 \<leadsto> \<langle> c\<^sub>2', env', mem\<^sub>2' \<rangle> \<and> ?lc\<^sub>1' \<approx> \<langle> c\<^sub>2', env', mem\<^sub>2' \<rangle>"
    by (simp add: strong_low_bisim_mm_def, blast)
qed


inductive
  step_other :: "('Com, 'Var, 'Val) LocalConf \<Rightarrow> ('Com, 'Var, 'Val) LocalConf \<Rightarrow> bool"
where
  mem_diff : "\<lbrakk> (\<forall> x. var_asm_not_written env' x \<longrightarrow> mem' x = mem'' x \<and> dma mem' x = dma mem'' x);
                (\<forall>R \<in> Relies env'. (mem',mem'') \<in> R) \<rbrakk> \<Longrightarrow>
              step_other \<langle>c', env', mem'\<rangle> \<langle> c', env', mem'' \<rangle>"

lemma lc_expand:
  "lc = \<langle>fst (fst lc), snd (fst lc), snd lc\<rangle>" by simp


(* cast loc_reach as reflexive transitive closure, so we can do induction in both "directions" 
   on it *)
lemma loc_reach_as_rtranclp:
  "(\<langle>c', env', mem'\<rangle> \<in> loc_reach lc) = rtranclp (\<lambda>x y. x \<leadsto> y \<or> step_other x y) lc \<langle>c', env', mem'\<rangle>"
proof
  assume "\<langle>c', env', mem'\<rangle> \<in> loc_reach lc"
  thus "(\<lambda>x y. x \<leadsto> y \<or> step_other x y)\<^sup>*\<^sup>* lc \<langle>c', env', mem'\<rangle>"
  proof(induct rule: loc_reach.induct)
    case refl
    show ?case
      using rtranclp.rtrancl_refl by auto
  next
    case step
    thus ?case 
      using rtranclp.rtrancl_into_rtrancl
      by (metis (no_types, lifting))
  next
    case mem_diff
    thus ?case 
      using rtranclp.rtrancl_into_rtrancl step_other.intros
      by (metis (no_types, lifting))
  qed
next
  assume "(\<lambda>x y. x \<leadsto> y \<or> step_other x y)\<^sup>*\<^sup>* lc \<langle>c', env', mem'\<rangle>"
  thus "\<langle>c', env', mem'\<rangle> \<in> loc_reach lc"
  proof(induct rule: rtranclp.induct)
    case (rtrancl_refl lc)
    show ?case using loc_reach.refl by auto
  next
    case (rtrancl_into_rtrancl lc lc' lc'')
    from rtrancl_into_rtrancl(3) show ?case
    proof
      assume "lc' \<leadsto> lc''"
      thus "lc'' \<in> loc_reach lc"
        using rtrancl_into_rtrancl(2) loc_reach.step lc_expand fst_conv snd_conv by metis
    next
      assume "step_other lc' lc''"
      hence [simp]: "fst (fst lc'') = fst (fst lc')" "snd (fst lc'') = snd (fst lc')"
        using lc_expand step_other.cases fst_conv by metis+
      thus "lc'' \<in> loc_reach lc"
        apply(subst lc_expand)
        apply(rule loc_reach.mem_diff)
          using rtrancl_into_rtrancl(2) apply clarsimp
          apply(subst (asm) surjective_pairing[where t="lc'"], assumption)
         using `step_other lc' lc''`
         apply -
         apply(erule step_other.cases)
         apply fastforce
        using `step_other lc' lc''`
        apply -
        apply(erule step_other.cases)
        apply fastforce
        done
    qed
  qed
qed

end

end
