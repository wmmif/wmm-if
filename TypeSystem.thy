(*
Title: Data-dependent security on weak memory models
Authors: Graeme Smith, Nicholas Coughlin, Toby Murray
Based on Covern, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
In turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewenv, Heiko Mantel, Daniel Schoepe
*)
section \<open> Type System for Ensuring Dependent SIFUM-Security of Commands for the Lock Language \<close>


theory TypeSystem
  imports Main Compositionality WeakMemoryLanguage WeakMemoryFlow 
begin

subsection \<open> Analysis State \<close>

text \<open>
  Types now depend on memories. To see why, consider an assignment in which some variable
  @{term x} for which we have a @{term AsmNoReadOrWrite} assumption is assigned the value in 
  variable @{term input}, but where @{term input}'s classification depends on some control
  variable. Then the new type of @{term x} depends on memory. If we were to just take the 
  upper bound of @{term input}'s classification, this would likely give us @{term High} as
  @{term x}'s type, but that would prevent us from treating @{term x} as @{term Low}
  if we later learn @{term input}'s original classification. 
   
  Instead we need to make @{term x}'s type explicitly depend on memory so later on, once we
  learn @{term input}'s classification, we can resolve @{term x}'s type to a concrete
  security level.
  
  We choose to deeply embed types as predicates. If the predicate
  evaluates to @{term True}, the type is @{term Low}; otherwise it is @{term High}.
\<close>
type_synonym ('Var,'Val) Type = "('Var,'Val) lpred"

text \<open>
  We require @{term \<Gamma>} to track all stable (i.e. @{term AsmNoWrite} or @{term AsmNoReadOrWrite}), 
  non-@{term \<C>} variables.
  
  This differs from Mantel a bit. Mantel would exclude from @{term \<Gamma>}, variables
  whose classification (according to @{term dma}) is @{term Low} for which we have only
  an @{term AsmNoWrite} assumption.
  
  We decouple the requirement for inclusion in @{term \<Gamma>} from a variable's classification
  so that we don't need to be updating @{term \<Gamma>} each time we alter a control variable.
  Even if we tried to keep @{term \<Gamma>} up-to-date in that case, we may not be able to 
  precisely compute the new classification of each variable after the modification anyway.

  This has been changed to map to classifications, rather than predicates.
  To avoid significant modifications if this limit is later removed, the data type still supports
  types, but has been limited via a well-formedness property.
\<close>
type_synonym ('Var,'Val) TyEnv = "'Var \<rightharpoonup> Sec"

text \<open>
  Known predicates at a program point.
  Simply takes the Conj of predicates to join them.
\<close>
type_synonym 'pred preds = "'pred"

text  \<open>
  Stability information used to limit predicates and \<Gamma>.
  This may be extended upon at a later date, so accesses are limited through a series of
  functions.
\<close>
datatype 'Var Stable = Stable "'Var set" "'Var set"

datatype 'var ActionInfo = Write 'var | Read 'var

print_locale weak_mem_flow

locale sifum_types =
  sifum_lang_no_dma ev\<^sub>A ev\<^sub>B aexp_vars bexp_vars aexp_replace bexp_replace bexp_neg aexp_to_lexp bexp_to_lpred +
  weak_mem_flow_no_assm writes reads limit\<^sub>w limit\<^sub>r D_gen D_kill UNIV forwardable  +
  sifum_security_init_no_det dma \<C>_vars \<C> eval\<^sub>w 
  for ev\<^sub>A :: "('Var::{linorder, finite}, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  and ev\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  and aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  and bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  and aexp_replace :: "'AExp \<Rightarrow> 'Var \<Rightarrow> 'AExp \<Rightarrow> 'AExp"
  and bexp_replace :: "'BExp \<Rightarrow> 'Var \<Rightarrow> 'AExp \<Rightarrow> 'BExp"
  and aexp_to_lexp :: "'AExp \<Rightarrow> ('Var,'Val) lexp"
  and bexp_to_lpred :: "'BExp \<Rightarrow> ('Var,'Val) lpred"
  and bexp_neg :: "'BExp \<Rightarrow> 'BExp"
  and dma :: "('Var ,'Val) Mem \<Rightarrow> 'Var \<Rightarrow> Sec"
  and \<C>_vars :: "'Var  \<Rightarrow> 'Var set"
  and \<C> :: "'Var set" +
  fixes dma_type :: "'Var \<Rightarrow> 'BExp"
  fixes P\<^sub>0 :: "('Var,'Val) lpred"
  fixes \<Gamma>\<^sub>0 :: "('Var,'Val) TyEnv"
  fixes mds\<^sub>0 :: "(Mode \<Rightarrow> ('Var) set)"
  assumes dma_correct: "\<And>x. dma mem x = (if ev\<^sub>B mem (dma_type x) then Low else High)"
  assumes \<C>_vars_correct: "\<C>_vars x = bexp_vars (dma_type x)"
  assumes initP: "INIT mem \<longrightarrow> lpred_eval mem P\<^sub>0"
  assumes onlyLocalsP: "lpred_vars P\<^sub>0 \<subseteq> (mds\<^sub>0 AsmNoReadOrWrite) \<union> (mds\<^sub>0 AsmNoWrite)"
  assumes localsProps: "locals \<subseteq> mds\<^sub>0 AsmNoReadOrWrite \<and> locals \<inter> \<C> = {}"
  assumes valid_\<Gamma>\<^sub>0_dom: "dom \<Gamma>\<^sub>0 = (mds\<^sub>0 AsmNoReadOrWrite \<union> mds\<^sub>0 AsmNoWrite) - \<C>"
  assumes valid_\<Gamma>\<^sub>0: "\<forall>x \<in> dom \<Gamma>\<^sub>0. 
                      (the (\<Gamma>\<^sub>0 x) = Low \<longrightarrow> (INIT mem\<^sub>1 \<and> INIT mem\<^sub>2 \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x)) \<and>
                      (the (\<Gamma>\<^sub>0 x) > dma mem\<^sub>1 x \<longrightarrow> x \<in> mds\<^sub>0 AsmNoReadOrWrite)"

sublocale sifum_types \<subseteq> sifum_security_init dma \<C>_vars \<C> eval\<^sub>w
  by (unfold_locales , auto simp: eval\<^sub>w_det)

sublocale sifum_types \<subseteq> weak_mem_flow writes reads limit\<^sub>w limit\<^sub>r D_gen D_kill UNIV forwardable
  by (unfold_locales , auto simp: forward_restrict')

context sifum_types
begin

subsection \<open> Type Checking Definitions \<close>

text \<open>The following abbreviations define infix operators for definitions inherited from
      locales\<close>

abbreviation mm_equiv_abv2 ::
  "(_, _, _) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infix "\<approx>" 60)
  where
  "mm_equiv_abv2 c c' \<equiv> mm_equiv_abv c c'"

abbreviation eval_abv\<^sub>a ::
  "(_, 'Var , 'Val) LocalConf \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  ("_ \<leadsto>\<^sub>_ _" [11,0,0] 70)
  where
  "x \<leadsto>\<^sub>\<alpha> y \<equiv> (x, \<alpha>, y) \<in> eval\<^sub>a"

abbreviation eval_abv\<^sub>w ::
  "(_, 'Var , 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>" 60)
  where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval\<^sub>w"
 
abbreviation conf_det_abv ::
  "('Var,'AExp,'BExp) Stmt  \<Rightarrow> DetChoice list \<Rightarrow> ('Var ,'Val) Env \<Rightarrow> ('Var ,'Val) Mem  \<Rightarrow> (_,_,_,_) LC"
  ("\<langle>_, _, _, _\<rangle>" [0, 60, 120] 70)
  where
  "\<langle>c, det, env, mem\<rangle> \<equiv> (((c, det), env), mem)"

abbreviation low_indistinguishable_abv ::
  "('Var ,'Val) Env \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<times> DetChoice list \<Rightarrow> (_, _, _) Stmt \<times> DetChoice list \<Rightarrow> bool"
  ("_ \<sim>\<index> _" [100, 100] 80)
  where
  "c \<sim>\<^bsub>env\<^esub> c' \<equiv> low_indistinguishable env c c'"

abbreviation pred :: "(('Var,'Val) lpred) preds \<Rightarrow> ('Var ,'Val) Mem \<Rightarrow> bool"
  where
  "pred P \<equiv> \<lambda>mem. (lpred_eval mem P)"

text \<open>
  The following definitions establish various wellformedness properties maintained throughout
  the analysis.
\<close>

abbreviation NoW :: "'Var Stable \<Rightarrow> 'Var set"
  where
  "NoW \<S> \<equiv> case \<S> of Stable f s \<Rightarrow> f"

abbreviation NoRW :: "'Var Stable \<Rightarrow> 'Var set"
  where
  "NoRW \<S> \<equiv> case \<S> of Stable f s \<Rightarrow> s"

definition stable :: "'Var Stable \<Rightarrow> 'Var set"
  where
  "stable \<S> \<equiv> NoW \<S> \<union> NoRW \<S>"

abbreviation \<L> :: "'Var \<Rightarrow> ('Var,'Val) Type"
  where "\<L> x \<equiv> bexp_to_lpred (dma_type x)"

abbreviation type_stable :: "'Var Stable \<Rightarrow> ('Var,'Val) Type \<Rightarrow> bool"
  where "type_stable \<S> P \<equiv> lpred_vars P \<subseteq> stable \<S>"

abbreviation type_wellformed :: "('Var,'Val) Type \<Rightarrow> bool"
  where "type_wellformed t \<equiv> lpred_vars t \<subseteq> \<C>"

definition tyenv_wellformed ::
  "('Var ,'Val) Env \<Rightarrow> ('Var, 'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> bool"
  where
  "tyenv_wellformed env \<Gamma> \<S> P \<equiv>
    AsmNoW env = NoW \<S> \<and> AsmNoRW env = NoRW \<S> \<and> locals \<subseteq> dom \<Gamma> \<and> 
    dom \<Gamma> = stable \<S> - \<C> \<and> type_stable \<S> P"

text \<open>
  The following definitions describe operations to compare and determine
  types/predicates.
\<close>

definition subtype :: "('Var,'Val) Type \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) Type \<Rightarrow> bool"
  ("_ \<le>:\<^sub>_ _" [120, 120, 120] 1000)
  where "t \<le>:\<^sub>P t' \<equiv> P !\<turnstile> t' \<or> P \<turnstile> t"

definition sec
  where "sec t \<equiv> if t = Low then PTrue else PFalse"

definition to_total :: "('Var, 'Val) TyEnv \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) Type"
  where "to_total \<Gamma> \<equiv> \<lambda>v. if v \<in> dom \<Gamma> then sec (the (\<Gamma> v)) else \<L> v"

definition type_max :: "('Var,'Val) Type \<Rightarrow> ('Var ,'Val) Mem \<Rightarrow> Sec"
  where "type_max t mem \<equiv> if lpred_eval mem t then Low else High"

text \<open> Determine the type of the reads of an Action \<close>
fun read_type ::
  "('Var,'Val) TyEnv \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var,'Val) Type"
  ("_ :\<^sub>r _" [120, 120] 1000)
  where
    "read_type \<Gamma> \<alpha> = fold PConj (map (to_total \<Gamma>) (sorted_list_of_set (reads \<alpha>))) PTrue"

text \<open>
  Determine the type of the writes of an Action. Ignores writes to non-readable variables.

  The idea of a 'writes' of an Action is overloaded for Guards and Fences.
  Both of these are considered to have a low write type, preventing changes in control flow
  due to high reads.
\<close>
fun write_type :: "'Var Stable \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var,'Val) Type"
  ("_ :\<^sub>w _"  [120, 120] 1000)
  where
  "write_type \<S> (x \<leftarrow> e) = (if x \<in> NoRW \<S> - \<C> then PFalse else \<L> x)" |
  "write_type \<S> _ = PTrue"

text \<open>
  The following definitions are related to tracking predicates during type checking.
\<close>

fun P_upd ::
  "('Var,'Val) lpred preds \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds"
  ("_+[_]\<^sub>_" [120, 120, 120] 900)
where
  "P +[ x \<leftarrow> e ]\<^sub>\<S> = reassign_var x (aexp_to_lexp e) P \<restriction> stable \<S>" |
  "P +[ [b?] ]\<^sub>\<S> = PConj P (bexp_to_lpred b) \<restriction> stable \<S>" |
  "P +[ Fence ]\<^sub>\<S> = P \<restriction> stable \<S>"

fun \<Gamma>_upd ::
  "('Var,'Val) TyEnv \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var,'Val) TyEnv"
  ("_+\<^bsub>_\<^esub>[_]" [120, 120, 120] 1000)
where
  "\<Gamma>+\<^bsub>P\<^esub>[ x \<leftarrow> e ] = (if x \<in> dom \<Gamma> then (\<Gamma> (x := Some (if P \<turnstile> (\<Gamma> :\<^sub>r (x \<leftarrow> e)) then Low else High))) else \<Gamma>)" |
  "\<Gamma>+\<^bsub>_\<^esub>[_] = \<Gamma>"

subsection \<open> Type Checking Helper Lemmas \<close>

subsubsection \<open> Lemmas for WF Types \<close>

lemma type_wellformed_PConj[intro!]:
  "type_wellformed a \<Longrightarrow> type_wellformed b \<Longrightarrow> type_wellformed (PConj a b)"
  by (auto)

lemma type_wellformed_fold_PConj:
  "\<forall>x \<in> set xs. type_wellformed x \<Longrightarrow> type_wellformed y \<Longrightarrow> type_wellformed (fold PConj xs y)"
  by (induct xs arbitrary: y, auto)

lemma [simp, intro!]:
  "type_wellformed PTrue"
  by (auto)

text \<open>
  Define a sufficient condition for a type to be stable, assuming the type is wellformed.
  
  We need this because there is no point tracking the fact that e.g. variable @{term x}'s data has
  a classification that depends on some control variable @{term c} (where @{term c} might be
  the control variable for some other variable @{term y} whose value we've just assigned to
  @{term x}) if @{term c} can then go and be modified, since now the classification of
  the data in @{term x} no longer depends on the value of @{term c}, instead it depends on
  @{term c}'s \emph{old} value, which has now been lost.
  
  Therefore, if a type depends on @{term c}, then @{term c} had better be stable.
\<close>

lemma type_stable_is_sufficient:
  "\<lbrakk>type_stable \<S> t\<rbrakk> \<Longrightarrow>
  \<forall>mem mem'. (\<forall>x \<in> stable \<S>. mem x = mem' x) \<longrightarrow> (lpred_eval mem) t = (lpred_eval mem') t"
  by (auto intro!: lpred_eval_vars_det)

lemma type_stable_is_sufficient':
  "\<lbrakk>type_stable \<S> t\<rbrakk> \<Longrightarrow>
  \<forall>mem mem'. (\<forall>x \<in> stable \<S>. mem x = mem' x) \<longrightarrow> type_max t mem = type_max t mem'"
  using type_stable_is_sufficient
  unfolding type_max_def image_def 
  by (metis (no_types, lifting))

subsubsection \<open> Lemmas for \<Gamma>_upd \<close>

lemma \<Gamma>_upd_dom:
  shows "dom (\<Gamma>+\<^bsub>P\<^esub>[\<alpha>]) = dom \<Gamma>"
  by (cases \<alpha>; auto split: if_splits)

lemma \<Gamma>_upd_nop:
  shows "y \<notin> writes \<alpha> \<Longrightarrow> \<Gamma>+\<^bsub>P\<^esub>[\<alpha>] y = \<Gamma> y"
  by (cases \<alpha>; auto split: if_splits)

subsubsection \<open> Lemmas for Expression Types \<close>

lemma read_type_det:
  assumes same: "\<forall>y \<in> reads \<alpha>. \<Gamma> y = \<Gamma>' y"
  shows "\<Gamma> :\<^sub>r \<alpha> = \<Gamma>' :\<^sub>r \<alpha>"
  using assms by (auto intro!: fold_cong simp: to_total_def split: if_splits)

lemma read_type_all_low:
  assumes "pred (\<Gamma> :\<^sub>r \<alpha>) mem"
  shows "\<forall>x \<in> reads \<alpha>. pred (to_total \<Gamma> x) mem"
proof -
  have "\<forall>P \<in> set (map (to_total \<Gamma>) (sorted_list_of_set (reads \<alpha>))). pred P mem"
    using assms pred_fold_PConj read_type.simps by metis
  thus ?thesis by auto
qed

lemma read_type_all_low_entailment:
  shows "\<forall>x \<in> reads \<alpha>. \<Gamma> :\<^sub>r \<alpha> \<turnstile> to_total \<Gamma> x"
  unfolding pred_entailment_def using read_type_all_low by auto

lemma read_type_entailment:
  assumes "\<forall>x \<in> reads \<alpha>. P \<turnstile> to_total \<Gamma> x"
  shows "P \<turnstile> \<Gamma> :\<^sub>r \<alpha>"
  unfolding pred_entailment_def
proof (intro allI impI)
  fix mem assume a: "pred P mem"
  let ?Ps = "map (to_total \<Gamma>) (sorted_list_of_set (reads \<alpha>))"
  have "\<forall>P \<in> set ?Ps. lpred_eval mem P"
    using assms a unfolding pred_entailment_def by simp
  thus "pred (\<Gamma> :\<^sub>r \<alpha>) mem" by (simp add: predicate_lang.pred_fold_PConj')
qed

lemma read_locals:
  assumes "reads \<alpha> \<subseteq> dom \<Gamma>"
  shows "\<Gamma> :\<^sub>r \<alpha> =\<^sub>P PTrue \<or> \<Gamma> :\<^sub>r \<alpha> =\<^sub>P PFalse"
proof (cases "\<exists>x \<in> reads \<alpha>. \<Gamma> x = Some High")
  case True
  then obtain x where a: "x \<in> reads \<alpha>" "\<Gamma> x = Some High"
    by auto
  hence k: "to_total \<Gamma> x = PFalse"
    unfolding to_total_def sec_def by auto
  have "\<forall>mem. \<not> pred (\<Gamma> :\<^sub>r \<alpha>) mem"
  proof (intro allI)
    fix mem
    show "\<not> pred (\<Gamma> :\<^sub>r \<alpha>) mem"
      using a k pred_fold_PConj[of mem "(map (to_total \<Gamma>) (sorted_list_of_set (reads \<alpha>)))"  PTrue]
      by force
  qed
  hence "\<Gamma> :\<^sub>r \<alpha> =\<^sub>P PFalse" unfolding pred_equiv_def
    using pred_fold_PConj_eq a by fastforce
  then show ?thesis by auto
next
  case False
  hence "\<forall>x \<in> reads \<alpha>. \<Gamma> x = Some Low" unfolding Ball_def
  proof (intro allI impI, goal_cases)
    case (1 x)
    hence "\<Gamma> x \<noteq> Some High" by auto
    then obtain a where "\<Gamma> x = Some a" "a \<noteq> High"
      using 1 assms by (cases "\<Gamma> x"; auto)
    then show ?case by (cases a; auto)
  qed
  hence k: "\<forall>x \<in> reads \<alpha>. to_total \<Gamma> x = PTrue" 
    unfolding to_total_def sec_def by auto
  have "\<forall>mem. pred (\<Gamma> :\<^sub>r \<alpha>) mem"
  proof (intro allI)
    fix mem
    show "pred (\<Gamma> :\<^sub>r \<alpha>) mem"
      using k pred_fold_PConj'[of "(map (to_total \<Gamma>) (sorted_list_of_set (reads \<alpha>)))" mem PTrue]
      by simp
  qed
  hence "\<Gamma> :\<^sub>r \<alpha> =\<^sub>P PTrue" unfolding pred_equiv_def by auto    
  thus ?thesis by auto
qed

subsubsection \<open> Lemmas for Subtype \<close>

lemma subtype_sound:
  assumes "t \<le>:\<^sub>P t'"
  shows "\<forall>mem. pred P mem \<longrightarrow> type_max t mem \<le> type_max t' mem"
  unfolding type_max_def less_eq_Sec_def
proof (auto)
  fix mem
  assume t': "pred t' mem"
  assume P: "pred P mem"
  assume t: "\<not> pred t mem"
  have "lpred_eval mem t" using assms P t'
    unfolding subtype_def pred_entailment_def by auto
  thus "False" using t by auto
qed

lemma subtype_entailment:
  "t \<le>:\<^sub>P l \<Longrightarrow> P' \<turnstile> P \<Longrightarrow> t \<turnstile> t' \<Longrightarrow> t' \<le>:\<^sub>P' l"
  by (auto simp: subtype_def pred_entailment_def)

lemma subtype_trans:
  "t \<le>:\<^sub>P t' \<Longrightarrow> t' \<le>:\<^sub>P' t'' \<Longrightarrow> P \<turnstile> P' \<Longrightarrow> t \<le>:\<^sub>P t''"
  "t \<le>:\<^sub>P' t' \<Longrightarrow> t' \<le>:\<^sub>P t'' \<Longrightarrow> P \<turnstile> P' \<Longrightarrow> t \<le>:\<^sub>P t''"
  by (auto simp: pred_entailment_def subtype_def)

lemma subtypeI [intro]:
  shows "P \<turnstile> t \<Longrightarrow> t \<le>:\<^sub>P t'"
  by (auto split: if_splits simp: subtype_def pred_entailment_def)

lemma subtypeD [dest]:
  assumes "t \<le>:\<^sub>P PTrue"
  shows "P \<turnstile> t"
  using assms by (auto split: if_splits simp: subtype_def pred_entailment_def)

lemma subtypeIF [intro]:
  shows "P !\<turnstile> t' \<Longrightarrow> t \<le>:\<^sub>P t'"
  by (auto split: if_splits simp: subtype_def pred_entailment_def)

lemma subtypeDF [dest]:
  assumes "\<not> P !\<turnstile> t'"
  assumes "t \<le>:\<^sub>P t'"
  shows "P \<turnstile> t"
  using assms by (auto split: if_splits simp: subtype_def pred_entailment_def)

lemma subtypeConj:
  shows "(t1 \<le>:\<^sub>P t' \<and> t2 \<le>:\<^sub>P t') = (PConj t1 t2) \<le>:\<^sub>P t'"
  by (auto split: if_splits simp: subtype_def pred_entailment_def) 

subsubsection \<open> Lemmas for Predicates & Restrictions \<close>

lemma P_upd_entailment [intro]:
  shows "P \<turnstile> P' \<longrightarrow> P+[\<alpha>]\<^sub>\<S> \<turnstile> P'+[\<alpha>]\<^sub>\<S>"
proof (cases \<alpha>)
  case (Assign x e)
  thus ?thesis by (auto simp: lpred_restrict_entailment pred_entailment_mono_PConj reassign_var_entailment)
next
  case Fence
  thus ?thesis by auto
next
  case (Guard x3)
  thus ?thesis by (auto simp: lpred_restrict_entailment pred_entailment_mono_PConj)
qed

lemma P_upd_vars [simp]:
  shows "lpred_vars (P+[\<alpha>]\<^sub>\<S>) = (lpred_vars P \<union> vars \<alpha>) \<inter> (stable \<S>)"
proof (cases \<alpha>)
  case (Assign x e)
  thus ?thesis by (auto simp: Un_Int_distrib sup_commute aexp_to_lexp_vars)
next
  case Fence
  thus ?thesis by auto
next
  case (Guard x3)
  thus ?thesis by (auto simp: bexp_to_lpred_vars)
qed

lemma P_upd_stable:
  "lpred_vars (P+[\<beta>]\<^sub>\<S>) \<subseteq> stable \<S>"
  using P_upd_vars by auto

lemma P_upd_restrict_comm:
  assumes "vars \<alpha> \<subseteq> V"
  shows "(P \<restriction> V)+[\<alpha>]\<^sub>\<S> =\<^sub>P P+[\<alpha>]\<^sub>\<S> \<restriction> V"
proof (cases \<alpha>)
  case (Assign x e)
  hence "x \<in> V" "lexp_vars (aexp_to_lexp e) \<subseteq> V" using assms
    by (auto simp: aexp_to_lexp_vars)
  then show ?thesis using Assign 
    by (auto, meson lpred_restrict_comm lpred_restrict_equiv pred_equiv_sym pred_equiv_trans reassign_restrict_comm)
next
  case Fence
  then show ?thesis
    by (simp add: lpred_restrict_comm)
next
  case (Guard b)
  hence "lpred_vars (bexp_to_lpred b) \<subseteq> V" using assms
    by (simp add: bexp_to_lpred_vars)
  then show ?thesis using Guard
    by (auto, meson lpred_restrict_comm lpred_restrict_conj_eq_l lpred_restrict_equiv pred_equiv_sym pred_equiv_trans)
qed

lemma P_upd_restrict_writes:
  assumes stable: "type_stable \<S> P"
  assumes entail: "P' \<turnstile> P"
  shows "P'+[\<beta>]\<^sub>\<S> \<restriction> (-writes \<beta>) \<turnstile> P \<restriction> (-writes \<beta>)"  (is "?P' \<restriction> ?V \<turnstile> P \<restriction> ?V")
proof (cases \<beta>)
  case (Assign x e)
  let ?P = "P' \<restriction> stable \<S>"
  let ?R = "reassign_var x (aexp_to_lexp e) ?P"
  have "?P' \<restriction> ?V \<turnstile> ((?R \<restriction> stable \<S>) \<restriction> ?V)"
    using Assign pred_equiv_def by auto
  also have "... \<turnstile> ((?R \<restriction> ?V) \<restriction> stable \<S>)"
    using Assign pred_equiv_entail lpred_restrict_comm
    by blast
  also have "... \<turnstile> ((?P \<restriction> ?V) \<restriction> stable \<S>)"
  proof -
    have "?R \<restriction> ?V \<turnstile> ?P \<restriction> ?V" using Assign reassign_restrict_comm_same pred_equiv_entail
      by (metis subset_Compl_singleton subset_refl writes.simps(1))
    thus ?thesis using lpred_restrict_entailment by metis
  qed
  also have "... \<turnstile> (?P \<restriction> ?V)"
  proof -
    have "lpred_vars (?P \<restriction> ?V) \<subseteq> stable \<S>" using lpred_restrict_vars by auto
    thus ?thesis using lpred_restrict_nop pred_equiv_entail by auto
  qed
  also have "... \<turnstile> P \<restriction> ?V"
  proof -
    have "P' \<restriction> stable \<S> \<turnstile> P \<restriction> stable \<S>" using entail lpred_restrict_entailment by metis
    also have "... \<turnstile> P" using stable lpred_restrict_nop pred_equiv_entail by auto
    finally show ?thesis
      by (meson lpred_restrict_entailment pred_entailment_trans pred_equiv_def)
  qed
  finally show ?thesis by simp
next
  case Fence
  hence "?P' \<turnstile> P' \<restriction> stable \<S>" by auto
  also have "... \<turnstile> P \<restriction> stable \<S>" by (metis entail lpred_restrict_entailment)
  also have "... \<turnstile> P" using stable by auto
  finally show ?thesis using lpred_restrict_entailment by metis
next
  case (Guard b)
  hence "?P' \<turnstile> P' \<restriction> stable \<S>" by (metis lpred_restrict_conj_l P_upd.simps(2))
  also have "... \<turnstile> P \<restriction> stable \<S>" by (metis entail lpred_restrict_entailment)
  also have "... \<turnstile> P" using stable by auto
  finally show ?thesis using lpred_restrict_entailment by metis
qed

lemma reassign_diff_var:
  assumes stable: "type_stable \<S> P"
  assumes type: "P \<turnstile> t"
  assumes ovr: "writes \<beta> \<inter> lpred_vars t = {}"
  shows "P+[\<beta>]\<^sub>\<S> \<turnstile> t" (is "?P' \<turnstile> t")
proof -
  have "?P' \<turnstile> ?P' \<restriction> (-writes \<beta>)" using lpred_restrict_weakens by metis
  also have "... \<turnstile>  P \<restriction> (-writes \<beta>)"
    using  P_upd_restrict_writes [OF stable] pred_entailment_refl by meson
  also have "... \<turnstile>  t \<restriction> (- writes \<beta>)"
    using type lpred_restrict_entailment by meson
  also have "... \<turnstile>  t" using ovr by (simp add: Int_commute disjoint_eq_subset_Compl)   
  finally show ?thesis by simp
qed

lemma reassign_diff_var_restrict:
  assumes stable: "type_stable \<S> P"
  assumes type: "P \<restriction> V \<turnstile> t" (is "?P \<turnstile> t")
  assumes ovr: "writes \<beta> \<inter> lpred_vars t = {}"
  shows "P+[\<beta>]\<^sub>\<S> \<restriction> V \<turnstile> t" (is "?P' \<turnstile> t")
proof -
  have "?P' \<turnstile> (P+[\<beta>]\<^sub>\<S> \<restriction> (- writes \<beta>)) \<restriction> V" using lpred_restrict_weakens by blast
  also have "... \<turnstile> (P \<restriction> (- writes \<beta>)) \<restriction> V" 
    by (simp add: P_upd_restrict_writes lpred_restrict_entailment stable)
  also have "... \<turnstile> t"  using type ovr
    by (metis Int_commute disjoint_eq_subset_Compl lpred_restrict_comm lpred_restrict_entailment
              lpred_restrict_nop pred_entailment_trans pred_equiv_entail)
  finally show ?thesis by simp
qed

lemma P_upd_stable_equiv [simp]:
  "P+[\<beta>]\<^sub>\<S> \<restriction> stable \<S> = P+[\<beta>]\<^sub>\<S> "
  using lpred_restrict_nop P_upd_stable by auto

lemma P_upd_comm:
  assumes stable: "type_stable \<S> P"
  assumes re: "\<alpha> \<hookleftarrow> \<beta>"
  shows "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>]\<^sub>\<S> =\<^sub>P (P+[\<beta>]\<^sub>\<S>)+[\<alpha>]\<^sub>\<S>"
proof (cases \<alpha>)
  case (Assign x e)
  hence a: "\<alpha> = x \<leftarrow> e" by assumption
  then show ?thesis
  proof (cases \<beta>)
    case (Assign y f)
    have "x \<noteq> y" "x \<notin> lexp_vars (aexp_to_lexp f)" "y \<notin> lexp_vars (aexp_to_lexp e)"
      using reorder_diff_writes[OF re] by (auto simp: Assign a aexp_to_lexp_vars) 
    thus ?thesis using Assign stable a reassign_comm by auto
  next
    case Fence
    then show ?thesis using a stable by auto
  next
    case (Guard b)
    hence "x \<notin> lpred_vars (bexp_to_lpred b)" 
      using reorder_diff_writes[OF re] by (auto simp: a bexp_to_lpred_vars) 
    thus ?thesis using Guard a stable conj_assign_comm by auto
  qed
next
  case Fence
  then show ?thesis by (simp add: stable)
next
  case (Guard b)
  hence a: "\<alpha> = [b?]" by assumption
  then show ?thesis
  proof (cases \<beta>)
    case (Assign x e)
    moreover have "x \<notin> lpred_vars (bexp_to_lpred b)"
      using reorder_diff_writes[OF re] by (auto simp: Assign a bexp_to_lpred_vars) 
    ultimately show ?thesis using a assign_conj_comm stable by auto
  next
    case Fence
    then show ?thesis using a stable by auto
  next
    case (Guard b)
    then show ?thesis using a stable conj_comm by auto
  qed
qed

lemma forward_guard:
  shows "x \<notin> lexp_vars e \<Longrightarrow> PConj (reassign_var x e P) (lpred_subst b e x) =\<^sub>P PConj (reassign_var x e P) b"
  by (simp add: pred_equiv_def reassign_var_def, metis lpred_subst_eval_const lpred_subst_mem_upd)

lemma forward_reassign:
  shows "x \<noteq> y \<Longrightarrow> x \<notin> lexp_vars e \<Longrightarrow> reassign_var y (lexp_subst f e x) (reassign_var x e P) =\<^sub>P reassign_var y f (reassign_var x e P)"
  by (simp add: pred_equiv_def reassign_var_def, metis fun_upd_triv fun_upd_twist lexp_subst_mem_eval)

lemma P_upd_forward:
  assumes stable: "type_stable \<S> P"
  assumes locals: "locals \<subseteq> stable \<S>"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  shows "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> \<turnstile> (P+[\<alpha>]\<^sub>\<S>)+[\<beta>]\<^sub>\<S>"
  using re
proof (cases rule: forward_elim)
  case normal
  then show ?thesis by auto
next
  case forward
  obtain x e where \<alpha>: "\<alpha> = x \<leftarrow> e" using forward by (cases \<alpha>; auto)
  let ?e = "aexp_to_lexp e"

  have \<beta>': "\<beta>' = forward \<beta> \<alpha>" using re by auto
  have self: "x \<notin> lexp_vars ?e"
    using re forward_reads[OF forward(2)] aexp_to_lexp_vars 
    by (cases \<beta>; auto simp: \<beta>' \<alpha> min_reorder_def)

  show ?thesis
  proof (cases "x \<in> stable \<S>")
    case True
    hence s: "reassign_var x ?e P \<restriction> stable \<S> = reassign_var x ?e P"
      using forward locals \<alpha> aexp_to_lexp_vars stable by auto
    then show ?thesis
    proof (cases \<beta>)
      case (Assign y f)
      let ?f = "aexp_to_lexp f" and ?f' = "aexp_to_lexp (aexp_replace f x e)"
      have x: "x \<noteq> y" using Assign \<alpha> re forward_writes by (auto simp: min_reorder_def)
      have "reassign_var y ?f' (reassign_var x ?e P) =\<^sub>P reassign_var y ?f (reassign_var x ?e P)"
        using forward_reassign[OF x self] by (auto simp: aexp_replace_correct)
      then show ?thesis by (simp add: \<alpha> \<beta>' Assign s lpred_restrict_entailment pred_equiv_entail)
    next
      case Fence
      then show ?thesis using re by auto
    next
      case (Guard b)
      let ?b = "bexp_to_lpred b" and ?b' = "bexp_to_lpred (bexp_replace b x e)"
      have "PConj (reassign_var x ?e P) ?b' =\<^sub>P PConj (reassign_var x ?e P) ?b"
        using forward_guard[OF self] by (auto simp: bexp_replace_correct)
      thus ?thesis by (simp add: \<alpha> \<beta>' Guard s lpred_restrict_entailment pred_equiv_entail)
    qed
  next
    case False
    hence e: "P+[\<alpha>]\<^sub>\<S> =\<^sub>P P" (is "?P =\<^sub>P P")
      unfolding \<alpha> by (simp, metis lpred_restrict_nop reassign_restrict_comm_same stable)    
    then show ?thesis
    proof (cases \<beta>)
      case (Assign y f)
      let ?f = "aexp_to_lexp f" and ?f' = "aexp_to_lexp (aexp_replace f x e)"
      have p: "x \<notin> lpred_vars ?P" using False by auto
      have x: "x \<noteq> y" using Assign \<alpha> re forward_writes by (auto simp: min_reorder_def)
      have other: "y \<notin> lexp_vars ?e" using re forward \<alpha> Assign
        by (metis IntI UnCI aexp_to_lexp_vars insertI1 insert_absorb 
                  insert_not_empty reads.simps(2) reorder_diff_writes\<^sub>f(1) writes.simps(1))
      have "lpred_subst (reassign_var y ?f ?P) ?e x = reassign_var y (lexp_subst ?f ?e x) (lpred_subst ?P ?e x)"
        using reassign_subst_comm' other x by auto
      hence "reassign_var y ?f' ?P \<turnstile> lpred_subst (reassign_var y ?f ?P) (aexp_to_lexp e) x"
        using aexp_replace_correct p by auto
      also have "... \<turnstile> weaken_var x (reassign_var y ?f ?P)" using lpred_subst_weaken_var by metis
      finally have "reassign_var y ?f' ?P \<turnstile> reassign_var y ?f ?P \<restriction> (- {x})" by auto
      hence "reassign_var y ?f' ?P \<restriction> stable \<S> \<turnstile> (reassign_var y ?f ?P \<restriction> (- {x})) \<restriction> stable \<S>" by auto
      hence "reassign_var y ?f' ?P \<restriction> stable \<S> \<turnstile> reassign_var y ?f ?P \<restriction> stable \<S>" using False
        by (smt inf.absorb2 pred_entailment_trans pred_equiv_entail 
                lpred_restrict_inter subset_Compl_singleton)
      then show ?thesis unfolding \<alpha> Assign \<beta>' by auto
    next
      case Fence
      then show ?thesis using re by auto
    next
      case (Guard b)
      let ?b = "bexp_to_lpred b" and ?b' = "bexp_to_lpred (bexp_replace b x e)"
      have "x \<notin> lpred_vars ?P" using False by auto
      hence "PConj ?P ?b' \<turnstile> lpred_subst (PConj ?P ?b) (aexp_to_lexp e) x" using bexp_replace_correct by auto 
      also have "... \<turnstile> weaken_var x (PConj ?P ?b)" using lpred_subst_weaken_var by metis
      finally have "PConj ?P ?b' \<turnstile> PConj ?P ?b \<restriction> (- {x})" by auto
      hence "PConj ?P ?b' \<restriction> stable \<S> \<turnstile> (PConj ?P ?b \<restriction> (- {x})) \<restriction> stable \<S>" by auto
      hence "PConj ?P ?b' \<restriction> stable \<S> \<turnstile> PConj ?P ?b \<restriction> stable \<S>" using False
        by (smt inf.absorb2 pred_entailment_trans pred_equiv_entail 
                lpred_restrict_inter subset_Compl_singleton)
      thus ?thesis unfolding \<alpha> Guard \<beta>' by auto
    qed
  qed
qed

lemma P_upd_nop:
  assumes wr: "writes \<alpha> \<inter> lpred_vars P = {}"
  assumes stable: "type_stable \<S> P"
  shows "P+[\<alpha>]\<^sub>\<S> \<turnstile> P"
proof (cases \<alpha>)
  case (Assign x e)
  hence notin: "x \<notin> lpred_vars P"
    using wr lpred_restrict_vars by auto
  have "\<forall>P P' S. PEx (\<lambda>x'. PConj (P x') (P' x')) \<turnstile> PEx (\<lambda>x'. (P x')) "
    unfolding pred_entailment_def by auto
  hence "\<forall>P P' S. PEx (\<lambda>x'. PConj (P x') (P' x')) \<restriction> S \<turnstile> PEx (\<lambda>x'. (P x')) \<restriction> S"
    using lpred_restrict_entailment by auto

  hence "P+[\<alpha>]\<^sub>\<S> \<turnstile> PEx (\<lambda>x'. (lpred_subst P (Const x') x)) \<restriction> stable \<S>"
    unfolding P_upd.simps Assign reassign_var_def by auto
  also have "... \<turnstile> PEx (\<lambda>x'. (lpred_subst P (Const x') x))"
  proof -
    have "lpred_vars (PEx (\<lambda>v. (lpred_subst P (Const v) x))) \<subseteq> stable \<S>"
      using stable notin
      unfolding lpred_vars.simps lpred_subst_vars
      by (auto)
    thus ?thesis using lpred_restrict_nop pred_equiv_def by auto
  qed
  also have "... \<turnstile> P"
    using lpred_subst_nop [OF notin]
    unfolding pred_entailment_def by auto
  finally show ?thesis  by simp    
next
  case Fence
  then show ?thesis using stable by auto
next
  case (Guard b)
  then show ?thesis using stable
    by (metis empty_subsetI inf.orderE pred_entailment_refl reassign_diff_var writes.simps(3))
qed

lemma reassign_var_false:
  "reassign_var x e PFalse \<turnstile> PFalse"
  unfolding reassign_var_def pred_entailment_def
  by auto

lemma P_upd_false:
  shows "PFalse+[\<beta>]\<^sub>\<S> \<turnstile> PFalse"
proof (cases \<beta>)
  case (Assign x11 x12)
  have "\<forall>x e. reassign_var x e PFalse \<restriction> stable \<S> \<turnstile> PFalse \<restriction> stable \<S>"
    using reassign_var_false lpred_restrict_entailment by metis
  then show ?thesis using Assign by auto
next
  case Fence
  then show ?thesis by auto
next
  case (Guard x3)
  then show ?thesis using lpred_restrict_conj_l [of PFalse] by auto
qed

lemma P_upd_entail_false:
  assumes "P \<turnstile> PFalse"
  shows "P+[\<beta>]\<^sub>\<S> \<turnstile> PFalse"
proof -
  have "P+[\<beta>]\<^sub>\<S> \<turnstile> PFalse+[\<beta>]\<^sub>\<S>" using assms P_upd_entailment by auto
  thus ?thesis using P_upd_false pred_entailment_trans by meson
qed

lemma P_upd_eval:
  assumes eval: "eval_action mem \<alpha> mem'"
  assumes orig: "pred P mem"
  shows "pred (P+[\<alpha>]\<^sub>\<S>) mem'"
proof (cases \<alpha>)
  case (Assign x e)
  moreover have "pred (reassign_var x (aexp_to_lexp e) P) (mem(x := leval mem (aexp_to_lexp e)))"
    using reassign_var_eval assms(2) by metis
  ultimately show ?thesis using aexp_to_lexp_correct assms(1) lpred_restrict_weakens
    by (auto simp: pred_entailment_def)
next
  case Fence
  then show ?thesis using assms lpred_restrict_weakens 
    by (auto simp: pred_entailment_def)
next
  case (Guard b)
  then show ?thesis using bexp_to_lpred_correct assms lpred_restrict_weakens 
    by (auto simp: pred_entailment_def)
qed

subsection \<open> Data Dependency Analysis \<close>

text \<open>
  We track known reads and writes as we analyse the program.
  This allows the type checking to demonstrate soundness based on events
  that have definitely occurred.
  Knowledge is propagated based on limits on reorderings.
\<close>

type_synonym 'var Info = "('var ActionInfo) set"

type_synonym 'var Flow = "('var, 'var ActionInfo) WMF"

abbreviation known\<^sub>w :: "'Var Flow \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var set"
  ("_[_]\<^sub>w" [120, 120] 1000)
  where
  "known\<^sub>w D \<alpha> \<equiv> {x. Write x \<in> known D \<alpha>}"

abbreviation known\<^sub>r :: "'Var Flow \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var set"
  ("_[_]\<^sub>r" [120, 120] 1000)
  where
  "known\<^sub>r D \<alpha> \<equiv> {x. Read x \<in> known D \<alpha>}"

abbreviation sub_eq_abv :: "'Var Flow \<Rightarrow> 'Var Flow \<Rightarrow> bool"
  (infix "\<subseteq>\<^sub>D" 120)
  where
  "sub_eq_abv \<equiv> sub_eq"

abbreviation update_abv :: "'Var Flow \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var Flow"
  ("_+[_]\<^sub>D" [950, 950] 950)
  where
  "D+[a]\<^sub>D \<equiv> update D a"

abbreviation force_abv :: "'Var Flow \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var Flow"
  ("_+[_]\<^sub>F" [950, 950] 950)
  where
  "D+[a]\<^sub>F \<equiv> force D a"

subsubsection \<open> Lemmas \<close>

lemma reorder_disjoint_gen_kill:
  shows "\<alpha> \<hookleftarrow> \<beta> \<Longrightarrow> D_gen \<beta> \<inter> D_kill \<alpha> = {}"
proof -
  assume re: "\<alpha> \<hookleftarrow> \<beta>"
  hence "vars \<beta> \<inter> writes \<alpha> = {} \<and> \<beta> \<noteq> Fence"
    by (metis inf.commute reorder_diff_writes reorder_not_fence)
  thus ?thesis unfolding D_gen_def D_kill_def by auto
qed

lemma reorder_disjoint_gen_kill\<^sub>f:
  shows "\<beta>' < \<alpha> < \<beta> \<Longrightarrow> D_gen \<beta> \<inter> D_kill \<alpha> = {}"
proof -
  assume re: "\<beta>' < \<alpha> < \<beta>"
  hence "\<beta> \<noteq> Fence" by (cases \<beta>; cases \<alpha>; auto)
  thus ?thesis unfolding D_gen_def by auto
qed

(*
lemma remove_write:
  shows "\<alpha> \<hookleftarrow> \<beta> \<Longrightarrow> D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w = D[\<beta>]\<^sub>w - writes \<alpha>"
proof -
  assume re: "\<alpha> \<hookleftarrow> \<beta>"
  hence "D_gen \<beta> \<inter> D_kill \<alpha> = {}" using reorder_disjoint_gen_kill by auto
  moreover have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta> - D_kill \<alpha> \<union> D_gen \<beta>"
    using re update_kill limit\<^sub>w_correct limit\<^sub>r_correct reorder_not_fence
    by (auto simp: D_gen_def D_kill_def)
  moreover have "D_gen \<beta> \<subseteq> known D \<beta>" unfolding known_def by auto
  ultimately have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta> - D_kill \<alpha>"
    by auto
  thus ?thesis by (auto simp: D_gen_def D_kill_def)
qed

lemma remove_read:
  shows "\<alpha> \<hookleftarrow> \<beta> \<Longrightarrow> D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>r = D[\<beta>]\<^sub>r - reads \<alpha>"
  using update_kill limit\<^sub>w_correct limit\<^sub>r_correct reorder_not_fence
  by (auto simp: D_gen_def D_kill_def)

 *)

lemma remove_write:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  shows "D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w = D[\<beta>']\<^sub>w - writes \<alpha>"
  using assms
proof (cases rule: forward_elim)
  case normal
  hence "D_gen \<beta> \<inter> D_kill \<alpha> = {}" 
    using re reorder_disjoint_gen_kill\<^sub>f by auto
  moreover have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta> - D_kill \<alpha> \<union> D_gen \<beta>"
    using normal re update_kill limit\<^sub>w_correct\<^sub>f limit\<^sub>r_correct\<^sub>f reorder_not_fence\<^sub>f
    by (auto simp: D_gen_def D_kill_def)
  moreover have "D_gen \<beta> \<subseteq> known D \<beta>" unfolding known_def by auto
  ultimately have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta> - D_kill \<alpha>"
    by auto
  thus ?thesis using normal by (auto simp: D_gen_def D_kill_def)
next
  case forward
  have gen: "D_gen \<beta> = D_gen \<beta>'" using re reorder_not_fence by (cases \<beta>'; auto simp: D_gen_def)
  have wr: "writes \<beta>' = writes \<beta>" using re forward_writes by auto
  have rd: "reads \<beta>' = reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)" using forward re forward_reads by auto
  
  have "D_gen \<beta> \<inter> D_kill \<alpha> = {}" 
    using re reorder_disjoint_gen_kill\<^sub>f by auto
  moreover have "known (D+[\<alpha>]\<^sub>D) \<beta> = (fromWrites D (writes \<beta>) \<union> fromReads D (reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)) \<union> D_gen \<beta> \<union> Base D) - D_kill \<alpha> \<union> D_gen \<beta>"
    using forward re update_kill\<^sub>f limit\<^sub>w_correct\<^sub>f limit\<^sub>r_correct\<^sub>f reorder_not_fence\<^sub>f
    by (auto simp: D_gen_def D_kill_def)
  moreover have "known D \<beta>' = (fromWrites D (writes \<beta>) \<union> fromReads D (reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)) \<union> D_gen \<beta> \<union> Base D)"
    unfolding known_def gen wr rd by auto
  moreover have "D_gen \<beta> \<subseteq> known D \<beta>" unfolding known_def by auto
  ultimately have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta>' - D_kill \<alpha>" by auto
  thus ?thesis by (auto simp: D_gen_def D_kill_def)
qed

lemma remove_read:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  shows "D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>r = D[\<beta>']\<^sub>r - reads \<alpha>"
  using assms
proof (cases rule: forward_elim)
  case normal
  hence "D_gen \<beta> \<inter> D_kill \<alpha> = {}" 
    using re reorder_disjoint_gen_kill\<^sub>f by auto
  moreover have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta> - D_kill \<alpha> \<union> D_gen \<beta>"
    using normal re update_kill limit\<^sub>w_correct\<^sub>f limit\<^sub>r_correct\<^sub>f reorder_not_fence\<^sub>f
    by (auto simp: D_gen_def D_kill_def)
  moreover have "D_gen \<beta> \<subseteq> known D \<beta>" unfolding known_def by auto
  ultimately have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta> - D_kill \<alpha>"
    by auto
  thus ?thesis using normal by (auto simp: D_gen_def D_kill_def)
next
  case forward
  have gen: "D_gen \<beta> = D_gen \<beta>'" using re reorder_not_fence by (cases \<beta>'; auto simp: D_gen_def)
  have wr: "writes \<beta>' = writes \<beta>" using re forward_writes by auto
  have rd: "reads \<beta>' = reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)" using forward re forward_reads by auto
  
  have "D_gen \<beta> \<inter> D_kill \<alpha> = {}" 
    using re reorder_disjoint_gen_kill\<^sub>f by auto
  moreover have "known (D+[\<alpha>]\<^sub>D) \<beta> = (fromWrites D (writes \<beta>) \<union> fromReads D (reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)) \<union> D_gen \<beta> \<union> Base D) - D_kill \<alpha> \<union> D_gen \<beta>"
    using forward re update_kill\<^sub>f limit\<^sub>w_correct\<^sub>f limit\<^sub>r_correct\<^sub>f reorder_not_fence\<^sub>f
    by (auto simp: D_gen_def D_kill_def)
  moreover have "known D \<beta>' = (fromWrites D (writes \<beta>) \<union> fromReads D (reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)) \<union> D_gen \<beta> \<union> Base D)"
    unfolding known_def gen wr rd by auto
  moreover have "D_gen \<beta> \<subseteq> known D \<beta>" unfolding known_def by auto
  ultimately have "known (D+[\<alpha>]\<^sub>D) \<beta> = known D \<beta>' - D_kill \<alpha>" by auto
  thus ?thesis by (auto simp: D_gen_def D_kill_def)
qed

lemma stronger_writes:
  shows "D\<^sub>2 \<subseteq>\<^sub>D D\<^sub>1 \<Longrightarrow> D\<^sub>2[\<alpha>]\<^sub>w \<subseteq> D\<^sub>1[\<alpha>]\<^sub>w" 
  using sub_known by auto 

lemma stronger_reads:
  shows "D\<^sub>2 \<subseteq>\<^sub>D D\<^sub>1 \<Longrightarrow> D\<^sub>2[\<alpha>]\<^sub>r \<subseteq> D\<^sub>1[\<alpha>]\<^sub>r" 
  using sub_known by auto

lemma D_upd_sub:
  shows "D\<^sub>2 \<subseteq>\<^sub>D D\<^sub>1 \<Longrightarrow> D\<^sub>2+[\<alpha>]\<^sub>D \<subseteq>\<^sub>D D\<^sub>1+[\<alpha>]\<^sub>D"
  using sub_update_preserve by auto

lemma D_force_sub:
  shows "D \<subseteq>\<^sub>D D+[\<alpha>]\<^sub>F"
  using sub_force by auto

lemma D_force_upd_sub:
  shows "D+[\<alpha>]\<^sub>D \<subseteq>\<^sub>D D+[\<alpha>]\<^sub>F"
  using sub_force_update by auto

lemma D_force_result:
  shows "D[\<beta>]\<^sub>w \<subseteq> D+[\<beta>]\<^sub>F[\<alpha>]\<^sub>w"
  using force_result by auto

subsection \<open> Type Checking Rules \<close>

text \<open>
  Context equivalence is intended to demonstrate the equivalence of two \<Gamma>s under a set
  of predicates. We need to allow classifications in \<Gamma> to increase under certain situations.
\<close>

(* weaken the requirements on sec levels t \<rightarrow> t' such that t' has higher sec *)

definition context_equiv_type :: "Sec \<Rightarrow> Sec \<Rightarrow> bool"
  where "context_equiv_type t t' \<equiv> t' \<ge> t"

definition context_equiv ::
  "('Var,'Val) TyEnv \<Rightarrow> ('Var,'Val) TyEnv \<Rightarrow> bool"
  where
    "context_equiv \<Gamma> \<Gamma>' \<equiv> 
      dom \<Gamma> = dom \<Gamma>' \<and> (\<forall>x \<in> dom \<Gamma>'. context_equiv_type (the (\<Gamma> x)) (the (\<Gamma>' x)))"

text\<open>
  Detect potential changes in classifications.

  Explicitly require lpred_vars \<Gamma> to have no overlap with changed variables for simplicity.
  wf makes this trivially true.
\<close>
definition rising ::
  "'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var set \<Rightarrow> (_,_,_) Action \<Rightarrow> 'Var set"
  where
  "rising \<S> P W a \<equiv>
    {y. writes a \<inter> \<C>_vars y \<noteq> {} \<and> \<not> (P \<restriction> W !\<turnstile> \<L> y) \<and> \<not> (P+[a]\<^sub>\<S> \<restriction> W \<turnstile> \<L> y)}"

definition falling ::
  "'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var set \<Rightarrow> (_,_,_) Action \<Rightarrow> 'Var set"
  where
  "falling \<S> P W a \<equiv>
    {y. writes a \<inter> \<C>_vars y \<noteq> {} \<and> \<not> (P \<restriction> W \<turnstile> \<L> y) \<and> \<not> (P+[a]\<^sub>\<S> \<restriction> W !\<turnstile> \<L> y)}"

abbreviation written_low
  where "written_low \<S> \<Gamma> P W \<equiv> {y. \<C>_vars y \<subseteq> W \<and> 
                        (y \<in> NoRW \<S> \<or> y \<in> W \<and> y \<in> dom \<Gamma> \<and> P \<restriction> W \<turnstile> sec (the (\<Gamma> y)))}"

abbreviation read_high
  where "read_high W Wr \<equiv> {y. y \<in> Wr \<and> \<C>_vars y \<subseteq> W}"

text\<open>
  Detect safe control variable assignments, based on potentially rising and falling classifications.
\<close>
definition secure_update ::
  "('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var set \<Rightarrow> 'Var set \<Rightarrow> (_,_,_) Action \<Rightarrow> bool"
where
  "secure_update \<Gamma> \<S> P W W' \<alpha> \<equiv>
    (falling \<S> P W \<alpha> \<subseteq> written_low \<S> \<Gamma> P W) \<and> (rising \<S> P W \<alpha> \<subseteq> read_high W W')"

text \<open>
  An Action's writes are secure if they have an equal or higher classification, within the current
  program context, than its reads.
\<close>
definition secure_write ::
  "('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var set \<Rightarrow> (_,_,_) Action \<Rightarrow> bool"
where
  "secure_write \<Gamma> \<S> P W \<alpha> \<equiv> (\<Gamma> :\<^sub>r \<alpha>) \<le>:\<^sub>(P \<restriction> W) (\<S> :\<^sub>w \<alpha>)"

text \<open>
  Modifications to the type checking state due to an Action.
\<close>
definition state_upd :: 
  "('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow>
  ('Var, 'AExp, 'BExp) Action \<Rightarrow>
   ('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow>  ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow> bool"
  ("_,_,_,_ +\<^sub>_ _,_,_,_" 100)
  where
  "state_upd \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<alpha> \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<equiv> 
    \<Gamma>\<^sub>2 = \<Gamma>\<^sub>1+\<^bsub>(P\<^sub>1 \<restriction> D\<^sub>1[\<alpha>]\<^sub>w)\<^esub>[\<alpha>] \<and> \<S>\<^sub>2 = \<S>\<^sub>1 \<and> P\<^sub>2 = P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>1 \<and> D\<^sub>2 = D\<^sub>1+[\<alpha>]\<^sub>D "

text \<open>
  Combine secure_* properties and state update to define a type checked Action.
\<close>
abbreviation action_type ::
  "('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow>  'Var Flow \<Rightarrow>
  ('Var, 'AExp, 'BExp) Action \<Rightarrow> 
  ('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow> bool"
  ("\<turnstile> _,_,_,_ {_}\<^sub>a _,_,_,_" [120, 120, 120, 120, 120, 120, 120] 900)
  where
  "\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>',\<S>',P',D' \<equiv> (secure_write \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) \<alpha> \<and> secure_update \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) (D[\<alpha>]\<^sub>r) \<alpha>) \<and>
                                   (\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D')"

text \<open>
  Ordering for type checking state, such that a program that type checks on a weaker state
  must also type check on a stronger state.
\<close>
definition state_ord ::
  "('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow>
   ('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow> bool"
  ("_,_,_,_ >\<^sub>s _,_,_,_" 100)
  where
  "state_ord \<Gamma> \<S> P D  \<Gamma>' \<S>' P' D' \<equiv>
    (\<forall>env. tyenv_wellformed env \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed env \<Gamma>' \<S>' P') \<and>
    context_equiv \<Gamma> \<Gamma>' \<and> \<S> = \<S>' \<and> D' \<subseteq>\<^sub>D D \<and> P \<turnstile> P'"

text \<open>
  Type checking rules for statements.
\<close>
inductive has_type ::
  "('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow>
  ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> 
  ('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow> bool"
  ("\<turnstile> _,_,_,_ {_} _,_,_,_" [120, 120, 120, 120, 120, 120, 120] 1000)
where
  stop_type:
  "\<turnstile> \<Gamma>,\<S>,P,D { Stop } \<Gamma>,\<S>,P,D" |
  act_type:
  "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { \<alpha> }\<^sub>a \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2; \<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 { c } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3 \<rbrakk> \<Longrightarrow>
   \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { \<alpha> ;;; c } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3" |
  choice_type:
  "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 ; \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>2 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 \<rbrakk> \<Longrightarrow>
   \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 ? c\<^sub>2 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2" |
  loop_type:
  "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 } \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 ; \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>2 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 \<rbrakk> \<Longrightarrow>
   \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { Loop c\<^sub>1 c\<^sub>2 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2" |
  rewrite:
  "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c } \<Gamma>\<^sub>1',\<S>\<^sub>1',P\<^sub>1',D\<^sub>1'; \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 >\<^sub>s \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 ; \<Gamma>\<^sub>1',\<S>\<^sub>1',P\<^sub>1',D\<^sub>1' >\<^sub>s \<Gamma>\<^sub>2',\<S>\<^sub>2',P\<^sub>2',D\<^sub>2' \<rbrakk> \<Longrightarrow>
   \<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 { c } \<Gamma>\<^sub>2',\<S>\<^sub>2',P\<^sub>2',D\<^sub>2'"

subsection \<open> Type Checking Lemmas \<close>

subsubsection \<open> context_equiv properties \<close>

lemma context_equiv_type_refl[simp]:
  "context_equiv_type t t"
  by (simp add: context_equiv_type_def)

lemma context_equiv_refl[simp]:
  "context_equiv \<Gamma> \<Gamma>"
  by (simp add: context_equiv_def)

lemma context_equiv_type_trans:
  assumes "context_equiv_type t\<^sub>1 t\<^sub>2"
  and "context_equiv_type t\<^sub>2 t\<^sub>3"
  shows "context_equiv_type t\<^sub>1 t\<^sub>3"
  using assms by (simp add: context_equiv_type_def pred_entailment_def)

lemma context_equiv_trans:
  assumes "context_equiv \<Gamma>\<^sub>1 \<Gamma>\<^sub>2"
  and "context_equiv \<Gamma>\<^sub>2 \<Gamma>\<^sub>3"
  shows "context_equiv \<Gamma>\<^sub>1 \<Gamma>\<^sub>3"
  using assms context_equiv_type_trans
  unfolding context_equiv_def by blast

subsubsection \<open> state_rel properties \<close>

lemma state_upd_\<Gamma>_dom:
  assumes "\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D'"
  shows "dom \<Gamma> = dom \<Gamma>'"
  using assms
  unfolding state_upd_def
  by (cases \<alpha>, auto split: if_splits)

lemma state_upd_\<Gamma>_eq:
  assumes "\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D'"
  shows "\<forall>x\<in>-(writes \<alpha>). \<Gamma> x = \<Gamma>' x"
  unfolding Ball_def
proof (intro allI impI)
  fix x
  assume a: "x \<in> - (writes \<alpha>)"
  show "\<Gamma> x = \<Gamma>' x"
  proof (cases "x \<in> dom \<Gamma>")
    case True
    hence in1: "x \<in> dom \<Gamma>'" using assms state_upd_\<Gamma>_dom by auto
    hence "the (\<Gamma>' x) = the (\<Gamma> x)" 
      using a assms unfolding state_upd_def by (auto simp: \<Gamma>_upd_nop)
    then show ?thesis using True in1 by auto
  next
    case False
    hence "x \<notin> dom \<Gamma>'" using assms state_upd_\<Gamma>_dom by auto
    then show ?thesis using False by auto
  qed
qed

lemma state_upd_wellformed:
  assumes up: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes wf: "tyenv_wellformed env \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1"
  shows "tyenv_wellformed env \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2"
proof -
  have "\<S>\<^sub>1 = \<S>\<^sub>2" using up by (simp add: state_upd_def)
  moreover have "dom \<Gamma>\<^sub>2 = dom \<Gamma>\<^sub>1" 
    using up by (simp add: state_upd_\<Gamma>_dom)
  moreover have "lpred_vars P\<^sub>2 \<subseteq> stable \<S>\<^sub>1" 
    using up P_upd_stable by (simp add: state_upd_def)
  ultimately show ?thesis using wf by (auto simp add: tyenv_wellformed_def)
qed

subsubsection \<open> state_ord properties \<close>

lemma state_ord_refl [simp]:
  "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>,\<S>,P,D"
  by (auto simp: state_ord_def)

lemma state_ord_trans:
  assumes "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 >\<^sub>s \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes "\<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  shows "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  using assms unfolding state_ord_def
  by (intro conjI, (meson context_equiv_trans sub_eq_trans pred_entailment_trans)+)

lemma D_state_ord:
  assumes ord: "\<Gamma>,\<S>,P,D\<^sub>1 >\<^sub>s \<Gamma>',\<S>',P',D\<^sub>2"
  assumes "D\<^sub>1 \<subseteq>\<^sub>D D\<^sub>3"
  assumes "D\<^sub>4 \<subseteq>\<^sub>D D\<^sub>3"
  shows "\<Gamma>,\<S>,P,D\<^sub>3 >\<^sub>s \<Gamma>',\<S>',P',D\<^sub>4"
  using assms by (auto simp: state_ord_def)

lemma state_ord_entailment:
  assumes "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'"
  assumes "P'' \<turnstile> P"
  assumes "lpred_vars P \<subseteq> stable \<S>"
  shows "\<Gamma>,\<S>,P'',D >\<^sub>s \<Gamma>',\<S>',P',D'"
  unfolding state_ord_def
proof (intro conjI allI impI, goal_cases)
  case (1 env)
  moreover have "\<S> = \<S>'" using assms(1) by (auto simp: state_ord_def)
  ultimately have "tyenv_wellformed env \<Gamma> \<S>' P"
    using assms(3) unfolding tyenv_wellformed_def by (auto simp: state_ord_def)
  then show ?case using assms by (auto simp: state_ord_def)
next
  case 2
  then show ?case using assms by (auto simp: state_ord_def)
next
  case 3
  then show ?case using assms by (auto simp: state_ord_def)
next
  case 4
  then show ?case using assms by (auto simp: state_ord_def)
next
  case 5
  then show ?case using assms by (auto simp: pred_entailment_trans state_ord_def)
qed

lemma context_equiv_total:
  assumes "context_equiv \<Gamma>\<^sub>1 \<Gamma>\<^sub>2"
  shows "to_total \<Gamma>\<^sub>2 x \<turnstile> to_total \<Gamma>\<^sub>1 x"
  using assms
  apply (cases "x \<in> dom \<Gamma>\<^sub>1" ; cases "the (\<Gamma>\<^sub>2 x)"; cases "the (\<Gamma>\<^sub>1 x)")
  apply (auto simp add: sec_def pred_entailment_def context_equiv_def context_equiv_type_def to_total_def)
  by (metis Sec.distinct(1) domI less_eq_Sec_def option.sel)

lemma context_equiv_reads:
  assumes c: "context_equiv \<Gamma>\<^sub>1 \<Gamma>\<^sub>2"
  shows "(\<Gamma>\<^sub>2 :\<^sub>r \<alpha>) \<turnstile> (\<Gamma>\<^sub>1 :\<^sub>r \<alpha>)"
proof -
  have sub: "\<forall>y \<in> reads \<alpha>. to_total \<Gamma>\<^sub>2 y \<turnstile> to_total \<Gamma>\<^sub>1 y"
    using c by (simp add: context_equiv_total)

  show ?thesis
  proof (cases \<alpha>)
    case (Assign x e)
    then show ?thesis using sub pred_entailment_list [where \<Gamma>\<^sub>2="to_total \<Gamma>\<^sub>2"] by auto
  next
    case Fence
    then show ?thesis unfolding pred_entailment_def by auto
  next
    case (Guard b)
    then show ?thesis using sub pred_entailment_list [where \<Gamma>\<^sub>2="to_total \<Gamma>\<^sub>2"] by auto
  qed
qed

subsubsection \<open> Falling and Rising properties \<close>

lemma falling_entailment:
  assumes "P \<restriction> W \<turnstile> P' \<restriction> W'"
  assumes "P+[\<beta>]\<^sub>\<S> \<restriction> W \<turnstile> P'+[\<alpha>]\<^sub>\<S> \<restriction> W'"
  assumes w: "writes \<alpha> = writes \<beta>"
  shows "falling \<S> P W \<beta> \<subseteq> falling \<S> P' W' \<alpha>"
proof -
  have a: "\<forall>t. P' \<restriction> W' \<turnstile> t \<longrightarrow> P \<restriction> W \<turnstile> t"
    using assms(1) pred_entailment_trans by meson
  have b: "\<forall>t. P'+[\<alpha>]\<^sub>\<S> \<restriction> W' \<turnstile> t \<longrightarrow> P+[\<beta>]\<^sub>\<S> \<restriction> W \<turnstile> t"
    using assms(2) pred_entailment_trans by meson
  show ?thesis using a b w by (auto simp: falling_def)
qed

lemma falling_entailment_ord:
  assumes ord: "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'"
  shows "falling \<S> P (D[a]\<^sub>w) a \<subseteq> falling \<S>' P' (D'[a]\<^sub>w) a"
proof -
  have "P \<restriction> D[a]\<^sub>w \<turnstile> P' \<restriction> D'[a]\<^sub>w" using ord stronger_writes state_ord_def by blast
  moreover have "\<S> = \<S>'" using ord state_ord_def by blast
  moreover have "P+[a]\<^sub>\<S> \<restriction> D[a]\<^sub>w \<turnstile> P'+[a]\<^sub>\<S> \<restriction> D'[a]\<^sub>w"
    using P_upd_entailment lpred_restrict_entailment_subset ord state_ord_def stronger_writes 
    by auto
  ultimately show ?thesis by (auto simp: falling_entailment)
qed

lemma rising_entailment:
  assumes "P \<restriction> W \<turnstile> P' \<restriction> W'"
  assumes "P+[\<beta>]\<^sub>\<S> \<restriction> W \<turnstile> P'+[\<alpha>]\<^sub>\<S> \<restriction> W'"
  assumes w: "writes \<alpha> = writes \<beta>"
  shows "rising \<S> P W \<beta> \<subseteq> rising \<S> P' W' \<alpha>"
proof -
  have a: "\<forall>t. P' \<restriction> W' \<turnstile> t \<longrightarrow> P \<restriction> W \<turnstile> t"
    using assms(1) pred_entailment_trans by meson
  have b: "\<forall>t. P'+[\<alpha>]\<^sub>\<S> \<restriction> W' \<turnstile> t \<longrightarrow> P+[\<beta>]\<^sub>\<S> \<restriction> W \<turnstile> t"
    using assms(2) pred_entailment_trans by meson
  show ?thesis using a b w by (auto simp: rising_def)
qed

lemma rising_entailment_ord:
  assumes ord: "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'"
  shows "rising \<S> P (D[a]\<^sub>w) a \<subseteq> rising \<S>' P' (D'[a]\<^sub>w) a"
proof -
  have "P \<restriction> D[a]\<^sub>w \<turnstile> P' \<restriction> D'[a]\<^sub>w" using ord stronger_writes state_ord_def by blast
  moreover have "\<S> = \<S>'" using ord state_ord_def by blast
  moreover have "P+[a]\<^sub>\<S> \<restriction> D[a]\<^sub>w \<turnstile> P'+[a]\<^sub>\<S> \<restriction> D'[a]\<^sub>w"
    using P_upd_entailment lpred_restrict_entailment_subset ord state_ord_def stronger_writes 
    by auto
  ultimately show ?thesis by (auto simp: rising_entailment)
qed

subsubsection \<open> sec_upd properties \<close>

lemma secure_update_entailment:
  assumes sec: "secure_update \<Gamma>' \<S>' P' W' Wr' \<alpha>"
  assumes "P \<restriction> W \<turnstile> P' \<restriction> W'" (is "?P \<turnstile> ?P'")
  assumes "P+[\<beta>]\<^sub>\<S> \<restriction> W \<turnstile> P'+[\<alpha>]\<^sub>\<S> \<restriction> W'"
  assumes "W' \<subseteq> W \<and> Wr' \<subseteq> Wr"
  assumes "dom \<Gamma>' = dom \<Gamma>"
  assumes "\<forall>y \<in> dom \<Gamma> \<inter> W'. sec (the (\<Gamma>' y)) \<turnstile> sec (the (\<Gamma> y))"
  assumes s: "\<S>' = \<S>"
  assumes w: "writes \<alpha> = writes \<beta>"
  shows "secure_update \<Gamma> \<S> P W Wr \<beta>"
proof -
  have "falling \<S> P W \<beta> \<subseteq> falling \<S> P' W' \<alpha>" (is "?f \<subseteq> ?f'")
    using assms falling_entailment by auto
  moreover have "rising \<S> P W \<beta> \<subseteq> rising \<S> P' W' \<alpha>" (is "?r \<subseteq> ?r'")
    using assms rising_entailment by auto
  moreover have "read_high W' Wr' \<subseteq> read_high W Wr"
    using assms by auto
  moreover have "written_low \<S> \<Gamma>' P' W' \<subseteq> written_low \<S> \<Gamma> P W"
  proof (auto, goal_cases)
    case (1 x xa)
    then show ?case using assms by auto
  next
    case (2 x y xa)
    then show ?case using assms by auto
  next
    case (3 x y)
    then show ?case using assms by auto
  next
    case (4 x y)
    then show ?case using assms by auto
  next
    case (5 x y)
    hence "x \<in> dom \<Gamma> \<inter> W'" using assms by (auto simp: state_ord_def context_equiv_def)
    hence "sec(the (\<Gamma>' x)) \<turnstile> sec(the (\<Gamma> x))" using assms by auto
    then show ?case using 5 assms by (metis (no_types, lifting) option.sel pred_entailment_trans)
  qed
  ultimately show ?thesis using sec unfolding s secure_update_def by blast
qed

lemma secure_update_entailment_ord:
  assumes sec: "secure_update \<Gamma>' \<S>' P' (D'[\<alpha>]\<^sub>w) (D'[\<alpha>]\<^sub>r) \<alpha>"
  assumes ord: "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'"
  shows "secure_update \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) (D[\<alpha>]\<^sub>r) \<alpha>"
proof (rule secure_update_entailment[OF sec])
  show "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> P' \<restriction> D'[\<alpha>]\<^sub>w" using ord
    by (simp add: lpred_restrict_entailment_subset state_ord_def stronger_writes)
next
  show "P+[\<alpha>]\<^sub>\<S> \<restriction> D[\<alpha>]\<^sub>w \<turnstile> P'+[\<alpha>]\<^sub>\<S> \<restriction> D'[\<alpha>]\<^sub>w" using ord
    by (simp add: P_upd_entailment lpred_restrict_entailment_subset state_ord_def stronger_writes)
next
  show "D'[\<alpha>]\<^sub>w \<subseteq> D[\<alpha>]\<^sub>w \<and> D'[\<alpha>]\<^sub>r \<subseteq> D[\<alpha>]\<^sub>r" using ord
    by (simp add: stronger_reads state_ord_def stronger_writes)
next
  show "dom \<Gamma>' = dom \<Gamma>" using ord by (simp add: context_equiv_def state_ord_def) 
next
  show "\<forall>y\<in>dom \<Gamma> \<inter> D'[\<alpha>]\<^sub>w. sec (the (\<Gamma>' y)) \<turnstile> sec (the (\<Gamma> y))" using ord
    unfolding context_equiv_def state_ord_def context_equiv_type_def sec_def pred_entailment_def
    by (simp, metis domIff less_eq_Sec_def)
next
  show "writes \<alpha> = writes \<alpha>" by simp
next
  show "\<S>' = \<S>" using ord by (simp add: state_ord_def)
qed

subsubsection \<open> secure_write properties \<close>

lemma secure_write_stronger:
  assumes sec: "secure_write \<Gamma>' \<S>' P' (D'[\<alpha>]\<^sub>w) \<alpha>"
  assumes ord: "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'"
  shows "secure_write \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) \<alpha>"
proof -
  have "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> P' \<restriction> D'[\<alpha>]\<^sub>w"
    using ord by (simp add: lpred_restrict_entailment_subset state_ord_def stronger_writes)
  moreover have "\<Gamma>' :\<^sub>r \<alpha> \<turnstile> \<Gamma> :\<^sub>r \<alpha>"
    by (meson context_equiv_reads ord state_ord_def)
  moreover have "\<S>' :\<^sub>w \<alpha> = \<S> :\<^sub>w \<alpha>" using ord by (simp add: state_ord_def)
  ultimately show ?thesis using subtype_entailment sec secure_write_def by auto
qed 

subsubsection \<open> Properties relating state_ord and state_rel \<close>

lemma \<Gamma>_upd_preserve:
  assumes ord: "context_equiv \<Gamma>\<^sub>1 \<Gamma>\<^sub>2"
  assumes pred: "P\<^sub>1 \<turnstile> P\<^sub>2"
  shows "context_equiv (\<Gamma>\<^sub>1+\<^bsub>P\<^sub>1\<^esub>[\<alpha>]) (\<Gamma>\<^sub>2+\<^bsub>P\<^sub>2\<^esub>[\<alpha>])"
proof (cases \<alpha>)
  case (Assign x e)
  let ?\<Gamma>1 = "\<Gamma>\<^sub>1+\<^bsub>P\<^sub>1\<^esub>[\<alpha>]" and ?\<Gamma>2 = "\<Gamma>\<^sub>2+\<^bsub>P\<^sub>2\<^esub>[\<alpha>]" and ?t1 = "\<Gamma>\<^sub>1 :\<^sub>r (x \<leftarrow> e)" and ?t2 = "\<Gamma>\<^sub>2 :\<^sub>r (x \<leftarrow> e)"
  let ?l = "sorted_list_of_set (aexp_vars e)"
  have dom_eq: "dom ?\<Gamma>1 = dom ?\<Gamma>2"
    using \<Gamma>_upd_dom context_equiv_def ord by auto
  moreover have "\<forall>y \<in> dom ?\<Gamma>1 - {x}. context_equiv_type (the (?\<Gamma>1 y)) (the (?\<Gamma>2 y))"
    using Assign context_equiv_def ord by auto
  moreover have "x \<in> dom \<Gamma>\<^sub>1 \<longrightarrow> context_equiv_type (the (?\<Gamma>1 x)) (the (?\<Gamma>2 x))"
  proof (intro impI)
    assume "x \<in> dom \<Gamma>\<^sub>1"
    moreover have "x \<in> dom \<Gamma>\<^sub>2" using \<Gamma>_upd_dom calculation(1) dom_eq by auto
    moreover have "P\<^sub>2 \<turnstile> ?t2 \<longrightarrow> P\<^sub>1 \<turnstile> ?t1"
      using pred_entailment_trans pred context_equiv_reads ord by meson
    moreover have "PFalse \<turnstile> PTrue" using pred_entailment_def by auto
    ultimately show "context_equiv_type (the (?\<Gamma>1 x)) (the (?\<Gamma>2 x))" using pred Assign
      apply (auto simp add: context_equiv_def state_ord_def context_equiv_type_def sec_def pred_entailment_def) 
      using less_eq_Sec_def by auto 

  qed
  ultimately show ?thesis using \<Gamma>_upd_dom by (auto simp: context_equiv_def)
next
  case Fence
  then show ?thesis using ord by auto
next
  case (Guard b)
  then show ?thesis using ord by auto
qed

lemma state_upd_preserve:     
  assumes ord: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 >\<^sub>s \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes s1: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  assumes s2: "\<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 +\<^sub>\<alpha> \<Gamma>\<^sub>4,\<S>\<^sub>4,P\<^sub>4,D\<^sub>4"
  shows "\<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3 >\<^sub>s \<Gamma>\<^sub>4,\<S>\<^sub>4,P\<^sub>4,D\<^sub>4"
  unfolding state_ord_def
proof (intro conjI impI allI, goal_cases)
  case (1 env)
  have props: "dom \<Gamma>\<^sub>3 = dom \<Gamma>\<^sub>4" "\<S>\<^sub>3 = \<S>\<^sub>4" "P\<^sub>4 = P\<^sub>2+[\<alpha>]\<^sub>\<S>\<^sub>4" "context_equiv \<Gamma>\<^sub>1 \<Gamma>\<^sub>2"
    using assms state_upd_def state_upd_\<Gamma>_dom context_equiv_def
    by (metis state_ord_def)+
  then show ?case using 1 props by (auto simp add: tyenv_wellformed_def state_ord_def)
next
  case 2
  have "context_equiv \<Gamma>\<^sub>1 \<Gamma>\<^sub>2" "\<Gamma>\<^sub>3 = \<Gamma>\<^sub>1+\<^bsub>(P\<^sub>1 \<restriction> D\<^sub>1[\<alpha>]\<^sub>w)\<^esub>[\<alpha>]" "\<Gamma>\<^sub>4 = \<Gamma>\<^sub>2+\<^bsub>(P\<^sub>2 \<restriction> D\<^sub>2[\<alpha>]\<^sub>w)\<^esub>[\<alpha>]"
    using assms state_ord_def state_upd_def by auto
  moreover have "P\<^sub>1 \<restriction> D\<^sub>1[\<alpha>]\<^sub>w \<turnstile> P\<^sub>2 \<restriction> D\<^sub>2[\<alpha>]\<^sub>w" using ord
    by (simp add: lpred_restrict_entailment_subset state_ord_def stronger_writes)
  ultimately show ?case using \<Gamma>_upd_preserve by auto
next
  case 3
  then show ?case using assms by (metis state_upd_def state_ord_def)
next
  case 4
  then show ?case using assms D_upd_sub by (metis state_upd_def state_ord_def)
next
  case 5
  then show ?case using assms P_upd_entailment by (metis state_upd_def state_ord_def)
qed

lemma action_type_stronger:
  assumes orig: "\<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 { \<alpha> }\<^sub>a \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  assumes ord: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 >\<^sub>s \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  shows "\<exists>\<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i. (\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { \<alpha> }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i) \<and> (\<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3)"
  using orig ord secure_write_stronger secure_update_entailment_ord state_upd_preserve
  by (auto simp: state_upd_def)

subsubsection \<open> Type Checking Elimination Rules \<close>

lemma act_elim:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { a ;;; c } \<Gamma>',\<S>',P',D'"
  shows "\<exists>\<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i. (\<turnstile> \<Gamma>,\<S>,P,D { a }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i) \<and> (\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i { c } \<Gamma>',\<S>',P',D')"
  using assms
proof (induct \<Gamma> \<S> P D "a ;;; c" \<Gamma>' \<S>' P' D' arbitrary: c rule: has_type.induct)
  case (act_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3)
  then show ?case using state_ord_refl by blast
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  then obtain \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i where props:
      "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { a }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i" "\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i { c } \<Gamma>\<^sub>1',\<S>\<^sub>1',P\<^sub>1',D\<^sub>1'"
    by metis
  then obtain \<Gamma>\<^sub>i' \<S>\<^sub>i' P\<^sub>i' D\<^sub>i' where props':
      "\<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 { a }\<^sub>a \<Gamma>\<^sub>i',\<S>\<^sub>i',P\<^sub>i',D\<^sub>i'" "\<Gamma>\<^sub>i',\<S>\<^sub>i',P\<^sub>i',D\<^sub>i' >\<^sub>s \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i"
    using rewrite action_type_stronger by metis
  hence "\<turnstile> \<Gamma>\<^sub>i',\<S>\<^sub>i',P\<^sub>i',D\<^sub>i' {c} \<Gamma>\<^sub>2',\<S>\<^sub>2',P\<^sub>2',D\<^sub>2'"
    using props rewrite has_type.rewrite by metis
  then show ?case using props' props rewrite(3) state_ord_trans by metis
qed

lemma choice_elim:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c\<^sub>1 ? c\<^sub>2 } \<Gamma>',\<S>',P',D'"
  shows "(\<turnstile> \<Gamma>,\<S>,P,D { c\<^sub>1 } \<Gamma>',\<S>',P',D') \<and> \<turnstile> \<Gamma>,\<S>,P,D { c\<^sub>2 } \<Gamma>',\<S>',P',D'"
  using assms
proof (induct \<Gamma> \<S> P D "c\<^sub>1 ? c\<^sub>2" \<Gamma>' \<S>' P' D' rule: has_type.induct)
  case (choice_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2)
  then show ?case by blast
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  hence "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 {c\<^sub>1 ? c\<^sub>2} \<Gamma>\<^sub>1',\<S>\<^sub>1',P\<^sub>1',D\<^sub>1'" by auto
  then show ?case using rewrite has_type.rewrite by blast
qed

lemma loop_elim:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { Loop c\<^sub>1 c\<^sub>2 } \<Gamma>',\<S>',P',D'"
  shows "\<exists>\<Gamma>i \<S>i Pi Di.
    (\<turnstile> \<Gamma>i,\<S>i,Pi,Di { c\<^sub>1 } \<Gamma>i,\<S>i,Pi,Di) \<and>
    (\<turnstile> \<Gamma>i,\<S>i,Pi,Di { c\<^sub>2 } \<Gamma>',\<S>',P',D') \<and>
    (\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>i,\<S>i,Pi,Di)"
  using assms
proof (induct \<Gamma> \<S> P D "Loop c\<^sub>1 c\<^sub>2" \<Gamma>' \<S>' P' D' rule: has_type.induct)
  case (loop_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2)
  have "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 >\<^sub>s \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1" by auto
  thus ?case using loop_type by blast
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  then obtain \<Gamma>i \<S>i Pi Di where props:
      "\<turnstile> \<Gamma>i,\<S>i,Pi,Di {c\<^sub>1} \<Gamma>i,\<S>i,Pi,Di"
      "\<turnstile> \<Gamma>i,\<S>i,Pi,Di {c\<^sub>2} \<Gamma>\<^sub>1',\<S>\<^sub>1',P\<^sub>1',D\<^sub>1'"
      "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 >\<^sub>s \<Gamma>i,\<S>i,Pi,Di"
    by metis
  have e: "\<turnstile> \<Gamma>i,\<S>i,Pi,Di {c\<^sub>2} \<Gamma>\<^sub>2',\<S>\<^sub>2',P\<^sub>2',D\<^sub>2'"
    using props rewrite has_type.rewrite by auto
  show ?case using props e state_ord_trans rewrite by blast
qed

lemma stop_elim:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { Stop } \<Gamma>',\<S>',P',D'"
  shows "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'"
  using assms
proof (induct \<Gamma> \<S> P D "Stop :: ('Var, 'AExp, 'BExp) Stmt" \<Gamma>' \<S>' P' D' rule: has_type.induct)
  case (stop_type \<Gamma> \<S> P D)
  then show ?case by auto
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  then show ?case using state_ord_trans by meson
qed

lemma seq_typeE:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c1 ;; c2 } \<Gamma>',\<S>',P',D'"
  assumes "\<beta>' < c1 <\<^sub>c \<beta>"
  shows "\<exists> \<Gamma>i \<S>i Pi Di. (\<turnstile> \<Gamma>,\<S>,P,D { c1 } \<Gamma>i,\<S>i,Pi,Di) \<and> (\<turnstile> \<Gamma>i,\<S>i,Pi,Di { c2 } \<Gamma>',\<S>',P',D')"
  using assms
proof (induct arbitrary: \<Gamma> \<S> P D  \<beta>' rule: Seq.induct)
  case (1 c)
  hence "\<turnstile> \<Gamma>,\<S>,P,D {c} \<Gamma>',\<S>',P',D'" using Seq.simps(1) by auto
  moreover have "\<turnstile> \<Gamma>,\<S>,P,D {Stop} \<Gamma>,\<S>,P,D" using stop_type by auto
  ultimately show ?case by blast
next
  case (2 \<alpha> c\<^sub>1 c\<^sub>2)
  then have "\<turnstile> \<Gamma>,\<S>,P,D {\<alpha> ;;; (c\<^sub>1 ;; c\<^sub>2)} \<Gamma>',\<S>',P',D'" using seq_assoc by auto
  then obtain \<Gamma>e \<S>e Pe De where p: 
      "(\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>e,\<S>e,Pe,De)"
      "\<turnstile> \<Gamma>e,\<S>e,Pe,De { c\<^sub>1 ;; c\<^sub>2 } \<Gamma>',\<S>',P',D'"
    using act_elim 2 by metis
  then obtain \<Gamma>e' \<S>e' Pe' De' where
      "\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>e,\<S>e,Pe,De"
      "\<turnstile> \<Gamma>e,\<S>e,Pe,De { c\<^sub>1 } \<Gamma>e',\<S>e',Pe',De'"
      "\<turnstile> \<Gamma>e',\<S>e',Pe',De' { c\<^sub>2 } \<Gamma>',\<S>',P',D'"
    using 2 reorder\<^sub>c.simps by metis
  then show ?case using has_type.act_type by metis
next
  case (3 c\<^sub>1 c\<^sub>2 c\<^sub>3)
  thus ?case by auto
next
  case (4 c\<^sub>1 c\<^sub>2 c\<^sub>3)
  thus ?case by auto
qed

subsubsection \<open> Type Checking Introduction Rules \<close>

lemma action_type_unstable:
  assumes "x \<notin> dom \<Gamma>"
  assumes "x \<notin> \<C>"
  assumes "P \<restriction> D[ x \<leftarrow> e ]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r (x \<leftarrow> e) \<or> P \<restriction> D[ x \<leftarrow> e ]\<^sub>w !\<turnstile> \<L> x"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { x \<leftarrow> e }\<^sub>a \<Gamma>,\<S>,P+[ x \<leftarrow> e ]\<^sub>\<S>,D+[ x \<leftarrow> e ]\<^sub>D"
  using assms
  unfolding secure_write_def secure_update_def subtype_def falling_def rising_def
            state_upd_def pred_entailment_def \<C>_def
  by auto

lemma action_type_stable_low:
  assumes "x \<in> dom \<Gamma>"
  assumes "x \<notin> \<C>"
  assumes "P \<restriction> D[ x \<leftarrow> e ]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r (x \<leftarrow> e)"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { x \<leftarrow> e }\<^sub>a \<Gamma>(x := Some Low),\<S>,P+[ x \<leftarrow> e ]\<^sub>\<S>,D+[ x \<leftarrow> e ]\<^sub>D"
  using assms
  unfolding secure_write_def secure_update_def subtype_def falling_def rising_def state_upd_def \<C>_def
  by (auto simp: pred_entailment_def)

lemma action_type_stable_high:
  assumes "x \<in> dom \<Gamma>"
  assumes "x \<notin> \<C>"
  assumes "P \<restriction> D[ x \<leftarrow> e ]\<^sub>w !\<turnstile> \<L> x"
  assumes "\<not> P \<restriction> D[ x \<leftarrow> e ]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r (x \<leftarrow> e)"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { x \<leftarrow> e }\<^sub>a \<Gamma>(x := Some High),\<S>,P+[ x \<leftarrow> e ]\<^sub>\<S>,D+[ x \<leftarrow> e ]\<^sub>D"
  using assms
  unfolding secure_write_def secure_update_def subtype_def falling_def rising_def state_upd_def \<C>_def
  by (auto simp: pred_entailment_def)

lemma action_type_control:
  assumes "x \<notin> dom \<Gamma>"
  assumes "x \<in> \<C>"
  assumes "P \<restriction> D[ x \<leftarrow> e ]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r (x \<leftarrow> e)"
  assumes "secure_update \<Gamma> \<S> P (D[x \<leftarrow> e]\<^sub>w) (D[x \<leftarrow> e]\<^sub>r) (x \<leftarrow> e)"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { x \<leftarrow> e }\<^sub>a \<Gamma>,\<S>,P+[ x \<leftarrow> e ]\<^sub>\<S>,D+[ x \<leftarrow> e ]\<^sub>D"
  using assms
  unfolding secure_write_def secure_update_def subtype_def falling_def rising_def state_upd_def \<C>_def
  by (auto simp: pred_entailment_def)

lemma guard_type:
  assumes "P \<restriction> D[ [b?] ]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r [b?]"    (* \<Gamma>, P \<turnstile> b : low *)
  shows "\<turnstile> \<Gamma>,\<S>,P,D { [b?] }\<^sub>a \<Gamma>,\<S>,P+[ [b?] ]\<^sub>\<S>,D+[ [b?] ]\<^sub>D"
  using assms
  unfolding secure_write_def secure_update_def subtype_def falling_def rising_def
            state_upd_def pred_entailment_def
  by auto

lemma fence_type:
  shows "\<turnstile> \<Gamma>,\<S>,P,D { Fence }\<^sub>a \<Gamma>,\<S>,P \<restriction> stable \<S>,D+[Fence]\<^sub>D"
  unfolding secure_write_def secure_update_def subtype_def falling_def rising_def
            state_upd_def pred_entailment_def
  by auto

text \<open>
  Compose two typed checked programs via seq.
\<close>
lemma seq_type:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c1 } \<Gamma>i,\<S>i,Pi,Di"
  assumes "\<turnstile> \<Gamma>i,\<S>i,Pi,Di { c2 } \<Gamma>',\<S>',P',D'"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { c1 ;; c2 } \<Gamma>',\<S>',P',D'"
  using assms
proof (induct rule: has_type.induct)
  case (stop_type \<Gamma> \<S> P D)
  then show ?case using Seq.simps by auto
next
  case (act_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 a \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3)
  then show ?case using Seq.simps has_type.act_type by auto
next
  case (choice_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c\<^sub>2)
  then show ?case using Seq.simps has_type.choice_type by auto
next
  case (loop_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 c\<^sub>2 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2)
  then show ?case using Seq.simps has_type.loop_type by auto
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  thus ?case
    using has_type.rewrite state_ord_refl by metis
qed

lemma rep_type:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>,\<S>,P,D"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { unroll c n } \<Gamma>,\<S>,P,D"
  using assms
  by (induct arbitrary: c rule: unroll.induct, auto simp: has_type.stop_type seq_type)

lemma loop_elim_rep:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { Loop c\<^sub>1 c\<^sub>2 } \<Gamma>',\<S>',P',D'"
  shows  "\<turnstile> \<Gamma>,\<S>,P,D { unroll c\<^sub>1 n ;; c\<^sub>2 } \<Gamma>',\<S>',P',D'"
  using assms loop_elim rep_type seq_type rewrite state_ord_refl by meson

lemma flat_type:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>',\<S>',P',D'"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { flat c det } \<Gamma>',\<S>',P',D'"
  using assms
proof (induct c det arbitrary: \<Gamma> \<S> P D \<Gamma>' \<S>' P' D' rule: flat.induct, auto, goal_cases)
  case (1 c1 c2 d \<Gamma> \<S> P D \<Gamma>' \<S>' P' D')
  hence "\<turnstile> \<Gamma>,\<S>,P,D {c1} \<Gamma>',\<S>',P',D'" using choice_elim by metis
  then show ?case using 1 by (auto elim: has_type.cases)
next
  case (2 c1 c2 d \<Gamma> \<S> P D \<Gamma>' \<S>' P' D')
  hence "\<turnstile> \<Gamma>,\<S>,P,D {c2} \<Gamma>',\<S>',P',D'" using choice_elim by metis
  then show ?case using 2 by (auto elim: has_type.cases)
next
  case (3 a c d \<Gamma> \<S> P D \<Gamma>' \<S>' P' D')
  then obtain  \<Gamma>i \<S>i Pi Di where props:
      "\<turnstile> \<Gamma>,\<S>,P,D {a}\<^sub>a \<Gamma>i,\<S>i,Pi,Di"
      "\<turnstile> \<Gamma>i,\<S>i,Pi,Di {c} \<Gamma>',\<S>',P',D'"
    using act_elim by metis
  hence " \<turnstile> \<Gamma>,\<S>,P,D {a ;;; flat c d} \<Gamma>',\<S>',P',D'"
    using 3(1) act_type by metis
  thus ?case using props(1) rewrite by auto
next
  case (4 c1 c2 n d \<Gamma> \<S> P D \<Gamma>' \<S>' P' D')
  then obtain \<Gamma>i \<S>i Pi Di where split:
      "\<turnstile> \<Gamma>i,\<S>i,Pi,Di {c1} \<Gamma>i,\<S>i,Pi,Di"
      "\<turnstile> \<Gamma>i,\<S>i,Pi,Di {c2} \<Gamma>',\<S>',P',D'"
      "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>i,\<S>i,Pi,Di"
    using loop_elim by metis
  hence "\<turnstile> \<Gamma>i,\<S>i,Pi,Di {flat (unroll c1 n ;; c2) d } \<Gamma>',\<S>',P',D'" 
    using 4 rep_type seq_type by metis
  then show ?case using split(3) rewrite by auto
qed

(*--------------------------------------*)

lemma wmfInter_subEq:
  assumes "D1 = D2 \<inter>\<^sub>D D3"
  shows "sub_eq D1 D2 \<and> sub_eq D1 D3"
  using assms wmf_intersect_def sub_eq_def by auto

lemma P_disj_entail:
  assumes "P1 = PDisj P2 P3 \<restriction> V"
  shows "P2 \<turnstile> P1 \<and> P3 \<turnstile> P1"
  using assms lpred_restrict_weaken pred_entailment_def lpred_restrict_weakens by auto 

lemma tyenv_remains_wellformed:
  assumes "\<exists> P3. P\<^sub>1' = PDisj P\<^sub>2 P3 \<restriction> stable \<S>\<^sub>1 \<or> P\<^sub>1' = PDisj P3 P\<^sub>2 \<restriction> stable \<S>\<^sub>1" 
  assumes "dom \<Gamma>\<^sub>2 = dom \<Gamma>\<^sub>1'"  
  shows "\<forall>env. tyenv_wellformed env \<Gamma>\<^sub>2 \<S>\<^sub>1 P\<^sub>2 \<longrightarrow> tyenv_wellformed env \<Gamma>\<^sub>1' \<S>\<^sub>1 P\<^sub>1'"
    using assms tyenv_wellformed_def AsmNoW_def AsmNoRW_def GuarNoW_def GuarNoRW_def
      stable_def
    by auto
   
lemma has_type_\<Gamma>_dom:
  assumes "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  shows "dom \<Gamma>\<^sub>1 = dom \<Gamma>\<^sub>2"
  using assms
proof (induct rule: has_type.induct)
  case (stop_type \<Gamma> \<S> P D)
  then show ?case by simp
next
  case (act_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<alpha> \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3)
  then show ?case using state_upd_\<Gamma>_dom by force 
next
  case (choice_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c\<^sub>2)
  then show ?case by simp
next
  case (loop_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 c\<^sub>2 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2)
  then show ?case by simp
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  then show ?case by (simp add: context_equiv_def state_ord_def) 
qed


definition sup_\<Gamma>:: "('Var,'Val) TyEnv \<Rightarrow> ('Var,'Val) TyEnv \<Rightarrow> ('Var,'Val) TyEnv"
   (infix "\<squnion>\<^sub>\<Gamma>" 60)
  where "sup_\<Gamma> \<Gamma>\<^sub>2 \<Gamma>\<^sub>3 \<equiv> (\<lambda>x. if x \<in> dom \<Gamma>\<^sub>2 then Some ( sup (the(\<Gamma>\<^sub>2 x)) (the(\<Gamma>\<^sub>3 x))) else None)" 


lemma maintain_dom_Gamma:
  assumes "\<Gamma>\<^sub>1 = \<Gamma>\<^sub>2 \<squnion>\<^sub>\<Gamma> \<Gamma>\<^sub>3"
  shows "dom \<Gamma>\<^sub>1 = dom \<Gamma>\<^sub>2" using assms sup_\<Gamma>_def
  by (metis (mono_tags, hide_lams) domIff option.distinct(1) subsetI subset_antisym) 

lemma has_type_\<S>:
  assumes "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  shows "\<S>\<^sub>1 = \<S>\<^sub>2"
  using assms
proof (induct rule: has_type.induct)
  case (stop_type \<Gamma> \<S> P D)
  then show ?case by auto
next
  case (act_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<alpha> \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3)
  then show ?case unfolding state_upd_def by auto
next
  case (choice_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c\<^sub>2)
  then show ?case by auto
next
  case (loop_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 c\<^sub>2 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2)
  then show ?case by auto
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  then show ?case unfolding state_ord_def by auto
qed

(*------------------------------------------*)
(* new if rule that computes the post-state *)

lemma if_type_compute:
  assumes "\<Gamma>\<^sub>1' = \<Gamma>\<^sub>2 \<squnion>\<^sub>\<Gamma> \<Gamma>\<^sub>3"
  assumes "P\<^sub>1' = (PDisj P\<^sub>2 P\<^sub>3) \<restriction> stable \<S>\<^sub>1"
  assumes "D\<^sub>1' = D\<^sub>2 \<inter>\<^sub>D D\<^sub>3"
  assumes "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>2 } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  shows
   "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 ? c\<^sub>2 } \<Gamma>\<^sub>1',\<S>\<^sub>1,P\<^sub>1',D\<^sub>1'" 
proof (rule choice_type)
  have d1: "dom \<Gamma>\<^sub>2 = dom \<Gamma>\<^sub>1'" by (metis (full_types) assms(1) maintain_dom_Gamma)
  have d2: "dom \<Gamma>\<^sub>2 = dom \<Gamma>\<^sub>3" using assms(4,5) has_type_\<Gamma>_dom by metis
  have s: "\<S>\<^sub>1 = \<S>\<^sub>2" "\<S>\<^sub>1 = \<S>\<^sub>3" using has_type_\<S> assms(4,5) by auto
  show "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 }  \<Gamma>\<^sub>1',\<S>\<^sub>1,P\<^sub>1',D\<^sub>1'"
  proof -
    have a1: "\<Gamma>\<^sub>2,\<S>\<^sub>1,P\<^sub>2,D\<^sub>2 >\<^sub>s \<Gamma>\<^sub>1',\<S>\<^sub>1,P\<^sub>1',D\<^sub>1'"   
    proof -
      have f1: "dom \<Gamma>\<^sub>2 = dom \<Gamma>\<^sub>1'" using d1 d2 by presburger
      then have "context_equiv \<Gamma>\<^sub>2 \<Gamma>\<^sub>1'"
        using assms(1) sup_\<Gamma>_def context_equiv_def context_equiv_type_def d1
        by (metis (no_types, lifting) option.sel sup_ge1)
      then show ?thesis
        using f1 by (metis (full_types) P_disj_entail assms(2) assms(3) state_ord_def 
                      tyenv_remains_wellformed wmfInter_subEq)
    qed 
    show ?thesis using a1 using s assms(4) rewrite by auto 
  qed
  show "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>2 }  \<Gamma>\<^sub>1',\<S>\<^sub>1,P\<^sub>1',D\<^sub>1'" 
  proof -
    have a2: "\<Gamma>\<^sub>3,\<S>\<^sub>1,P\<^sub>3,D\<^sub>3 >\<^sub>s \<Gamma>\<^sub>1',\<S>\<^sub>1,P\<^sub>1',D\<^sub>1'" 
    proof -
      have f1: "dom \<Gamma>\<^sub>3 = dom \<Gamma>\<^sub>1'" using d1 d2 by presburger
      then have "context_equiv \<Gamma>\<^sub>3 \<Gamma>\<^sub>1'"
        using assms(1) context_equiv_def context_equiv_type_def d1
        by (metis option.sel sifum_types.sup_\<Gamma>_def sifum_types_axioms sup_ge2)        
      then show ?thesis
        by (metis P_disj_entail assms(2) assms(3) f1 state_ord_def tyenv_remains_wellformed 
                  wmfInter_subEq) 
    qed 
    show ?thesis using s a2 assms(5) rewrite by auto
  qed
qed

lemma if_type_weak:
  assumes "\<exists> \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 . \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes  "\<exists> \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3 .  \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>2 } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  shows
   "\<exists> \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' . \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 ? c\<^sub>2 } \<Gamma>\<^sub>1',\<S>\<^sub>1',P\<^sub>1',D\<^sub>1'" 
proof -
  obtain \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 where a3: "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>1 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2" using assms(1) by metis
  obtain \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3 where a4: "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>2 } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3" using assms(2) by metis
  show ?thesis 
  proof (intro exI; rule if_type_compute [where ?\<Gamma>\<^sub>2=\<Gamma>\<^sub>2 and ?\<Gamma>\<^sub>3=\<Gamma>\<^sub>3], auto)
    show "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 {c\<^sub>1} \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2" using a3 by auto
    show "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c\<^sub>2 } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3" using a4 by auto
  qed
qed

(*------------------------------------------*)
(* new while rule that computes the post-state *)

lemma same_known:
  assumes "reads \<alpha> = reads \<beta>"
  assumes "writes \<alpha> = writes \<beta>"
  assumes "D_gen \<alpha> = D_gen \<beta>"
  shows "known D \<alpha> = known D \<beta>"
  unfolding known_def assms by auto

lemma same_update:
  assumes "reads \<alpha> = reads \<beta>"
  assumes "writes \<alpha> = writes \<beta>"
  assumes "D_gen \<alpha> = D_gen \<beta>"
  assumes "D_kill \<alpha> = D_kill \<beta>"
  assumes "limit\<^sub>w \<alpha> = limit\<^sub>w \<beta>"
  assumes "limit\<^sub>r \<alpha> = limit\<^sub>r \<beta>"
  assumes "forwardable \<alpha> = forwardable \<beta>"
  shows "D+[\<alpha>]\<^sub>D = D+[\<beta>]\<^sub>D"
  unfolding update_def assms same_known[OF assms(1,2,3)]
  by auto

lemma D_negGuard:
  "(D+[[b?]]\<^sub>D) = (D+[[(bexp_neg b)?]]\<^sub>D)" 
  by (intro same_update, auto simp: bexp_neg_vars D_gen_def D_kill_def)

lemma negGuard_low:
  "P \<restriction> D[ [b?] ]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r [b?] \<longrightarrow> P \<restriction> D[ [(bexp_neg b)?] ]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r [(bexp_neg b)?]" 
  using bexp_neg_vars D_negGuard D_gen_def known_def by auto 

lemma loop_type_compute:
  assumes a1: "P\<^sub>i \<restriction> D\<^sub>i[ [b?] ]\<^sub>w \<turnstile> \<Gamma>\<^sub>i :\<^sub>r [b?]"      (* \<Gamma>\<^sub>i, P\<^sub>i \<turnstile> b:Low *) 
  assumes "context_equiv \<Gamma> \<Gamma>\<^sub>i"                    (* types can go higher: \<Gamma>(x) \<sqsubseteq> \<Gamma>\<^sub>i(x) *)
  assumes "lpred_vars P\<^sub>i \<subseteq> stable \<S>" 
  assumes "P \<turnstile> P\<^sub>i"                                (* P\<^sub>i less restrictive than P *)
  assumes "D\<^sub>i \<subseteq>\<^sub>D D"                               (* D\<^sub>i knows less *)  
  assumes "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [b?] ]\<^sub>\<S>,D\<^sub>i+[[b?]]\<^sub>D { c1 } \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i"
  assumes "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [(bexp_neg b)?] ]\<^sub>\<S>,(D\<^sub>i+[[b?]]\<^sub>D) { c2 } \<Gamma>',\<S>,P',D'"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { While b c1 c2 } \<Gamma>',\<S>,P',D'" 
  unfolding While_def
proof (rule rewrite [where ?\<Gamma>\<^sub>1=\<Gamma>\<^sub>i and ?\<S>\<^sub>1=\<S> and ?P\<^sub>1=P\<^sub>i and ?D\<^sub>1=D\<^sub>i and 
                           ?\<Gamma>\<^sub>1'=\<Gamma>' and ?\<S>\<^sub>1'=\<S> and ?P\<^sub>1'=P' and ?D\<^sub>1'=D'])
  show "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i" using assms state_ord_def tyenv_wellformed_def 
     context_equiv_def by auto
  show "\<Gamma>',\<S>,P',D' >\<^sub>s \<Gamma>',\<S>,P',D'" using state_ord_def by simp
  show "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i {Loop ([b?] ;;; c1) ([bexp_neg b?] ;;; c2)} \<Gamma>',\<S>,P',D'"
  proof (rule loop_type)
  show "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i { ([b?] ;;; c1) }  \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i" 
    proof (rule act_type)
      show b1: "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i { [b?] }\<^sub>a  \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [b?] ]\<^sub>\<S>,(D\<^sub>i+[[b?]]\<^sub>D)"
        using guard_type a1 by blast 
      show b2: "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [b?] ]\<^sub>\<S>,D\<^sub>i+[[b?]]\<^sub>D { c1 } \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i" using assms by blast
    qed
  show "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i { [bexp_neg b?] ;;; c2 }  \<Gamma>',\<S>,P',D'" 
    proof (rule act_type) 
      show " \<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i {[(bexp_neg b)?]}\<^sub>a \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [(bexp_neg b)?] ]\<^sub>\<S>,(D\<^sub>i+[[(b)?]]\<^sub>D)" 
        using guard_type negGuard_low a1 D_negGuard by metis
      show "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [bexp_neg b?] ]\<^sub>\<S>,(D\<^sub>i+[[b?]]\<^sub>D) {c2} \<Gamma>',\<S>,P',D'" using assms by blast
    qed
  qed
qed

text \<open>Support for do-while syntax by merging seq and While rules\<close>
lemma do_loop_type_compute:
  assumes "P\<^sub>i \<restriction> D\<^sub>i[ [b?] ]\<^sub>w \<turnstile> \<Gamma>\<^sub>i :\<^sub>r [b?]"
  assumes "context_equiv \<Gamma>\<^sub>2 \<Gamma>\<^sub>i"
  assumes "lpred_vars P\<^sub>i \<subseteq> stable \<S>"
  assumes "P\<^sub>2 \<turnstile> P\<^sub>i"
  assumes "D\<^sub>i \<subseteq>\<^sub>D D\<^sub>2"
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c1 } \<Gamma>\<^sub>2,\<S>,P\<^sub>2,D\<^sub>2"
  assumes "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [b?] ]\<^sub>\<S>,D\<^sub>i+[[b?]]\<^sub>D { c1 } \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i"
  assumes "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i+[ [(bexp_neg b)?] ]\<^sub>\<S>,(D\<^sub>i+[[b?]]\<^sub>D) { c2 } \<Gamma>',\<S>,P',D'"
  shows "\<turnstile> \<Gamma>,\<S>,P,D { DoWhile c1 b c2 } \<Gamma>',\<S>,P',D'" 
  unfolding DoWhile_def
proof -
  have "\<turnstile> \<Gamma>\<^sub>2,\<S>,P\<^sub>2,D\<^sub>2 { While b c1 c2 } \<Gamma>',\<S>,P',D'" using loop_type_compute assms by auto
  thus "\<turnstile> \<Gamma>,\<S>,P,D {c1 ;; Loop ([b?] ;;; c1) ([bexp_neg b?] ;;; c2)} \<Gamma>',\<S>,P',D'"
    using assms(6) seq_type unfolding While_def by auto
qed

lemma loop_type_weak:
  assumes "\<exists> \<Gamma>\<^sub>i P\<^sub>i D\<^sub>i . (\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i { ([b?] ;;; c1) }  \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i)
                       \<and> (\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i { [bexp_neg b?] ;;; c2 }  \<Gamma>',\<S>,P',D')"
  shows "\<exists> \<Gamma> P D . \<turnstile> \<Gamma>,\<S>,P,D { Loop ([b?] ;;; c1) ( [(bexp_neg b)?] ;;; c2 ) } \<Gamma>',\<S>,P',D'" 
proof -
  obtain \<Gamma>\<^sub>i P\<^sub>i D\<^sub>i where a3: "(\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i { ([b?] ;;; c1) } \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i) \<and>
                         (\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i { ([bexp_neg b?] ;;; c2) } \<Gamma>',\<S>,P',D')" using assms by blast  
  show ?thesis
    using assms loop_type by blast
qed

(*--------------------------------------*)

lemma spec_seq_type:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>',\<S>',P',D'"
  shows "\<exists>\<Gamma>f \<S>f Pf Df. \<turnstile> \<Gamma>,\<S>,P,D { spec_seq c det } \<Gamma>f,\<S>f,Pf,Df"
  using assms
  by (induct c det arbitrary: \<Gamma> \<S> P D \<Gamma>' \<S>' P' D' rule: spec_seq.induct ; auto ;
      meson stop_type choice_elim loop_elim_rep act_elim act_type)

subsection \<open> Relation Definitions \<close>

definition tyenv_eq :: "('Var, 'Val) Mem \<Rightarrow>('Var,'Val) TyEnv \<Rightarrow> ('Var, 'Val) Mem \<Rightarrow> bool"
  ("_ =\<^bsub>_\<^esub> _" 60)
  where "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 \<equiv> \<forall>x. type_max (to_total \<Gamma> x) mem\<^sub>1 = Low \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x"

abbreviation
  tyenv_sec :: "('Var ,'Val) Env \<Rightarrow> ('Var,'Val) TyEnv \<Rightarrow> ('Var ,'Val) Mem \<Rightarrow> bool"
where
  "tyenv_sec env \<Gamma> mem \<equiv> \<forall>x \<in> dom \<Gamma> - AsmNoRW env. (the (\<Gamma> x)) \<le> dma mem x"

inductive \<R> :: 
  "('Var, 'Val, 'AExp, 'BExp) LC \<Rightarrow>
  ('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow>
  ('Var, 'Val, 'AExp, 'BExp) LC \<Rightarrow> bool"
  ("_ \<R>\<^bsub>_,_,_,_\<^esub> _" [20, 120, 120, 120, 120, 20] 1000)
  and \<R>_set :: "('Var,'Val) TyEnv \<Rightarrow> 'Var Stable \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 'Var Flow \<Rightarrow> ('Var, 'Val, 'AExp, 'BExp) LC rel"
where
  "\<R>_set \<Gamma>' \<S>' P' D' \<equiv> {(lc\<^sub>1, lc\<^sub>2). \<R> lc\<^sub>1 \<Gamma>' \<S>' P' D' lc\<^sub>2}" |
  intro [intro] :
  "\<lbrakk> \<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>',\<S>',P',D';
     tyenv_wellformed env \<Gamma> \<S> P;
     mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2;
     pred P mem\<^sub>1;
     pred P mem\<^sub>2;
     tyenv_sec env \<Gamma> mem\<^sub>1 \<rbrakk> \<Longrightarrow> 
  \<langle>c, det, env, mem\<^sub>1\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c, det, env, mem\<^sub>2\<rangle>" 

inductive_cases \<R>_elim [elim]: "\<langle>c\<^sub>1, det\<^sub>1, env, mem\<^sub>1\<rangle> \<R>\<^bsub>\<Gamma>,\<S>,P,D\<^esub> \<langle>c\<^sub>2, det\<^sub>2, env\<^sub>2, mem\<^sub>2\<rangle>"

subsection \<open> Relation Lemmas \<close>

lemma type_max_dma_type [simp]:
  "type_max (bexp_to_lpred (dma_type x)) (mem) = dma mem x"
  using dma_correct unfolding type_max_def by (auto simp: bexp_to_lpred_correct)

lemma \<R>_mem_eq:
  assumes "\<langle>c\<^sub>1, det, env, mem\<^sub>1\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>2, det', env, mem\<^sub>2\<rangle>"
  shows "low_mds_eq env mem\<^sub>1 mem\<^sub>2"
  using assms
proof (auto elim!: \<R>.cases)
  fix \<Gamma> \<S> P sp
  assume wf: "tyenv_wellformed env \<Gamma> \<S> P"
  assume tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assume leq: "\<forall>x \<in> dom \<Gamma> - AsmNoRW env.  (the (\<Gamma> x))  \<le> dma mem\<^sub>1 x"
  assume pred: "lpred_eval mem\<^sub>1 P"
  
  show "low_mds_eq env mem\<^sub>1 mem\<^sub>2"
  unfolding low_mds_eq_def
  proof(clarify)
    fix x
    assume is_Low: "dma mem\<^sub>1 x = Low"
    assume is_readable: "x \<in> \<C> \<or> x \<notin> AsmNoRW env"
    show "mem\<^sub>1 x = mem\<^sub>2 x"

      proof (cases "x \<in> dom \<Gamma>")
        case True
        with wf have "x \<notin> \<C>" unfolding tyenv_wellformed_def by blast
        with is_readable have notin: "x \<notin> AsmNoRW env" by auto
         
        with leq have "type_max (to_total \<Gamma> x) (mem\<^sub>1) \<le> dma mem\<^sub>1 x"
          unfolding to_total_def sec_def using True is_Low notin
          apply (auto simp: type_max_def less_eq_Sec_def split: if_splits)
          by force
        with tyenv_eq is_Low have "type_max (to_total \<Gamma> x) (mem\<^sub>1) = Low"
          unfolding tyenv_eq_def using less_eq_Sec_def by simp
        with tyenv_eq show ?thesis
          unfolding tyenv_eq_def  by blast
      next
        case False
        with is_Low tyenv_eq  show ?thesis
          unfolding tyenv_eq_def to_total_def
           by simp
      qed
    qed
qed

lemma \<R>_dma_eq:
  "\<langle>c\<^sub>1, xs, mds, mem\<^sub>1\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>2, xs', mds, mem\<^sub>2\<rangle> \<Longrightarrow> dma mem\<^sub>1 = dma mem\<^sub>2"
  apply(drule \<R>_mem_eq)
  apply(erule low_mds_eq_dma)
  done

(* \<R> meets the criteria of a "simple bisim"
   which can be used to simplify the establishment of a refinement relation *)

lemma bisim_simple_\<R>:
  "lc \<R>\<^bsub>\<Gamma>,\<S>,P,D\<^esub> lc' \<Longrightarrow> (fst (fst (fst lc))) = (fst (fst (fst lc')))"
  by(cases rule: \<R>.cases, simp+)

lemma \<R>_det_eq:
  "lc \<R>\<^bsub>\<Gamma>,\<S>,P,D\<^esub> lc' \<Longrightarrow> (snd (fst (fst lc))) = (snd (fst (fst lc')))"
  by(cases rule: \<R>.cases, simp+)

lemma \<R>_env [simp]:
  "\<langle>c\<^sub>1, det, env, mem\<^sub>1\<rangle> \<R>\<^bsub>\<Gamma>,\<S>,P,D\<^esub> \<langle>c\<^sub>2, det', env', mem\<^sub>2\<rangle> \<Longrightarrow> env = env'"
  by (rule \<R>.cases, auto)

(* To prove that \<R> is a bisimulation, we first show symmetry *)

lemma vars_of_type_eq_type_max_eq:
  assumes mem_eq: "\<forall>x \<in> lpred_vars t. mem\<^sub>1 ( x) = mem\<^sub>2 ( x)" 
  shows "type_max t (mem\<^sub>1) = type_max t (mem\<^sub>2)"
proof -
  have "\<forall>x \<in> lpred_vars t. mem\<^sub>1 ( x) = mem\<^sub>2 ( x)"
    using assms by auto
  hence "pred t mem\<^sub>1 = pred t mem\<^sub>2"
    using assms lpred_eval_vars_det  by blast
  thus ?thesis
    unfolding type_max_def using lpred_eval_vars_det by auto
qed

lemma \<C>_eq_type_max_eq:
  assumes wf: "type_wellformed t"
  assumes \<C>_eq: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>2 x" 
  shows "type_max t mem\<^sub>1 = type_max t mem\<^sub>2"
proof -
  have "\<forall>x\<in>lpred_vars t. mem\<^sub>1 x = mem\<^sub>2 x" using wf \<C>_eq by blast
  thus ?thesis
    using vars_of_type_eq_type_max_eq by auto
qed

lemma flat_type_max_eq:
  assumes wf: "lpred_vars t \<subseteq> {}"
  shows "type_max t mem\<^sub>1 = type_max t mem\<^sub>2"
proof -
  have "\<forall>x\<in>lpred_vars t. mem\<^sub>1 x = mem\<^sub>2 x" using wf by auto
  thus ?thesis
    using vars_of_type_eq_type_max_eq by auto
qed

lemma \<R>_closed_glob_consistent: "closed_glob_consistent (\<R>_set \<Gamma>' \<S>' P' D')"
  unfolding closed_glob_consistent_def
proof (clarify)
  fix c\<^sub>1 xs\<^sub>1 env mem\<^sub>1 c\<^sub>2 xs\<^sub>2 mem\<^sub>2 A
  assume R1: "\<langle>c\<^sub>1, xs\<^sub>1, env, mem\<^sub>1\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>2, xs\<^sub>2, env, mem\<^sub>2\<rangle>"
  hence [simp]: "c\<^sub>2 = c\<^sub>1" by blast

  assume A_updates_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written env x"
  assume A_updates_dma: "\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written env x"
  assume A_updates_rel: " \<forall>R\<in>Relies env. (mem\<^sub>1, mem\<^sub>1 [\<parallel>\<^sub>1 A]) \<in> R \<and> (mem\<^sub>2, mem\<^sub>2 [\<parallel>\<^sub>2 A]) \<in> R"
  assume A_updates_sec: "\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = Low \<and> (x \<notin> AsmNoRW env \<or> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
  from R1 obtain \<Gamma> \<S> P D where \<Gamma>_props:
       "c\<^sub>1 = c\<^sub>2" "xs\<^sub>1 = xs\<^sub>2" 
       "\<turnstile> \<Gamma>,\<S>,P,D {c\<^sub>1} \<Gamma>',\<S>',P',D'"
       "tyenv_wellformed env \<Gamma> \<S> P"
       "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
       "pred P mem\<^sub>1" "pred P mem\<^sub>2"
       "\<forall>x\<in> dom \<Gamma> - AsmNoRW env.  (the (\<Gamma> x))  \<le> dma mem\<^sub>1 x" 
    by blast

  from \<Gamma>_props(4) have stable_not_written: "\<forall>x \<in> stable \<S>. var_asm_not_written env x"
    by(auto simp: tyenv_wellformed_def stable_def var_asm_not_written_def)
  with A_updates_vars have stable_unchanged\<^sub>1: "\<forall>x \<in> stable \<S>. (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = mem\<^sub>1 x" and
                           stable_unchanged\<^sub>2: "\<forall>x \<in> stable \<S>. (mem\<^sub>2 [\<parallel>\<^sub>2 A]) x = mem\<^sub>2 x"
    by(auto simp: apply_adaptation_def split: option.splits)

  from stable_not_written A_updates_dma 
  have stable_unchanged_dma\<^sub>1: "\<forall>x \<in> stable \<S>. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = dma mem\<^sub>1 x" by blast

  let ?pmem\<^sub>1' = "(mem\<^sub>1 [\<parallel>\<^sub>1 A])"
  let ?pmem\<^sub>2' = "(mem\<^sub>2 [\<parallel>\<^sub>2 A])"

  have tyenv_eq': "mem\<^sub>1 [\<parallel>\<^sub>1 A] =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 [\<parallel>\<^sub>2 A]"
  proof (clarsimp simp: tyenv_eq_def)
    fix x 

    assume a: "type_max (to_total \<Gamma> x) ?pmem\<^sub>1' = Low"
    show "mem\<^sub>1 [\<parallel>\<^sub>1 A] ( x) = mem\<^sub>2 [\<parallel>\<^sub>2 A]  ( x)"
    proof (cases "x \<in> dom \<Gamma>")
      assume in_dom: "x \<in> dom \<Gamma>"
      with \<Gamma>_props(4) have "var_asm_not_written env  ( x)"
        by(auto simp: tyenv_wellformed_def var_asm_not_written_def stable_def)
      hence [simp]: "mem\<^sub>1 [\<parallel>\<^sub>1 A]  ( x) = mem\<^sub>1  ( x)" and [simp]: "mem\<^sub>2 [\<parallel>\<^sub>2 A]  ( x) = mem\<^sub>2  ( x)"
        using A_updates_vars by(auto simp: apply_adaptation_def split: option.splits)
      from in_dom a obtain t\<^sub>x where \<Gamma>\<^sub>x: "\<Gamma> x = Some t\<^sub>x" and t\<^sub>x_Low': " t\<^sub>x  = Low"
        by(auto simp: to_total_def sec_def type_max_def split: if_splits)

      with t\<^sub>x_Low' have t\<^sub>x_Low: "type_max (to_total \<Gamma> x) (mem\<^sub>1) = Low"
        by(auto simp: to_total_def sec_def type_max_def split: if_splits)
      with \<Gamma>\<^sub>x \<Gamma>_props(5) have "mem\<^sub>1 x = mem\<^sub>2 x"
        by(force simp: tyenv_eq_def to_total_def split: if_splits)
      thus ?thesis by simp
    next
      assume nin_dom: "x \<notin> dom \<Gamma>"
      with a have is_Low': "dma (mem\<^sub>1[\<parallel>\<^sub>1 A]) ( x) = Low"
        by(simp add: to_total_def)
      show ?thesis
      proof(cases " ( x) \<notin> AsmNoRW env \<or> x \<in> \<C>")
        assume " ( x) \<notin> AsmNoRW env \<or> x \<in> \<C>"
        with is_Low' show ?thesis
          using A_updates_sec  by blast
      next
        assume "\<not> ( ( x) \<notin> AsmNoRW env \<or> x \<in> \<C>)"
        hence stable_NoRW': " ( x) \<in> AsmNoRW env" and nin_\<C>: "x \<notin> \<C>"
          by auto
        have "dom \<Gamma> = stable \<S> - \<C>"
          using \<Gamma>_props(4) unfolding tyenv_wellformed_def by blast
        with nin_dom nin_\<C> have unstable: "x \<notin> stable \<S>" by blast
        from stable_NoRW' have stable_NoRW: "x \<in> NoRW \<S>"
          using \<Gamma>_props(4) unfolding tyenv_wellformed_def 
          by auto
        with unstable have "False"
          unfolding stable_def by blast
        thus ?thesis by blast
      qed
    qed
  qed

  have sec': "\<forall>x\<in>dom \<Gamma> -  (AsmNoRW env).  (the (\<Gamma> x))  \<le> dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) ( x)"
  proof(intro ballI impI)
    fix x
    assume in_dom: "x \<in> dom \<Gamma> -  (AsmNoRW env)"
    with \<Gamma>_props(4) have "var_asm_not_written env ( x)"
      by(auto simp: tyenv_wellformed_def var_asm_not_written_def stable_def)
    hence [simp]: "dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) ( x) = dma mem\<^sub>1 ( x)"
      using A_updates_dma by(auto simp: apply_adaptation_def split: option.splits)
    from in_dom obtain t\<^sub>x where \<Gamma>\<^sub>x: "\<Gamma> x = Some t\<^sub>x"
      by(auto simp: to_total_def)
    
    show " (the (\<Gamma> x))  \<le> dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) ( x)"
      apply simp
      using in_dom \<Gamma>_props by metis
  qed

  have st: "lpred_vars P \<subseteq> stable \<S>"
    by (meson \<Gamma>_props(4) sifum_types.tyenv_wellformed_def sifum_types_axioms)

  have 1: "\<forall>x \<in> lpred_vars P. (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = mem\<^sub>1 x"
    using stable_unchanged\<^sub>1 st by auto
  have 2: "\<forall>x \<in> lpred_vars P. (mem\<^sub>2 [\<parallel>\<^sub>2 A]) x = mem\<^sub>2 x"
    using  stable_unchanged\<^sub>2 st  by auto

  from 1 2 
  have "lpred_eval  (mem\<^sub>1 [\<parallel>\<^sub>1 A]) P = lpred_eval mem\<^sub>1 P \<and> 
        lpred_eval  (mem\<^sub>2 [\<parallel>\<^sub>2 A]) P = lpred_eval mem\<^sub>2 P"
    using lpred_eval_vars_det 
    by blast

  hence preds: 
        "pred P ?pmem\<^sub>1' = pred P (mem\<^sub>1)" 
        "pred P ?pmem\<^sub>2' = pred P (mem\<^sub>2)"
    by auto

  with preds \<Gamma>_props tyenv_eq' sec'
  show "\<langle>(c\<^sub>1, xs\<^sub>1), env, mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>(c\<^sub>2, xs\<^sub>2), env, mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
    by auto
qed

lemma tyenv_sec_eq:
  "\<forall>x \<in> \<C>. mem x = mem' x \<Longrightarrow> tyenv_sec mds \<Gamma> mem = tyenv_sec mds \<Gamma> mem'"
  by (smt DiffD1 \<C>_eq_type_max_eq dma_\<C>)

subsection \<open> Type Checking over Reorderings \<close>

subsubsection \<open> Speculation \<close>

definition spec\<^sub>\<S> :: "('Var,'Val) lpred preds \<Rightarrow> 'Var Stable \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> 'Var set \<Rightarrow> ('Var,'Val) lpred preds"
  where "spec\<^sub>\<S> P \<S> c V \<equiv> PConj P (PConj (specr PTrue (stable \<S>) c) P \<restriction> V)"

abbreviation spec_flat\<^sub>\<S> :: "('Var,'Val) lpred preds \<Rightarrow> 'Var Stable \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> DetChoice list \<Rightarrow> 'Var set \<Rightarrow> ('Var,'Val) lpred preds"
  where "spec_flat\<^sub>\<S> P \<S> c det V \<equiv> spec\<^sub>\<S> P \<S> (spec_seq c det) V"

lemma spec_entails:
  shows "spec\<^sub>\<S> P \<S> c V \<turnstile> P"
  using lpred_restrict_conj_r
  unfolding spec\<^sub>\<S>_def pred_entailment_def by auto

lemma spec_vars:
  shows "lpred_vars P \<subseteq> lpred_vars (spec\<^sub>\<S> P \<S> c V)"
  unfolding spec\<^sub>\<S>_def by auto

lemma specr_stable:
  assumes "lpred_vars P \<subseteq> \<S>"
  shows "lpred_vars (specr P \<S> \<alpha>) \<subseteq> \<S>"
  using assms lpred_restrict_vars
  by (induct P \<S> \<alpha> rule: specr.induct ; simp)

lemma spec_stable:
  assumes "lpred_vars P \<subseteq> \<S>"
  shows "lpred_vars (spec P \<S> c) \<subseteq> \<S>"
  using assms specr_stable unfolding spec_def by auto

lemma spec\<^sub>\<S>_stable:
  assumes "lpred_vars P \<subseteq> stable \<S>"
  shows "lpred_vars (spec\<^sub>\<S> P \<S> c V) \<subseteq> (stable \<S>)"
proof -
  have "lpred_vars (specr PTrue (stable \<S>) c) \<subseteq> stable \<S>"
    using assms lpred_restrict_vars specr_stable by auto
  thus ?thesis using assms lpred_restrict_vars unfolding spec\<^sub>\<S>_def by auto
qed

lemma specr_equiv_modulo_writes:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes stable: "lpred_vars P \<subseteq> stable \<S>"
  shows "PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P \<restriction> (-writes \<alpha>) =\<^sub>P 
         PConj (specr PTrue (stable \<S>) c) (P+[\<alpha>]\<^sub>\<S>) \<restriction> (-writes \<alpha>)"
    (is "?l =\<^sub>P ?r")
proof (cases \<alpha>)
  case (Assign x e)
  moreover have "lpred_vars (specr PTrue (stable \<S>) c) \<subseteq> stable \<S>" using specr_stable by auto
  ultimately show ?thesis using stable spec_reassign_equiv by auto
next
  case Fence
  then show ?thesis using re reorder_not_fence by auto
next
  case (Guard b)
  moreover have "lpred_vars (specr PTrue (stable \<S>) c) \<subseteq> stable \<S>" using specr_stable by auto
  ultimately show ?thesis using stable lpred_restrict_conj_eq_r pred_equiv_def by auto
qed

lemma spec\<^sub>\<S>_equiv_modulo_writes:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wr: "writes \<alpha> \<inter> V = {}"
  assumes stable: "lpred_vars P \<subseteq> stable \<S>"
  shows "(spec\<^sub>\<S> P \<S> (\<alpha> ;;; c) V) \<restriction> V =\<^sub>P (spec\<^sub>\<S> (P+[\<alpha>]\<^sub>\<S>) \<S> c V) \<restriction> V" (is "?l =\<^sub>P ?r")
proof -
  have "PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P \<restriction> (-writes \<alpha>) =\<^sub>P 
         PConj (specr PTrue (stable \<S>) c) (P+[\<alpha>]\<^sub>\<S>) \<restriction> (-writes \<alpha>)" (is "?l' \<restriction> ?V =\<^sub>P ?r' \<restriction> ?V")
    using assms specr_equiv_modulo_writes by metis
  moreover have "- writes \<alpha> \<inter> V = V" using wr by auto
  ultimately have "?l' \<restriction> V =\<^sub>P ?r' \<restriction> V"
    using lpred_restrict_equiv [of "?l' \<restriction> ?V" "?r' \<restriction> ?V" "V"]
    using lpred_restrict_inter  by (auto simp: pred_equiv_def)
  moreover have "?r' \<restriction> V \<turnstile> P+[\<alpha>]\<^sub>\<S> \<restriction> V" by (simp add: lpred_restrict_conj_r)
  moreover have "P+[\<alpha>]\<^sub>\<S> \<restriction> V \<turnstile> P \<restriction> V"
    using stable wr
    by (metis inf_bot_right inf_le2 inf_left_commute lpred_restrict_entailment
              lpred_restrict_nop lpred_restrict_vars lpred_restrict_weakens reassign_diff_var)
  moreover have "?l =\<^sub>P PConj (P \<restriction> V) (?l' \<restriction> V)" 
    by (metis inf_le2 lpred_restrict_conj_eq_l lpred_restrict_vars spec\<^sub>\<S>_def)
  moreover have "?r =\<^sub>P PConj (P+[\<alpha>]\<^sub>\<S> \<restriction> V) (?r' \<restriction> V)"
    by (metis inf_le2 lpred_restrict_conj_eq_l lpred_restrict_vars spec\<^sub>\<S>_def)
  ultimately show ?thesis by (auto simp: pred_equiv_def pred_entailment_def)
qed

lemma lexp_expand:
  shows "leval mem (lexp_subst f v y) = leval mem (lexp_subst f (Const (leval mem v)) y)"
  by (induct f, auto)


text \<open>This is terrible. Can't seem to show this property with the high level theorems we already
      have. As a result, we need to break it down into cases and work it through manually.
      Seems there should be a series of theorems related to forwarding that are missing.
      This can be viewed as a consequence of its late addition.\<close>
lemma spec\<^sub>\<S>_equiv_modulo_writes':
  assumes "\<exists>x e. \<beta>' = x \<leftarrow> e \<and> x \<in> globals"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wr: "writes \<alpha> \<inter> V = {}"
  assumes stable: "lpred_vars P \<subseteq> stable \<S> "
  shows "(spec\<^sub>\<S> P \<S> (\<alpha> ;;; c) V)+[\<beta>']\<^sub>\<S> \<restriction> V \<turnstile> (spec\<^sub>\<S> (P+[\<alpha>]\<^sub>\<S>) \<S> c V)+[\<beta>]\<^sub>\<S> \<restriction> V" (is "?l \<turnstile> ?r")
proof -
  obtain x e where \<beta>': "\<beta>' = x \<leftarrow> e" "x \<in> globals" using assms by auto
  then obtain f where \<beta>: "\<beta> = x \<leftarrow> f" using re by (cases \<beta>; cases \<alpha>; auto)
  have w: "x \<notin> vars \<alpha>" using re \<beta>' by (cases \<alpha>; auto simp: min_reorder_def)

  have "PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P \<restriction> (-writes \<alpha>) =\<^sub>P 
         PConj (specr PTrue (stable \<S>) c) (P+[\<alpha>]\<^sub>\<S>) \<restriction> (-writes \<alpha>)" (is "?l' \<restriction> ?V =\<^sub>P ?r' \<restriction> ?V")
    using assms specr_equiv_modulo_writes by metis
  moreover have "- writes \<alpha> \<inter> V = V" using wr by auto
  ultimately have e: "?l' \<restriction> V =\<^sub>P ?r' \<restriction> V"
    using lpred_restrict_equiv [of "?l' \<restriction> ?V" "?r' \<restriction> ?V" V] lpred_restrict_inter
    by (auto simp: pred_equiv_def)

  (* Expand left *)
  have "?l =\<^sub>P PConj P (PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P \<restriction> V) +[\<beta>']\<^sub>\<S> \<restriction> V"
    by (simp add: spec\<^sub>\<S>_def)
  also have "... =\<^sub>P (reassign_var x (aexp_to_lexp e) (PConj P (PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P \<restriction> V)) \<restriction> stable \<S>) \<restriction> V"
    by (simp add: \<beta>')
  also have "... =\<^sub>P (PEx (\<lambda>v. (PConj (lpred_subst (PConj P (PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P \<restriction> V)) (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x)))) \<restriction> stable \<S>) \<restriction> V"
    by (simp add: reassign_var_def)
  also have "... =\<^sub>P (PEx (\<lambda>v. PConj (PConj (lpred_subst P (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x))) (lpred_subst (PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P \<restriction> V) (Const v) x)) \<restriction> stable \<S>) \<restriction> V"
    apply (intro lpred_restrict_equiv)
    unfolding pred_equiv_def
    by auto
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst P (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x))) (lpred_subst (?l' \<restriction> V) (Const v) x)) \<restriction> (stable \<S> \<inter> V)"
    using lpred_restrict_inter by blast 
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst P (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x))) (lpred_subst (?l' \<restriction> V) (Const v) x) \<restriction> (stable \<S> \<inter> V))"
    using ex_lpred_restrict by blast
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst P (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x)) \<restriction> (stable \<S> \<inter> V)) (lpred_subst (?l' \<restriction> V) (Const v) x))"
  proof -
    have "\<forall>v. lpred_vars (lpred_subst (?l' \<restriction> V) (Const v) x) \<subseteq> stable \<S> \<inter> V"
      by (smt Diff_subset Int_subset_iff le_sup_iff lexp_vars.simps(2) lpred_restrict_vars lpred_vars.simps(5) predicate_lang.lpred_subst_vars spec\<^sub>\<S>_def spec\<^sub>\<S>_stable stable subset_trans sup_bot.right_neutral)
    thus ?thesis by (simp add: ex_equiv lpred_restrict_conj_eq_l)
  qed
  finally have l: "?l =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst P (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x)) \<restriction> (stable \<S> \<inter> V)) (lpred_subst (?l' \<restriction> V) (Const v) x))"
    (is "?l =\<^sub>P PEx (\<lambda>v. PConj (PConj (?P v) (?e v) \<restriction> (stable \<S> \<inter> V)) (?ls v)) ")
    by simp

  (* Expand right *)
  have "?r =\<^sub>P PConj (P+[\<alpha>]\<^sub>\<S>) (PConj (specr PTrue (stable \<S>) c) (P+[\<alpha>]\<^sub>\<S>) \<restriction> V)+[\<beta>]\<^sub>\<S> \<restriction> V"
    by (simp add: spec\<^sub>\<S>_def)
  also have "... =\<^sub>P (reassign_var x (aexp_to_lexp f) (PConj (P+[\<alpha>]\<^sub>\<S>) (PConj (specr PTrue (stable \<S>) c) (P+[\<alpha>]\<^sub>\<S>) \<restriction> V)) \<restriction> stable \<S>) \<restriction> V"
    by (simp add: \<beta>)
  also have "... =\<^sub>P (PEx (\<lambda>v. PConj (PConj (lpred_subst (P+[\<alpha>]\<^sub>\<S>) (Const v) x) (lpred_subst (PConj (specr PTrue (stable \<S>) c) (P+[\<alpha>]\<^sub>\<S>) \<restriction> V) (Const v) x)) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x))) \<restriction> stable \<S>) \<restriction> V"
    by (simp add: reassign_var_def)
  also have "... =\<^sub>P (PEx (\<lambda>v. PConj (PConj (lpred_subst (P+[\<alpha>]\<^sub>\<S>) (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x))) (lpred_subst (PConj (specr PTrue (stable \<S>) (c)) (P+[\<alpha>]\<^sub>\<S>) \<restriction> V) (Const v) x)) \<restriction> stable \<S>) \<restriction> V"
    apply (intro lpred_restrict_equiv)
    unfolding pred_equiv_def
    by auto
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst (P+[\<alpha>]\<^sub>\<S>) (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x))) (lpred_subst (?r' \<restriction> V) (Const v) x)) \<restriction> (stable \<S> \<inter> V)"
    using lpred_restrict_inter by blast 
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst (P+[\<alpha>]\<^sub>\<S>) (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x))) (lpred_subst (?r' \<restriction> V) (Const v) x) \<restriction> (stable \<S> \<inter> V))"
    using ex_lpred_restrict by blast
  also have "... =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst (P+[\<alpha>]\<^sub>\<S>) (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x)) \<restriction> (stable \<S> \<inter> V)) (lpred_subst (?r' \<restriction> V) (Const v) x))"
  proof -
    have "\<forall>v. lpred_vars (lpred_subst (?r' \<restriction> V) (Const v) x) \<subseteq> stable \<S> \<inter> V"
      by (smt Diff_subset Int_subset_iff P_upd_stable le_sup_iff lexp_vars.simps(2) lpred_restrict_vars lpred_vars.simps(5) predicate_lang.lpred_subst_vars spec\<^sub>\<S>_def spec\<^sub>\<S>_stable subset_trans sup_bot.right_neutral)
    thus ?thesis by (simp add: ex_equiv lpred_restrict_conj_eq_l)
  qed
  finally have r: "?r =\<^sub>P PEx (\<lambda>v. PConj (PConj (lpred_subst (P+[\<alpha>]\<^sub>\<S>) (Const v) x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x)) \<restriction> (stable \<S> \<inter> V)) (lpred_subst (?r' \<restriction> V) (Const v) x))"
    (is "?r =\<^sub>P PEx (\<lambda>v. PConj (PConj (?P' v) (?f v) \<restriction> (stable \<S> \<inter> V)) (?rs v))")
    by simp

  (* Show non-spec portions are related *)
  have "\<forall>v. (PConj (?P v) (?e v) \<restriction> (stable \<S> \<inter> V)) \<turnstile> PConj (?P' v) (?f v) \<restriction> (stable \<S> \<inter> V)"
  proof (cases \<alpha>)
    case (Assign y g) (* No speculation, need to handle complexities of forwarding *)
    have r: "\<forall>v. lpred_subst (P+[\<alpha>]\<^sub>\<S>) (Const v) x = (lpred_subst P (Const v) x) +[\<alpha>]\<^sub>\<S>"
      using w 
      by (metis Assign Int_iff P_upd.simps(1) P_upd_vars UnI1 UnI2 aexp_to_lexp_vars insert_iff le_iff_sup predicate_lang.lpred_subst_nop reads.simps(2) reassign_subst_comm stable subst_in writes.simps(1)) 
    have "\<beta>' = forward \<beta> \<alpha>" using re by auto
    hence f: "e = aexp_replace f y g" "x \<noteq> y" "x \<notin> lexp_vars (aexp_to_lexp g)"
      using re unfolding \<beta>' \<beta> Assign by (auto simp: min_reorder_def aexp_to_lexp_vars)

    show ?thesis 
    proof (cases "y \<in> stable \<S>")
      case True
      then show ?thesis
      proof (intro allI)
        fix v assume "y \<in> stable \<S>"
        let ?r = "PConj (?P' v) (?f v) \<restriction> (stable \<S> \<inter> V)"
        let ?l = "PConj (?P v) (?e v) \<restriction> (stable \<S> \<inter> V)"

        have r: "?r =\<^sub>P PConj ((lpred_subst P (Const v) x) +[\<alpha>]\<^sub>\<S>) (?f v \<restriction> stable \<S>) \<restriction> V" (is "?r =\<^sub>P ?r2 \<restriction> V")
          by (smt P_upd_stable lpred_restrict_conj_eq_r lpred_restrict_equiv lpred_restrict_inter pred_equiv_sym pred_equiv_trans r)

        have l: "?l =\<^sub>P PConj (?P v) (?e v \<restriction> stable \<S>) \<restriction> V"  (is "?l =\<^sub>P ?l2 \<restriction> V")
          by (smt Diff_subset lexp_vars.simps(2) lpred_restrict_equiv lpred_restrict_inter pred_equiv_sym pred_equiv_trans predicate_lang.lpred_restrict_conj_eq_r predicate_lang.lpred_subst_vars stable subset_trans sup_bot.right_neutral)

        have "?r2 =\<^sub>P PConj
                      (PEx (\<lambda>va. PConj (lpred_subst (lpred_subst P (Const v) x) (Const va) y)
                        (PCmp (=) (Var y) (lexp_subst (aexp_to_lexp g) (Const va) y))) \<restriction> stable \<S>)
                      (?f v \<restriction> stable \<S>)"
          unfolding Assign by (auto simp: reassign_var_def)

        also have "... =\<^sub>P PConj
                      (PEx (\<lambda>va. PConj (lpred_subst (lpred_subst P (Const v) x) (Const va) y)
                        (PCmp (=) (Var y) (lexp_subst (aexp_to_lexp g) (Const va) y)) \<restriction> stable \<S>))
                      (?f v \<restriction> stable \<S>)"
          using conj_equiv ex_lpred_restrict by blast

        also have "... =\<^sub>P PConj
                      (PEx (\<lambda>va. PConj (lpred_subst (lpred_subst P (Const v) x) (Const va) y)
                        ((PCmp (=) (Var y) (lexp_subst (aexp_to_lexp g) (Const va) y)) \<restriction> stable \<S>)))
                      (?f v \<restriction> stable \<S>)"
        proof -
          have "\<forall>va. lpred_vars (lpred_subst (lpred_subst P (Const v) x) (Const va) y) \<subseteq> stable \<S>"
            using stable by auto
          thus ?thesis by (simp add: conj_equiv ex_equiv predicate_lang.lpred_restrict_conj_eq_r)
        qed

        finally have "weaken_var y ?r2 =\<^sub>P weaken_var y (PConj
                      (PEx (\<lambda>va. PConj (lpred_subst (lpred_subst P (Const v) x) (Const va) y)
                        (PCmp (=) (Var y) (lexp_subst (aexp_to_lexp g) (Const va) y) \<restriction> stable \<S>)))
                      (?f v \<restriction> stable \<S>))" using weaken_var_equiv by blast 
        
        also have "... =\<^sub>P PEx(\<lambda>vb. PConj
                      (PEx (\<lambda>va. PConj (lpred_subst (lpred_subst P (Const v) x) (Const va) y)
                        (PCmp (=) (Const vb) (lexp_subst (aexp_to_lexp g) (Const va) y) \<restriction> stable \<S>)))
                      (lpred_subst (?f v) (Const vb) y \<restriction> stable \<S>))" 
          (is "weaken_var y ?r3 =\<^sub>P PEx (\<lambda>vb. PConj (PEx (\<lambda>va. PConj (?I1 va) (?I2 va vb))) (?I3 vb))")
        proof -
          have "y \<in> lpred_vars ?r3" unfolding Assign using True by auto
          thus ?thesis using  f True
            apply (auto simp add: weaken_var_def)
            by (smt conj_equiv ex_equiv lexp_subst.simps(1) lexp_subst.simps(2) lexp_subst_comm_same lpred_subst.simps(7) predicate_lang.pred_equiv_def subst_in)
        qed

        moreover have "?l2 \<turnstile> PEx (\<lambda>vb. PConj (PEx (\<lambda>va. PConj (?I1 va) (?I2 va vb))) (?I3 vb))"
          using re
        proof (cases rule: forward_elim)
          case normal
          hence c: "y \<noteq> x " "e = f" "y \<notin> lexp_vars (aexp_to_lexp e)" using re Assign \<beta> \<beta>' by (auto simp: min_reorder_def aexp_to_lexp_vars)
          have "\<forall>va mem. pred (PEx(\<lambda>vb. PCmp (=) (Const vb) (lexp_subst (aexp_to_lexp g) (Const va) y))) mem"
            by simp
          hence "\<forall>va. PTrue \<turnstile> PEx(\<lambda>vb. PCmp (=) (Const vb) (lexp_subst (aexp_to_lexp g) (Const va) y))"
            unfolding pred_entailment_def 
            by simp
          hence "\<forall>va. \<exists>vb. PTrue \<turnstile> PEx(\<lambda>vb. PCmp (=) (Const vb) (lexp_subst (aexp_to_lexp g) (Const va) y)) \<restriction> stable \<S>" 
            using pred_entailment_trans by blast
          hence "\<forall>va. \<exists>vb. PTrue \<turnstile> PEx(\<lambda>vb. PCmp (=) (Const vb) (lexp_subst (aexp_to_lexp g) (Const va) y) \<restriction> stable \<S>)" 
            using pred_entailment_trans ex_lpred_restrict pred_equiv_entail by fast
          then show ?thesis using c unfolding pred_entailment_def
            apply simp 
            by (metis fun_upd_triv leval.simps(1) lpred_subst_eval_const lpred_subst_mem_eval) 
        next
          case forward
          hence c: "y \<notin> lexp_vars (aexp_to_lexp g)" using re Assign \<beta> \<beta>'
            by (metis (full_types) Un_iff aexp_to_lexp_vars forward_reads insert_disjoint(1) reads.simps(2) reorder_diff_writes\<^sub>f(2) reorder_full_def writes.simps(1))
          have a: "lpred_subst P (Const v) x \<turnstile> PEx (\<lambda>va. lpred_subst (lpred_subst P (Const v) x) (Const va) y)"
            by (metis pred_entailment_trans pred_equiv_entail weaken_var_D weaken_var_weakens)
          have "PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x) \<turnstile> PEx (\<lambda>vb. PConj (PCmp (=) (Const vb) (aexp_to_lexp g)) (lpred_subst (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x)) (Const vb) y))"
            unfolding pred_entailment_def 
            using f aexp_replace_correct lexp_expand 
            by (auto simp: predicate_lang.lexp_subst_comm_diff)
          hence "PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const v) x) \<restriction> stable \<S> \<turnstile> PEx (\<lambda>vb. PConj (PCmp (=) (Const vb) (aexp_to_lexp g)) (lpred_subst (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x)) (Const vb) y))  \<restriction> stable \<S>"
            by blast
          also have "... \<turnstile> PEx (\<lambda>vb. PConj (PCmp (=) (Const vb) (aexp_to_lexp g) \<restriction> stable \<S>) (lpred_subst (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const v) x)) (Const vb) y \<restriction> stable \<S>))"
            by (smt predicate_lang.ex_entail predicate_lang.ex_lpred_restrict predicate_lang.lpred_restrict_conj_l predicate_lang.lpred_restrict_conj_r predicate_lang.pred_entailment_PConj predicate_lang.pred_equiv_common)
          finally show ?thesis using a c unfolding pred_entailment_def by auto
        qed

        ultimately have "?l2 \<turnstile>  ?r2 \<restriction> (-{y})" by (auto simp: pred_entailment_def pred_equiv_def)
        hence "?l2 \<restriction> V \<turnstile> ?r2 \<restriction> (-{y} \<inter> V)" 
          using lpred_restrict_inter pred_entailment_trans pred_equiv_entail by blast
        hence "?l2 \<restriction> V \<turnstile> ?r2 \<restriction> V" using Assign \<open>- writes \<alpha> \<inter> V = V\<close> by auto
        thus "?l \<turnstile> ?r" using r l by (auto simp: pred_entailment_def pred_equiv_def)
      qed
    next
      case False (* Write from y was unstable, need to show we can ignore it *)
      hence "P+[\<alpha>]\<^sub>\<S> =\<^sub>P P" using stable Assign by (metis P_upd.simps(1) lpred_restrict_nop reassign_restrict_comm_same)
      hence 1: "\<forall>v. ?P' v =\<^sub>P ?P v" using lpred_subst_equiv by blast

      have "\<forall>v. ?e v \<turnstile> weaken_var y (?f v)" 
      proof (intro allI)
        fix v
        show "?e v \<turnstile> weaken_var y (?f v)"
        proof (cases "y \<in> lpred_vars (?f v)")
          case True

          have "PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (aexp_to_lexp g) y) \<turnstile>
                PEx (\<lambda>x'. PCmp (=) (Var x) (lexp_subst (aexp_to_lexp f) (Const x') y))"
            unfolding pred_entailment_def
          proof (auto)
            fix mem assume "mem x = leval mem (lexp_subst (aexp_to_lexp f) (aexp_to_lexp g) y)"
            let ?v = "leval mem (aexp_to_lexp g)"
            have "leval mem (lexp_subst (aexp_to_lexp f) (aexp_to_lexp g) y) = leval mem (lexp_subst (aexp_to_lexp f) (Const ?v) y)"
              by (simp add: predicate_lang.lexp_subst_eval_const)
            thus "\<exists>v. leval mem (lexp_subst (aexp_to_lexp f) (aexp_to_lexp g) y) = leval mem (lexp_subst (aexp_to_lexp f) (Const v) y)"
              by auto
          qed

          moreover have "PCmp (=) (Var x) (lexp_subst (lexp_subst (aexp_to_lexp f) (Const v) x) (aexp_to_lexp g) y) \<turnstile>
                PEx (\<lambda>x'. PCmp (=) (Var x) (lexp_subst (lexp_subst (aexp_to_lexp f) (Const v) x) (Const x') y))"
            unfolding pred_entailment_def
          proof (auto)
            fix mem assume "mem x = leval mem (lexp_subst (lexp_subst (aexp_to_lexp f) (Const v) x) (aexp_to_lexp g) y)"
            let ?v = "leval mem (aexp_to_lexp g)"
            have "leval mem (lexp_subst (lexp_subst (aexp_to_lexp f) (Const v) x) (aexp_to_lexp g) y) = 
                       leval mem (lexp_subst (lexp_subst (aexp_to_lexp f) (Const v) x) (Const ?v) y)"
              by (simp add: predicate_lang.lexp_subst_eval_const)
            thus "\<exists>va. leval mem (lexp_subst (lexp_subst (aexp_to_lexp f) (Const v) x) (aexp_to_lexp g) y) = 
                       leval mem (lexp_subst (lexp_subst (aexp_to_lexp f) (Const v) x) (Const va) y)"
              by blast
          qed

          ultimately show ?thesis using True
            unfolding weaken_var_def using f lexp_subst_comm_diff[of x y "aexp_to_lexp g" "Const v" "(aexp_to_lexp f)"] 
            by (auto split: if_splits simp: aexp_replace_correct)
        next
          case False
          then show ?thesis using weaken_var_def aexp_replace_correct f by auto
        qed
      qed
      hence "\<forall>v. ?e v \<turnstile> ?f v \<restriction> (- {y})" by simp
      hence 2: "\<forall>v. ?e v \<restriction> stable \<S> \<turnstile> ?f v \<restriction> stable \<S>" 
        by (smt False lpred_restrict_entailment lpred_restrict_single pred_entailment_trans pred_equiv_entail predicate_lang.lpred_restrict_weaken)

      hence "\<forall>v. PConj (?P v) (?e v) \<restriction> (stable \<S>) \<turnstile> PConj (?P' v) (?f v) \<restriction> (stable \<S>)"
      proof -
        have "\<forall>v. PConj (?P v) (?e v) \<restriction> (stable \<S>) =\<^sub>P PConj (?P v) (?e v \<restriction> (stable \<S>))"
          by (smt Diff_subset lexp_vars.simps(2) predicate_lang.lpred_restrict_conj_eq_r predicate_lang.lpred_subst_vars stable subset_trans sup_bot.right_neutral)
        moreover have "\<forall>v. PConj (?P' v) (?f v) \<restriction> (stable \<S>) =\<^sub>P PConj (?P' v) (?f v \<restriction> (stable \<S>))"
          by (simp add: r lpred_restrict_conj_eq_r)
        ultimately show ?thesis using 1 2 unfolding pred_equiv_def pred_entailment_def by auto
      qed
      hence "\<forall>v. PConj (?P v) (?e v) \<restriction> (stable \<S> \<inter> V) \<turnstile> PConj (?P' v) (?f v) \<restriction> (stable \<S> \<inter> V)"
        by (meson lpred_restrict_entailment lpred_restrict_inter pred_entailment_trans pred_equiv_entail) 
      then show ?thesis by (auto simp: pred_equiv_def pred_entailment_def)
    qed
  next
    case Fence
    then show ?thesis using re by auto
  next
    case (Guard b) (* Can't reorder global assign with guard *)
    thus ?thesis using re \<beta> Guard \<beta>' min_reorder_def globals_locals_disj by auto
  qed

  moreover have "\<forall>v. ?ls v =\<^sub>P ?rs v"
    using e unfolding pred_entailment_def 
    using lpred_subst_equiv by blast

  ultimately have  "PEx (\<lambda>v. PConj (PConj (?P v) (?e v) \<restriction> (stable \<S> \<inter> V)) (?ls v)) \<turnstile> PEx (\<lambda>v. PConj (PConj (?P' v) (?f v) \<restriction> (stable \<S> \<inter> V)) (?rs v))"
    using e
    unfolding pred_entailment_def pred_equiv_def
    by auto

  thus ?thesis using l r by (auto simp: pred_equiv_def pred_entailment_def)
qed

lemma spec\<^sub>\<S>_equiv_state_upd:
  assumes upd: "\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D'"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes d: "writes \<alpha> \<inter> V = {}"
  assumes stable: "lpred_vars P \<subseteq> stable \<S>"
  shows "(spec\<^sub>\<S> P \<S> (\<alpha> ;;; c) V) \<restriction> V =\<^sub>P (spec\<^sub>\<S> P' \<S>' c V) \<restriction> V"  (is "?L \<restriction> ?V =\<^sub>P ?R \<restriction> ?V")
proof -
  have "\<S>' = \<S>" "P' = P+[\<alpha>]\<^sub>\<S>" using upd by (auto simp: state_upd_def)
  thus ?thesis using re d stable spec\<^sub>\<S>_equiv_modulo_writes by auto
qed

lemma spec\<^sub>\<S>_equiv_state_upd':
  assumes a: "\<exists>x e. \<beta>' = x \<leftarrow> e \<and> x \<in> globals"
  assumes upd: "\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D'"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes d: "writes \<alpha> \<inter> V = {}"
  assumes stable: "lpred_vars P \<subseteq> stable \<S>"
  shows "(spec\<^sub>\<S> P \<S> (\<alpha> ;;; c) V)+[\<beta>']\<^sub>\<S> \<restriction> V \<turnstile> (spec\<^sub>\<S> P' \<S>' c V)+[\<beta>]\<^sub>\<S> \<restriction> V"  (is "?L \<restriction> ?V \<turnstile> ?R \<restriction> ?V")
proof -
  have "\<S>' = \<S>" "P' = P+[\<alpha>]\<^sub>\<S>" using upd by (auto simp: state_upd_def)
  thus ?thesis using re d stable spec\<^sub>\<S>_equiv_modulo_writes' a by auto
qed

lemma spec_entailment:
  shows "P \<turnstile> P' \<Longrightarrow> spec P \<S> c \<turnstile> spec P' \<S> c"
  by (auto simp: spec_def pred_entailment_def)

lemma spec\<^sub>\<S>_entailment:
  assumes "P \<turnstile> P'" and "V \<supseteq> V'"
  shows "spec\<^sub>\<S> P \<S> c V \<turnstile> spec\<^sub>\<S> P' \<S> c V'"
proof -
  have "PConj (specr PTrue (stable \<S>) c) P \<turnstile> PConj (specr PTrue (stable \<S>) c) P'"
    using assms unfolding pred_entailment_def by auto
  hence "PConj (specr PTrue (stable \<S>) c) P \<restriction> V \<turnstile> PConj (specr PTrue (stable \<S>) c) P' \<restriction> V'"
    using lpred_restrict_entailment_subset assms by auto
  thus ?thesis using assms unfolding spec\<^sub>\<S>_def pred_entailment_def by auto
qed

lemma spec_state_ord:
  assumes "type_stable \<S> P"
  assumes "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'"
  assumes "V \<supseteq> V'"
  shows "\<Gamma>,\<S>,spec\<^sub>\<S> P \<S> c V,D >\<^sub>s \<Gamma>',\<S>',spec\<^sub>\<S> P' \<S>' c V',D'"
  using assms unfolding state_ord_def
proof (auto, goal_cases)
  case (1 env)
  hence "tyenv_wellformed env \<Gamma> \<S>' P"
    unfolding tyenv_wellformed_def 
    by auto
  hence "tyenv_wellformed env \<Gamma>' \<S>' P'" using 1 by auto
  then show ?case 
    unfolding tyenv_wellformed_def  
    using spec\<^sub>\<S>_stable by auto
next
  case 2
  thus ?case using spec\<^sub>\<S>_entailment by metis
qed

lemma spec_repeat:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wr: "writes \<alpha> \<inter> V = {}"
  assumes stable: "type_stable \<S> P"
  shows "spec\<^sub>\<S> P \<S> (\<alpha> ;;; c) V +[\<alpha>]\<^sub>\<S> \<turnstile> (spec\<^sub>\<S> (P+[\<alpha>]\<^sub>\<S>) \<S> c V)" (is "?l+[\<alpha>]\<^sub>\<S> \<turnstile> ?r")
proof -
  have "?l \<turnstile> P" using spec_entails by auto
  hence a: "?l+[\<alpha>]\<^sub>\<S> \<turnstile> P+[\<alpha>]\<^sub>\<S>" using P_upd_entailment by blast

  let ?s1 = "PConj (specr PTrue (stable \<S>) (\<alpha> ;;; c)) P"
  let ?s2 = "PConj (specr PTrue (stable \<S>) c) (P+[\<alpha>]\<^sub>\<S>)"
  let ?V = "-writes \<alpha>"

  have s1: "lpred_vars ?s1 \<subseteq> stable \<S>"
    using specr_stable stable by auto

  have "?s1 \<restriction> ?V =\<^sub>P ?s2 \<restriction> ?V" using specr_equiv_modulo_writes re stable by metis
  moreover have "- writes \<alpha> \<inter> V = V" using wr by auto
  ultimately have "?s1 \<restriction> V =\<^sub>P ?s2 \<restriction> V" 
    using lpred_restrict_equiv [of "?s1 \<restriction> ?V" "?s2 \<restriction> ?V" "V"]
    using lpred_restrict_inter by (auto simp: pred_equiv_def)

  moreover have "(?s1 \<restriction> V) +[\<alpha>]\<^sub>\<S> \<turnstile> ?s1 \<restriction> V"
  proof -
    have "writes \<alpha> \<inter> lpred_vars (?s1 \<restriction> V) = {}" using wr lpred_restrict_vars by auto
    thus ?thesis using P_upd_nop lpred_restrict_vars stable
      by (metis le_infI1 s1)
  qed

  ultimately have p: "(?s1 \<restriction> V) +[\<alpha>]\<^sub>\<S> \<turnstile> ?s2 \<restriction> V"
    using pred_entailment_trans pred_equiv_entail by meson

  hence "?l+[\<alpha>]\<^sub>\<S> \<turnstile> ?s2 \<restriction> V"
  proof -
    have "?l \<turnstile> (?s1 \<restriction> V)" unfolding spec\<^sub>\<S>_def pred_entailment_def by auto
    hence "?l+[\<alpha>]\<^sub>\<S> \<turnstile> (?s1 \<restriction> V) +[\<alpha>]\<^sub>\<S>" using P_upd_entailment by auto
    thus ?thesis using pred_entailment_trans p by meson
  qed

  thus ?thesis using a by (auto simp: pred_equiv_def pred_entailment_def spec\<^sub>\<S>_def)
qed

subsubsection \<open> Reordering Lemmas for Earlier \<close>

text \<open>Types for an expression's reads are not modified by a reorder-able Action\<close>
lemma read_type_reorder:
  assumes upd: "\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D'"
  and re: "\<beta> \<hookleftarrow> \<alpha> \<or> \<alpha> \<hookleftarrow> \<beta>"
  shows "\<Gamma> :\<^sub>r \<beta> = \<Gamma>' :\<^sub>r \<beta>"
proof -
  have "\<forall>x\<in>- writes \<alpha>. \<Gamma> x = \<Gamma>' x" using state_upd_\<Gamma>_eq upd by auto
  moreover have "reads \<beta> \<inter> writes \<alpha> = {}" using re reorder_diff_writes by auto
  ultimately show ?thesis using read_type_det by blast
qed

text \<open>Types for an expression's writes are not modified by a reorder-able Action\<close>
lemma write_type_constant:
  assumes "\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D'"
  assumes "\<beta>' < \<alpha> < \<beta>"
  shows "\<S> :\<^sub>w \<beta>' = \<S>' :\<^sub>w \<beta>"  
  using assms forward_writes
  unfolding state_upd_def 
  by (cases \<beta>; cases \<alpha>; auto)

lemma write_type_constant':
  assumes "\<Gamma>,\<S>,P,D +\<^sub>\<beta>' \<Gamma>',\<S>',P',D'"
  assumes "\<beta>' < \<alpha> < \<beta>"
  shows "\<S> :\<^sub>w \<alpha> = \<S>' :\<^sub>w \<alpha>"  
  using assms forward_writes
  unfolding state_upd_def 
  by (cases \<beta>; cases \<alpha>; auto)

text \<open>Speculation is implied across a reorder-able Action\<close>
lemma state_upd_spec:
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes stable: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1"
  shows "spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<restriction> (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<turnstile> spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w) \<restriction> D\<^sub>2[\<beta>]\<^sub>w"
    (is "?P\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> ?P\<^sub>2 \<restriction> ?W\<^sub>2")
proof -
  have w: "?W\<^sub>2 = ?W\<^sub>1" using re remove_write upd by (auto simp: state_upd_def)
  hence "?P\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w)" using spec\<^sub>\<S>_entailment by auto
  hence "?P\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w) \<restriction> ?W\<^sub>2" using w by blast
  also have "... \<turnstile> ?P\<^sub>2 \<restriction> ?W\<^sub>2" using w spec\<^sub>\<S>_equiv_state_upd[OF upd re] stable pred_equiv_entail by simp
  finally show ?thesis .
qed

lemma state_upd_spec':
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes stable: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1"
  shows "spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w) \<restriction> (D\<^sub>1[\<beta>']\<^sub>w) \<turnstile> spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w) \<restriction> D\<^sub>2[\<beta>]\<^sub>w"
    (is "?P\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> ?P\<^sub>2 \<restriction> ?W\<^sub>2")
proof -
  have "spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<restriction> (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<turnstile> spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w) \<restriction> D\<^sub>2[\<beta>]\<^sub>w"
    using assms state_upd_spec by auto
  moreover have "?P\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>)" by (simp add: spec\<^sub>\<S>_entailment)
  ultimately show ?thesis unfolding secure_write_def subtype_def
    by (meson Diff_subset pred_entailment_trans lpred_restrict_entailment_subset)
qed

text \<open> An Actions writes cannot control themselves \<close>
lemma self_control:
  "\<forall>y \<in> writes \<alpha>. lpred_vars (\<L> y) \<inter> writes \<alpha> = {}"
  unfolding Ball_def
proof (intro allI impI)
  fix x assume a: "x \<in> writes \<alpha>"
  show "lpred_vars (\<L> x) \<inter> writes \<alpha> = {}"
  proof (cases "x \<in> \<C>")
    case True
    then show ?thesis using \<C>_vars_\<C> \<C>_vars_correct bexp_to_lpred_vars by auto
  next
    case False
    moreover have "lpred_vars (\<L> x) \<subseteq> \<C>" 
      using \<C>_vars_correct \<C>_vars_subset_\<C> bexp_to_lpred_vars by auto
    ultimately show ?thesis using a by (cases \<alpha>; auto)
  qed
qed

text \<open>
  Consider the secure_write property for \<beta>', given \<beta>' < \<alpha> < \<beta> and successful
  type-checking of \<alpha> ; \<beta>.

  In the case of normal reordering, where \<beta>' = \<beta>, this is straight forward.
  \<beta> will not read any writes of \<alpha>. As a result, whether \<beta> executes before or after 
  \<alpha> will not change the information flow. Its necessary to show that D[\<beta>] successfully
  blocks any control influence from \<alpha>, avoiding any value-dependent modifications.  

  In the case of forwarding reordering, this is no longer a trivial property.
  Its necessary to show that introducing the expression of \<alpha> into \<beta> should preserve
  information-flow, regardless of whether the write of \<alpha> is high/low or stable/unstable.
  This relies on \<alpha> performing a secure write originally and that its reads only access locals.

  A few considerations for future work. First, do we need to rely on the reads of \<alpha> being stable?
  The property is useful when reasoning with a low expression for \<alpha>, as we can ensure the expression
  remains low when introduced into \<beta>, without relating P. Without this, it would be essential to
  show that the unstable variables in the expression for \<alpha> remain low in P restricted to the
  constraints of \<beta>'. This seems difficult, maybe impossible.

  Second consideration, re-factor the various case splitting conditions that are used in validating
  the forwarding case. A lot of it seems redundant and repeatedly has to handle cases where
  P \<turnstile> PFalse.
\<close>
lemma read_type_reorder':
  assumes upd: "\<Gamma>,\<S>,P,D +\<^sub>\<alpha> \<Gamma>',\<S>',P',D'"
  assumes sec: "secure_write \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) \<alpha>"
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S> \<and> locals \<subseteq> dom \<Gamma> \<and> dom \<Gamma> = stable \<S> - \<C>"
  shows "\<Gamma>' :\<^sub>r \<beta> \<turnstile> \<Gamma> :\<^sub>r \<beta>' \<or> P' !\<turnstile> \<Gamma>' :\<^sub>r \<beta>"
  using re
proof (cases rule: forward_elim)
  case normal
  then show ?thesis using read_type_reorder upd re normal by auto
next
  case forward
  then obtain x e where \<alpha>: "\<alpha> = x \<leftarrow> e" by (cases \<alpha>; auto)
  have subtype: "(\<Gamma> :\<^sub>r \<alpha>) \<le>:\<^sub>(P \<restriction> D[\<alpha>]\<^sub>w) (\<S> :\<^sub>w \<alpha>)" using sec by (auto simp: secure_write_def)
  have reads: "reads \<beta>' = reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)" using forward_reads forward re by auto

  consider (low) "\<Gamma> :\<^sub>r \<alpha> =\<^sub>P PTrue" | (high) "\<Gamma> :\<^sub>r \<alpha> =\<^sub>P PFalse \<and> \<not> P' \<turnstile> PFalse" | (false) "P' \<turnstile> PFalse" 
    using read_locals forward(1) wf by blast

  thus ?thesis
  proof (cases)
    case low (* Low expression read by \<alpha>, performing these reads earlier is safe *)
    have "\<forall>x \<in> reads \<beta>'. \<Gamma>' :\<^sub>r \<beta> \<turnstile> to_total \<Gamma> x"
      unfolding Ball_def
    proof (intro allI impI)
      fix x assume "x \<in> reads \<beta>'"
      then consider "x \<in> reads \<alpha>" | "x \<in> reads \<beta> - writes \<alpha>" using reads by auto
      thus "\<Gamma>' :\<^sub>r \<beta> \<turnstile> to_total \<Gamma> x"
      proof (cases)
        case 1 (* Injected reads are trivially low *)
        hence "PTrue \<turnstile> to_total \<Gamma> x" 
          using read_type_all_low_entailment low pred_equiv_common by blast 
        then show ?thesis by (auto simp: pred_entailment_def)
      next
        case 2 (* Read should be equal to original in \<beta> *)
        hence "\<Gamma> x = \<Gamma>' x" using upd by (simp add: \<Gamma>_upd_nop state_upd_def)
        hence "to_total \<Gamma> x = to_total \<Gamma>' x" using upd state_upd_\<Gamma>_dom by (auto simp: to_total_def)
        thus ?thesis using read_type_all_low_entailment 2 by auto
      qed
    qed
    thus ?thesis using read_type_entailment[of \<beta>' "\<Gamma>' :\<^sub>r \<beta>" \<Gamma>] by blast
  next
    case high
    hence p: "\<not> P \<turnstile> PFalse" using upd P_upd_entail_false by (auto simp: state_upd_def)
    have high_exp: "\<not> P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r \<alpha>" using p high
      by (meson lpred_restrict_weakens pred_equiv_def pred_entailment_def)
    show ?thesis 
    proof (cases "x \<in> dom \<Gamma>'")
      case True (* \<alpha> was a high read, stored into a stable var. Read of \<beta> from \<Gamma> cannot be low *)
      hence "x \<in> dom \<Gamma> \<inter> dom \<Gamma>'" using state_upd_\<Gamma>_dom upd by auto
      hence "to_total \<Gamma>' x = PFalse"
        using high_exp upd by (auto simp: state_upd_def \<alpha> to_total_def sec_def)
      moreover have "\<Gamma>' :\<^sub>r \<beta> \<turnstile> to_total \<Gamma>' x" 
        using read_type_all_low_entailment forward \<alpha> by auto
      ultimately show ?thesis by (simp add: pred_entailment_def)
    next
      case False (* \<alpha> was a high read, stored into unstable x. Read of \<beta> from \<L> cannot be low *)
      hence "x \<notin> dom \<Gamma>" using state_upd_\<Gamma>_dom upd by auto
      hence s: "x \<notin> NoRW \<S> - \<C>" using wf unfolding stable_def by auto
      have "P' !\<turnstile> \<L> x" 
      proof -
        have "P \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<S> :\<^sub>w \<alpha>" using high_exp subtype by (auto simp: subtype_def)
        hence "P !\<turnstile> \<L> x" using s pred_entailment_trans by (auto simp: \<alpha> split: if_splits)
        moreover have "x \<notin> lpred_vars (\<L> x)" using self_control[of \<alpha>] \<alpha> by auto
        ultimately have "P \<restriction> (- {x}) !\<turnstile> \<L> x"
          by (metis lpred_vars.simps(3) lpred_restrict_single weaken_var_def weaken_var_entailment) 
        hence "P+[\<alpha>]\<^sub>\<S> \<restriction> (- {x}) !\<turnstile> \<L> x"
          using wf P_upd_restrict_writes[of P \<S> P \<alpha>] unfolding \<alpha>
          by (metis (no_types, lifting) pred_entailment_refl pred_entailment_trans writes.simps(1))
        thus ?thesis using upd \<alpha> pred_entailment_trans unfolding state_upd_def by blast
      qed
      moreover have "\<Gamma>' :\<^sub>r \<beta> \<turnstile> \<L> x" 
        using read_type_all_low_entailment[of \<beta> \<Gamma>'] forward \<alpha> False 
        unfolding to_total_def
        by (smt disjoint_insert(2) inf_sup_aci(1) limit\<^sub>r.simps(2) limit\<^sub>r_correct\<^sub>f re writes.simps(1))
      ultimately show ?thesis using high by (auto simp: pred_entailment_def)
    qed
  next
    case false
    then show ?thesis by (auto simp: pred_entailment_def)
  qed
qed

lemma pred_restrict_not:
  assumes "P !\<turnstile> Q"
  shows "P \<restriction> W \<turnstile> PFalse \<or> \<not> P \<restriction> W \<turnstile> Q"
proof (cases "P \<restriction> W \<turnstile> Q")
  case True
  hence "P \<turnstile> Q" using pred_entailment_trans by blast
  hence "P \<turnstile> PFalse" using assms by (auto simp: pred_entailment_def)
  then show ?thesis using lpred_restrict_entailment by fastforce
next
  case False
  then show ?thesis by simp
qed

lemma earlier_secure_write_modulo_\<alpha>:
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes seca: "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 (D\<^sub>1[\<alpha>]\<^sub>w) \<alpha>"
  assumes sec: "secure_write \<Gamma>\<^sub>2 \<S>\<^sub>2 (spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)) (D\<^sub>2[\<beta>]\<^sub>w) \<beta>" (is "secure_write \<Gamma>\<^sub>2 \<S>\<^sub>2 ?P\<^sub>2 ?W\<^sub>2 \<beta>")
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> locals \<subseteq> dom \<Gamma>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 = stable \<S>\<^sub>1 - \<C>"
  shows "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>)) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'" (is "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 ?W\<^sub>1 \<beta>'")
proof -
  have "\<Gamma>\<^sub>2 :\<^sub>r \<beta> \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<beta>' \<or> P\<^sub>2 !\<turnstile> \<Gamma>\<^sub>2 :\<^sub>r \<beta>" using read_type_reorder' assms by metis
  hence "\<Gamma>\<^sub>2 :\<^sub>r \<beta> \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<beta>' \<or> ?P\<^sub>2 !\<turnstile> \<Gamma>\<^sub>2 :\<^sub>r \<beta>" using spec_entails pred_entailment_trans by blast 
  hence "\<Gamma>\<^sub>2 :\<^sub>r \<beta> \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<beta>' \<or> \<not> ?P\<^sub>2 \<restriction> ?W\<^sub>2 \<turnstile> \<Gamma>\<^sub>2 :\<^sub>r \<beta> \<or> ?P\<^sub>2 \<restriction> ?W\<^sub>2 \<turnstile> PFalse" using pred_restrict_not by auto
  moreover have "?P\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> ?P\<^sub>2 \<restriction> ?W\<^sub>2" using wf state_upd_spec[OF upd re] by simp
  moreover have "\<S>\<^sub>1 :\<^sub>w \<beta>' = \<S>\<^sub>2 :\<^sub>w \<beta>" using write_type_constant upd re by auto
  ultimately show ?thesis using sec by (auto simp: secure_write_def subtype_def pred_entailment_def)
qed

lemma earlier_secure_write_modulo_\<alpha>':
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes seca: "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 (D\<^sub>1[\<alpha>]\<^sub>w) \<alpha>"
  assumes sec: "secure_write \<Gamma>\<^sub>2 \<S>\<^sub>2 (spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)) (D\<^sub>2[\<beta>]\<^sub>w) \<beta>" (is "secure_write \<Gamma>\<^sub>2 \<S>\<^sub>2 ?P\<^sub>2 ?W\<^sub>2 \<beta>")
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> locals \<subseteq> dom \<Gamma>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 = stable \<S>\<^sub>1 - \<C>"
  shows "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w)) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'" (is "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 ?W\<^sub>1 \<beta>'")
  using re
proof -
  have "?P\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W\<^sub>1" by (simp add: spec\<^sub>\<S>_entailment)
  hence "?P\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W\<^sub>1 \<restriction> ?W\<^sub>1" by auto
  moreover have "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W\<^sub>1) ?W\<^sub>1 \<beta>'"
    using earlier_secure_write_modulo_\<alpha> assms by auto
  ultimately show ?thesis unfolding secure_write_def subtype_def by (meson pred_entailment_trans)
qed

text \<open>Add in information concerning \<alpha>, to achieve full earlier_secure_write\<close>
lemma earlier_secure_write:
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes seca: "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 (D\<^sub>1[\<alpha>]\<^sub>w) \<alpha>"
  assumes sec: "secure_write \<Gamma>\<^sub>2 \<S>\<^sub>2 (spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)) (D\<^sub>2[\<beta>]\<^sub>w) \<beta>" (is "secure_write \<Gamma>\<^sub>2 \<S>\<^sub>2 ?P\<^sub>2 ?W\<^sub>2 \<beta>")
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> locals \<subseteq> dom \<Gamma>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 = stable \<S>\<^sub>1 - \<C>"
  shows "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w)) (D\<^sub>1[\<beta>']\<^sub>w) \<beta>'" (is "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 ?W\<^sub>1 \<beta>'")
proof -
  have "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>)) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'"
    using earlier_secure_write_modulo_\<alpha> assms by auto
  moreover have "?P\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>)" by (simp add: spec\<^sub>\<S>_entailment)
  ultimately show ?thesis unfolding secure_write_def subtype_def
    by (meson Diff_subset pred_entailment_trans lpred_restrict_entailment_subset)
qed

text \<open>
  In a similar fashion, ignore the writes of \<alpha> and incorporate any speculation to show this
  new \<beta> is valid under the stronger P.
  We initially show secure_update under @{term D\<^sub>2 }, as this simplifies reasoning for 
  the later case.
\<close>
lemma earlier_secure_update_modulo_\<alpha>:
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes sec: "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>2 (spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)) (D\<^sub>2[\<beta>]\<^sub>w) (D\<^sub>2[\<beta>]\<^sub>r) \<beta>" (is "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>2 ?P\<^sub>2 ?W\<^sub>2 ?R\<^sub>2 \<beta>")
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1"
  shows "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>)) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) (D\<^sub>1[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'" (is "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 ?W\<^sub>1 ?R\<^sub>1 \<beta>'")
proof (cases "\<exists>x e. \<beta>' = x \<leftarrow> e \<and> x \<in> globals")
  case True
  show ?thesis 
  proof (rule secure_update_entailment[OF sec])
    have rel: "?W\<^sub>1 = ?W\<^sub>2" using re remove_write upd by (auto simp: state_upd_def)
    hence "?P\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w)" using spec\<^sub>\<S>_entailment by auto
    hence "?P\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w) \<restriction> ?W\<^sub>2" using rel by blast
    also have "... \<turnstile> ?P\<^sub>2 \<restriction> ?W\<^sub>2"
      by (metis rel Diff_disjoint wf pred_equiv_entail spec\<^sub>\<S>_equiv_state_upd[OF upd re])
    finally show "?P\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> ?P\<^sub>2 \<restriction> ?W\<^sub>2" by simp
  next
    have rel: "?W\<^sub>1 = ?W\<^sub>2" using re remove_write upd by (auto simp: state_upd_def)
    hence "?P\<^sub>1+[\<beta>']\<^sub>\<S>\<^sub>1 \<restriction> ?W\<^sub>2 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W\<^sub>2+[\<beta>']\<^sub>\<S>\<^sub>1 \<restriction> ?W\<^sub>2" using spec\<^sub>\<S>_entailment rel by auto
    also have "... \<turnstile> ?P\<^sub>2+[\<beta>]\<^sub>\<S>\<^sub>1 \<restriction> ?W\<^sub>2" using spec\<^sub>\<S>_equiv_state_upd'[OF True upd re] rel wf by auto
    finally show "?P\<^sub>1+[\<beta>']\<^sub>\<S>\<^sub>1 \<restriction> ?W\<^sub>1 \<turnstile> ?P\<^sub>2+[\<beta>]\<^sub>\<S>\<^sub>1 \<restriction> ?W\<^sub>2" using rel by simp
  next
    show "?W\<^sub>2 \<subseteq> ?W\<^sub>1 \<and> ?R\<^sub>2 \<subseteq> ?R\<^sub>1" using re remove_write remove_read upd by (auto simp: state_upd_def)
  next
    show "dom \<Gamma>\<^sub>2 = dom \<Gamma>\<^sub>1" using upd state_upd_\<Gamma>_dom by auto
  next
    have "dom \<Gamma>\<^sub>1 \<inter> D\<^sub>2[\<beta>]\<^sub>w \<subseteq> - writes \<alpha>" using re upd by (auto simp: remove_write state_upd_def)
    thus "\<forall>y\<in>dom \<Gamma>\<^sub>1 \<inter> D\<^sub>2[\<beta>]\<^sub>w. sec (the (\<Gamma>\<^sub>2 y)) \<turnstile> sec (the (\<Gamma>\<^sub>1 y))" 
      using state_upd_\<Gamma>_eq[OF upd] by fastforce
  next
    show "\<S>\<^sub>2 = \<S>\<^sub>1" using upd state_upd_def by simp
  next
    show "writes \<beta> = writes \<beta>'" using re forward_writes by simp
  qed
next
  case False
  hence "writes \<beta>' \<inter> \<C> = {}" using localsProps globals_locals_disj by (cases \<beta>'; auto)
  hence "\<forall>y. writes \<beta>' \<inter> \<C>_vars y = {}" using \<C>_vars_subset_\<C> by auto
  hence "\<forall>S P W. falling S P W \<beta>' = {}" "\<forall>S P W. rising S P W \<beta>' = {}"
    by (auto simp: falling_def rising_def)
  then show ?thesis by (auto simp: secure_update_def)
qed

lemma earlier_secure_update_modulo_\<alpha>':
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes sec: "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>2 (spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)) (D\<^sub>2[\<beta>]\<^sub>w) (D\<^sub>2[\<beta>]\<^sub>r) \<beta>" (is "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>2 ?P\<^sub>2 ?W\<^sub>2 ?R\<^sub>2 \<beta>")
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1"
  shows "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w)) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) (D\<^sub>1[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'" (is "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 ?W\<^sub>1 ?R\<^sub>1 \<beta>'")
proof -
  let ?W = "D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>" and ?R="D\<^sub>1[\<beta>']\<^sub>r - reads \<alpha>"
  have p: "?P\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W" by (simp add: spec\<^sub>\<S>_entailment)
  have "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W) ?W ?R \<beta>'"
    using earlier_secure_update_modulo_\<alpha> assms by auto
  thus ?thesis by (rule secure_update_entailment, insert p P_upd_entailment, auto)
qed

lemma earlier_secure_update:
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes sec: "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>2 (spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)) (D\<^sub>2[\<beta>]\<^sub>w) (D\<^sub>2[\<beta>]\<^sub>r) \<beta>" (is "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>2 ?P\<^sub>2 ?W\<^sub>2 ?R\<^sub>2 \<beta>")
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1"
  shows "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w)) (D\<^sub>1[\<beta>']\<^sub>w) (D\<^sub>1[\<beta>']\<^sub>r) \<beta>'" (is "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 ?W\<^sub>1 ?R\<^sub>1 \<beta>'")
proof -
  let ?W = "D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>" and ?R="D\<^sub>1[\<beta>']\<^sub>r - reads \<alpha>"
  have p: "?P\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W" by (simp add: spec\<^sub>\<S>_entailment)
  have "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) ?W) ?W ?R \<beta>'"
    using earlier_secure_update_modulo_\<alpha> assms by auto
  thus ?thesis by (rule secure_update_entailment, insert p P_upd_entailment, auto)
qed

text \<open>Combine these to show that an Action can be moved before a reorder-able Action
      and still type check, given the speculation is extended\<close>
lemma earlier_action_type:
  assumes upd: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes sec: "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 (D\<^sub>1[\<alpha>]\<^sub>w) \<alpha>"
  assumes orig: "\<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w),D\<^sub>2 { \<beta> }\<^sub>a \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> locals \<subseteq> dom \<Gamma>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 = stable \<S>\<^sub>1 - \<C>"
  shows "\<exists>\<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i. \<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w),D\<^sub>1 { \<beta>' }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i"
  using assms earlier_secure_write earlier_secure_update 
  by (meson state_upd_def)

subsubsection \<open> Lemmas for a later secure_write \<close>

text \<open>Certain actions have a constrained secure_write, such that their reads must be low
      This lemma shows that this constraint remains trivially true\<close>
lemma lsw_constrained_write:
  assumes orig: "P \<restriction> V' !\<turnstile> PTrue"
  shows "P+[\<beta>]\<^sub>\<S> \<restriction> V !\<turnstile> PTrue"
proof -
  have "P \<restriction> V' \<turnstile> PFalse" using orig by (auto simp: pred_entailment_def)
  hence "P \<turnstile> PFalse" using lpred_restrict_weakens pred_entailment_trans by meson
  hence "P+[\<beta>]\<^sub>\<S> \<turnstile> PFalse" using P_upd_entail_false by auto
  hence "P+[\<beta>]\<^sub>\<S> \<restriction> V  \<turnstile> PFalse \<restriction> V" using lpred_restrict_entailment by blast
  then show ?thesis by (auto simp: pred_entailment_def)
qed

text \<open>A classification that is not falling across \<beta> should remain high\<close>
lemma not_falling:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>"
  assumes fall: "x \<notin> falling \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'"
  shows "P \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<L> x \<longrightarrow> P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w !\<turnstile> \<L> x"
proof -
  have D_rel: "D[\<beta>']\<^sub>w - writes \<alpha> \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" using D_force_result by blast
  have P_rel: "P \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> P \<restriction> D[\<alpha>]\<^sub>w" using sub_force sub_known by fastforce

  show ?thesis using fall unfolding falling_def
  proof (auto, goal_cases)
    case 1
    hence "writes \<beta>' \<inter> lpred_vars (PNeg (\<L> x)) = {}" by (simp add: \<C>_vars_correct bexp_to_lpred_vars)
    hence "P+[\<beta>']\<^sub>\<S> \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<L> x" using 1 wf reassign_diff_var_restrict by metis
    moreover have "D[\<alpha>]\<^sub>w \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" by (simp add: stronger_writes sub_force) 
    ultimately show ?case 
      by (meson lpred_restrict_entailment_subset pred_entailment_refl pred_entailment_trans) 
  next
    case 2
    hence "P \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<L> x" using D_rel 
      by (meson lpred_restrict_entailment_subset pred_entailment_refl pred_entailment_trans) 
    moreover have "P \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w !\<turnstile> \<L> x" using 2 P_rel by (meson pred_entailment_trans)
    ultimately show ?case using lsw_constrained_write pred_entailment_def by auto
  next
    case 3
    then show ?case using D_rel 
      by (meson lpred_restrict_entailment_subset pred_entailment_refl pred_entailment_trans) 
  qed
qed

text \<open>A classification that is not rising across \<beta> should remain low\<close>
lemma not_rising:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>"
  assumes rise: "x \<notin> rising \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'"
  shows "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<L> x \<longrightarrow> P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<L> x"
proof -
  have D_rel: "D[\<beta>']\<^sub>w - writes \<alpha> \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" using D_force_result by blast 
  have P_rel: "P \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> P \<restriction> D[\<alpha>]\<^sub>w" using sub_force sub_known by fastforce

  show ?thesis using rise unfolding rising_def
  proof (auto, goal_cases)
    case 1
    hence "writes \<beta>' \<inter> lpred_vars (\<L> x) = {}" by (simp add: \<C>_vars_correct bexp_to_lpred_vars)
    hence "P+[\<beta>']\<^sub>\<S> \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<L> x" using 1 wf reassign_diff_var_restrict by metis
    moreover have "D[\<alpha>]\<^sub>w \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" by (simp add: stronger_writes sub_force) 
    ultimately show ?case 
      by (meson lpred_restrict_entailment_subset pred_entailment_refl pred_entailment_trans) 
  next
    case 2
    hence "P \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w !\<turnstile> \<L> x" using D_rel 
      by (meson lpred_restrict_entailment_subset pred_entailment_refl pred_entailment_trans) 
    moreover have "P \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<L> x" using 2 P_rel by (meson pred_entailment_trans)
    ultimately show ?case using lsw_constrained_write pred_entailment_def by auto
  next
    case 3
    then show ?case using D_rel 
      by (meson lpred_restrict_entailment_subset pred_entailment_refl pred_entailment_trans) 
  qed
qed

text \<open>Show that any write_types known to be high under the original state remain high under the
      modified due to secure_update\<close>
lemma lsw_write:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>"
  shows "P \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<S> :\<^sub>w \<alpha> \<longrightarrow> P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w !\<turnstile> \<S> :\<^sub>w \<alpha>"
proof (cases "writes \<alpha> = {}")
  case True
  hence "\<S> :\<^sub>w \<alpha> = PTrue" by (cases \<alpha>; auto) 
  then show ?thesis using lsw_constrained_write by (auto simp: pred_entailment_def)
next
  case False
  then show ?thesis
  proof (cases "writes \<alpha> \<subseteq> NoRW \<S>")
    case True
    moreover have "\<forall>mem x. x \<in> \<C> \<longrightarrow> pred (\<L> x) mem"
      by (meson High_not_in_\<C> bexp_to_lpred_correct dma_correct)
    ultimately show ?thesis using lsw_constrained_write
      by (cases \<alpha>; auto simp: pred_entailment_def)
  next
    case False
    then obtain x where wr: "writes \<alpha> = {x}" "\<S> :\<^sub>w \<alpha> = \<L> x" by (cases \<alpha>; auto)
    moreover have "x \<notin> falling \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'" 
      using re wr sec False by (auto simp: secure_update_def remove_write)
    ultimately show ?thesis using not_falling assms by auto
  qed
qed

text \<open>Show that any low variables read by \<alpha> will remain low across \<beta> 
      due to secure_update\<close>
lemma lsw_total_read:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>"
  shows "x \<in> reads \<alpha> \<longrightarrow> P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> to_total \<Gamma> x \<longrightarrow> P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> to_total \<Gamma> x"
proof (cases "x \<in> dom \<Gamma>")
  case True
  hence "writes \<beta>' \<inter> lpred_vars (to_total \<Gamma> x) = {}"
    using wf unfolding to_total_def sec_def by auto
  hence "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> to_total \<Gamma> x \<longrightarrow> P+[\<beta>']\<^sub>\<S> \<restriction> D[\<alpha>]\<^sub>w \<turnstile> to_total \<Gamma> x"
    using wf reassign_diff_var_restrict by metis
  moreover have "D[\<alpha>]\<^sub>w \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" by (simp add: stronger_writes sub_force) 
  ultimately show ?thesis
    by (meson lpred_restrict_entailment_subset pred_entailment_refl pred_entailment_trans) 
next
  case False
  moreover have "x \<in> reads \<alpha> \<longrightarrow> x \<notin> rising \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'" 
    using re sec by (auto simp: secure_update_def remove_read)
  ultimately show ?thesis using not_rising assms unfolding to_total_def by auto
qed

text \<open>We can then extend this property to a series of low reads\<close>
lemma lsw_all_total_reads:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
  assumes orig: "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> fold PConj (map (to_total \<Gamma>) l) PTrue"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>"
  assumes rd: "set l \<subseteq> reads \<alpha>"
  shows "P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> fold PConj (map (to_total \<Gamma>) l) PTrue"
  using orig rd
proof (induct l)
  case Nil
  then show ?case by (auto simp: pred_entailment_def)
next
  case (Cons a l)
  have f: "fold PConj (map (to_total \<Gamma>) (a # l)) PTrue =\<^sub>P
        PConj (PConj (to_total \<Gamma> a) PTrue) (fold PConj (map (to_total \<Gamma>) l) PTrue)"
    using pred_equiv_fold_split by auto
  hence "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> to_total \<Gamma> a \<and> a \<in> reads \<alpha>"
    using Cons by (auto simp: pred_equiv_def pred_entailment_def)
  hence "P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> to_total \<Gamma> a" using lsw_total_read assms by auto
  moreover have "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> fold PConj (map (to_total \<Gamma>) l) PTrue \<and> set l \<subseteq> reads \<alpha>" 
    using Cons f by (auto simp: pred_equiv_def pred_entailment_def)
  ultimately show ?case using Cons(1) f by (auto simp: pred_equiv_def pred_entailment_def)
qed

text \<open>We can then show an expression consisting of low reads will remain low\<close>
lemma lsw_read:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
  assumes orig: "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r \<alpha>"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>"
  shows "P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r \<alpha>"
  using lsw_all_total_reads assms by auto

text \<open>This is sufficient to show the writes and reads in a secure_write remain consistent\<close>
lemma later_secure_write:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
  assumes orig: "secure_write \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) \<alpha>"
  assumes upd: "\<Gamma>,\<S>,P,D +\<^sub>\<beta>' \<Gamma>',\<S>',P',D'"
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>"
  shows "secure_write \<Gamma>' \<S>' P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha>"
proof -
  have "\<Gamma>' :\<^sub>r \<alpha> = \<Gamma> :\<^sub>r \<alpha>" using read_type_reorder upd re by auto
  moreover have "\<S>' :\<^sub>w \<alpha> = \<S> :\<^sub>w \<alpha>" using write_type_constant'[OF upd re] by auto
  moreover have "P' = P+[\<beta>']\<^sub>\<S>" using upd by (auto simp: state_upd_def)
  ultimately show ?thesis using assms lsw_read lsw_write
    by (auto simp: secure_write_def subtype_def)
qed

subsubsection \<open> Lemmas for a later secure_update \<close>

lemma P_upd_reorder:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes stable: "type_stable \<S> P"
  shows "P+[\<alpha>]\<^sub>\<S> \<restriction> (D+[\<alpha>]\<^sub>D)[\<beta>]\<^sub>w \<turnstile> P \<restriction> (D+[\<alpha>]\<^sub>D)[\<beta>]\<^sub>w" (is "?P \<restriction> ?D \<turnstile> P \<restriction> ?D")
proof -
  have "?D = -writes \<alpha> \<inter> D[\<beta>']\<^sub>w" using re remove_write by auto
  moreover have "?P \<restriction> (-writes \<alpha>) \<turnstile> P \<restriction> (-writes \<alpha>)"
    using P_upd_restrict_writes stable pred_entailment_refl by auto
  ultimately show ?thesis by (simp add: reassign_diff_var_restrict stable)
qed

text \<open>We show that the set of falling and rising classifications should remain unmodified when
      delaying \<alpha> past \<beta>\<close>
lemma later_falling_subset:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'" (is "secure_update \<Gamma> \<S> P ?W\<^sub>1 ?W\<^sub>2 \<beta>'")
  assumes sec': "secure_update \<Gamma>' \<S> (P+[\<alpha>]\<^sub>\<S>) (D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w) (D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>r) \<beta>"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>" "locals \<subseteq> stable \<S>"
  shows "falling \<S> (P+[\<beta>']\<^sub>\<S>) (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<subseteq> falling \<S> P (D[\<alpha>]\<^sub>w) \<alpha>" (is "?F' \<subseteq> ?F")
proof -

  have wr: "\<And>x. x \<in> ?F' \<Longrightarrow> x \<notin> falling \<S> (P+[\<alpha>]\<^sub>\<S>) ?W\<^sub>1 \<beta> \<and> x \<notin> rising \<S> P ?W\<^sub>1 \<beta>'"
  proof -
    fix x assume a: "x \<in> ?F'"
    hence wr: "\<not> writes \<alpha> \<inter> \<C>_vars x = {}" using falling_def by auto
    moreover have d: "D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w = D[\<beta>']\<^sub>w - writes \<alpha>" using re remove_write by auto
    ultimately have "x \<notin> rising \<S> P ?W\<^sub>1 \<beta>'" using sec by (auto simp: secure_update_def)
    moreover have "x \<notin> falling \<S> (P+[\<alpha>]\<^sub>\<S>) ?W\<^sub>1 \<beta>"
      using a wr d sec' by (auto simp: secure_update_def)
    ultimately show "?thesis x" by auto
  qed

  have r: "falling \<S> (P+[\<alpha>]\<^sub>\<S>) (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>' \<subseteq> falling \<S> (P+[\<alpha>]\<^sub>\<S>) (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>"
    unfolding falling_def
  proof (auto, goal_cases)
    case (1 x xa)
    have "writes \<beta> = writes \<beta>'" using forward_writes re by auto
    then show ?case using 1(3,4,5) by auto
  next
    case (2 x xa)
    have "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> \<turnstile> (P+[\<alpha>]\<^sub>\<S>)+[\<beta>]\<^sub>\<S>" using P_upd_forward[OF wf re] by auto
    hence "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> \<restriction> (D[\<beta>']\<^sub>w - writes \<alpha>) !\<turnstile> \<L> x" using 2(5) pred_entailment_trans by blast 
    then show ?case using 2(2) by simp
  qed

  have "\<And>x. x \<in> ?F' \<Longrightarrow> \<not> P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<L> x"
  proof -
    fix x assume a: "x \<in> ?F'"
    have "\<not> P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<L> x" using a by (auto simp: falling_def)
    thus "?thesis x" using a assms not_rising wr by (meson P_upd_stable) 
  qed

  moreover have "\<And>x. x \<in> ?F' \<Longrightarrow> \<not> P+[\<alpha>]\<^sub>\<S> \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<L> x"
  proof -
    fix x assume a: "x \<in> ?F'"
    have "\<not> (P+[\<beta>']\<^sub>\<S>)+[\<alpha>]\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w !\<turnstile> \<L> x" using a by (auto simp: falling_def) 
    moreover have "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> =\<^sub>P (P+[\<beta>']\<^sub>\<S>)+[\<alpha>]\<^sub>\<S>" using re P_upd_comm[OF wf(1)] by auto
    ultimately have "\<not> (P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w !\<turnstile> \<L> x" 
      by (meson lpred_restrict_entailment pred_entailment_trans pred_equiv_entail) 
    moreover have "x \<notin> falling \<S> (P+[\<alpha>]\<^sub>\<S>) (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'" using a wr r by auto
    ultimately show "?thesis x" using assms not_falling by (meson P_upd_stable)
  qed

  ultimately show ?thesis unfolding falling_def by auto
qed

lemma later_rising_subset:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'" (is "secure_update \<Gamma> \<S> P ?W\<^sub>1 ?W\<^sub>2 \<beta>'")
  assumes sec': "secure_update \<Gamma>' \<S> (P+[\<alpha>]\<^sub>\<S>) (D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w) (D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>r) \<beta>"
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S>" "locals \<subseteq> stable \<S>"
  shows "rising \<S> (P+[\<beta>']\<^sub>\<S>) (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<subseteq> rising \<S> P (D[\<alpha>]\<^sub>w) \<alpha>" (is "?F' \<subseteq> ?F")
proof -

  have wr: "\<And>x. x \<in> ?F' \<Longrightarrow> x \<notin> rising \<S> (P+[\<alpha>]\<^sub>\<S>) ?W\<^sub>1 \<beta> \<and> x \<notin> falling \<S> P ?W\<^sub>1 \<beta>'"
  proof -
    fix x assume a: "x \<in> ?F'"
    hence wr: "\<not> writes \<alpha> \<inter> \<C>_vars x = {}" using rising_def by auto
    moreover have d: "D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w = D[\<beta>']\<^sub>w - writes \<alpha>" using re remove_write by auto
    ultimately have "x \<notin> falling \<S> P ?W\<^sub>1 \<beta>'" using a sec by (auto simp: secure_update_def)
    moreover have "x \<notin> rising \<S> (P+[\<alpha>]\<^sub>\<S>) ?W\<^sub>1 \<beta>"
      using wr d sec' by (auto simp: secure_update_def)
    ultimately show "?thesis x" by auto
  qed

  have r: "rising \<S> (P+[\<alpha>]\<^sub>\<S>) (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>' \<subseteq> rising \<S> (P+[\<alpha>]\<^sub>\<S>) (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>"
    unfolding rising_def
  proof (auto, goal_cases)
    case (1 x xa)
    have "writes \<beta> = writes \<beta>'" using forward_writes re by auto
    then show ?case using 1(3,4,5) by auto
  next
    case (2 x xa)
    have "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> \<turnstile> (P+[\<alpha>]\<^sub>\<S>)+[\<beta>]\<^sub>\<S>" using P_upd_forward[OF wf re] by auto
    hence "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> \<restriction> (D[\<beta>']\<^sub>w - writes \<alpha>) \<turnstile> \<L> x" using 2(5) pred_entailment_trans by blast 
    then show ?case using 2(2) by simp
  qed

  have "\<And>x. x \<in> ?F' \<Longrightarrow> \<not> P \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<L> x"
  proof -
    fix x assume a: "x \<in> ?F'"
    have "\<not> P+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w !\<turnstile> \<L> x" using a by (auto simp: rising_def)
    thus "?thesis x" using a wr assms not_falling by auto
  qed

  moreover have "\<And>x. x \<in> ?F' \<Longrightarrow> \<not> P+[\<alpha>]\<^sub>\<S> \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<L> x"
  proof -
    fix x assume a: "x \<in> ?F'"
    have "\<not> (P+[\<beta>']\<^sub>\<S>)+[\<alpha>]\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<L> x" using a by (auto simp: rising_def) 
    moreover have "(P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> =\<^sub>P (P+[\<beta>']\<^sub>\<S>)+[\<alpha>]\<^sub>\<S>" using re P_upd_comm[OF wf(1)] by auto
    ultimately have "\<not> (P+[\<alpha>]\<^sub>\<S>)+[\<beta>']\<^sub>\<S> \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<L> x" 
      by (meson lpred_restrict_entailment pred_entailment_trans pred_equiv_entail)
    moreover have "x \<notin> rising \<S> (P+[\<alpha>]\<^sub>\<S>) (D[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'" using a wr r by auto
    ultimately show "?thesis x" using assms not_rising by auto
  qed

  ultimately show ?thesis unfolding rising_def by auto
qed

text \<open>Given these sets are unmodified, we can show the secure_update is preserved\<close>
lemma later_secure_update:
  assumes sec: "secure_update \<Gamma> \<S> P (D[\<beta>']\<^sub>w - writes \<alpha>) (D[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'" (is "secure_update \<Gamma> \<S> P ?W\<^sub>1 ?W\<^sub>2 \<beta>'")
  assumes sec': "secure_update \<Gamma>'' \<S> (P+[\<alpha>]\<^sub>\<S>) (D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w) (D+[\<alpha>]\<^sub>D[\<beta>]\<^sub>r) \<beta>"
  assumes wr: "secure_write \<Gamma> \<S> P ?W\<^sub>1 \<beta>'"
  assumes orig: "secure_update \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) (D[\<alpha>]\<^sub>r) \<alpha>"
  assumes upd: "\<Gamma>,\<S>,P,D +\<^sub>\<beta>' \<Gamma>',\<S>',P',D'"
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P \<subseteq> stable \<S> \<and> dom \<Gamma> \<inter> \<C> = {} \<and> locals \<subseteq> stable \<S>"
  shows "secure_update \<Gamma>' \<S>' P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>r) \<alpha>"
proof (cases "vars \<alpha> = {}")
  case True
  thus ?thesis unfolding secure_update_def falling_def rising_def by auto
next
  case False
  (* Show the rising property is maintained *)
  have "rising \<S> (P+[\<beta>']\<^sub>\<S>) (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<subseteq> rising \<S> P (D[\<alpha>]\<^sub>w) \<alpha>"
    using assms later_rising_subset by auto
  moreover have "rising \<S> P (D[\<alpha>]\<^sub>w) \<alpha> \<subseteq> read_high (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>r)"
  proof -
    have "\<forall>x \<in> rising \<S> P (D[\<alpha>]\<^sub>w) \<alpha>. \<C>_vars x \<subseteq> D[\<alpha>]\<^sub>w" using orig by (auto simp: secure_update_def)
    moreover have "D[\<alpha>]\<^sub>w \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" by (simp add: stronger_writes sub_force)
    ultimately have "\<forall>x \<in> rising \<S> P (D[\<alpha>]\<^sub>w) \<alpha>. \<C>_vars x \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" by auto
    moreover have "D[\<alpha>]\<^sub>r \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>r" by (simp add: stronger_reads sub_force)
    ultimately show ?thesis using orig by (auto simp: secure_update_def)
  qed
  ultimately have rise: "rising \<S>' P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<subseteq> read_high (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>r)"
    using upd by (auto simp: state_upd_def)

  have stable': "lpred_vars P' \<subseteq> stable \<S>" using P_upd_stable upd by (auto simp: state_upd_def)

  (* Show the falling property is maintained *)
  have "\<S>' = \<S>" using upd by (auto simp: state_upd_def)
  moreover have "falling \<S> P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<subseteq> falling \<S> P (D[\<alpha>]\<^sub>w) \<alpha>"
    using assms later_falling_subset by (auto simp: state_upd_def)
  moreover have "falling \<S> P (D[\<alpha>]\<^sub>w) \<alpha> \<subseteq> written_low \<S> \<Gamma> P (D[\<alpha>]\<^sub>w)" 
    using orig by (auto simp: secure_update_def)
  moreover have "falling \<S> P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<inter> written_low \<S> \<Gamma> P (D[\<alpha>]\<^sub>w) \<subseteq> written_low \<S> \<Gamma>' P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w)"
  proof -
    have "D[\<alpha>]\<^sub>w \<subseteq> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w" by (simp add: stronger_writes sub_force)
    moreover have dom: "dom \<Gamma> = dom \<Gamma>'" using state_upd_\<Gamma>_dom upd by blast 
    moreover have "\<And>x. x \<in> falling \<S> P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<inter> dom \<Gamma> \<Longrightarrow> ((P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> sec (the (\<Gamma> x)) \<longrightarrow> P' \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> sec(the (\<Gamma>' x))) \<or> x \<in> NoRW \<S>)"
    proof - (* A long sub-goal to show predicate reasoning remains valid, TODO: refactor *)
      fix x assume a: "x \<in> falling \<S> P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<inter> dom \<Gamma>"
      show "?thesis x"
      proof (cases "x \<in> writes \<beta>'")
        case True
        then obtain e where b: "\<beta>' = x \<leftarrow> e" by (cases \<beta>'; auto)
        have "P \<restriction> ?W\<^sub>1 !\<turnstile> \<S> :\<^sub>w \<beta>' \<or> P \<restriction> ?W\<^sub>1 \<turnstile> \<Gamma> :\<^sub>r \<beta>'" using wr unfolding secure_write_def subtype_def by auto
        then show ?thesis 
        proof (auto, goal_cases)
          case 1
          hence "P \<restriction> ?W\<^sub>1 !\<turnstile> \<L> x" using b by auto
          moreover have "x \<notin> \<C>" using a wf by auto
          have "P'+[\<alpha>]\<^sub>\<S> \<restriction> (D+[\<beta>']\<^sub>F)[\<alpha>]\<^sub>w !\<turnstile> \<L> x" 
          proof -
            have "P \<restriction> ?W\<^sub>1 !\<turnstile> \<L> x" using 1 b by auto
            hence "P' \<restriction> ?W\<^sub>1 !\<turnstile> \<L> x" using upd b  unfolding state_upd_def
              by (metis Int_commute True local.wf lpred_vars.simps(3) reassign_diff_var_restrict self_control)
            moreover have "P'+[\<alpha>]\<^sub>\<S> \<restriction> ?W\<^sub>1 \<turnstile> P' \<restriction> ?W\<^sub>1"  
              by (simp add: inf_left_commute reassign_diff_var_restrict stable')
            ultimately have a: "P'+[\<alpha>]\<^sub>\<S> \<restriction> ?W\<^sub>1 !\<turnstile> \<L> x" using pred_entailment_trans by blast
            moreover have "?W\<^sub>1 \<subseteq> (D+[\<beta>']\<^sub>F)[\<alpha>]\<^sub>w" using False D_force_result by blast
            ultimately have "P'+[\<alpha>]\<^sub>\<S> \<restriction> (D+[\<beta>']\<^sub>F)[\<alpha>]\<^sub>w \<turnstile> P'+[\<alpha>]\<^sub>\<S> \<restriction> ?W\<^sub>1" by (simp add: lpred_restrict_entailment_subset) 
            thus ?thesis using a by (auto simp: pred_entailment_def)
          qed
          moreover have "\<not> P'+[\<alpha>]\<^sub>\<S> \<restriction> (D+[\<beta>']\<^sub>F)[\<alpha>]\<^sub>w !\<turnstile> \<L> x" using a unfolding falling_def by auto
          ultimately show ?case by auto
        next
          case 2
          have "P \<restriction> D[\<beta>']\<^sub>w \<turnstile> P \<restriction> ?W\<^sub>1" using re remove_write by (auto simp: lpred_restrict_entailment_subset) 
          also have "... \<turnstile>  \<Gamma> :\<^sub>r \<beta>'" using 2 by auto
          finally have "the (\<Gamma>' x) = Low" using a b upd by (auto simp: state_upd_def)
          then show ?case by (auto simp: pred_entailment_def sec_def)
        qed
      next
        case False (* Long winded proof to show non-writes are preserved *)
        hence "the (\<Gamma> x) = Low \<or> the (\<Gamma> x) = High" using calculation lpred_vars_empty by (cases "the (\<Gamma> x)"; auto)
        hence "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> sec (the (\<Gamma> x)) \<longrightarrow> the (\<Gamma> x) = Low \<or> P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> PFalse" using a
          unfolding sec_def using pred_entailment_trans pred_equiv_entail
          by presburger  
        hence "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> sec (the (\<Gamma> x)) \<longrightarrow> the (\<Gamma> x) = Low \<or> P' \<restriction> D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> PFalse" using lsw_constrained_write upd
          by (auto simp: pred_entailment_def pred_equiv_def state_upd_def)
        moreover have "the (\<Gamma>' x) = the (\<Gamma> x)" using state_upd_\<Gamma>_eq upd False by fastforce
        ultimately show ?thesis by (auto simp: pred_entailment_def pred_equiv_def sec_def )
      qed
    qed
    ultimately show ?thesis by auto
  qed
  ultimately have fall: "falling \<S>' P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w) \<alpha> \<subseteq> written_low \<S>' \<Gamma>' P' (D+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w)"
    by blast

  show ?thesis using rise fall by (auto simp: secure_update_def)
qed

text \<open>
  To simplify the reasoning, we break properties over \<beta> into two:
    - secure_update uses @{term D\<^sub>1} + \<alpha>, to demonstrate the lack of dependence on \<alpha>
    - state_upd uses @{term D\<^sub>1}, to unify the states within the inductive proof
\<close>
lemma later_action_type:
  assumes "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { \<alpha> }\<^sub>a \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) (D\<^sub>1[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
  assumes "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'"
  assumes "secure_update \<Gamma>' \<S>\<^sub>1 (P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>1) (D\<^sub>1+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w) (D\<^sub>1+[\<alpha>]\<^sub>D[\<beta>]\<^sub>r) \<beta>"
  assumes "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<beta>' \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  assumes "\<beta>' < \<alpha> < \<beta>"
  assumes "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 \<inter> \<C> = {} \<and> locals \<subseteq> stable \<S>\<^sub>1"
  shows "\<exists>\<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i. \<turnstile> \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>1+[\<beta>']\<^sub>F { \<alpha> }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i"
  using later_secure_write later_secure_update assms state_upd_def by auto

subsubsection \<open> Merge Reordered States \<close>

text \<open>Show that the two possible type checking paths (\<alpha> ;;; \<beta> or \<beta> ;;; \<alpha>) result in ordered final
      type checking states\<close>
lemma reorder_state_ord:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  assumes stable: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> locals \<subseteq> dom \<Gamma>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 = stable \<S>\<^sub>1 - \<C>"
  assumes sec: "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w)) (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) (D\<^sub>1[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
  assumes wr: "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 (D\<^sub>1[\<alpha>]\<^sub>w) \<alpha>"
  assumes fst: "\<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 +\<^sub>\<alpha> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes snd: "\<Gamma>\<^sub>2,\<S>\<^sub>2,spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w),D\<^sub>2 +\<^sub>\<beta> \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3" (is "\<Gamma>\<^sub>2,\<S>\<^sub>2,?P\<^sub>2,D\<^sub>2 +\<^sub>\<beta> \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3")
  assumes fst': "\<Gamma>\<^sub>1,\<S>\<^sub>1,spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w),D\<^sub>1 +\<^sub>\<beta>' \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i" (is "\<Gamma>\<^sub>1,\<S>\<^sub>1,?P\<^sub>1,D\<^sub>1 +\<^sub>\<beta>' \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i")
  assumes snd': "\<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>1+[\<beta>']\<^sub>F +\<^sub>\<alpha> \<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f"
  shows "\<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>2+[\<beta>]\<^sub>F"
  unfolding state_ord_def
proof (intro conjI allI impI, goal_cases)
  case (1 env)
  moreover have "\<S>\<^sub>f = \<S>\<^sub>3" using assms by (auto simp: state_upd_def)
  moreover have "dom \<Gamma>\<^sub>f = dom \<Gamma>\<^sub>3" using assms by (metis state_upd_\<Gamma>_dom)
  moreover have "lpred_vars P\<^sub>3 \<subseteq> stable \<S>\<^sub>3" using snd by (auto simp: state_upd_def)
  ultimately show ?case by (auto simp: tyenv_wellformed_def)
next
  case 2                                                               
  have \<Gamma>\<^sub>3: "\<Gamma>\<^sub>3 = \<Gamma>\<^sub>2+\<^bsub>(?P\<^sub>2 \<restriction> D\<^sub>2[\<beta>]\<^sub>w)\<^esub>[\<beta>]" using assms state_upd_def by auto
  have \<Gamma>\<^sub>f: "\<Gamma>\<^sub>f = \<Gamma>\<^sub>i+\<^bsub>(P\<^sub>i \<restriction> (D\<^sub>1+[\<beta>']\<^sub>F)[\<alpha>]\<^sub>w)\<^esub>[\<alpha>]" using assms state_upd_def by auto
  have w: "writes \<beta> = writes \<beta>'" using re forward_writes by auto

  have dom: "dom \<Gamma>\<^sub>1 = dom \<Gamma>\<^sub>f" "dom \<Gamma>\<^sub>2 = dom \<Gamma>\<^sub>f" "dom \<Gamma>\<^sub>f = dom \<Gamma>\<^sub>i" "dom \<Gamma>\<^sub>f = dom \<Gamma>\<^sub>3"
    using fst snd fst' snd' state_upd_\<Gamma>_dom by metis+

  (* Unmodified \<Gamma> entries should remain equal after operations in either order *)
  have "\<And>x. x \<in> dom \<Gamma>\<^sub>f - (writes \<alpha>) - (writes \<beta>') \<Longrightarrow> context_equiv_type (the (\<Gamma>\<^sub>f x)) (the (\<Gamma>\<^sub>3 x))"
  proof -
    fix x assume a: "x \<in> dom \<Gamma>\<^sub>f - writes \<alpha> - writes \<beta>'"
    hence "x \<in> - writes \<alpha> \<and> x \<in> - writes \<beta>'" by auto
    hence "\<Gamma>\<^sub>f x = \<Gamma>\<^sub>3 x" using state_upd_\<Gamma>_eq assms w by metis
    thus "?thesis x" by auto
  qed

  (* The secure_update property for \<beta> should ensure context equivalence when performing \<alpha> later *)
  moreover have "\<And>x. x \<in> dom \<Gamma>\<^sub>f \<inter> writes \<alpha> \<Longrightarrow> context_equiv_type (the (\<Gamma>\<^sub>f x)) (the (\<Gamma>\<^sub>3 x))"
  proof -
    fix x assume a: "x \<in> dom \<Gamma>\<^sub>f \<inter> writes \<alpha>"

    moreover have "P\<^sub>1 \<restriction> D\<^sub>1[\<alpha>]\<^sub>w \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<alpha> \<Longrightarrow> P\<^sub>i \<restriction> D\<^sub>1+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<alpha>"
    proof -
      assume "P\<^sub>1 \<restriction> D\<^sub>1[\<alpha>]\<^sub>w \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<alpha>"
      hence "?P\<^sub>1 \<restriction> D\<^sub>1[\<alpha>]\<^sub>w \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<alpha>" 
        by (meson lpred_restrict_entailment pred_entailment_trans spec_entails) 
      moreover have "lpred_vars ?P\<^sub>1 \<subseteq> stable \<S>\<^sub>1" by (simp add: spec\<^sub>\<S>_stable stable) 
      moreover have "P\<^sub>i = ?P\<^sub>1+[\<beta>']\<^sub>\<S>\<^sub>1" using fst' by (auto simp: state_upd_def)
      ultimately have "P\<^sub>i \<restriction> D\<^sub>1+[\<beta>']\<^sub>F[\<alpha>]\<^sub>w \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<alpha>" using lsw_read re stable sec by auto
      thus ?thesis using pred_entailment_trans by meson
    qed
    moreover have "\<Gamma>\<^sub>1 :\<^sub>r \<alpha> = \<Gamma>\<^sub>i :\<^sub>r \<alpha>" using read_type_reorder[OF assms(7)] re by auto
    ultimately have "context_equiv_type (the (\<Gamma>\<^sub>f x)) (the (\<Gamma>\<^sub>2 x))" using snd' fst dom
      by (cases \<alpha>; auto simp: state_upd_def context_equiv_type_def pred_entailment_def less_eq_Sec_def)

    moreover have "\<Gamma>\<^sub>2 x = \<Gamma>\<^sub>3 x" using a re \<Gamma>\<^sub>3 \<Gamma>_upd_nop reorder_diff_writes\<^sub>f by fastforce
    ultimately show "?thesis x" by simp
  qed

  (* Due to the stronger state when type-checking \<beta> earlier, its \<Gamma> entries are stronger *)
  moreover have "\<And>x. x \<in> dom \<Gamma>\<^sub>f \<inter> writes \<beta>' \<Longrightarrow> context_equiv_type (the (\<Gamma>\<^sub>f x)) (the (\<Gamma>\<^sub>3 x))"
  proof -
    fix x assume a: "x \<in> dom \<Gamma>\<^sub>f \<inter> writes \<beta>'"
    have "\<Gamma>\<^sub>2 :\<^sub>r \<beta> \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<beta>' \<or> P\<^sub>2 !\<turnstile> \<Gamma>\<^sub>2 :\<^sub>r \<beta>" using read_type_reorder'[OF fst wr re stable] by auto
    hence "\<Gamma>\<^sub>2 :\<^sub>r \<beta> \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<beta>' \<or> ?P\<^sub>2 !\<turnstile> \<Gamma>\<^sub>2 :\<^sub>r \<beta>" using spec_entails pred_entailment_trans by blast 
    hence "\<Gamma>\<^sub>2 :\<^sub>r \<beta> \<turnstile> \<Gamma>\<^sub>1 :\<^sub>r \<beta>' \<or> \<not> ?P\<^sub>2 \<restriction> D\<^sub>2[\<beta>]\<^sub>w \<turnstile> \<Gamma>\<^sub>2 :\<^sub>r \<beta> \<or> ?P\<^sub>2 \<restriction> D\<^sub>2[\<beta>]\<^sub>w \<turnstile> PFalse" using pred_restrict_not by auto
    moreover have "?P\<^sub>1 \<restriction> D\<^sub>1[\<beta>']\<^sub>w \<turnstile> ?P\<^sub>2 \<restriction> D\<^sub>2[\<beta>]\<^sub>w" using fst stable re state_upd_spec' by simp
    moreover have "writes \<beta> = writes \<beta>'" using forward_writes re by auto
    ultimately have "context_equiv_type (the (\<Gamma>\<^sub>i x)) (the (\<Gamma>\<^sub>3 x))" using a snd fst' dom(1,2)
      by (cases \<beta>'; cases \<beta>; auto simp: state_upd_def context_equiv_type_def pred_entailment_def less_eq_Sec_def) 

    moreover have "\<Gamma>\<^sub>i x = \<Gamma>\<^sub>f x" using a re \<Gamma>\<^sub>f \<Gamma>_upd_nop reorder_diff_writes\<^sub>f by fastforce
    ultimately show "?thesis x" by simp
  qed

  ultimately show ?case using dom(4) unfolding context_equiv_def by auto
next
  case 3
  then show ?case using assms by (auto simp: state_upd_def)
next
  case 4
  have a: "D_gen \<beta> \<inter> D_kill \<alpha> = {}" using re reorder_disjoint_gen_kill\<^sub>f by auto
  have b: "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {} \<and> reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
    using limit\<^sub>w_correct\<^sub>f limit\<^sub>r_correct\<^sub>f re by auto
  have "D\<^sub>1+[\<alpha>]\<^sub>D+[\<beta>]\<^sub>F \<subseteq>\<^sub>D D\<^sub>1+[\<beta>']\<^sub>F+[\<alpha>]\<^sub>D"
    using re
  proof (cases rule: forward_elim)
    case normal
    then show ?thesis using force_update_comm[OF b normal(2) a] by auto 
  next
    case forward
    moreover have "D_gen \<beta> = D_gen \<beta>'" using re reorder_not_fence\<^sub>f by (auto simp: D_gen_def)
    moreover have "writes \<beta>' = writes \<beta>" using forward re forward_writes by auto
    moreover have "reads \<beta>' = reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)" using forward re forward_reads by auto
    ultimately show ?thesis using force_update_comm\<^sub>f[OF b] a by metis
  qed
  moreover have "D\<^sub>2 = D\<^sub>1+[\<alpha>]\<^sub>D" "D\<^sub>f = D\<^sub>1+[\<beta>']\<^sub>F+[\<alpha>]\<^sub>D" using assms by (auto simp: state_upd_def)
  ultimately show ?case by (meson sub_eq_trans)
next
  case 5
  have s: "lpred_vars (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>f (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w)) \<subseteq> stable \<S>\<^sub>f"
    using assms state_upd_def by (simp add: spec\<^sub>\<S>_stable)
  have l: "locals \<subseteq> stable \<S>\<^sub>f" 
    using assms state_upd_def by (auto simp add: spec\<^sub>\<S>_stable)
  have d: "D\<^sub>1[\<beta>']\<^sub>w \<supseteq> D\<^sub>2[\<beta>]\<^sub>w" using assms(1,5) remove_write by (auto simp: state_upd_def)
  have r: "\<alpha> \<hookleftarrow> \<beta>'" using re by auto

  have "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>f" using assms by (auto simp: state_upd_def)
  moreover have "writes \<alpha> \<inter> D\<^sub>2[\<beta>]\<^sub>w = {}" 
    using fst unfolding state_upd_def using remove_write [OF assms(1)] by auto
  ultimately have "spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>f (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w)+[\<alpha>]\<^sub>\<S>\<^sub>f \<turnstile> spec\<^sub>\<S> (P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>f) \<S>\<^sub>f c (D\<^sub>2[\<beta>]\<^sub>w)" 
    using spec_repeat assms(1) by auto
  hence "(spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>f (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w)+[\<alpha>]\<^sub>\<S>\<^sub>f)+[\<beta>]\<^sub>\<S>\<^sub>f \<turnstile> (spec\<^sub>\<S> (P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>f) \<S>\<^sub>f c (D\<^sub>2[\<beta>]\<^sub>w))+[\<beta>]\<^sub>\<S>\<^sub>f"
    using P_upd_entailment by auto
  hence "(spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>f (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w)+[\<alpha>]\<^sub>\<S>\<^sub>f)+[\<beta>']\<^sub>\<S>\<^sub>f \<turnstile> (spec\<^sub>\<S> (P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>f) \<S>\<^sub>f c (D\<^sub>2[\<beta>]\<^sub>w))+[\<beta>]\<^sub>\<S>\<^sub>f"
    using P_upd_forward[OF s l re] by (meson pred_equiv_entail pred_entailment_trans)
  hence "(spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>f (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w)+[\<beta>']\<^sub>\<S>\<^sub>f)+[\<alpha>]\<^sub>\<S>\<^sub>f \<turnstile> (spec\<^sub>\<S> (P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>f) \<S>\<^sub>f c (D\<^sub>2[\<beta>]\<^sub>w))+[\<beta>]\<^sub>\<S>\<^sub>f"
    using P_upd_comm [OF s r] by (meson pred_equiv_entail pred_entailment_trans)
  hence "(spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>f (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w)+[\<beta>']\<^sub>\<S>\<^sub>f)+[\<alpha>]\<^sub>\<S>\<^sub>f \<turnstile> (spec\<^sub>\<S> (P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>f) \<S>\<^sub>f c (D\<^sub>2[\<beta>]\<^sub>w))+[\<beta>]\<^sub>\<S>\<^sub>f"
    using d by (meson P_upd_entailment pred_entailment_refl pred_entailment_trans spec\<^sub>\<S>_entailment)
  thus ?case using assms by (auto simp: state_upd_def)
qed

subsubsection \<open> Reordering Type \<close>

text \<open>Compose the prior lemmas into a cleaner, final form\<close>
lemma reorder_type:
  assumes fst: "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { \<alpha> }\<^sub>a \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes snd: "\<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w),D\<^sub>2 { \<beta> }\<^sub>a \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  assumes re:  "\<beta>' < \<alpha> < \<beta>"
  assumes wf: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> locals \<subseteq> dom \<Gamma>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 = stable \<S>\<^sub>1 - \<C>"
  shows "\<exists>\<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i \<Gamma>\<^sub>f \<S>\<^sub>f P\<^sub>f D\<^sub>f.
          (\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w),D\<^sub>1 { \<beta>' }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i) \<and>
          (\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>1+[\<beta>']\<^sub>F { \<alpha> }\<^sub>a \<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f) \<and>
          (\<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>2+[\<beta>]\<^sub>F)"
proof -
  let ?P\<^sub>1 = "spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w)" and ?P\<^sub>2 = "spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>2[\<beta>']\<^sub>w)"

  have l: "locals \<subseteq> stable \<S>\<^sub>1" using wf by auto
  have s: "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1" using wf by auto

  (* Show \<beta>' type checks under the initial state extended with speculation *)
  obtain \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i where fst': "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,?P\<^sub>1,D\<^sub>1 { \<beta>' }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i" 
    using earlier_action_type re wf fst snd by metis

  (* Show \<alpha> type checks under the initial state extended with speculation *)
  have "?P\<^sub>1 \<turnstile> P\<^sub>1" using spec_entails by auto
  hence "\<Gamma>\<^sub>1,\<S>\<^sub>1,?P\<^sub>1,D\<^sub>1 >\<^sub>s \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1" using wf state_ord_entailment state_ord_refl by metis
  then obtain \<Gamma>' \<S>' P' where a: "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,?P\<^sub>1,D\<^sub>1 { \<alpha> }\<^sub>a \<Gamma>',\<S>',P',D\<^sub>2"
    by (metis (no_types, lifting) action_type_stronger fst state_upd_def)
  moreover have "lpred_vars ?P\<^sub>1 \<subseteq> stable \<S>\<^sub>1" using spec\<^sub>\<S>_stable wf by auto

  (* Show secure_update for \<beta>' does not depend on \<alpha> *)
  moreover have sec: "secure_update \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) (D\<^sub>1[\<beta>']\<^sub>r - reads \<alpha>) \<beta>'"
    using earlier_secure_update_modulo_\<alpha>' assms by (auto simp: state_upd_def)
  moreover have "secure_write \<Gamma>\<^sub>1 \<S>\<^sub>1 ?P\<^sub>1 (D\<^sub>1[\<beta>']\<^sub>w - writes \<alpha>) \<beta>'"
    using earlier_secure_write_modulo_\<alpha>' assms by (auto simp: state_upd_def)

  (* Show secure_update for \<beta>' does not depend on \<alpha> even if it has been evaluated *)
  moreover have  "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>1 (?P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>1) (D\<^sub>1+[\<alpha>]\<^sub>D[\<beta>]\<^sub>w) (D\<^sub>1+[\<alpha>]\<^sub>D[\<beta>]\<^sub>r) \<beta>"
  proof -
    have "D\<^sub>1[\<beta>']\<^sub>w \<supseteq> D\<^sub>2[\<beta>]\<^sub>w" using re remove_write fst by (auto simp: state_upd_def)
    hence "?P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>1 \<turnstile> (spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>2[\<beta>]\<^sub>w))+[\<alpha>]\<^sub>\<S>\<^sub>1"
      by (simp add: P_upd_entailment spec\<^sub>\<S>_entailment)
    also have "... \<turnstile> spec\<^sub>\<S> (P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>1) \<S>\<^sub>1 c (D\<^sub>2[\<beta>]\<^sub>w)"
    proof (rule spec_repeat)
      show "writes \<alpha> \<inter> D\<^sub>2[\<beta>]\<^sub>w = {}" using re remove_write fst by (auto simp: state_upd_def)
      show "\<beta>' < \<alpha> < \<beta>" using re by auto
      show "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1" using assms by auto
    qed
    finally have "?P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>1 \<turnstile> spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)"
      using fst by (auto simp: state_upd_def)
    hence "\<Gamma>\<^sub>2,\<S>\<^sub>1,?P\<^sub>1+[\<alpha>]\<^sub>\<S>\<^sub>1,D\<^sub>1+[\<alpha>]\<^sub>D >\<^sub>s \<Gamma>\<^sub>2,\<S>\<^sub>2,spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w),D\<^sub>2"
      by (metis (no_types, lifting) P_upd_stable fst spec\<^sub>\<S>_stable state_ord_entailment state_ord_refl state_upd_def)
    moreover have "secure_update \<Gamma>\<^sub>2 \<S>\<^sub>2 (spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>]\<^sub>w)) (D\<^sub>2[\<beta>]\<^sub>w) (D\<^sub>2[\<beta>]\<^sub>r) \<beta>" using snd by auto
    ultimately show ?thesis using secure_update_entailment_ord by auto
  qed 

  (* Show \<alpha> type checks under the initial state modified by \<beta> *)
  ultimately obtain \<Gamma>\<^sub>f \<S>\<^sub>f P\<^sub>f D\<^sub>f where snd': "\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>1+[\<beta>']\<^sub>F { \<alpha> }\<^sub>a \<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f"
    using later_action_type re wf fst' l by (metis Diff_disjoint Int_commute)

  (* Show this ordering is just as strong as the original *)
  have ord: "\<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>2+[\<beta>]\<^sub>F" 
    using reorder_state_ord re wf sec fst snd fst' snd' by metis
  show ?thesis using fst' snd' ord by metis
qed

text \<open> Reorder action and stmt \<alpha> ;;; c \<close>
lemma stmt_reorder_type:
  assumes "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { c } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
  assumes "\<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 { \<beta> }\<^sub>a \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  assumes wf: "tyenv_wellformed env \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1"
  assumes re: "\<beta>' < c <\<^sub>c \<beta>"
  shows  "\<exists>\<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i.
          (\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 c (D\<^sub>1[\<beta>']\<^sub>w),D\<^sub>1 { \<beta>' }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i) \<and>
          (\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>1+[\<beta>']\<^sub>F { c } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3)"
  using assms
proof (induct arbitrary: \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3 \<beta>' rule: has_type.induct)
  case (stop_type \<Gamma> \<S> P D)

  (* Show \<beta> type checks with null speculation *)
  hence ord: "\<Gamma>,\<S>,spec\<^sub>\<S> P \<S> Stop (D[\<beta>]\<^sub>w),D >\<^sub>s \<Gamma>,\<S>,P,D"
    by (meson tyenv_wellformed_def spec_entails state_ord_entailment state_ord_refl)
  then obtain \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i where type:
      "\<turnstile> \<Gamma>,\<S>,spec\<^sub>\<S> P \<S> Stop (D[\<beta>]\<^sub>w),D { \<beta> }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i" 
      "\<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
    using action_type_stronger stop_type(1) by meson

  (* Show Stop type checks given \<beta> has been evaluated *)
  hence "\<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D+[\<beta>]\<^sub>F >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
    using state_ord_def state_upd_def stop_type.prems(1) sub_force_update by auto
  hence "\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D+[\<beta>]\<^sub>F { Stop } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
    using has_type.stop_type rewrite state_ord_refl by metis

  moreover have "\<beta> = \<beta>'" using stop_type(3) by auto
  ultimately show ?case using type by auto
next
  case (act_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 \<alpha> \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c \<Gamma>\<^sub>4 \<S>\<^sub>4 P\<^sub>4 D\<^sub>4 \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3)
  (* Satisfy conditions for inductive hypothesis *)
  obtain \<beta>'' where re: "\<beta>' < \<alpha> < \<beta>''" "\<beta>'' < c <\<^sub>c \<beta>" using act_type by auto
  have wf: "tyenv_wellformed env \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2" using act_type(1,5) state_upd_wellformed by metis

  (* Use inductive hypothesis to reorder \<beta> and c *)
  obtain \<Gamma>\<^sub>r \<S>\<^sub>r P\<^sub>r D\<^sub>r where hypo:
      "\<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>'']\<^sub>w),D\<^sub>2 { \<beta>'' }\<^sub>a \<Gamma>\<^sub>r,\<S>\<^sub>r,P\<^sub>r,D\<^sub>r"
      "\<turnstile> \<Gamma>\<^sub>r,\<S>\<^sub>r,P\<^sub>r,D\<^sub>2+[\<beta>'']\<^sub>F { c } \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
    using re(2) act_type(3,4) wf by metis

  moreover have "lpred_vars P\<^sub>1 \<subseteq> stable \<S>\<^sub>1 \<and> locals \<subseteq> dom \<Gamma>\<^sub>1 \<and> dom \<Gamma>\<^sub>1 = stable \<S>\<^sub>1 - \<C>"
    using act_type unfolding tyenv_wellformed_def by auto

  (* Reorder \<alpha> and \<beta> using reorder_type *)
  ultimately obtain \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i \<Gamma>\<^sub>f \<S>\<^sub>f P\<^sub>f D\<^sub>f where step:
      "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 (\<alpha> ;;; c) (D\<^sub>1[\<beta>']\<^sub>w),D\<^sub>1 { \<beta>' }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i"
      "\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>1+[\<beta>']\<^sub>F { \<alpha> }\<^sub>a \<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f" 
      "\<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f >\<^sub>s \<Gamma>\<^sub>r,\<S>\<^sub>r,P\<^sub>r,D\<^sub>2+[\<beta>'']\<^sub>F"
    using re(1) reorder_type act_type(1,5,6) by metis

  (* Combine later \<alpha> with reordered c *)
  thus ?case using hypo(2) rewrite has_type.act_type state_ord_refl by meson
next
  case (choice_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 c\<^sub>2)
  then show ?case by auto 
next
  case (loop_type \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c\<^sub>1 c\<^sub>2 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2)
  then show ?case by auto
next
  case (rewrite \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1 c \<Gamma>\<^sub>1' \<S>\<^sub>1' P\<^sub>1' D\<^sub>1' \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 \<Gamma>\<^sub>2' \<S>\<^sub>2' P\<^sub>2' D\<^sub>2')
  (* Satisfy conditions for inductive hypothesis *)
  obtain \<Gamma>\<^sub>f \<S>\<^sub>f P\<^sub>f D\<^sub>f where props: 
      "\<turnstile> \<Gamma>\<^sub>1',\<S>\<^sub>1',P\<^sub>1',D\<^sub>1' { \<beta> }\<^sub>a \<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f" 
      "\<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f >\<^sub>s \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
    using action_type_stronger[OF rewrite(5,4)] by auto
  moreover have "tyenv_wellformed env \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1"
    using rewrite unfolding state_ord_def by auto

  (* Use inductive hypothesis *)
  ultimately obtain \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i where hypo:
      "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 c (D\<^sub>1[\<beta>']\<^sub>w),D\<^sub>1 { \<beta>' }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i"
      "\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>1+[\<beta>']\<^sub>F { c } \<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f"
    using rewrite by metis

  (* Show ordering between pre-states is preserved across speculation *)
  moreover have "\<Gamma>\<^sub>2,\<S>\<^sub>2,(spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>']\<^sub>w)),D\<^sub>2 >\<^sub>s \<Gamma>\<^sub>1,\<S>\<^sub>1,(spec\<^sub>\<S> P\<^sub>1 \<S>\<^sub>1 c (D\<^sub>1[\<beta>']\<^sub>w)),D\<^sub>1"
  proof (rule spec_state_ord)
    show "lpred_vars P\<^sub>2 \<subseteq> stable \<S>\<^sub>2" using rewrite(6) by (auto simp: tyenv_wellformed_def)
    show "\<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 >\<^sub>s \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1" using rewrite(3) by assumption
    show "D\<^sub>1[\<beta>']\<^sub>w \<subseteq> D\<^sub>2[\<beta>']\<^sub>w" using rewrite(3) by (auto simp add: state_ord_def stronger_writes)
  qed

  (* Type check \<beta> on stronger state *)
  ultimately obtain \<Gamma>\<^sub>i' \<S>\<^sub>i' P\<^sub>i' D\<^sub>i' where props':
      "\<turnstile> \<Gamma>\<^sub>2,\<S>\<^sub>2,(spec\<^sub>\<S> P\<^sub>2 \<S>\<^sub>2 c (D\<^sub>2[\<beta>']\<^sub>w)),D\<^sub>2 { \<beta>'}\<^sub>a \<Gamma>\<^sub>i',\<S>\<^sub>i',P\<^sub>i',D\<^sub>i'"
      "\<Gamma>\<^sub>i',\<S>\<^sub>i',P\<^sub>i',D\<^sub>i' >\<^sub>s \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i"
    using action_type_stronger by meson

  (* Strengthen pre and weaken post on c *)
  moreover have "\<turnstile> \<Gamma>\<^sub>i',\<S>\<^sub>i',P\<^sub>i',D\<^sub>2+[\<beta>']\<^sub>F {c} \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
  proof -
    have d: "D\<^sub>1+[\<beta>']\<^sub>F \<subseteq>\<^sub>D D\<^sub>2+[\<beta>']\<^sub>F" using rewrite(3) by (meson state_ord_def sub_force_preserve)
    hence "\<Gamma>\<^sub>i',\<S>\<^sub>i',P\<^sub>i',D\<^sub>2+[\<beta>']\<^sub>F >\<^sub>s \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>1+[\<beta>']\<^sub>F" using props' by (simp add: state_ord_def)
    thus ?thesis using has_type.rewrite hypo(2) props(2) by meson
  qed

  ultimately show ?case by auto
qed

text \<open> Break evaluated program into parts, show it type checks \<close>
lemma typed_sub_prog: 
  assumes typed: "\<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>',\<S>',P',D'"
  assumes eval: "\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>\<beta> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  assumes wf: "tyenv_wellformed env \<Gamma> \<S> P"
  shows "\<exists>\<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i.
          (\<turnstile> \<Gamma>,\<S>,(spec_flat\<^sub>\<S> P \<S> c det (D[\<beta>]\<^sub>w)),D { \<beta> }\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i) \<and>
          (\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D+[\<beta>]\<^sub>F { c' } \<Gamma>',\<S>',P',D')"
proof -
  (* Split the program into 3 sections: before, the action, after *)
  obtain c\<^sub>1 c\<^sub>2 \<beta>' where props:
      "flat c det = c\<^sub>1 ;; (\<beta>' ;;; c\<^sub>2)"
      "c' = c\<^sub>1 ;; c\<^sub>2" "\<beta> < c\<^sub>1 <\<^sub>c \<beta>'" "spec_seq c det = c\<^sub>1"
    using eval prog_split by metis

  hence s: "spec_flat\<^sub>\<S> P \<S> c det = spec\<^sub>\<S> P \<S> c\<^sub>1" by auto

  (* Decompose the type checking into the 3 sections *)
  hence "\<turnstile> \<Gamma>,\<S>,P,D { c\<^sub>1 ;; (\<beta>' ;;; c\<^sub>2) } \<Gamma>',\<S>',P',D'"
    using typed props flat_type by metis
  then obtain \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 D\<^sub>1  \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 D\<^sub>2 where split:
      "\<turnstile> \<Gamma>,\<S>,P,D { c\<^sub>1 } \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1"
      "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1,D\<^sub>1 { \<beta>' }\<^sub>a \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
      "\<turnstile>\<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2 { c\<^sub>2 } \<Gamma>',\<S>',P',D'"
    using seq_typeE act_elim props(3) by metis

  (* Rearrange c\<^sub>1 and \<beta> *)
  then obtain \<Gamma>\<^sub>3 \<S>\<^sub>3 P\<^sub>3 D\<^sub>3 where reorder:
      "\<turnstile> \<Gamma>,\<S>,spec\<^sub>\<S> P \<S> c\<^sub>1 (D[\<beta>]\<^sub>w),D { \<beta> }\<^sub>a \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D\<^sub>3"
      "\<turnstile> \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D+[\<beta>]\<^sub>F { c\<^sub>1 } \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2,D\<^sub>2"
    using props wf stmt_reorder_type by metis

  (* Recompose to show the remaining program type checks *)
  hence "\<turnstile> \<Gamma>\<^sub>3,\<S>\<^sub>3,P\<^sub>3,D+[\<beta>]\<^sub>F { c' } \<Gamma>',\<S>',P',D'"
    using props split seq_type by metis
  then show ?thesis using reorder(1) s by metis
qed

subsection \<open> Typed Step \<close>

subsubsection \<open> action_type Invariants \<close>

text \<open>The falling component of secure_update guarantees that all variables are either:
      - Not readable by other threads
      - Low according to \<Gamma>
      - Increasing in classification\<close>
lemma falling_correct:
  assumes pred: "pred P mem"
  assumes eval: "eval_action mem \<alpha> mem'" 
  assumes sec:  "secure_update \<Gamma> \<S> P (D[\<alpha>]\<^sub>w) (D[\<alpha>]\<^sub>r) \<alpha>"
  assumes wf:   "tyenv_wellformed env \<Gamma> \<S> P"
  shows "x \<in> NoRW \<S> \<or> ( (the (\<Gamma> x))  = Low \<and> x \<in> dom \<Gamma>) \<or> dma mem x \<le> dma mem' x"
proof (cases "x \<in> falling \<S> P (D[\<alpha>]\<^sub>w) \<alpha>")
  case True
  hence "x \<in> written_low \<S> \<Gamma> P (D[\<alpha>]\<^sub>w)" using sec secure_update_def by auto
  moreover have "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> sec (the (\<Gamma> x)) \<Longrightarrow> the (\<Gamma> x) = Low"
    using pred lpred_restrict_weakens
    unfolding sec_def pred_entailment_def by (auto split: if_splits)
  ultimately show ?thesis by auto
next
  case False
  then have "dma mem x \<le> dma mem' x"
  using sec unfolding falling_def
  proof (auto, goal_cases)
    case 1
    have "\<forall>x. x \<notin> writes \<alpha> \<longrightarrow> mem x = mem' x" using eval by (cases \<alpha>; auto)
    hence "\<forall>x \<in> lpred_vars (\<L> x). mem x = mem' x" using 1 \<C>_vars_correct bexp_to_lpred_vars by auto
    hence "pred (\<L> x) mem = pred (\<L> x) mem'" by (simp add: lpred_eval_vars_det) 
    then show ?case by (simp add: bexp_to_lpred_correct dma_correct)
  next
    case 2
    hence "pred (\<L> x) mem" using pred lpred_restrict_weakens by (auto simp: pred_entailment_def)
    then show ?case by (simp add: bexp_to_lpred_correct dma_correct less_eq_Sec_def)
  next
    case 3
    have "pred (P+[\<alpha>]\<^sub>\<S>) mem'" using P_upd_eval pred eval by auto
    hence "pred (PNeg (\<L> x)) mem'" using 3 lpred_restrict_weakens by (auto simp: pred_entailment_def)
    then show ?case by (simp add: bexp_to_lpred_correct dma_correct less_eq_Sec_def)
  qed
  thus ?thesis by simp
qed


text \<open>As a result, we can show that all unmodified variables have increasing or constant
      classifications across an action\<close>
lemma increasing_type:
  assumes type: "\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>',\<S>',P',D'"
  assumes eval: "eval_action mem \<alpha> mem'"
  assumes pred: "pred P mem"
  assumes wf:   "tyenv_wellformed env \<Gamma> \<S> P"
  shows "\<forall>x \<in> - writes \<alpha>. type_max (to_total \<Gamma> x) mem \<le> type_max (to_total \<Gamma>' x) mem'"
  unfolding Ball_def
proof (intro allI impI)
  fix x
  assume asm: "x \<in> - writes \<alpha>"
  hence eq_\<Gamma>: "to_total \<Gamma>' x = to_total \<Gamma> x" using type state_upd_\<Gamma>_eq to_total_def by fastforce
  have eq_dom: "dom \<Gamma> = dom \<Gamma>'" using type state_upd_\<Gamma>_dom by blast
  have in\<C>: "x \<notin> dom \<Gamma> \<and> x \<in> NoRW \<S> \<Longrightarrow> \<forall>mem. dma mem x = Low" 
    using wf tyenv_wellformed_def stable_def \<C>_Low by auto

  thus "type_max (to_total \<Gamma> x) mem \<le> type_max (to_total \<Gamma>' x) mem'"
  proof (cases "x \<in> dom \<Gamma>")
    case True
    thus ?thesis using True unfolding eq_\<Gamma> unfolding type_max_def to_total_def sec_def by auto
  next
    case False
    hence "x \<in> NoRW \<S> \<or> dma mem x \<le> dma mem' x" using assms falling_correct by blast
    thus ?thesis using in\<C> False eq_dom unfolding to_total_def by auto
  qed
qed

text \<open> We can now show tyenv_sec is preserved across Actions \<close>
lemma action_type_sec:
  assumes type: "\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>',\<S>',P',D'"
  assumes eval: "eval_action mem \<alpha> mem'"
  assumes pred: "pred P mem"
  assumes wf:   "tyenv_wellformed env \<Gamma> \<S> P"
  assumes orig: "tyenv_sec env \<Gamma> mem"
  shows "tyenv_sec env \<Gamma>' mem'"
  using assms
proof -
  have dom: "dom \<Gamma> = dom \<Gamma>'" using state_upd_\<Gamma>_dom type by metis

  (* Show non-writes are secure *)
  have "\<And>y. y \<in> dom \<Gamma>' - AsmNoRW env \<Longrightarrow> y \<notin> writes \<alpha> \<Longrightarrow>  (the (\<Gamma>' y))  \<le> dma mem' y"
  proof -
    fix y assume noteq: "y \<notin> writes \<alpha>" and notin: "y \<in> dom \<Gamma>' - AsmNoRW env"

    hence " (the (\<Gamma> y))  \<le> dma mem y" using dom orig by auto
    moreover have "y \<in> NoRW \<S> \<or>  (the (\<Gamma> y))  = Low \<or> dma mem y \<le> dma mem' y"
      using assms(1,2) falling_correct wf pred by blast
    moreover have "y \<notin> NoRW \<S>" using wf notin unfolding tyenv_wellformed_def by auto
    moreover have "\<Gamma> y = \<Gamma>' y" using noteq state_upd_\<Gamma>_eq type by auto 
    ultimately show "?thesis y" using wf unfolding tyenv_wellformed_def 
      by (metis less_eq_Sec_def)
  qed

  (* Show writes are secure *)
  moreover have "\<And>y. y \<in> dom \<Gamma>' - AsmNoRW env \<Longrightarrow> y \<in> writes \<alpha> \<Longrightarrow>  (the (\<Gamma>' y))  \<le> dma mem' y"
  proof -
    fix y assume wr: "y \<in> writes \<alpha>" and notin: "y \<in> dom \<Gamma>' - AsmNoRW env"
    show " (the (\<Gamma>' y))  \<le> dma mem' y"
    proof (cases "pred (\<L> y) mem")
      case True (* Show the reads must be low and will be preserved *)
      have "y \<notin> NoRW \<S>" using wf notin by (auto simp: tyenv_wellformed_def)
      hence "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r \<alpha>" using type notin True pred lpred_restrict_weakens
        using wr by (cases \<alpha>; auto simp: secure_write_def subtype_def pred_entailment_def)
      moreover have indom: "y \<in> dom \<Gamma>" "y \<in> dom \<Gamma>'" using dom notin by auto
      ultimately have " (the (\<Gamma>' y)) = Low" using type wr by (cases \<alpha>; simp add: state_upd_def)
      
      then show ?thesis by (simp add: less_eq_Sec_def type_max_def) 
    next
      case False (* Show it cannot change its own classification, so must remain high *)
      have "lpred_vars (\<L> y) \<inter> writes \<alpha> = {}" using self_control wr by auto
      hence "\<forall>x \<in> lpred_vars (\<L> y). mem x = mem' x" using eval eval_const by fastforce
      hence "\<not> pred (\<L> y) mem'" using False by (metis lpred_eval_vars_det)
      hence "dma mem' y = High"
        using type_max_def type_max_dma_type by presburger
      thus ?thesis 
        by (simp add: sup.absorb_iff2 sup_Sec_def)
    qed
  qed
  ultimately show ?thesis by force
qed

text \<open>Given high reads from a type-able Action, we can show the Action was a 
      non-control assignment and will be considered high in the updated state\<close>
lemma high_reads_assign:
  assumes type: "\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>',\<S>',P',D'"
  assumes pred: "pred P mem"
  assumes rds:  "\<not> P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r \<alpha>"
  obtains (con) x e where "\<alpha> = x \<leftarrow> e \<and> x \<notin> \<C>"
proof -
  have "P \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<S> :\<^sub>w \<alpha>" using type rds by (auto simp: secure_write_def subtype_def)
  hence p: "pred (PNeg (\<S> :\<^sub>w \<alpha>)) mem"
    using pred lpred_restrict_weakens by (auto simp: pred_entailment_def)
  then obtain x e where "\<alpha> = x \<leftarrow> e" by (cases \<alpha>; auto)
  moreover have "\<forall>y \<in> \<C>. pred (\<L> y) mem"
    by (meson High_not_in_\<C> bexp_to_lpred_correct dma_correct)
  ultimately show ?thesis using p con by (cases "x \<in> \<C>"; auto)
qed

lemma high_reads_high_writes:
  assumes type: "\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>',\<S>',P',D'"
  assumes eval: "eval_action mem\<^sub>1 \<alpha> mem\<^sub>1'"
  assumes pred: "pred P mem\<^sub>1"
  assumes wf:   "tyenv_wellformed env \<Gamma> \<S> P"
  assumes rds:  "\<not> P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r \<alpha>"
  shows "\<forall>x \<in> writes \<alpha>. type_max (to_total \<Gamma>' x) mem\<^sub>1' = High"
proof -
  obtain x e where \<alpha>: "\<alpha> = x \<leftarrow> e \<and> x \<notin> \<C>" using assms high_reads_assign by metis
  have "type_max (to_total \<Gamma>' x) mem\<^sub>1' = High"
  proof (cases "x \<in> dom \<Gamma>'")
    case True
    moreover have "x \<in> dom \<Gamma>" using state_upd_\<Gamma>_dom True type by metis
    ultimately have  "\<Gamma>' x = Some High" using type rds \<alpha> by (auto simp: state_upd_def)
    then show ?thesis using True unfolding to_total_def type_max_def sec_def by auto
  next
    case False
    hence "x \<notin> dom \<Gamma>" using state_upd_\<Gamma>_dom type by metis
    hence "\<S> :\<^sub>w \<alpha> = \<L> x" using wf \<alpha> by (auto simp: tyenv_wellformed_def stable_def)
    moreover have "P \<restriction> D[\<alpha>]\<^sub>w !\<turnstile> \<S> :\<^sub>w \<alpha>" using type rds by (auto simp: secure_write_def subtype_def)
    ultimately have "pred (PNeg (\<L> x)) mem\<^sub>1" using pred pred_entailment_def by auto 
    moreover have "x \<notin> lpred_vars (PNeg (\<L> x))"
      using \<C>_vars_subset_\<C> \<alpha> bexp_to_lpred_vars \<C>_vars_correct by fastforce 
    ultimately have "pred (PNeg (\<L> x)) mem\<^sub>1'" using \<alpha> eval
      by (metis fun_upd_other local.assign lpred_eval_vars_det)
    then show ?thesis using False unfolding to_total_def type_max_def by auto
  qed
  thus ?thesis using \<alpha> by auto
qed

text \<open>We can now show that evaluation on a memory implies evaluation on a low equivalent memory
      and the Action preserves low equivalence\<close>
lemma action_type_step_equiv:
  assumes type: "\<turnstile> \<Gamma>,\<S>,P,D { \<alpha> }\<^sub>a \<Gamma>',\<S>',P',D'"
  assumes eval: "eval_action mem\<^sub>1 \<alpha> mem\<^sub>1'"
  assumes pred: "pred P mem\<^sub>1"
  assumes wf:   "tyenv_wellformed env \<Gamma> \<S> P"
  assumes leq:  "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  shows "\<exists>mem\<^sub>2'. eval_action mem\<^sub>2 \<alpha> mem\<^sub>2' \<and> (mem\<^sub>1' =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2')"
proof -
  let ?low_eq = "\<lambda>x mem\<^sub>2'. type_max (to_total \<Gamma>' x) mem\<^sub>1' = Low \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x"

  obtain (con) mem\<^sub>2' where eval': "eval_action mem\<^sub>2 \<alpha> mem\<^sub>2' \<and> (\<forall>x\<in> writes \<alpha>. ?low_eq x mem\<^sub>2')"
  proof (cases "P \<restriction> D[\<alpha>]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r \<alpha>")
    case True (* We show that an expression with only low reads is safe *)
    hence "pred (\<Gamma> :\<^sub>r \<alpha>) mem\<^sub>1"
      using pred lpred_restrict_weakens by (auto simp: pred_entailment_def)
    hence "\<forall>x \<in> reads \<alpha>. mem\<^sub>1 x = mem\<^sub>2 x"
      using read_type_all_low leq by (auto simp: tyenv_eq_def type_max_def)
    then show ?thesis using eval_step eval con by force 
  next
    case False (* We show only a write can have high reads and must therefore be unconstrained *)
    then obtain x e where \<alpha>: "\<alpha> = x \<leftarrow> e" using assms high_reads_assign by metis
    then obtain mem\<^sub>2' where "eval_action mem\<^sub>2 \<alpha> mem\<^sub>2'" by auto
    moreover have "type_max (to_total \<Gamma>' x) mem\<^sub>1' = High"
      using high_reads_high_writes assms False \<alpha> by auto
    ultimately show ?thesis using con \<alpha> by auto
  qed

  (* We show all non-writes are preserved as their classifications can only increase *)
  moreover have "\<forall>x \<in> -writes \<alpha>. ?low_eq x mem\<^sub>2'" unfolding Ball_def
  proof (intro allI impI)
    fix x
    assume asm: "x \<in> - writes \<alpha>" and "type_max (to_total \<Gamma>' x) mem\<^sub>1' = Low"
    hence "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low"
      using assms by (metis less_eq_Sec_def increasing_type)
    moreover have "mem\<^sub>1' x = mem\<^sub>1 x \<and> mem\<^sub>2' x = mem\<^sub>2 x" using asm eval eval' eval_const by metis
    ultimately show "mem\<^sub>1' x = mem\<^sub>2' x" using leq by (auto simp: tyenv_eq_def)
  qed

  ultimately show ?thesis unfolding tyenv_eq_def by blast
qed

text \<open>We combine these properties to show that type checking over an action preserves the
      bisimulation for a single step\<close>
lemma action_type_step:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D {\<alpha>}\<^sub>a \<Gamma>',\<S>',P',D'"
  assumes "eval_action mem\<^sub>1 \<alpha> mem\<^sub>1'"
  assumes "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes "pred P mem\<^sub>1"
  assumes "pred P mem\<^sub>2"
  assumes "tyenv_sec env \<Gamma> mem\<^sub>1"
  assumes "tyenv_wellformed env \<Gamma> \<S> P"
  shows "\<exists>mem\<^sub>2'.
    eval_action mem\<^sub>2 \<alpha> mem\<^sub>2' \<and>
    (mem\<^sub>1' =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2') \<and>
    pred P' mem\<^sub>1' \<and> pred P' mem\<^sub>2' \<and>
    tyenv_wellformed env \<Gamma>' \<S>' P' \<and>
    tyenv_sec env \<Gamma>' mem\<^sub>1'"
proof -
  obtain mem\<^sub>2' where "eval_action mem\<^sub>2 \<alpha> mem\<^sub>2'" "mem\<^sub>1' =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2'"
    using assms action_type_step_equiv by blast
  moreover have "pred P' mem\<^sub>1'"
    using assms(1,2,4) P_upd_eval state_upd_def by metis
  moreover have "pred P' mem\<^sub>2'"
    using assms(1,5) calculation(1) P_upd_eval state_upd_def by metis
  ultimately show ?thesis
    using assms state_upd_wellformed action_type_sec by meson
qed

subsubsection \<open> Equivalent Speculation \<close>

text \<open>
  Given valid speculation of an assignment operation (x \<leftarrow> e), it should be possible to show
  valid speculation after the evaluation of the assignment. This is complicated by the restriction
  to stable variables, as those that are not stable may change.

  As a result, the predicate is true for a memory with the assignment evaluated, such that
  it is only restricted to have stable variables equivalent to the original memory.
\<close>
lemma specr_assign_step:
  assumes pred: "pred (specr M (stable \<S>) (x \<leftarrow> e ;;; c)) mem\<^sub>1"
  assumes leq:  "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes stable: "lpred_vars M \<subseteq> stable \<S>"
  assumes wf:     "tyenv_wellformed env \<Gamma> \<S> P"
  shows "\<exists>m\<^sub>1 m\<^sub>2. pred (specr M (stable \<S>) c) (m\<^sub>1 (x := ev\<^sub>A m\<^sub>1 e)) \<and> (m\<^sub>1 =\<^bsub>\<Gamma>\<^esub> m\<^sub>2) \<and>
                (\<forall>x \<in> stable \<S>. mem\<^sub>1 x = m\<^sub>1 x \<and> mem\<^sub>2 x = m\<^sub>2 x)"
proof -
  let ?P="specr M (stable \<S>) c"
  have "lpred_vars ?P \<subseteq> stable \<S>" using stable specr_stable by auto
  then obtain m where p1: "pred ?P (m (x := leval m (aexp_to_lexp e)))" "\<forall>v \<in> stable \<S>. m v = mem\<^sub>1 v"
    using restricted_spec_var pred specr.simps by metis

  let ?m2="\<lambda>x. if x \<in> stable \<S> then mem\<^sub>2 x else m x"
  have p2: "\<forall>x \<in> stable \<S>. mem\<^sub>2 x = ?m2 x" by auto
  moreover have "m =\<^bsub>\<Gamma>\<^esub> ?m2" unfolding tyenv_eq_def
  proof (auto)
    fix x
    assume s: "x \<in> stable \<S>" and l: "type_max (to_total \<Gamma> x) m = Low"
    have "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low"
    proof (cases "x \<in> \<C>")
      case True
      moreover have "x \<notin> dom \<Gamma>" using True s wf tyenv_wellformed_def by auto
      ultimately show ?thesis by (metis \<C>_Low to_total_def type_max_dma_type)
    next
      case False
      hence d: "x \<in> dom \<Gamma>" using s wf unfolding tyenv_wellformed_def by auto
      hence "lpred_vars (to_total \<Gamma> x) = {}" using wf
        unfolding to_total_def tyenv_wellformed_def sec_def by auto
      then show ?thesis using l by (metis empty_iff lpred_eval_vars_det type_max_def)
    qed
    thus "m x = mem\<^sub>2 x" using s p1(2) leq unfolding tyenv_eq_def by auto
  qed
  ultimately show ?thesis using p1 aexp_to_lexp_correct by fastforce
qed

text \<open>
  This is the main part of the proof for equivalent speculation on low equivalent memory.
  It is assumed that the program is flattened, containing only guards and assignments.
  Induction occurs over these Actions in reverse, building the speculative predicate as 
  a weakest precondition, M. As a result, we assume that all variables in M are considered low
  in the final type checking state, rather than the initial.
\<close>
lemma specr_equiv:
  assumes "pred (specr M (stable \<S>) c) mem\<^sub>1"
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>',\<S>',P',D'"
  assumes "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes "pred P mem\<^sub>1"
  assumes "tyenv_wellformed env \<Gamma> \<S> P"
  assumes "\<forall>x \<in> lpred_vars M. P' \<turnstile> to_total \<Gamma>' x"
  assumes "lpred_vars M \<subseteq> stable \<S>"
  assumes "is_spec_seq c"
  assumes "\<beta>' < c <\<^sub>c \<beta>"
  shows "pred (specr M (stable \<S>) c) mem\<^sub>2"
  using assms
proof (induct M "stable \<S>" c arbitrary: \<beta>' mem\<^sub>1 \<Gamma> P D mem\<^sub>2 rule: specr.induct)
  case (1 M x e c)
  let ?EQ="\<lambda>m1 m2. \<forall>x \<in> stable \<S>. m1 x = m2 x"
  let ?UPD="\<lambda>m. m (x := ev\<^sub>A m e)"
  let ?P="specr M (stable \<S>) c" and ?P'="(specr M (stable \<S>) (x \<leftarrow> e ;;; c))"

  have stable: "lpred_vars ?P \<subseteq> stable \<S>" "lpred_vars ?P' \<subseteq> stable \<S>" using 1 specr_stable by auto
  have eval: "\<forall>m. eval_action m (x \<leftarrow> e) (?UPD m)" using assign by auto
  have pred: "\<And>P m. ?EQ m mem\<^sub>1 \<Longrightarrow> pred P mem\<^sub>1 \<Longrightarrow> lpred_vars P \<subseteq> stable \<S> \<Longrightarrow> pred P m" 
    using lpred_eval_vars_det by (metis rev_subsetD)

  (* Split the type-able program into parts *)
  obtain \<Gamma>\<^sub>i P\<^sub>i D\<^sub>i where props: "\<turnstile> \<Gamma>,\<S>,P,D {x \<leftarrow> e}\<^sub>a \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i" "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i {c} \<Gamma>',\<S>',P',D'"
    using 1(3) act_elim state_upd_def by metis

  (* Generate new memories, such that they are equivalent to the original for stable variables *)
  obtain m\<^sub>1 m\<^sub>2 where mprops:
      "pred (specr M (stable \<S>) c) (m\<^sub>1 (x := ev\<^sub>A m\<^sub>1 e))" "m\<^sub>1 =\<^bsub>\<Gamma>\<^esub> m\<^sub>2" "?EQ m\<^sub>1 mem\<^sub>1" "?EQ m\<^sub>2 mem\<^sub>2"
    using 1(6,8) specr_assign_step [OF 1(2) 1(4)] by fastforce

  (* Establish low equivalence between the two memories is maintained across an update *)
  moreover have p: "pred P m\<^sub>1" using 1 tyenv_wellformed_def mprops pred by metis
  ultimately have "eval_action m\<^sub>2 (x \<leftarrow> e) (?UPD m\<^sub>2) \<and> (?UPD m\<^sub>1 =\<^bsub>\<Gamma>\<^sub>i\<^esub> ?UPD m\<^sub>2)"
    using 1(6) props(1) eval action_type_step_equiv by fastforce
  
  (* Now use the inductive hypothesis to show the same outcome for the speculative predicate *)
  moreover have "pred P\<^sub>i (?UPD m\<^sub>1)" using eval props p P_upd_eval state_upd_def by metis
  moreover have "pred (specr M (stable \<S>) c) (?UPD m\<^sub>1)" using 1 mprops pred by metis
  moreover have "tyenv_wellformed env \<Gamma>\<^sub>i \<S> P\<^sub>i" using 1 props state_upd_wellformed by metis
  moreover obtain \<beta>'' where "reorder\<^sub>c \<beta>'' c \<beta>" using 1 by auto
  ultimately have "pred ?P (?UPD m\<^sub>2)" using 1 props is_spec_seq.simps(1) by metis

  (* Undo the assignment Action by shifting it to a speculated operation *)
  hence p: "pred ?P' m\<^sub>2"
    using aexp_to_lexp_correct restricted_spec_var_rev [OF stable(1)] specr.simps(1) by metis

  (* We show the predicate holds on the true low equivalent memory due to stability *)
  thus ?case using mprops(4) stable lpred_eval_vars_det by (metis set_rev_mp) 
next
  case (2 M b c)
  let ?P="specr M (stable \<S>) c"

  obtain \<Gamma>\<^sub>i P\<^sub>i D\<^sub>i where props:
      "\<turnstile> \<Gamma>,\<S>,P,D {[b?]}\<^sub>a \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i" "\<turnstile> \<Gamma>\<^sub>i,\<S>,P\<^sub>i,D\<^sub>i {c} \<Gamma>',\<S>',P',D'"
    using 2(3) act_elim state_upd_def by metis
  have stable: "lpred_vars ?P \<subseteq> stable \<S>" using 2 specr_stable by simp

  (* Show all variables referenced in the Guard are low *)
  have "\<forall>x \<in> lpred_vars (bexp_to_lpred b). P \<turnstile> to_total \<Gamma> x"
  proof -
    have "P \<restriction> D[[b?]]\<^sub>w \<turnstile> \<Gamma> :\<^sub>r ([b?])" using props(1) by (auto simp: secure_write_def)
    hence "\<forall>x \<in> lpred_vars (bexp_to_lpred b). P \<restriction> D[[b?]]\<^sub>w \<turnstile> to_total \<Gamma> x"
      using bexp_to_lpred_vars pred_entailment_def read_type_all_low reads.simps(1) by blast
    thus ?thesis using lpred_restrict_weakens pred_entailment_trans by metis
  qed

  (* Therefore the Guard will have the same outcome for both memories *)
  hence "\<forall>x \<in> lpred_vars (bexp_to_lpred b \<restriction> stable \<S>). mem\<^sub>1 x = mem\<^sub>2 x"
    using 2(4,5) lpred_restrict_vars unfolding pred_entailment_def type_max_def tyenv_eq_def
    by (metis IntD1)
  moreover have true_guard: "lpred_eval mem\<^sub>1 (bexp_to_lpred b \<restriction> stable \<S>)"
    using 2 stable lpred_restrict_conj_r 
    unfolding specr.simps pred_entailment_def by metis
  ultimately have "lpred_eval mem\<^sub>2 (bexp_to_lpred b \<restriction> stable \<S>)"
    by (metis lpred_eval_vars_det)  

  (* Use the inductive hypothesis to show the same outcome for the rest of the predicate *)
  moreover have "pred ?P mem\<^sub>2"
  proof -
    have "pred (PConj P (bexp_to_lpred b \<restriction> stable \<S>)) mem\<^sub>1"
      using 2(5) true_guard by auto
    hence "pred (PConj P (bexp_to_lpred b) \<restriction> stable \<S>) mem\<^sub>1"
      by (meson 2 lpred_restrict_conj_eq_r pred_equiv_def tyenv_wellformed_def)
    hence "pred P\<^sub>i mem\<^sub>1" using props(1) by (simp add: state_upd_def)

    moreover have "mem\<^sub>1 =\<^bsub>\<Gamma>\<^sub>i\<^esub> mem\<^sub>2" using 2 props by (simp add: state_upd_def)
    moreover have "pred (specr M (stable \<S>) c) mem\<^sub>1"
      using stable 2 lpred_restrict_conj_l pred_entailment_def
      by (metis lpred_restrict_nop specr.simps(2))
    moreover have "tyenv_wellformed env \<Gamma>\<^sub>i \<S> P\<^sub>i"
      using 2 props state_upd_wellformed by auto
    moreover obtain \<beta>'' where "reorder\<^sub>c \<beta>'' c \<beta>" using 2 by auto
    ultimately show ?thesis using props 2(7,8,9) by (intro 2(1) [where mem\<^sub>25=mem\<^sub>2] ; auto)
  qed

  ultimately show ?case using stable lpred_restrict_conj_eq_r pred_equiv_def by auto
next
  case ("3_1" M)
  hence ord: "\<Gamma>,\<S>,P,D >\<^sub>s \<Gamma>',\<S>',P',D'" using stop_elim by auto
  hence p: "P \<turnstile> P'" unfolding state_ord_def by auto
  
  (* Show all variables in the speculative predicate are low in the initial state *)
  have "\<And>x. x \<in> lpred_vars M \<Longrightarrow> P \<turnstile> to_total \<Gamma> x" 
  proof -
    fix x assume vars: "x \<in> lpred_vars M"
    have "P \<turnstile> P'" using ord by (auto simp: state_ord_def)
    also have  "... \<turnstile> to_total \<Gamma>' x" using "3_1"(6) vars by auto
    also have "... \<turnstile> to_total \<Gamma> x" using ord by (simp add: context_equiv_total state_ord_def)
    finally show "?thesis x" .
  qed

  (* Given low equivalence, the predicate must also hold for the low equivalent memory *)
  hence "\<forall>x\<in>lpred_vars (M \<restriction> stable \<S>). mem\<^sub>1 x = mem\<^sub>2 x"
    using "3_1"(3,4) unfolding pred_entailment_def type_max_def tyenv_eq_def
    by (metis IntD1 lpred_restrict_vars)

  thus ?case using "3_1"(1) by (metis specr.simps(3) lpred_eval_vars_det) 
next
  case ("3_2" M va)
  then show ?case by auto
next
  case ("3_3" M v va)
  then show ?case by auto
next
  case ("3_4" M v va)
  then show ?case by auto
qed

text \<open>
  Given speculation on a memory over a typed program,
  this speculation should also hold for any low equivalent memory.
\<close>
lemma spec_equiv:
  assumes spec: "pred (spec_flat\<^sub>\<S> P \<S> c det\<^sub>1 UNIV) mem\<^sub>1"
  assumes type: "\<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>',\<S>',P',D'"
  assumes mem:  "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes p1:   "pred P mem\<^sub>1"
  assumes p2:   "pred P mem\<^sub>2"
  assumes wf:   "tyenv_wellformed env \<Gamma> \<S> P"
  assumes re:   "\<beta>' < spec_seq c det\<^sub>1 <\<^sub>c \<beta>"
  shows "pred (spec_flat\<^sub>\<S> P \<S> c det\<^sub>1 UNIV) mem\<^sub>2"
proof -
  obtain \<Gamma>\<^sub>f \<S>\<^sub>f P\<^sub>f D\<^sub>f where "\<turnstile> \<Gamma>,\<S>,P,D { spec_seq c det\<^sub>1 } \<Gamma>\<^sub>f,\<S>\<^sub>f,P\<^sub>f,D\<^sub>f"
    using type spec_seq_type by blast
  with assms have "pred (specr PTrue (stable \<S>) (spec_seq c det\<^sub>1)) mem\<^sub>2"
    by (auto simp: spec\<^sub>\<S>_def specr_equiv is_spec_seq)
  thus ?thesis using p2 by (auto simp: spec\<^sub>\<S>_def)
qed

subsubsection \<open> \<R> Typed Step \<close>

text \<open>
  This is the main component of the proof.
  We show that given two programs in the bisimulation, where one has
  a valid evaluation step and speculation, the other must also have a valid
  evaluation step, using the same Action, and speculation. Additionally, their
  resultant states should be in the bisimulation.
\<close>
lemma \<R>_typed_step':
  assumes "\<langle>c\<^sub>1, det\<^sub>1, env, mem\<^sub>1\<rangle> \<leadsto>\<^sub>\<beta> \<langle>c\<^sub>1', det\<^sub>1', env', mem\<^sub>1'\<rangle>"
  assumes "pred (spec_env env c\<^sub>1 det\<^sub>1) mem\<^sub>1"
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c\<^sub>1 } \<Gamma>',\<S>',P',D'"
  assumes "tyenv_wellformed env \<Gamma> \<S> P"
  assumes "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes "pred P mem\<^sub>1"
  assumes "pred P mem\<^sub>2"
  assumes "tyenv_sec env \<Gamma> mem\<^sub>1"
  shows "\<exists>c\<^sub>2' det\<^sub>2' mem\<^sub>2'. 
    (eval\<^sub>a_abv (\<langle>c\<^sub>1, det\<^sub>1, env, mem\<^sub>2\<rangle>) \<beta> (\<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>)) \<and>
    pred (spec_env env c\<^sub>1 det\<^sub>1) mem\<^sub>2 \<and>
    (\<langle>c\<^sub>1', det\<^sub>1', env', mem\<^sub>1'\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>)"
  using assms(1)
proof (cases rule: eval\<^sub>a.cases)
  case evala
  show ?thesis using evala(3,2,1) assms(2,3,4,5,6,7,8)
  proof (induct arbitrary: mem\<^sub>2 \<Gamma> \<S> P D \<Gamma>' \<S>' P' D' rule: next\<^sub>a.induct)
    case (act \<alpha> c det)
    (* Split type checking to show \<alpha> is a type-able Action *)
    then obtain \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i where split: "\<turnstile> \<Gamma>,\<S>,P,D {\<alpha>}\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i" "\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i {c} \<Gamma>',\<S>',P',D'"
      by (metis act_elim)

    (* Show properties are maintained across the type-able Action *)
    then obtain mem\<^sub>2' where act':
      "eval_action mem\<^sub>2 \<alpha> mem\<^sub>2'" "mem\<^sub>1' =\<^bsub>\<Gamma>\<^sub>i\<^esub> mem\<^sub>2'"
      "tyenv_wellformed env \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i" "tyenv_sec env \<Gamma>\<^sub>i mem\<^sub>1'"
      "pred P\<^sub>i mem\<^sub>1'" "pred P\<^sub>i mem\<^sub>2'"
      using act action_type_step [OF split(1)] by metis

    (* Its trivial to introduce the three properties from these results *)
    hence "\<langle>\<alpha> ;;; c, L # det, env, mem\<^sub>2\<rangle> \<leadsto>\<^sub>\<alpha> \<langle>c, det, env, mem\<^sub>2'\<rangle>" by (simp add: eval\<^sub>a.intros next\<^sub>a.act)
    moreover have "\<langle>c, det, env, mem\<^sub>1'\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c, det, env, mem\<^sub>2'\<rangle>" using act' split by auto
    moreover have "pred (spec_env env (\<alpha> ;;; c) (L # det)) mem\<^sub>2" by (auto simp: spec_def)
    ultimately show ?case using act(2) by metis
  next
    case (reorder c det \<beta> c' det' \<beta>' \<alpha>)
    (* Reintroduce evaluation of the Action *)
    hence eval: "\<langle>\<alpha> ;;; c, R # det, env, mem\<^sub>1\<rangle> \<leadsto>\<^sub>\<beta>' \<langle>\<alpha> ;;; c', det', env', mem\<^sub>1'\<rangle>"
      using next\<^sub>ap.reorder next\<^sub>ap_next\<^sub>a_eq by auto

    (* Show the selected Action is type-able with speculation, as well as the remaining program *)
    then obtain \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i D\<^sub>i V where typed:
      "\<turnstile> \<Gamma>,\<S>,spec_flat\<^sub>\<S> P \<S> (\<alpha> ;;; c) (R # det) V,D {\<beta>'}\<^sub>a \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D\<^sub>i"
      "\<turnstile> \<Gamma>\<^sub>i,\<S>\<^sub>i,P\<^sub>i,D+[\<beta>']\<^sub>F { \<alpha> ;;; c' } \<Gamma>',\<S>',P',D'"
    using reorder(7,8) typed_sub_prog by metis

    obtain \<beta>'' where re:
         "\<beta>' < spec_seq (\<alpha> ;;; c) (R # det) <\<^sub>c \<beta>''"
      using prog_split[OF eval] by metis

    (* Show that the full speculation is valid on low equivalent memory *)
    let ?P = "\<lambda>F. spec_flat\<^sub>\<S> P \<S> (\<alpha> ;;; c) (R # det) F"
    have env_s: "AsmNoRW env \<union> AsmNoW env = stable \<S>"
      using reorder by (auto simp: tyenv_wellformed_def stable_def)
    have spec2: "pred (?P UNIV) mem\<^sub>2"
      by (rule spec_equiv ; insert env_s reorder re ; auto simp: spec_def spec\<^sub>\<S>_def)

    (* Build up necessary state for the limited speculation *)
    have "pred (?P V) mem\<^sub>1" using env_s reorder lpred_restrict_weakens 
      unfolding spec_def spec\<^sub>\<S>_def pred_entailment_def by auto
    moreover have "pred (?P V) mem\<^sub>2" using spec2 lpred_restrict_weakens 
      unfolding spec_def spec\<^sub>\<S>_def pred_entailment_def by auto
    moreover have "tyenv_wellformed env \<Gamma> \<S> (?P V)"
      using reorder spec\<^sub>\<S>_stable unfolding tyenv_wellformed_def by auto
  
    (* Show properties are maintained across the type-able Action *)
    ultimately obtain mem\<^sub>2' where act:
        "eval_action mem\<^sub>2 \<beta>' mem\<^sub>2'" "mem\<^sub>1' =\<^bsub>\<Gamma>\<^sub>i\<^esub> mem\<^sub>2'"
        "tyenv_wellformed env \<Gamma>\<^sub>i \<S>\<^sub>i P\<^sub>i" "tyenv_sec env \<Gamma>\<^sub>i mem\<^sub>1'"
        "pred P\<^sub>i mem\<^sub>1'" "pred P\<^sub>i mem\<^sub>2'"
      using reorder action_type_step [OF typed(1)] eval_mem by metis
  
    (* Show the evaluation is also possible on low equivalent memory *)
    hence "\<langle>\<alpha> ;;; c, R # det, env, mem\<^sub>2\<rangle> \<leadsto>\<^sub>\<beta>' \<langle>\<alpha> ;;; c', det', env', mem\<^sub>2'\<rangle>"
      using eval parallel_eval by metis
  
    (* Introduce the relation between the two evaluated states *)
    moreover have "\<langle>\<alpha> ;;; c', det', env', mem\<^sub>1'\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>\<alpha> ;;; c', det', env', mem\<^sub>2'\<rangle>"
      using eval const_env typed act pred_entailment_def by auto
  
    (* Reconstruct speculation on the low equivalent memory *)
    moreover have "pred (spec_env env (\<alpha> ;;; c) (R # det)) mem\<^sub>2"
      using spec2 env_s unfolding spec\<^sub>\<S>_def spec_def by auto
  
    ultimately show ?case by blast
  next
    case (choice1 c\<^sub>1 det \<alpha> c\<^sub>1' det' c\<^sub>2)
    have "\<turnstile> \<Gamma>,\<S>,P,D {c\<^sub>1} \<Gamma>',\<S>',P',D'" using choice_elim choice1(6) by blast
    hence "\<exists>c\<^sub>2' det\<^sub>2' mem\<^sub>2'.
       (\<langle>c\<^sub>1, det, env, mem\<^sub>2\<rangle> \<leadsto>\<^sub>\<alpha> \<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>) \<and>
       pred (spec PTrue (AsmNoRW env \<union> AsmNoW env) (spec_seq c\<^sub>1 det)) mem\<^sub>2 \<and>
       (\<langle>c\<^sub>1', det', env', mem\<^sub>1'\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>)"
      using choice1 by (intro choice1(2), auto)
    thus ?case using next\<^sub>a.choice1
      by (metis eval\<^sub>a.intros eval\<^sub>a_elim spec_seq.simps(3))
  next
    case (choice2 c\<^sub>2 det \<alpha> c\<^sub>2' det' c\<^sub>1)
    have "\<turnstile> \<Gamma>,\<S>,P,D {c\<^sub>2} \<Gamma>',\<S>',P',D'" using choice_elim choice2(6) by blast
    hence "\<exists>c\<^sub>1' det\<^sub>2' mem\<^sub>2'.
       (\<langle>c\<^sub>2, det, env, mem\<^sub>2\<rangle> \<leadsto>\<^sub>\<alpha> \<langle>c\<^sub>1', det\<^sub>2', env', mem\<^sub>2'\<rangle>) \<and>
       pred (spec PTrue (AsmNoRW env \<union> AsmNoW env) (spec_seq c\<^sub>2 det)) mem\<^sub>2 \<and>
       (\<langle>c\<^sub>2', det', env', mem\<^sub>1'\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>1', det\<^sub>2', env', mem\<^sub>2'\<rangle>)"
      using choice2 by (intro choice2(2), auto)
    thus ?case using next\<^sub>a.choice2
      by (metis eval\<^sub>a.intros eval\<^sub>a_elim spec_seq.simps(4))
  next
    case (loop c\<^sub>1 n c\<^sub>2 det \<beta> c det')
    have "\<turnstile> \<Gamma>,\<S>,P,D { unroll c\<^sub>1 n ;; c\<^sub>2 } \<Gamma>',\<S>',P',D'" using loop_elim_rep loop(6) by blast
    hence "\<exists>c\<^sub>2' det\<^sub>2' mem\<^sub>2'.
       (\<langle>unroll c\<^sub>1 n ;; c\<^sub>2, det, env, mem\<^sub>2\<rangle> \<leadsto>\<^sub>\<beta> \<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>) \<and>
       pred (spec PTrue (AsmNoRW env \<union> AsmNoW env) (spec_seq (unroll c\<^sub>1 n ;; c\<^sub>2) det)) mem\<^sub>2 \<and>
       (\<langle>c, det', env', mem\<^sub>1'\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>)"
      using loop by (intro loop(2), auto)
    then show ?case using next\<^sub>a.loop
      by (metis eval\<^sub>a.intros eval\<^sub>a_elim spec_seq.simps(5))
  qed
qed

text \<open>
  Use the prior result to demonstrate high level evaluation, ignoring
  Action choices and speculation, also remains in the bi-simulation.
\<close>
lemma \<R>_typed_step:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c\<^sub>1 } \<Gamma>',\<S>',P',D'"
  assumes "tyenv_wellformed env \<Gamma> \<S> P"
  assumes "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes "pred P mem\<^sub>1"
  assumes "pred P mem\<^sub>2"
  assumes "tyenv_sec env \<Gamma> mem\<^sub>1"
  assumes "\<langle>c\<^sub>1, det\<^sub>1, env, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', det\<^sub>1', env', mem\<^sub>1'\<rangle>"
  shows "\<exists>c\<^sub>2' det\<^sub>2' mem\<^sub>2'. 
    (\<langle>c\<^sub>1, det\<^sub>1, env, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>) \<and>
    (\<langle>c\<^sub>1', det\<^sub>1', env', mem\<^sub>1'\<rangle> \<R>\<^bsub>\<Gamma>',\<S>',P',D'\<^esub> \<langle>c\<^sub>2', det\<^sub>2', env', mem\<^sub>2'\<rangle>)"
  using eval_iff assms \<R>_typed_step' by metis

subsection \<open> Bisimulation \<close>

text\<open> Hence \<R> is a bisimulation \<close>
lemma \<R>_bisim: "strong_low_bisim_mm (\<R>_set \<Gamma>' \<S>' P' D')"
  unfolding strong_low_bisim_mm_def
proof (auto simp: \<R>_closed_glob_consistent, goal_cases)
  case (1 c\<^sub>1 det\<^sub>1 env mem\<^sub>1 c\<^sub>2 det\<^sub>2 mem\<^sub>2)
  thus ?case using \<R>_mem_eq by (auto elim!: \<R>.cases)
next
  case (2 c\<^sub>1 det\<^sub>1 env mem\<^sub>1 c\<^sub>2 det\<^sub>2 mem\<^sub>2 c\<^sub>1' det\<^sub>1' env' mem\<^sub>1')
  thus ?case by (cases rule: \<R>.cases, metis \<R>_typed_step)
qed

text \<open>
  We prove the main soundness theorem using the fact that type-able
  configurations can be related using \<R> and the bisimulation
\<close>
theorem type_soundness:
  assumes "\<turnstile> \<Gamma>,\<S>,P,D { c } \<Gamma>',\<S>',P',D'"
  assumes "tyenv_wellformed env \<Gamma> \<S> P"
  assumes "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 "
  assumes "pred P mem\<^sub>1"
  assumes "pred P mem\<^sub>2"
  assumes "tyenv_sec env \<Gamma> mem\<^sub>1"
  shows "\<langle>c, det\<^sub>1, env, mem\<^sub>1\<rangle> \<approx> \<langle>c, det\<^sub>1, env, mem\<^sub>2\<rangle>"
proof 
  show "strong_low_bisim_mm (\<R>_set \<Gamma>' \<S>' P' D')" using \<R>_bisim by auto
next
  show "(\<langle>c, det\<^sub>1, env, mem\<^sub>1\<rangle>, \<langle>c, det\<^sub>1, env, mem\<^sub>2\<rangle>) \<in> \<R>_set \<Gamma>' \<S>' P' D'" using assms by auto
qed

subsection \<open> Initial Conditions \<close>

text \<open> We define a series of initial conditions for the analysis \<close>

definition env\<^sub>0 :: "('Var,'Val) Env"
  where
  "env\<^sub>0 \<equiv> Env mds\<^sub>0 agrels\<^sub>0"

definition \<S>\<^sub>0 :: "'Var Stable"
  where
  "\<S>\<^sub>0 = Stable (mds\<^sub>0 AsmNoWrite) (mds\<^sub>0 AsmNoReadOrWrite)"

definition D\<^sub>0 :: "'Var Flow"
  where
  "D\<^sub>0 = \<lparr> Vars = (\<lambda>x. \<lparr> NextRead = UNIV, NextWrite = UNIV \<rparr>), Base = UNIV \<rparr>"

subsection \<open> Global Soundness \<close>

text\<open>
  The typing relation for lists of commands ("thread pools")
  Requires sound env use for all possible ordering of the program
\<close>
inductive type_global ::
  "((('Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<times> ('Var, 'Val) Env) list \<Rightarrow> bool"
  ("\<turnstile> _" [120] 1000)
  where
  "\<lbrakk> \<forall>((c,d),env) \<in> set cs. \<exists>\<Gamma>' \<S>' P' D'. \<turnstile> \<Gamma>\<^sub>0,\<S>\<^sub>0,P\<^sub>0,D\<^sub>0 { c } \<Gamma>',\<S>',P',D';
     \<forall>((c,d),env) \<in> set cs. env = env\<^sub>0;
     \<forall>mem. INIT mem \<longrightarrow> global_invariant mem;
     \<forall>mem. INIT mem \<longrightarrow> sound_env_use (cs, mem) \<rbrakk> \<Longrightarrow>
    type_global cs"

text\<open>
  A type-able thread from the defined initial conditions implies the
  local security property.
\<close>
lemma typed_secure:
  assumes typed: "\<turnstile> \<Gamma>\<^sub>0,\<S>\<^sub>0,P\<^sub>0,D\<^sub>0 { c } \<Gamma>',\<S>',P',D'"
  assumes ginv: "\<forall>mem. INIT mem \<longrightarrow> global_invariant mem"
  shows "com_sifum_secure ((c,det), Env mds\<^sub>0 agrels\<^sub>0)"
  unfolding com_sifum_secure_def low_indistinguishable_def
  using typed 
proof (auto elim!: type_soundness simp: tyenv_eq_def, goal_cases)
  case (1 mem\<^sub>1 mem\<^sub>2)
  then show ?case
    using onlyLocalsP localsProps valid_\<Gamma>\<^sub>0_dom
    unfolding tyenv_wellformed_def stable_def AsmNoW_def AsmNoRW_def \<S>\<^sub>0_def 
    by (auto split: if_splits)
next
  case (2 mem\<^sub>1 mem\<^sub>2 x)
  show ?case
  proof (cases "x \<in> dom \<Gamma>\<^sub>0")
    case True
    hence l: "\<Gamma>\<^sub>0 x = Some Low" using 2(4) unfolding type_max_def to_total_def sec_def by (auto split: if_splits)
    thus ?thesis using valid_\<Gamma>\<^sub>0 True 2(1,2) by auto
  next
    case False
    hence l: "pred (\<L> x) mem\<^sub>1" using 2(4) unfolding type_max_def to_total_def sec_def by (auto split: if_splits)
    hence "dma mem\<^sub>1 x = Low" using type_max_def type_max_dma_type by presburger
    moreover have "x \<in> \<C> \<or> x \<notin> AsmNoRW (Env mds\<^sub>0 agrels\<^sub>0)" 
      using valid_\<Gamma>\<^sub>0_dom False by (auto simp: AsmNoRW_def) 
    ultimately show ?thesis using 2(3) unfolding low_mds_eq_def by auto
  qed
next
  case (3 mem\<^sub>1 mem\<^sub>2)
  then show ?case using initP by auto
next
  case (4 mem\<^sub>1 mem\<^sub>2)
  then show ?case using initP by auto
next
  case (5 mem\<^sub>1 mem\<^sub>2 x y)
  have "x \<notin> mds\<^sub>0 AsmNoReadOrWrite" using 5(4) by (auto simp: AsmNoRW_def)
  hence "\<not> the (\<Gamma>\<^sub>0 x) > dma mem\<^sub>1 x" using valid_\<Gamma>\<^sub>0 5(5) by blast
  hence "the (\<Gamma>\<^sub>0 x) \<le> dma mem\<^sub>1 x"
    by (metis (full_types) Sec.exhaust less_eq_Sec_def order.not_eq_order_implies_strict)
  thus ?case using 5 by auto
qed

lemma list_all_set: "\<forall>x. x \<in> set xs \<longrightarrow> P x \<Longrightarrow> list_all P xs"
  by (metis (lifting) list_all_iff)

text \<open> Given a series of type-able threads with valid initial conditions,
       we can derive the global security property. \<close>
theorem type_soundness_global:
  assumes typeable: "\<turnstile> cs"
  shows "prog_sifum_secure_cont cs"
  using typeable type_global.simps
proof (auto intro!: sifum_compositionality_cont)
  have props:
      "\<forall>((c,det),env) \<in> set cs. \<exists>\<Gamma>' \<S>' P' D'. \<turnstile> \<Gamma>\<^sub>0,\<S>\<^sub>0,P\<^sub>0,D\<^sub>0 {c} \<Gamma>',\<S>',P',D'"
      "\<forall>(c,env) \<in> set cs. env = env\<^sub>0"
      "\<forall>mem. INIT mem \<longrightarrow> global_invariant mem"
    using typeable type_global.simps by auto
  show "list_all com_sifum_secure cs"
  proof (auto intro!: list_all_set)
    fix c det env
    assume asm: "((c, det), env) \<in> set cs"
    obtain  \<Gamma>' \<S>' P' D' where "\<turnstile> \<Gamma>\<^sub>0,\<S>\<^sub>0,P\<^sub>0,D\<^sub>0 {c} \<Gamma>',\<S>',P',D'" using asm props(1) by auto
    moreover have "env = Env mds\<^sub>0 agrels\<^sub>0" using asm props(2) env\<^sub>0_def by auto
    ultimately show "com_sifum_secure ((c, det), env)" using typed_secure props(3) by auto
  qed
qed

end

end