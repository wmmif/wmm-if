(*
Title: Data-dependent security on weak memory models
Authors: Graeme Smith, Nicholas Coughlin, Toby Murray
Based on Covern, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
In turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewenv, Heiko Mantel, Daniel Schoepe
*)

theory WeakMemoryFlow
imports Main
begin


record 'info rw_info =
  NextRead :: "'info set"             (* (W\<^sub>r, R\<^sub>r) *)
  NextWrite :: "'info set"            (* (W\<^sub>w, R\<^sub>w) *)

record ('var, 'info) WMF =
  Vars :: "'var \<Rightarrow> 'info rw_info"
  Base :: "'info set"

locale weak_mem_flow_no_assm =
  fixes writes :: "'Action \<Rightarrow> 'Var set"
  fixes reads  :: "'Action \<Rightarrow> 'Var set"
  fixes limit\<^sub>w :: "'Action \<Rightarrow> 'Var set"      (* later\<^sub>w: set of vars whose writes cannot be reordered with Action *)
  fixes limit\<^sub>r :: "'Action \<Rightarrow> 'Var set"      (* later\<^sub>r: set of vars whose reads cannot be reordered with Action *)
  fixes gen :: "'Action \<Rightarrow> 'Info set"        (* gen(fence)=Var; gen(a) = emptyset *) 
  fixes kill :: "'Action \<Rightarrow> 'Info set"       (* wr(a),rd(a) *)
  fixes init :: "'Info set"
  fixes forwardable :: "'Action \<Rightarrow> bool"      (* Allow for forwarding to occur on certain instructions *)

locale weak_mem_flow = weak_mem_flow_no_assm writes reads limit\<^sub>w limit\<^sub>r gen kill init forwardable
  for writes :: "'Action \<Rightarrow> 'Var set"
  and reads  :: "'Action \<Rightarrow> 'Var set"
  and limit\<^sub>w :: "'Action \<Rightarrow> 'Var set"      
  and limit\<^sub>r :: "'Action \<Rightarrow> 'Var set"      
  and gen :: "'Action \<Rightarrow> 'Info set"
  and kill :: "'Action \<Rightarrow> 'Info set"
  and init :: "'Info set"
  and forwardable :: "'Action \<Rightarrow> bool" +
  assumes forward_restrict: "x \<in> writes \<alpha> \<Longrightarrow> forwardable \<alpha> \<Longrightarrow> x \<notin> limit\<^sub>r \<alpha>"

context weak_mem_flow
begin

section \<open> Definitions \<close>

text \<open> Information known at the next write of x \<close>              (* W\<^sub>w  and R\<^sub>w *)
abbreviation next\<^sub>w :: "('Var, 'Info) WMF => 'Var \<Rightarrow> 'Info set"
  where
  "next\<^sub>w w \<equiv> NextWrite o Vars w"

text \<open> Information known at the next read of x \<close>                (* W\<^sub>r and R\<^sub>r *)
abbreviation next\<^sub>r :: "('Var, 'Info) WMF => 'Var \<Rightarrow> 'Info set"
  where
  "next\<^sub>r w \<equiv> NextRead o Vars w"

(* added: -----------------------*)

definition wmf_intersect:: "('Var, 'Info) WMF => ('Var, 'Info) WMF => ('Var, 'Info) WMF"
(infix "\<inter>\<^sub>D" 60)
  where
   "wmf_intersect D\<^sub>1 D\<^sub>2 \<equiv> \<lparr> Vars = \<lambda>x. \<lparr> NextRead = next\<^sub>r D\<^sub>1 x \<inter> next\<^sub>r D\<^sub>2 x,
                             NextWrite = next\<^sub>w D\<^sub>1 x \<inter> next\<^sub>w D\<^sub>2 x \<rparr>, Base = Base D\<^sub>1 \<inter> Base D\<^sub>2 \<rparr>"
(*--------------------------------*)


text \<open> Information known when performing the action \<alpha> \<close>
definition fromReads :: "('Var, 'Info) WMF \<Rightarrow> 'Var set \<Rightarrow> 'Info set"
  where
  "fromReads wmf S \<equiv> Set.bind S (next\<^sub>r wmf)"

definition fromWrites :: "('Var, 'Info) WMF \<Rightarrow> 'Var set \<Rightarrow> 'Info set"
  where
  "fromWrites wmf S \<equiv> Set.bind S (next\<^sub>w wmf)"

definition known :: "('Var, 'Info) WMF \<Rightarrow> 'Action \<Rightarrow> 'Info set"
  where
  "known wmf \<alpha> \<equiv> fromWrites wmf (writes \<alpha>) \<union> fromReads wmf (reads \<alpha>) \<union> gen \<alpha> \<union> Base wmf"

text \<open> Apply the effects of an action \<alpha> to the data flow \<close>
definition update :: "('Var, 'Info) WMF \<Rightarrow> 'Action \<Rightarrow> ('Var, 'Info) WMF"
  where                                     (* definition of upd of W\<^sub>r(a)(x),R\<^sub>r(a)(x) and W\<^sub>w(a)(x),R\<^sub>w(a)(x)  *)
  "update wmf \<alpha> \<equiv> \<lparr> Vars = \<lambda>x. \<lparr>
   NextRead = 
    if x \<in> limit\<^sub>r \<alpha> then next\<^sub>r wmf x \<union> known wmf \<alpha> else
    if forwardable \<alpha> \<and> x \<in> writes \<alpha> then fromReads wmf (reads \<alpha>) - kill \<alpha> else
    next\<^sub>r wmf x - kill \<alpha>, 
   NextWrite = if x \<in> limit\<^sub>w \<alpha> then next\<^sub>w wmf x \<union> known wmf \<alpha> else next\<^sub>w wmf x - kill \<alpha>\<rparr>, 
   Base = Base wmf - kill \<alpha> \<rparr>"

text \<open> Ignore limit constraints and assume all future actions occur after \<alpha> \<close>  (* if \<alpha> is picked as the next action no reordering occurs *)
definition force :: "('Var, 'Info) WMF \<Rightarrow> 'Action \<Rightarrow> ('Var, 'Info) WMF"
  where
  "force wmf \<alpha> \<equiv> \<lparr> Vars = Vars wmf, Base = Base wmf \<union> known wmf \<alpha> \<rparr>"

text \<open> Compare two data flow states \<close>
definition sub_eq :: "('Var, 'Info) WMF \<Rightarrow> ('Var, 'Info) WMF \<Rightarrow> bool"
  where
  "sub_eq wmf1 wmf2 \<equiv> \<forall>\<alpha>. next\<^sub>w wmf1 \<alpha> \<subseteq> next\<^sub>w wmf2 \<alpha> \<union> Base wmf2 \<and> next\<^sub>r wmf1 \<alpha> \<subseteq> next\<^sub>r wmf2 \<alpha> \<union> Base wmf2 \<and> Base wmf1 \<subseteq> Base wmf2"

section \<open> Lemmas \<close>

lemma known_min:
  shows "gen \<alpha> \<subseteq> known D \<alpha>"
  by (auto simp: known_def)

subsection \<open> Update Lemmas \<close>

lemma update_kill [simp]:
  assumes "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {} \<and> reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
  assumes "writes \<alpha> \<inter> reads \<beta> = {} \<or> \<not> forwardable \<alpha>"
  shows "known (update D \<alpha>) \<beta> = known D \<beta> - kill \<alpha> \<union> gen \<beta>"
  using assms 
  by (auto simp: update_def known_def fromReads_def fromWrites_def) (auto split: if_splits)

lemma update_kill\<^sub>f [simp]:
  assumes "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {} \<and> reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
  assumes "writes \<alpha> \<inter> reads \<beta> \<noteq> {} \<and> forwardable \<alpha>"
  shows "known (update D \<alpha>) \<beta> = (fromWrites D (writes \<beta>) \<union> fromReads D (reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)) \<union> gen \<beta> \<union> Base D) - kill \<alpha> \<union> gen \<beta>"
  using assms
  by (auto simp: update_def known_def fromReads_def fromWrites_def) (auto split: if_splits)
  
subsection \<open> Sub Eq Lemmas \<close>

lemma sub_known:
  assumes "sub_eq D2 D1"
  shows "known D2 \<alpha> \<subseteq> known D1 \<alpha>" 
  using assms by (auto simp: sub_eq_def known_def fromReads_def fromWrites_def, blast+)

lemma sub_eq_refl [simp]:
  "sub_eq D D"
  unfolding sub_eq_def by auto  

lemma sub_eq_trans [simp]:
  assumes "sub_eq D2 D1"
  assumes "sub_eq D3 D2"
  shows "sub_eq D3 D1"
  using assms unfolding sub_eq_def by blast

lemma subwrites:
  assumes "sub_eq D2 D1"
  shows "fromWrites D2 a \<subseteq> fromWrites D1 a \<union> Base D1"
  unfolding fromWrites_def
proof (auto)
  fix y x assume a: "y \<in> a" "x \<in> NextWrite (Vars D2 y)" "x \<notin> Base D1"
  hence "x \<in> NextWrite (Vars D1 y) \<union> Base D1" using assms by (auto simp: sub_eq_def)
  thus "\<exists>xa\<in>a. x \<in> NextWrite (Vars D1 xa)" using a by auto
qed

lemma subreads:
  assumes "sub_eq D2 D1"
  shows "fromReads D2 a \<subseteq> fromReads D1 a \<union> Base D1"
  unfolding fromReads_def
proof (auto)
  fix y x assume a: "y \<in> a" "x \<in> NextRead (Vars D2 y)" "x \<notin> Base D1"
  hence "x \<in> NextRead (Vars D1 y) \<union> Base D1" using assms by (auto simp: sub_eq_def)
  thus "\<exists>xa\<in>a. x \<in> NextRead (Vars D1 xa)" using a by auto
qed

lemma sub_update_preserve:
  assumes "sub_eq D2 D1"
  shows "sub_eq (update D2 a) (update D1 a)"
  unfolding sub_eq_def
proof 
  fix \<alpha>
  have a: "next\<^sub>w D2 \<alpha> \<union> Base D2 \<subseteq> next\<^sub>w D1 \<alpha> \<union> Base D1 \<and> 
           next\<^sub>r D2 \<alpha> \<union> Base D2 \<subseteq> next\<^sub>r D1 \<alpha> \<union> Base D1 \<and> Base D2 \<subseteq> Base D1"
    using assms by (auto simp: sub_eq_def)
  show "next\<^sub>w (update D2 a) \<alpha> \<subseteq> next\<^sub>w (update D1 a) \<alpha> \<union> Base (update D1 a) \<and>
         next\<^sub>r (update D2 a) \<alpha> \<subseteq> next\<^sub>r (update D1 a) \<alpha> \<union> Base (update D1 a) \<and> 
         Base (update D2 a) \<subseteq> Base (update D1 a)"  
    apply (cases "\<alpha> \<in> limit\<^sub>w a"; cases "\<alpha> \<in> limit\<^sub>r a")
    using a subwrites[OF assms] subreads[OF assms] 
    by (auto simp: update_def known_def sub_eq_def)
qed

lemma sub_force_preserve:
  assumes "sub_eq D2 D1"
  shows "sub_eq (force D2 a) (force D1 a)"
  using assms by (auto simp: sub_eq_def force_def known_def fromReads_def fromWrites_def, blast+)

lemma sub_force:
  shows "sub_eq D (force D \<alpha>)"
  by (auto simp: sub_eq_def force_def known_def)

lemma sub_force_update:
  shows "sub_eq (update D \<alpha>) (force D \<alpha>)"
  by (auto simp: sub_eq_def force_def update_def known_def)

lemma force_nop:
  assumes "known D \<beta> \<subseteq> Base D "
  shows " (force D \<beta>) = D"
proof -
  have "Base D \<union> known D \<beta> = Base D" using assms by auto
  thus ?thesis by (auto simp: sub_eq_def force_def)
qed

lemma force_result:
  shows "\<forall>x. known D \<beta> \<subseteq> known (force D \<beta>) \<alpha>"
  by (auto simp: force_def known_def)

lemma update_min_writes:
  shows "next\<^sub>w (update D \<alpha>) x \<supseteq> next\<^sub>w D x - kill \<alpha>"
  by (auto simp: sub_eq_def force_def update_def known_def)

lemma update_min_reads:
  assumes "x \<notin> writes \<alpha> \<or> \<not> forwardable \<alpha>"  
  shows "next\<^sub>r (update D \<alpha>) x \<supseteq> next\<^sub>r D x - kill \<alpha>"
  using assms
  by (auto simp: sub_eq_def force_def update_def known_def fromReads_def fromWrites_def)

lemma update_min_reads\<^sub>f:
  assumes "x \<in> writes \<alpha>" and "forwardable \<alpha>"  
  shows "next\<^sub>r (update D \<alpha>) x = fromReads D (reads \<alpha>) - kill \<alpha>"
  using assms forward_restrict
  by (auto simp: sub_eq_def force_def update_def known_def fromReads_def fromWrites_def)

lemma update_kill\<^sub>f':
  assumes "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {} \<and> reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
  assumes "writes \<alpha> \<inter> reads \<beta> \<noteq> {} \<and> forwardable \<alpha>"
  assumes "writes \<beta>' = writes \<beta>"
  assumes "reads \<beta>' = reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)"
  assumes "gen \<beta>' = gen \<beta>"
  shows "known (update D \<alpha>) \<beta> = known D \<beta>' - kill \<alpha> \<union> gen \<beta>"
proof -
  have "known (update D \<alpha>) \<beta> = (fromWrites D (writes \<beta>) \<union> fromReads D (reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)) \<union> gen \<beta> \<union> Base D) - kill \<alpha> \<union> gen \<beta>"
    using assms by auto
  moreover have "known D \<beta>' = fromWrites D (writes \<beta>) \<union> fromReads D (reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)) \<union> gen \<beta> \<union> Base D"
    unfolding known_def assms(3,4,5) by auto
  ultimately show ?thesis by auto
qed

lemma force_update_comm\<^sub>f:
  assumes "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {} \<and> reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
  assumes "writes \<alpha> \<inter> reads \<beta> \<noteq> {} \<and> forwardable \<alpha>"
  assumes "gen \<beta> \<inter> kill \<alpha> = {}"
  assumes "writes \<beta>' = writes \<beta>"
  assumes "reads \<beta>' = reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)"
  assumes "gen \<beta>' = gen \<beta>"
  shows "sub_eq (force (update D \<alpha>) \<beta>) (update (force D \<beta>') \<alpha>) " (is "sub_eq ?D' ?D")
proof -
  have sub: "sub_eq ?D' (force ?D \<beta>)"
    by (metis sub_force sub_update_preserve sub_force_preserve)

  have "known (update (force D \<beta>') \<alpha>) \<beta> = known (force D \<beta>') \<beta>' - kill \<alpha> \<union> gen \<beta>"
    using update_kill\<^sub>f'[OF assms(1,2,4,5,6)] by auto
  moreover have "gen \<beta> \<subseteq> known (force D \<beta>') \<beta>'" using assms(6) unfolding known_def by auto
  ultimately have "known (update (force D \<beta>') \<alpha>) \<beta> = known (force D \<beta>') \<beta>' - kill \<alpha>"
    using assms(3) by auto

  also have "... = (Base D \<union> known D \<beta>') - kill \<alpha>"
    unfolding known_def fromWrites_def force_def fromReads_def
    by auto

  finally have rel: "known (update (force D \<beta>') \<alpha>) \<beta> = (Base D \<union> known D \<beta>') - kill \<alpha>"
    by simp

  have "force ?D \<beta> = ?D"
  proof (intro force_nop allI conjI)
    have "Base (update (force D \<beta>') \<alpha>) = (Base D \<union> known D \<beta>') - kill \<alpha>"
      by (auto simp: update_def force_def)
    thus "known (update (force D \<beta>') \<alpha>) \<beta> \<subseteq> Base (update (force D \<beta>') \<alpha>)"
      using rel by auto
  qed
  thus ?thesis using sub by auto
qed

lemma force_update_comm:
  assumes "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {} \<and> reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
  assumes "writes \<alpha> \<inter> reads \<beta> = {} \<or> \<not> forwardable \<alpha>"
  assumes "gen \<beta> \<inter> kill \<alpha> = {}"
  shows "sub_eq (force (update D \<alpha>) \<beta>) (update (force D \<beta>) \<alpha>) " (is "sub_eq ?D' ?D")
proof -
  have sub: "sub_eq ?D' (force ?D \<beta>)"
    by (metis sub_force sub_update_preserve sub_force_preserve)

  have "known (update (force D \<beta>) \<alpha>) \<beta> = known (force D \<beta>) \<beta> - kill \<alpha> \<union> gen \<beta>"
    using assms by auto
  moreover have "gen \<beta> \<subseteq> known (force D \<beta>) \<beta>" unfolding known_def by auto
  ultimately have rel: "known (update (force D \<beta>) \<alpha>) \<beta> = known (force D \<beta>) \<beta> - kill \<alpha>"
    using assms(3) by auto

  also have "... = (Base D \<union> known D \<beta>) - kill \<alpha>"
    unfolding known_def fromWrites_def force_def fromReads_def
    by auto

  finally have rel: "known (update (force D \<beta>) \<alpha>) \<beta> = (Base D \<union> known D \<beta>) - kill \<alpha>"
    by simp

  have "force ?D \<beta> = ?D"
  proof (intro force_nop allI conjI)
    have "Base (update (force D \<beta>) \<alpha>) = (Base D \<union> known D \<beta>) - kill \<alpha>"
      by (auto simp: update_def force_def)
    thus "known (update (force D \<beta>) \<alpha>) \<beta> \<subseteq> Base (update (force D \<beta>) \<alpha>)"
      using rel by auto
  qed  
  thus ?thesis using sub by auto
qed

end

end