(*
Title: Data-dependent security on weak memory models
Authors: Graeme Smith, Nicholas Coughlin, Toby Murray
Based on Covern, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
In turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewenv, Heiko Mantel, Daniel Schoepe
*)
section \<open> Language for Instantiating the Dependent SIFUM-Security Property on Weak Memory Models \<close>


theory WeakMemoryLanguage
  imports Main Security PredicateLanguage
begin

subsection \<open> Syntax \<close>

datatype ('var, 'aexp, 'bexp) Action =
  Assign "'var" "'aexp"  (infix "\<leftarrow>" 999)
  | Fence
  | Guard "'bexp" ("[_?]" [0] 999)

datatype ('var, 'aexp, 'bexp) Stmt =
  Stop
  | Act "('var, 'aexp, 'bexp) Action" "('var, 'aexp, 'bexp) Stmt" (infixr ";;;" 150)
  | Choice "('var, 'aexp, 'bexp) Stmt" "('var, 'aexp, 'bexp) Stmt" (infixr "?" 150)
  | Loop "('var, 'aexp, 'bexp) Stmt" "('var, 'aexp, 'bexp) Stmt"

datatype DetChoice = L | R | U nat

locale sifum_lang_no_dma = predicate_lang "undefined::'Var::{linorder, finite} \<times> 'Val::type" +
  fixes eval\<^sub>A :: "('Var::{linorder, finite}, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  fixes eval\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  fixes aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  fixes bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  fixes aexp_replace :: "'AExp \<Rightarrow> 'Var \<Rightarrow> 'AExp \<Rightarrow> 'AExp"
  fixes bexp_replace :: "'BExp \<Rightarrow> 'Var \<Rightarrow> 'AExp \<Rightarrow> 'BExp"
  fixes bexp_neg :: "'BExp \<Rightarrow> 'BExp"
  fixes aexp_to_lexp :: "'AExp \<Rightarrow> ('Var,'Val) lexp"
  fixes bexp_to_lpred :: "'BExp \<Rightarrow> ('Var,'Val) lpred"
  fixes globals :: "'Var set"
  fixes locals :: "'Var set"
  assumes globals_locals_disj : "globals \<inter> locals = {} \<and> globals \<union> locals = UNIV"
  assumes eval_vars_det\<^sub>A : "\<lbrakk> \<forall> x \<in> aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<rbrakk> \<Longrightarrow> eval\<^sub>A mem\<^sub>1 e = eval\<^sub>A mem\<^sub>2 e"
  assumes eval_vars_det\<^sub>B : "\<lbrakk> \<forall> x \<in> bexp_vars b. mem\<^sub>1 x = mem\<^sub>2 x \<rbrakk> \<Longrightarrow> eval\<^sub>B mem\<^sub>1 b = eval\<^sub>B mem\<^sub>2 b"
  assumes aexp_to_lexp_correct: "\<And>e mem. leval mem (aexp_to_lexp e) = eval\<^sub>A mem e"
  assumes bexp_to_lpred_correct: "\<And>e mem. lpred_eval mem (bexp_to_lpred e) = eval\<^sub>B mem e"
  assumes aexp_to_lexp_vars: "\<And>e. lexp_vars (aexp_to_lexp e) = aexp_vars e"
  assumes bexp_to_lpred_vars: "\<And>e. lpred_vars (bexp_to_lpred e) = bexp_vars e"
  assumes bexp_neg_correct: "\<And>e. eval\<^sub>B mem\<^sub>1 e = (\<not> eval\<^sub>B mem\<^sub>1 (bexp_neg e))"
  assumes bexp_neg_vars: "\<And>e. bexp_vars (bexp_neg e) = bexp_vars e" 
  assumes aexp_replace_correct: "aexp_to_lexp (aexp_replace e x f) = lexp_subst (aexp_to_lexp e) (aexp_to_lexp f) x"
  assumes bexp_replace_correct: "bexp_to_lpred (bexp_replace b x f) = lpred_subst (bexp_to_lpred b) (aexp_to_lexp f) x"
  assumes aexp_replace_nop: "\<forall>x. x \<notin> aexp_vars e \<longrightarrow> aexp_replace e x f = e"
  assumes bexp_replace_nop: "\<forall>x. x \<notin> bexp_vars b \<longrightarrow> bexp_replace b x f = b"

context sifum_lang_no_dma
begin

type_synonym ('var, 'val, 'aexp, 'bexp) LC =
  "(('var, 'aexp, 'bexp) Stmt \<times> DetChoice list, 'var , 'val) LocalConf"

text \<open> Abbreviation for program state \<close>

abbreviation conf\<^sub>w_abv :: "('Var, 'AExp, 'BExp) Stmt  \<Rightarrow> DetChoice list \<Rightarrow>
  ('Var , 'Val) Env \<Rightarrow> ('Var , 'Val) Mem  \<Rightarrow> (_,_,_) LocalConf"
  ("\<langle>_, _, _, _\<rangle>\<^sub>w" [0, 60, 120] 70)
  where
  "\<langle> c, det, env, mem \<rangle>\<^sub>w \<equiv> (((c, det), env), mem)"

subsection \<open> Stmt Functions \<close>

text \<open> Define paper's Seq, If and While in terms of existing Stmts \<close>
fun Seq :: "('var, 'aexp, 'bexp) Stmt \<Rightarrow> ('var, 'aexp, 'bexp) Stmt \<Rightarrow> ('var, 'aexp, 'bexp) Stmt"
  (infixr ";;" 190)
  where
  "Seq Stop c = c" |
  "Seq (a ;;; c\<^sub>1) c\<^sub>2 = (a ;;; (c\<^sub>1 ;; c\<^sub>2))" |
  "Seq (Choice c\<^sub>1 c\<^sub>2) c\<^sub>3 = (Choice (c\<^sub>1 ;; c\<^sub>3) (c\<^sub>2 ;; c\<^sub>3))" |
  "Seq (Loop c\<^sub>1 c\<^sub>2) c\<^sub>3 = Loop c\<^sub>1 (c\<^sub>2 ;; c\<^sub>3)"

definition If :: 
  "'BExp \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "If b c1 c2 = ([b?] ;;; c1) ? ( [(bexp_neg b)?] ;;; c2 )"

definition While ::
  "'BExp \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "While b c1 c2 = Loop ([b?] ;;; c1) ( [(bexp_neg b)?] ;;; c2 )"

definition DoWhile ::
  "('Var, 'AExp, 'BExp) Stmt \<Rightarrow>  'BExp \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "DoWhile c1 b c2 = c1 ;; Loop ([b?] ;;; c1) ( [(bexp_neg b)?] ;;; c2 )"

fun unroll :: "('Var, 'AExp, 'BExp) Stmt \<Rightarrow> nat \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "unroll c 0 = Stop" |
  "unroll c (Suc n) = c ;; (unroll c n)"

subsection \<open> Reordering Semantics \<close>

text \<open> Sets for variables used in Actions \<close>
fun reads :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var set"
  where
  "reads ([b?]) = (bexp_vars b)" |
  "reads (x \<leftarrow> e) = (aexp_vars e)" |
  "reads _ = {}"

fun writes :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var set"
  where
  "writes (x \<leftarrow> e) = {x}" |
  "writes _ = {}"

abbreviation vars :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var set"
  where
  "vars a \<equiv> reads a \<union> writes a"

text\<open>
  Common properties for a reordering to occur.
  Between the two operations:
    - Common loads can only be locals
    - Writes of one cannot be referenced by the other
\<close>
definition min_reorder ::  "('Var, 'AExp, 'BExp) Action \<Rightarrow>
  ('Var, 'AExp, 'BExp) Action \<Rightarrow> bool"
  where
  "min_reorder \<alpha> \<beta> \<equiv>
    reads(\<alpha>) \<inter> reads(\<beta>) \<subseteq> locals \<and>
    vars(\<alpha>) \<inter> writes(\<beta>) = {} \<and>
    writes(\<alpha>) \<inter> vars(\<beta>) = {}"

fun forward :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> (_,_,_) Action \<Rightarrow> (_,_,_) Action"
  ("_[_]" 60)
  where
  "forward (y \<leftarrow> f) (x \<leftarrow> e) = y \<leftarrow> (aexp_replace f x e)" |
  "forward ([b?]) (x \<leftarrow> e) = [(bexp_replace b x e) ?]" |
  "forward a _ = a"

text\<open>
  Reordering property for actions.
\<close>
fun reorder :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> bool"
  (infixr "\<hookleftarrow>" 60)
  where
  "reorder Fence _ = False" |
  "reorder _ Fence = False" |
  "reorder ([b?]) a = (min_reorder ([b?]) a \<and> writes a \<subseteq> locals)" |
  "reorder a b = min_reorder a b"

definition reorder_full :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> bool"
  ("_ < _ < _" 60)
  where "reorder_full a b c \<equiv> a = forward c b \<and> b \<hookleftarrow> a"

declare reorder_full_def [simp]

text\<open>
  Reordering property for Stmts.
  Assumes the Stmt is flat.
\<close>
fun reorder\<^sub>c :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> bool"
  ("_ < _ <\<^sub>c _" 60)
  where 
  "reorder\<^sub>c \<beta>' Stop \<beta> = (\<beta>' = \<beta>)" |
  "reorder\<^sub>c \<beta>' (\<alpha> ;;; c) \<beta> = (\<exists>\<beta>''. (\<beta>' < \<alpha> < \<beta>'') \<and> (reorder\<^sub>c \<beta>'' c \<beta>))" |
  "reorder\<^sub>c _ _ _ = False"

text \<open>
  Extract sequence of speculated actions.
  Results in a flat Stmt.
\<close>
fun spec_seq :: "('Var, 'AExp, 'BExp) Stmt \<Rightarrow> DetChoice list \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "spec_seq (\<alpha> ;;; c) (L#det) = Stop" |
  "spec_seq (\<alpha> ;;; c) (R#det) = (\<alpha> ;;; (spec_seq c det))" |
  "spec_seq (c1 ? c2) (L#det) = (spec_seq c1 det)" |
  "spec_seq (c1 ? c2) (R#det) = (spec_seq c2 det)" |
  "spec_seq (Loop c\<^sub>1 c\<^sub>2) (U n#det) = spec_seq (unroll c\<^sub>1 n ;; c\<^sub>2) det"  |
  "spec_seq _ _ = Stop"

fun is_spec_seq :: "('Var, 'AExp, 'BExp) Stmt \<Rightarrow> bool"
  where
  "is_spec_seq (\<alpha> ;;; c) = is_spec_seq c" |
  "is_spec_seq Stop = True" |
  "is_spec_seq _ = False"

subsection \<open> Semantics \<close>

text \<open>
  Evaluate an Action, relating the pre and post memory states.
\<close>
fun eval_action :: "('Var , 'Val) Mem \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var , 'Val) Mem \<Rightarrow> bool"
  where
  assign: "eval_action mem (x \<leftarrow> e) mem' = (mem' = mem (x := (eval\<^sub>A (mem) e)))" |
  guard:  "eval_action mem ([b?]) mem' = (mem = mem' \<and> eval\<^sub>B ( mem) b)" |
  other:  "eval_action mem a mem' = (mem = mem')"

inductive_set next\<^sub>a ::
  "((('Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<times> ('Var, 'AExp, 'BExp) Action \<times> (('Var, 'AExp, 'BExp) Stmt \<times> DetChoice list)) set"
and next\<^sub>a_abv :: "(('Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> (('Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<Rightarrow> bool"
  ("_ \<rightarrow>\<^bsub>_\<^esub> _" [11,0,0] 70)
  where
  "c \<rightarrow>\<^bsub>\<alpha>\<^esub> c' \<equiv> (c, \<alpha>, c') \<in> next\<^sub>a" |
  act: "(\<alpha> ;;; c, L#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c, det)" |
  reorder: "\<lbrakk> (c, det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c', det') ; \<beta>' < \<alpha> < \<beta> \<rbrakk> \<Longrightarrow> (\<alpha> ;;; c, R#det) \<rightarrow>\<^bsub>\<beta>'\<^esub> (\<alpha> ;;; c', det')" |
  choice1: "(c\<^sub>1, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>1', det') \<Longrightarrow> (c\<^sub>1 ? c\<^sub>2, L#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>1', det')" |
  choice2: "(c\<^sub>2, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>2', det') \<Longrightarrow> (c\<^sub>1 ? c\<^sub>2, R#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>2', det')" |
  loop: "(unroll c\<^sub>1 n ;; c\<^sub>2, det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c, det') \<Longrightarrow> (Loop c\<^sub>1 c\<^sub>2, U n#det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c, det')"

text \<open>
  Evaluation of a statement, specifying the atomic action that has been carried out.
\<close>
inductive_set eval\<^sub>a ::
  "(('Var, 'Val, 'AExp, 'BExp) LC \<times> ('Var, 'AExp, 'BExp) Action \<times> ('Var, 'Val, 'AExp, 'BExp) LC) set"
and eval\<^sub>a_abv :: "('Var, 'Val, 'AExp, 'BExp) LC \<Rightarrow> ('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var, 'Val, 'AExp, 'BExp) LC \<Rightarrow> bool"
  ("_ \<leadsto>\<^bsub>_\<^esub> _" [11,0,0] 70)
  where
  "c \<leadsto>\<^bsub>\<alpha>\<^esub> c' \<equiv> (c, \<alpha>, c') \<in> eval\<^sub>a" |
  evala[intro]: "\<lbrakk> eval_action mem \<alpha> mem' ; (c, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det') \<rbrakk> \<Longrightarrow> \<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env, mem'\<rangle>\<^sub>w"

lemma eval\<^sub>a_elim [elim]:
  assumes "\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains "(c, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'" "env = env'"
  using assms by (blast elim: eval\<^sub>a.cases)

subsection \<open> Speculation \<close>

text \<open>
  Build up a predicate to express valid speculation for a sequence of actions
\<close>

fun specr :: "('Var,'Val) lpred \<Rightarrow> 'Var set \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var,'Val) lpred"
  where
    "specr P S (x \<leftarrow> e ;;; c) = spec_var x (aexp_to_lexp e) (specr P S c) \<restriction> S" |
    "specr P S ([b?] ;;; c) = PConj (specr P S c) (bexp_to_lpred b) \<restriction> S" |
    "specr P S _ = P \<restriction> S"

definition spec :: "('Var,'Val) lpred \<Rightarrow> 'Var set \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var,'Val) lpred"
  where
   "spec P S c = PConj (specr PTrue S c) P"

abbreviation spec_env :: "('Var ,'Val) Env \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> DetChoice list \<Rightarrow> ('Var,'Val) lpred"
  where "spec_env env c det \<equiv> spec PTrue ( (AsmNoRW env \<union> AsmNoW env)) (spec_seq c det)"

inductive_set eval\<^sub>w :: "('Var, 'Val, 'AExp, 'BExp) LC rel"
and eval\<^sub>w_abv :: "('Var, 'Val, 'AExp, 'BExp) LC \<Rightarrow> ('Var, 'Val, 'AExp, 'BExp) LC \<Rightarrow> bool"
  ("_ \<leadsto>\<^sub>w _" 70)
  where
  "c \<leadsto>\<^sub>w c' \<equiv> (c, c') \<in> eval\<^sub>w" |
  eval_a: "\<lbrakk> \<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w ; lpred_eval mem (spec_env env c det) \<rbrakk>
            \<Longrightarrow> \<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w"

subsection \<open> Global Invariant \<close>

text \<open>
  Keeping global_invariant structure from covern.
  Not used in current work.
\<close>
definition
  global_invariant :: "('Var ,'Val) Mem \<Rightarrow> bool"
where
  "global_invariant mem \<equiv> True"

definition
  INIT :: "('Var ,'Val) Mem \<Rightarrow> bool"
where
  "INIT mem \<equiv> global_invariant mem"

text \<open>
  This is the global rely and guarantee condition of all threads on all others, all the time.
\<close>
definition
  agrels\<^sub>0 :: "('Var ,'Val) AGRels"
where
  "agrels\<^sub>0 \<equiv> \<lambda>x. case x of AsmRel \<Rightarrow> {{(m,m'). global_invariant m \<longrightarrow> global_invariant m'}} |
                           GuarRel \<Rightarrow> {{(m,m'). global_invariant m \<longrightarrow> global_invariant m'}}"

text \<open>
  If we use this global condition everywhere, then we get compatibility straightaway.
\<close>
lemma agrels\<^sub>0_compatible:
  "agrels = replicate n agrels\<^sub>0 \<Longrightarrow>
  \<forall>i<length agrels. \<forall>R\<in>(agrels ! i) AsmRel. \<forall>j<length agrels. j \<noteq> i \<longrightarrow> \<Inter> ((agrels ! j) GuarRel) \<subseteq> R"
  by(simp add: agrels\<^sub>0_def)

subsection \<open> Semantic Properties \<close>

lemma eval_iff: 
  "(\<exists>\<alpha>. (\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w) \<and> lpred_eval mem (spec_env env c det)) =
   (\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w)"
  by (rule iffI, auto simp: eval_a eval\<^sub>w.simps)

lemma stop_no_next_a: "\<not> ((Stop, det) \<rightarrow>\<^bsub>a\<^esub> (c, det'))"
  by (blast elim: next\<^sub>a.cases)

lemma nil_next_a: "\<not> ((c, []) \<rightarrow>\<^bsub>a\<^esub> (c', det'))"
  by (blast elim: next\<^sub>a.cases)

lemma stop_no_eval_a: "\<not> (\<langle>Stop, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c, det', env', mem'\<rangle>\<^sub>w)"
  using stop_no_next_a by blast

lemma nil_eval_a: "\<not> (\<langle>c, [], env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w)"
  using nil_next_a by blast

lemma stop_no_eval: "\<not> (\<langle>Stop, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c, det', env', mem'\<rangle>\<^sub>w)"
  using eval_iff stop_no_eval_a by metis

lemma nil_eval: "\<not> (\<langle>c, [], env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w)"
  using eval_iff nil_eval_a by metis

lemma actD_a [dest]:
  assumes "\<langle>\<alpha> ;;; c, L#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "\<beta> = \<alpha> \<and> c' = c \<and> det' = det \<and> env' = env \<and> eval_action mem \<alpha> mem'"
  using assms next\<^sub>a.cases[of "\<alpha> ;;; c" "L # det" \<beta>] by auto

lemma assignD_a [dest]:
  assumes "\<langle>x \<leftarrow> e ;;; c, L#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "\<alpha> = x \<leftarrow> e \<and> c' = c \<and> env' = env \<and> mem' = mem (x := (eval\<^sub>A ( mem) e) ) \<and> det' = det"
  using assms by auto

lemma fenceD_a [dest]:
  assumes "\<langle>Fence ;;; c, L#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "\<alpha> = Fence \<and> c' = c \<and> env' = env \<and> det' = det \<and> mem' = mem"
  using assms by auto

lemma guardD_a [dest]:
  assumes "\<langle>[b?] ;;; c, L#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "\<alpha> = [b?] \<and> c' = c \<and> env' = env \<and> det' = det \<and> mem' = mem \<and> eval\<^sub>B ( mem) b"
  using assms by auto

lemma choice1D_a [dest]:
  assumes "\<langle>c1 ? c2, L#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "\<langle>c1, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  using assms 
proof (elim eval\<^sub>a_elim)
  assume a: "(c1 ? c2, L # det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'" "env = env'"
  hence "(c1, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" by (blast elim: next\<^sub>a.cases)
  thus "\<langle>c1, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w" using a by (simp add: eval\<^sub>a.intros)
qed

lemma choice2D_a [dest]:
  assumes "\<langle>c1 ? c2, R#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "\<langle>c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  using assms
proof (elim eval\<^sub>a_elim)
  assume a: "(c1 ? c2, R # det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'" "env = env'"
  hence "(c2, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" by (blast elim: next\<^sub>a.cases)
  thus "\<langle>c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w" using a by (simp add: eval\<^sub>a.intros)
qed

lemma loopD_a [dest]:
  assumes "\<langle>Loop c1 c2, U n#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "\<langle>unroll c1 n ;; c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  using assms
proof (elim eval\<^sub>a_elim)
  assume a: "(Loop c1 c2, U n#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'" "env = env'"
  hence "(unroll c1 n ;; c2, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" by (blast elim: next\<^sub>a.cases)
  thus "\<langle>unroll c1 n ;; c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w" using a by (simp add: eval\<^sub>a.intros)
qed

lemma assignD [dest]:
  assumes "\<langle>x \<leftarrow> e ;;; c, L#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "c' = c \<and> env' = env \<and> mem' = mem ( x := (eval\<^sub>A ( mem) e))  \<and> det' = det"
  using assms eval_iff actD_a  by blast

lemma actD [dest]:
  assumes "\<langle>a ;;; c, L#det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "c' = c \<and> det' = det \<and> env' = env \<and> eval_action mem a mem'"
  using assms eval_iff actD_a by blast

lemma const_env:
  assumes "\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "env' = env"
  using assms by (induct rule: eval\<^sub>a.induct, auto)

lemma eval_mem:
  assumes "\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  shows "eval_action mem \<alpha> mem'"
  using assms by (induct rule: eval\<^sub>a.induct, auto)

lemma seq_assoc:
  "(c1 ;; c2) ;; c3 = c1 ;; (c2 ;; c3)"
  by (induct arbitrary: c3 rule: Seq.induct, auto)

lemma actE_a [elim]:
  assumes "\<langle>\<alpha> ;;; c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains (1) "\<beta> = \<alpha> \<and> c' = c \<and> det = L#det' \<and> env' = env \<and> eval_action mem \<alpha> mem'" |
          (2) \<beta>' c'' det'' where "\<beta> < \<alpha> < \<beta>'" "env' = env" "eval_action mem \<beta> mem'"
          "c' = \<alpha> ;;; c''" "det = R#det''" "(c, det'') \<rightarrow>\<^bsub>\<beta>'\<^esub> (c'', det')"
  using assms 
proof (cases rule: eval\<^sub>a.cases)
  case evala
  hence "(\<alpha> ;;; c, det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c', det')" by auto
  then show ?thesis
  proof (cases rule: next\<^sub>a.cases)
    case act
    then show ?thesis using 1 evala by auto
  next
    case (reorder det \<beta> c')
    then show ?thesis using 2 evala by blast
  qed
qed

lemma actE [elim]:
  assumes "\<langle>\<alpha> ;;; c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains (prefix)  "c' = c" "det = L#det'" "env' = env" "eval_action mem \<alpha> mem'" |
          (reorder) \<beta> \<beta>' c'' det'' where 
          "\<beta> < \<alpha> < \<beta>'" "env' = env" "eval_action mem \<beta> mem'"
          "c' = \<alpha> ;;; c''" "det = R#det''" "(c, det'') \<rightarrow>\<^bsub>\<beta>'\<^esub> (c'', det')"
  using assms actE_a
proof -
  obtain \<beta> where "\<langle>\<alpha> ;;; c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
    using assms eval_iff by blast 
  thus ?thesis
  proof (cases rule: actE_a)
    case 1
    then show ?thesis using prefix eval_iff by blast
  next
    case (2 \<beta>' c'' d'')
    then show ?thesis using reorder eval_iff by blast
  qed
qed 

lemma choiceE_a [elim]:
  assumes "\<langle>c1 ? c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains (1) "\<exists>d. (\<langle>c1, d, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w) \<and> det = L#d" |
          (2) "\<exists>d. (\<langle>c2, d, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w) \<and> det = R#d"
  using assms
proof (cases rule: eval\<^sub>a_elim)
  case 1
  then show ?thesis using that by (cases rule: next\<^sub>a.cases, auto)
qed

lemma choiceE [elim]:
  assumes "\<langle>c1 ? c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains (left) "\<exists>d. (\<langle>c1, d, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w) \<and> det = L#d" |
          (right) "\<exists>d. (\<langle>c2, d, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w) \<and> det = R#d"
proof -
  obtain \<beta> where eval:
      "\<langle>c1 ? c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
      "lpred_eval mem (spec_env env (c1 ? c2) det)"
    using assms eval_iff by blast
  thus ?thesis using left right
  proof (cases rule: choiceE_a)
    case 1
    then show ?thesis using eval left eval_iff spec_seq.simps(3) by metis
  next
    case 2
    then show ?thesis using eval right eval_iff spec_seq.simps(4) by metis
  qed
qed 

lemma loopE_a [elim]:
  assumes "\<langle>Loop c1 c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains d n where "\<langle>unroll c1 n ;; c2, d, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w" "det = U n#d" 
  using assms 
proof (cases rule: eval\<^sub>a_elim)
  case 1
  then show ?thesis using that by (cases rule: next\<^sub>a.cases, auto)
qed

lemma loopE [elim]:
  assumes "\<langle>Loop c1 c2, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains d n where "(\<langle>unroll c1 n ;; c2, d, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', env', mem'\<rangle>\<^sub>w)" "det = U n#d" 
  by (metis assms eval_iff loopE_a spec_seq.simps(5))

subsection \<open> Lemmas for Program Transforms \<close>

fun flat :: "('Var, 'AExp, 'BExp) Stmt \<Rightarrow> DetChoice list \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "flat (c1 ? c2) (L#d) = flat c1 d" |
  "flat (c1 ? c2) (R#d) = flat c2 d" |
  "flat (a ;;; c) (R#d) = a ;;; flat c d" |
  "flat (Loop c1 c2) (U n#d) = flat (unroll c1 n ;; c2) d" |
  "flat c d = c"

lemma eval_step:
  assumes read: "\<forall>x \<in> reads \<alpha>. mem\<^sub>1 x = mem\<^sub>2 x"
  assumes eval: "eval_action mem\<^sub>1 \<alpha> mem\<^sub>1'"
  shows "\<exists>mem\<^sub>2'. eval_action mem\<^sub>2 \<alpha> mem\<^sub>2' \<and> (\<forall>x \<in> writes \<alpha>. mem\<^sub>1' x = mem\<^sub>2' x)"
  using assms eval_vars_det\<^sub>B eval_vars_det\<^sub>A  by (cases \<alpha>; auto)

lemma eval_const:
  assumes "eval_action mem \<alpha> mem'"
  shows "\<forall>x \<in> -writes \<alpha>. mem x = mem' x"
  using assms by (metis Compl_iff eval_action.elims(2) fun_upd_apply insertI1 writes.simps(1))

lemma is_spec_seq:
  shows "is_spec_seq (spec_seq c det)"
  by (induct c det rule: spec_seq.induct ; auto)

lemma prog_split':
  assumes eval: "(c, det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c', det')"
  obtains \<beta>' c\<^sub>1 c\<^sub>2 where "flat c det = c\<^sub>1 ;; (\<beta>' ;;; c\<^sub>2)" "c' = c\<^sub>1 ;; c\<^sub>2" "\<beta> < c\<^sub>1 <\<^sub>c \<beta>'" "spec_seq c det = c\<^sub>1"
  using eval
proof (induct rule: next\<^sub>a.induct)
  case (act \<alpha> c det)
  thus ?case using spec_seq.simps flat.simps Seq.simps(1) reorder\<^sub>c.simps by metis
next
  case (reorder c det \<beta> c' det' \<beta>' \<alpha>)
  show ?case using reorder reorder_full_def 
    by (metis (no_types, lifting) Seq.simps(2) flat.simps(3) 
        reorder\<^sub>c.simps(2) spec_seq.simps(2))
next
  case (choice1 c\<^sub>1 det \<alpha> c\<^sub>1' det' c\<^sub>2)
  thus ?case using spec_seq.simps flat.simps by metis
next
  case (choice2 c\<^sub>2 det \<alpha> c\<^sub>2' det' c\<^sub>1)
  thus ?case using spec_seq.simps flat.simps by metis
next
  case (loop c\<^sub>1 n c\<^sub>2 det \<beta> c det')
  thus ?case using spec_seq.simps flat.simps by metis
qed

lemma prog_split:
  assumes eval: "\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  obtains \<beta>' c\<^sub>1 c\<^sub>2 where "flat c det = c\<^sub>1 ;; (\<beta>' ;;; c\<^sub>2)" "c' = c\<^sub>1 ;; c\<^sub>2" "\<beta> < c\<^sub>1 <\<^sub>c \<beta>'" "spec_seq c det = c\<^sub>1"
  by (meson eval eval\<^sub>a_elim prog_split')

subsection \<open> Lemmas for Deterministic Behaviour \<close>

lemma eval_action_det:
  assumes "eval_action mem \<alpha> mem'"
  assumes "eval_action mem \<alpha> mem''"
  shows "mem'' = mem'" using assms
  by (cases \<alpha>, auto)

lemma reorder_det:
  assumes "\<beta>1 < \<alpha> < \<beta>"
  assumes "\<beta>2 < \<alpha> < \<beta>"
  shows "\<beta>1 = \<beta>2" 
  using assms by (cases \<alpha>; cases \<beta>, auto)

lemma next\<^sub>a_det:
  assumes "(c, det) \<rightarrow>\<^bsub>\<alpha>1\<^esub> (c1, det1)"
  assumes "(c, det) \<rightarrow>\<^bsub>\<alpha>2\<^esub> (c2, det2)"
  shows "c1 = c2 \<and> det1 = det2 \<and> \<alpha>1 = \<alpha>2"
  using assms
proof (induct arbitrary: c2 det2 \<alpha>2 rule: next\<^sub>a.induct)
  case (act \<alpha> c det)
  then show ?case by (blast elim: next\<^sub>a.cases)
next
  case (reorder c det \<beta> c' det' \<beta>' \<alpha>)
  obtain ci bi where a: "(c, det) \<rightarrow>\<^bsub>bi\<^esub> (ci, det2)" "c2 = \<alpha> ;;; ci" "\<alpha>2 < \<alpha> < bi"
    using reorder(4) by (blast elim: next\<^sub>a.cases)    
  then show ?case using reorder_det reorder(1,2,3) by auto
next
  case (choice1 c\<^sub>1 det \<alpha> c\<^sub>1' det' c\<^sub>2)
  then show ?case by (blast elim: next\<^sub>a.cases)
next
  case (choice2 c\<^sub>2 det \<alpha> c\<^sub>2' det' c\<^sub>1)
  then show ?case by (blast elim: next\<^sub>a.cases)
next
  case (loop c\<^sub>1 n c\<^sub>2 det \<beta> c det')
  then show ?case by (blast elim: next\<^sub>a.cases)
qed

lemma eval\<^sub>a_det:
  "\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>1\<^esub> \<langle>c1, det1, env1, mem1\<rangle>\<^sub>w \<Longrightarrow>
   \<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>2\<^esub> \<langle>c2, det2, env2, mem2\<rangle>\<^sub>w \<Longrightarrow>
   c1 = c2 \<and> mem1 = mem2 \<and> env1 = env2 \<and> det1 = det2 \<and> \<beta>1 = \<beta>2"
  by (metis eval\<^sub>a_elim eval_action_det next\<^sub>a_det)

lemma lc_elim:
  obtains c det env mem where "lc = \<langle>c, det, env, mem\<rangle>\<^sub>w"
  by (case_tac lc; auto)

lemma eval\<^sub>w_det:
  "lc \<leadsto>\<^sub>w lc1 \<Longrightarrow> lc \<leadsto>\<^sub>w lc2 \<Longrightarrow> lc1 = lc2"
  apply (cases lc rule: lc_elim; cases lc1 rule: lc_elim ; cases lc2 rule: lc_elim)
  using eval\<^sub>a_det eval_iff by (metis (no_types, lifting))

lemma parallel_eval:
  assumes "\<langle>c, det, env, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem'\<rangle>\<^sub>w"
  assumes "eval_action mem\<^sub>2 \<alpha> mem\<^sub>2'"
  shows "\<langle>c, det, env, mem\<^sub>2\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', env', mem\<^sub>2'\<rangle>\<^sub>w"
  using assms(1)
proof (cases rule: eval\<^sub>a.cases)
  case evala
  then show ?thesis using assms(2) by (simp add: eval\<^sub>a.intros)
qed

subsection \<open> Lemmas for Reordering \<close>

fun limit\<^sub>w :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var set"
  where
  "limit\<^sub>w Fence = UNIV" |
  "limit\<^sub>w (x \<leftarrow> e) = vars (x \<leftarrow> e)" |
  "limit\<^sub>w ([b?]) = globals \<union> vars ([b?])"

fun limit\<^sub>r :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> 'Var set"
  where
  "limit\<^sub>r Fence = UNIV" |
  "limit\<^sub>r (x \<leftarrow> e) = (if reads (x \<leftarrow> e) \<subseteq> locals then {} else {x} \<union> (globals \<inter> reads (x \<leftarrow> e)))" |
  "limit\<^sub>r ([b?]) = globals \<inter> reads ([b?])"

fun forwardable :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> bool"
  where
  "forwardable (x \<leftarrow> e) = (reads (x \<leftarrow> e) \<subseteq> locals)" |
  "forwardable _ = False"
  
datatype 'var ActionInfo = Write 'var | Read 'var

definition D_gen :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var ActionInfo) set"
  where
  "D_gen a \<equiv> if a = Fence then UNIV else {}"

definition D_kill :: "('Var, 'AExp, 'BExp) Action \<Rightarrow> ('Var ActionInfo) set"
  where
  "D_kill \<alpha> \<equiv> Write ` (writes \<alpha>) \<union> Read ` (reads \<alpha>)"

lemma limit\<^sub>w_correct:
  assumes "\<alpha> \<hookleftarrow> \<beta>"
  shows "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {}"
  using assms globals_locals_disj by (cases \<alpha> ; cases \<beta> ; (auto simp: min_reorder_def)+)

lemma limit\<^sub>r_correct:
  assumes "\<alpha> \<hookleftarrow> \<beta>"
  shows "reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
  using assms globals_locals_disj 
  by (cases \<alpha> ; cases \<beta> ; (auto simp: min_reorder_def)+)

lemma reorder_diff_writes:
  assumes re: "\<alpha> \<hookleftarrow> \<beta>"
  shows "writes \<alpha> \<inter> vars \<beta> = {} \<and> writes \<beta> \<inter> vars \<alpha> = {}"
  using assms by (cases \<alpha> ; cases \<beta> ; auto simp: min_reorder_def)

lemma forward_writes:
  shows "writes a = writes (forward a b)"
  by (cases a; cases b; auto)

lemma aexp_replace_vars:
  assumes "x \<in> aexp_vars e"
  shows "aexp_vars (aexp_replace e x f) = (aexp_vars e - {x}) \<union> aexp_vars f" 
    (is "aexp_vars ?r = (?e - {x}) \<union> ?f")
proof -
  have "aexp_vars ?r = lexp_vars (lexp_subst (aexp_to_lexp e) (aexp_to_lexp f) x)"
    using aexp_to_lexp_vars aexp_replace_correct by metis
  also have "... = lexp_vars (aexp_to_lexp e) - {x} \<union> (lexp_vars (aexp_to_lexp f))"
    using lexp_subst_vars aexp_to_lexp_vars assms by auto
  finally show ?thesis using aexp_to_lexp_vars by auto
qed

lemma bexp_replace_vars:
  assumes "x \<in> bexp_vars b"
  shows "bexp_vars (bexp_replace b x f) = (bexp_vars b - {x}) \<union> aexp_vars f" 
    (is "bexp_vars ?r = (?e - {x}) \<union> ?f")
proof -
  have "bexp_vars ?r = lpred_vars (lpred_subst (bexp_to_lpred b) (aexp_to_lexp f) x)"
    using bexp_to_lpred_vars bexp_replace_correct by metis
  also have "... = lpred_vars (bexp_to_lpred b) - {x} \<union> (lexp_vars (aexp_to_lexp f))"
    using lpred_subst_vars aexp_to_lexp_vars bexp_to_lpred_vars assms by auto
  finally show ?thesis using aexp_to_lexp_vars bexp_to_lpred_vars by auto
qed

lemma forward_reads:
  assumes "writes b \<inter> reads a \<noteq> {}"
  shows "reads (forward a b) = reads b \<union> (reads a - writes b)"
  using assms by (cases a; cases b; auto simp: aexp_replace_vars bexp_replace_vars)

lemma forward_no_overlap:
  assumes "writes a \<inter> reads b = {}"
  shows "forward b a = b"
  using assms by (cases a; cases b; auto simp: aexp_replace_nop bexp_replace_nop)

lemma forward_elim:
  assumes "\<beta>' < \<alpha> < \<beta>"
  obtains (normal) "\<beta>' = \<beta>" "writes \<alpha> \<inter> reads \<beta> = {} \<or> \<not> forwardable \<alpha>" | 
          (forward) "reads \<alpha> \<subseteq> locals" "writes \<alpha> \<inter> reads \<beta> \<noteq> {}" "forwardable \<alpha>"
proof (cases "writes \<alpha> \<inter> reads \<beta> \<noteq> {}")
  case True
  have "\<alpha> \<hookleftarrow> \<beta>'" using assms by auto
  hence "reads \<alpha> \<inter> reads \<beta>' \<subseteq> locals" by (cases \<alpha>; cases \<beta>'; auto simp: min_reorder_def)
  moreover have "reads \<beta>' = reads \<alpha> \<union> (reads \<beta> - writes \<alpha>)" 
    using True assms forward_reads by auto
  ultimately show ?thesis using forward True by (cases \<alpha>; auto)
next
  case False
  then show ?thesis using assms normal forward_no_overlap by force
qed

lemma limit\<^sub>w_correct\<^sub>f:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  shows "writes \<beta> \<inter> limit\<^sub>w \<alpha> = {}"
proof -
  have "\<beta>' = forward \<beta> \<alpha>" using re by auto
  hence "writes \<beta> = writes \<beta>'" using forward_writes by auto
  thus ?thesis using limit\<^sub>w_correct re by auto
qed

lemma limit\<^sub>r_correct\<^sub>f:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  shows "reads \<beta> \<inter> limit\<^sub>r \<alpha> = {}"
  using assms
proof (cases rule: forward_elim)
  case normal
  then show ?thesis using re limit\<^sub>r_correct by auto
next
  case forward
  then show ?thesis by (cases \<alpha>; auto)
qed

lemma reorder_diff_writes\<^sub>f:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  shows "writes \<beta> \<inter> vars \<alpha> = {}" "writes \<alpha> \<inter> vars \<beta>' = {}"
  using re forward_writes[of \<beta> \<alpha>] reorder_diff_writes[of \<alpha> \<beta>'] by auto

lemma reorder_not_fence:
  assumes re: "\<alpha> \<hookleftarrow> \<beta>"
  shows "\<alpha> \<noteq> Fence \<and> \<beta> \<noteq> Fence"
  using assms by (cases \<alpha> ; cases \<beta> ; auto simp: min_reorder_def)

lemma reorder_not_fence\<^sub>f:
  assumes re: "\<beta>' < \<alpha> < \<beta>"
  shows "\<alpha> \<noteq> Fence \<and> \<beta> \<noteq> Fence \<and> \<beta>' \<noteq> Fence"
  using assms by (cases \<alpha> ; cases \<beta> ; auto simp: min_reorder_def)

lemma forward_restrict':
  shows "x \<in> writes \<alpha> \<Longrightarrow> forwardable \<alpha> \<Longrightarrow> x \<notin> limit\<^sub>r \<alpha>"
  by (cases \<alpha>; auto)

end

end